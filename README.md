# Requirements:

* Node LTS
* Electron (```npm install electron --global```)
* Typescript (```npm install typescript --global```)
* Angular CLI (```npm install @angular/cli --global```)
* PM2 (```npm install pm2 --global```)
* PostgreSQL

# Developer Setup (Run Cycurity solution on a local machine)
## Windows:
### Prerequisites for a Development machine

* Miniconda (For Python Version 3.6)

### Cycurity services
- Go to project root directory
```shell
    # build services
    npm run build
    # run via pm2
    pm2 start process.json 
    pm2 start process_export.json
    pm2 start process_job.json
```
### Scraper
- Go to project root directory
- install miniconda (python 3^ and pip) - make sure that python added to PATH via install
- open cmd/shell as run:
```shell
    # go to scraper folder
    cd scraper/
    # upgrade pip to latest 
    python -m pip install --upgrade pip
    # install requirements via pip 
    conda install twisted
    pip install -r requirements.txt;
    # run scraper via pm2
    pm2 start process_scrapers.json
```
## Mac/Linux:
### Prerequisites for a Development machine

* Python Version 3.6

### Cycurity services
- Go to project root directory
```shell
    # build services
    npm run build
    # run services via pm2
    pm2 start process.json
    pm2 start process_export.json
    pm2 start process_job.json
```
### Scraper
- Go to project root directory
- open cmd/shell as run:
```shell
    # go to scraper folder
    cd scraper/
    # fetch Node install
    curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    # prerequisites 
    sudo add-apt-repository ppa:deadsnakes/ppa -y
    sudo apt-get update
    # install nodeJS python3 and pip
    sudo apt-get install nodejs python3 python3-pip -y
    sudo rm /usr/bin/python
    sudo ln -s /usr/bin/python3 /usr/bin/python

    # upgrade pip to the latest version
    sudo pip3 install --upgrade pip
    # install pm2 globally
    sudo npm install -g pm2
    # install all required npm packages
    sudo npm install
    # install all required pip packages
    sudo pip3 install -r requirements.txt
    # run scraper via pm2
    pm2 start process_scrapers.json
```
# Remote Deploy (Linux Machine):
## Diagram:
![Alt text](./diagram.svg)

In order to deploy the project to a remote environment you need to deploy Cycurity services into one machine
and the scraper into an image that will be part of a scaling group.

## Cycurity services

Services should be deployed with unprivileged user, but should run with root privileges. After deploying, pm2 will automatically run services with a non-root account. So we need to manually stop all services and run under root account.

### From scratch
When deploying from scratch, use the following:

```shell
    # run services via pm2 with required environment
    pm2 deploy process.json --env-- setup
    pm2 deploy process_export.json --env-- setup
    pm2 deploy process_job.json --env-- setup
```
Where *--env--*: staging / production

After deployment is complete, connect to target in process.json via SSH and run:

```shell
    # run as user (if used 'sudo su' type 'exit'):
    pm2 delete all
    sudo su
    cd ~/cycurity/develop/current
    npm run pm2:--env--
```

### Existing deployment
To update existing deployment use:

```shell
    pm2 deploy process.json --env--
    pm2 deploy process_export.json --env--
    pm2 deploy process_job.json --env--
```
Where *--env--*: staging / production

When updating existing deployment, make sure to stop all services on deployment targets before deploying. Connect to target in process.json via SSH and run:

```shell
    sudo su
    pm2 delete all
```

After deployment is complete, run on target:

```shell
    # run as user (if used 'sudo su' type 'exit'):
    pm2 delete all
    sudo su
    cd ~/cycurity/develop/current
    npm run pm2:--env--
```

Where *--env--*: stage/prod

### Note

If pm2 on target machine won't run unprivileged, make sure no service is running and remove ~/.pm2

## Scraper (image for scrapers scaling group)
```shell
    # go to scraper folder
    cd scraper/
    # run scraper via pm2 with required environment
    pm2 deploy process_scrapers.json --env-- setup
```
Where *--env--*: staging / production

# Services ports
## Cycurity services
 - Admin client - 3005
 - Job Queue Manager - 3016 
 - Scraper Manager Service - 3001 
## Scraper
 - Flask REST- 5000