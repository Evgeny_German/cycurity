export interface KolConfig {
    following?: RangeKolConfigItem;
    followers?: RangeKolConfigItem;
    favorites?: RangeKolConfigItem
    followersToFollowingRate?: RangeKolConfigItem,
    averageTweetsPerDay?: RangeKolConfigItem;
    averageRepliesPerDay?: RangeKolConfigItem,
    repliesToTweetRate?: RangeKolConfigItem,
    averageRetweets?: RangeKolConfigItem,
    averageReplies?: RangeKolConfigItem,
    averageFavorites?: RangeKolConfigItem,
    location?: ValueKolConfigItem<string[]>,
    language?: ValueKolConfigItem<string[]>,
    bioKeywords?: ValueKolConfigItem<string[]>,
    tweetsKeywords?: ValueKolConfigItem<string[]>
}

export interface KolConfigItem {
    importance?: number;

}

export interface RangeKolConfigItem extends KolConfigItem {
    from?: number;
    to?: number;
}

export interface ValueKolConfigItem<T> extends KolConfigItem {
    value?: T;
}