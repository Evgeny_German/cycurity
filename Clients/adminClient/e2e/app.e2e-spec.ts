import { AdminClientPage } from './app.po';

describe('admin-client App', () => {
  let page: AdminClientPage;

  beforeEach(() => {
    page = new AdminClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
