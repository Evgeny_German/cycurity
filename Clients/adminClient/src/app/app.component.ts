import {AfterContentInit, AfterViewInit, Component} from '@angular/core';
import {IAppState, ILoggedUserState} from "./modules/shared/store/store";
import {NgRedux} from "@angular-redux/store";
import {AppCacheProvider} from "./modules/shared/providers/app-cache-provider";
import * as $ from 'jquery';

@Component({
    selector: 'app-root',
    styles: [`
        .loading {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 999999;
            background-color: rgba(255, 255, 255, 0.8);
        }

        .loading i {
            position: absolute;
            top: 50%;
            left: 50%;
            font-size: 60px;
            margin-left: -30px;
            margin-top: -30px;
        }
    `],
    template: `

        <div *ngIf=isLoggedIn>
            <navigation-main></navigation-main>
        </div>
        <router-outlet></router-outlet>

        <div *ngIf="flags?.loading" class="loading text-primary">
            <i class="fa fa-circle-o-notch fa-spin"></i>
        </div>

        <div *ngIf="flags?.progressTotal" class="loading text-primary">
            <div class="progress" style="width:300px;position:Absolute;top:50%;left:50%;margin-left:-150px;">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                     [ngStyle]="{'width': ((flags.progressCurrent/flags.progressTotal)*100)+'%'}">
                    <span class="sr-only">{{(flags.progressCurrent / flags.progressTotal) * 100}}% Complete</span>
                </div>
            </div>
            <div
                style="position:absolute;width:300px;margin-left:-150px;margin-top:4px;text-align: center;top:50%;left:50%;">
                {{flags.progressCurrent}}/{{flags.progressTotal}}
            </div>
        </div>



    `
})
export class AppComponent implements AfterContentInit {

    ngAfterContentInit(): void {
        window['wrapperLoaded'] = true;
    }

    title = 'app';
    isLoggedIn = false;
    flags: any;

    constructor(private appCacheProvider: AppCacheProvider,
        private redux: NgRedux<IAppState>) {
            this.updateLoggedIn();
            this.redux.subscribe(() => {
                this.updateLoggedIn();
            });
        this.redux.select(store => store.flags).subscribe(state => {
            this.flags = state;
        });

        window['printHelper'] = {
            ripBySelector: (selector: string, subSelector: string, maxWidth: number) => {
                const sel = $(selector);
                const topElement = sel.detach(selector);
                const bod = $('body');
                bod.find('*').detach();
                bod.append(topElement);
                bod.addClass('body-export');
                bod.css('margin', '0px');
                bod.css('overflow', 'hidden');
                $(topElement).css('margin', '0px');

                const elements = $(selector).find(subSelector).toArray();

                window['ripElement'] = (id: number) => {
                    $(selector).find(subSelector).css('display', 'none');
                    const el = $(elements[id]);
                    el.css('display', 'block').css('margin', '0px').css('padding', '0px').css('top', '0px').css('max-width', maxWidth).css('width', maxWidth);
                };

                return elements.map(element => {
                    const el = $(element);
                    return {
                        height: el.outerHeight(),
                        width: Math.min(element.clientWidth || maxWidth, maxWidth)
                    };
                });

            }
        };

    }

    updateLoggedIn = () => {
        this.isLoggedIn = this.appCacheProvider.isLoggedIn()
    }

}
