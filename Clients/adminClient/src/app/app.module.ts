import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, Injector, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NavigationModule} from "./modules/navigation/navigation.module";
import {SharedModule} from "./modules/shared/shared.module";
import {ConversationModule} from "./modules/conversation/conversation.module";
import {CommonModule, HashLocationStrategy, LocationStrategy} from "@angular/common";

import {HomeModule} from "./modules/home/home.module";
import {JobsModule} from "./modules/jobs/jobs.module";
import {SearchModule} from "./modules/search/search.module";
import {ReportsModule} from "./modules/reports/reports.module";
import {VpModule} from "./modules/vp/vp.module";
import {ProjectsModule} from "./modules/projects/projects.module";
import {SettingsModule} from "./modules/settings/settings.module";
import {UsersModule} from "./modules/users/users.module";

import {Http, HttpModule, RequestOptions, XHRBackend} from "@angular/http";
import {HttpInterceptor} from "./modules/shared/interceptors/httpInterceptor";
import {LoginModule} from "./modules/login/login.module";
import "./rxjs-ext";
import {NgRedux} from "@angular-redux/store";
import {IAppState, rootReducer} from "./modules/shared/store/store";
import {Store, applyMiddleware, createStore} from "redux";
import {logger} from "redux-logger";
import {combineEpics, createEpicMiddleware} from "redux-observable";
import {FormsModule} from "@angular/forms";
import {LoginEpics} from "./modules/login/store/loginEpics";
import {SearchEpics} from "./modules/search/store/searchEpics";
import {ConversationEpics} from "./modules/conversation/store/conversationEpics";
import {FlagActions} from "./modules/shared/store/flagsActions";
import {RouterModule, Routes} from "@angular/router";
import { MassModule } from './modules/mass/mass.module';
import { ProxyModule } from './modules/proxy/proxy.module';

export const httpInterceptorFunction = (xhrBackend: XHRBackend, requestOptions: RequestOptions, injector: Injector) =>
    new HttpInterceptor(xhrBackend, requestOptions, injector)

const defaultRoutes: Routes = [
        {path:'', redirectTo:"/home",pathMatch:"full"}
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        CommonModule,
        FormsModule,
        RouterModule.forRoot(defaultRoutes),
        SharedModule,
        NavigationModule,
        SearchModule,
        ReportsModule,
        VpModule,
        ProjectsModule,
        SettingsModule,
        HomeModule,
        JobsModule,
        UsersModule,
        ConversationModule,
        LoginModule,
        MassModule,
        ProxyModule
    ],
    exports: [],
    providers: [
        FlagActions,
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        {provide: Http, useFactory: httpInterceptorFunction, deps: [XHRBackend, RequestOptions, Injector]}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(private loginEpics: LoginEpics,
                private searchEpics: SearchEpics,
                private conversationEpics: ConversationEpics,
                redux: NgRedux<IAppState>) {

        let rootEpicMiddleware = combineEpics(...this.loginEpics.epics, ...this.searchEpics.epics, ...this.conversationEpics.epics);
        let epicMiddleware = createEpicMiddleware(rootEpicMiddleware);

        let store: Store<IAppState> = createStore(
            rootReducer,
            applyMiddleware(epicMiddleware, logger));

        redux.provideStore(store);
    }

}
