import {
    NgModule,
    ModuleWithProviders,
    Optional,
    SkipSelf
} from "@angular/core";
import { PushProvider } from "../shared/providers/push-provider";

@NgModule({
    providers: [PushProvider]
})
export class CoreModule {
    constructor(
        @Optional()
        @SkipSelf()
        parentModule: CoreModule
    ) {
        if (parentModule) {
            throw new Error(
                "CoreModule is already loaded. Import it in the AppModule only"
            );
        }
    }

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: []
        };
    }
}
