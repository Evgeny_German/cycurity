import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { order } from "./components/sort-controller";
import { Observable } from "rxjs/Observable";
import * as _ from "lodash";

@Component({
    styles: [``],
    template: `
        <vpSubNav></vpSubNav>

        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link" [ngClass]="{ active: (vpState | async)?.view==='maintenance'}" href="javascript:;" (click)="updateState({view: 'maintenance'})">Maintenance View</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" [ngClass]="{ active: (vpState | async)?.view==='management'}" href="javascript:;" (click)="updateState({view: 'management'})">Management View</a>
            </li>
            <li class="nav-item pull-right">
                <a class="nav-link disabled" href="javascript:;">Statistics</a>
            </li>
        </ul>

        <vpMaintenanceView *ngIf="(vpState | async)?.view=='maintenance'" [vpState]="vpState"></vpMaintenanceView>
        <vpManagementView *ngIf="(vpState | async)?.view=='management'" [vpState]="vpState"></vpManagementView>

    `
})
export class VpMain implements OnInit {
    vpState: Observable<{
        view: "maintenance" | "management";
        sortColumn: string;
        sortOrder: order;
        filter: any;
    }>;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.vpState = this.activatedRoute.params.map(params => {
            return params;
        });
        this.vpState.subscribe(state => {
            if (!state) {
                this.router.navigate([
                    ".",
                    {
                        view: "maintenance",
                        sortColumn: "daysDelayed",
                        sortOrder: "desc",
                        filter: {}
                    }
                ]);
                return;
            }
            if (
                !state["view"] ||
                <any>state["view"] == "undefined" ||
                !state["sortColumn"] ||
                <any>state["sortColumn"] == "undefined" ||
                !state["sortOrder"] ||
                <any>state["sortOrder"] == "undefined" ||
                !state["filter"] ||
                <any>state["filter"] == "undefined"
            ) {
                const view =
                    state["view"] && <any>state["view"] != "undefined"
                        ? state["view"]
                        : "maintenance";
                this.router.navigate([
                    ".",
                    {
                        view,
                        sortColumn:
                            state["sortColumn"] &&
                            <any>state["sortColumn"] != "undefined"
                                ? state["sortColumn"]
                                : view == "maintenance"
                                    ? "daysDelayed"
                                    : "name",
                        sortOrder:
                            state["sortOrder"] &&
                            <any>state["sortOrder"] != "undefined"
                                ? state["sortOrder"]
                                : view == "maintenance"
                                    ? "desc"
                                    : "asc",
                        filter: state["filter"] || "{}"
                    }
                ]);
            }
        });
    }

    updateState(value: { [field: string]: any }) {
        this.vpState.first().subscribe(state => {
            const cState = _.cloneDeep(state);
            Object.keys(value)
                .map(key => ({
                    [key]:
                        typeof value[key] === "object"
                            ? JSON.stringify(value[key])
                            : value[key]
                }))
                .forEach(kv => Object.assign(cState, kv));
            this.router.navigate([".", cState]);
        });
    }
}
