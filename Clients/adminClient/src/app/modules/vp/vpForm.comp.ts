import {
    Component,
    EventEmitter,
    OnInit,
    Output,
    ViewChild,
    ElementRef
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { GenericApiProvider } from "../shared/providers/genericApiProvider";
import * as alertify from "alertifyjs";
import { AppCacheProvider } from "../shared/providers/app-cache-provider";
import { GravatarProvider } from "../shared/providers/gravatarProvider";
import { Languages } from "../shared/providers/languageProvider";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { VpApiService } from "./services/vp.service";
import * as moment from "moment";
import { isNumber } from "util";
import { HasSavableChanges } from "../shared/has-savable-changes";
import * as _ from "lodash";

@Component({
    selector: "vpForm",
    styles: [
        `
        .card {
            padding: 1.4em
        }

        textarea {
            line-height: 1.4em;
            height: 5.2em;
            border: 1px solid #eee;
            padding: 0.3em;
        }

        ::-webkit-inner-spin-button{
            position:relative;
            top:10px;
        }

        [disabled]{
            /* opacity:0.4; */
            background-color:#f5f5f5;
        }
    `
    ],
    template: `
        <vpSubNav></vpSubNav>

        <ng-container *ngIf="vp && allActions && allMaintenanceProfiles && allConnections && allUsers && show">


            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a [routerLink]="['/vp']">VPs</a></li>
                <li class="breadcrumb-item active">{{(vp.id) ? vp.name || 'Unnamed' : 'New'}}</li>
                <li>
                    <div class="pull-right"><electron-launcher [vp]="vp"></electron-launcher></div>
                </li>
            </ol>

            <div *ngIf="showAppIntro" class="card" style="padding:16px;position:absolute;top:91px;right:30px;z-index:99999;box-shadow:1px 1px 10px rgba(0,0,0,0.5)">
                <a style="color:#000;position:absolute;top:2px;right:4px;" href="javascript:;" (click)="showAppIntro=false">&times;</a>
                <h4>Agent not active on your system</h4>
                To use the launch feature, you will need to install and run
                <div style="text-align:center;margin-top:17px;"><a class="btn btn-primary btn-xs" target="_blank" href="/assets/launcher-setup.exe">the Launcher</a></div>
            </div>

            <div class="row">
                <div class="col-{{(vp.id)?9:12}} {{(canEdit())?'':'disabled'}}">

                    <div class="card">
                        <div class="row">
                            <div class="col-6">

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Project/Topic</label>
                                    <div class="col-8">
                                        <projectTopicComboSelector [disabled]="fieldPermissions('projectTopicCombo')" [(ngModel)]="vp.settings.projectTopicCombo"></projectTopicComboSelector>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Service and Zone</label>
                                    <div class="col-8">
                                        <select tabIndex="4" [attr.disabled]="fieldPermissions('currentLocation') ? '' : null" [(ngModel)]="vp.settings.currentLocation" class="form-control">
                                            <option value=""></option>
                                            <option *ngFor="let r of regions" [value]="r.region_key">{{r.provider}} {{r.display_name}}</option>
                                        </select>
                                        <!--<input [attr.disabled]="fieldPermissions('currentLocation') ? '' : null" [(ngModel)]="vp.settings.currentLocation" class="form-control"/>-->
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Creation Date</label>
                                    <div class="col-8">
                                        <input tabIndex="6" [attr.disabled]="fieldPermissions('CreationDate') ? '' : null" type="date" [(ngModel)]="vp.settings.CreationDate" class="form-control"/>
                                    </div>
                                </div>

                            </div>

                            <div class="col-6">

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">M. Profile</label>
                                    <div class="col-8">
                                        <select tabIndex="3" [attr.disabled]="fieldPermissions('statusId') ? '' : null" class="form-control" [(ngModel)]="vp.statusId">
                                            <option *ngFor="let maintenanceProfile of allMaintenanceProfiles" value="{{maintenanceProfile.id}}">
                                                {{maintenanceProfile.caption}}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Phone Number</label>
                                    <div class="col-8">
                                        <input tabIndex="5" [attr.disabled]="fieldPermissions('phoneNumber') ? '' : null" [(ngModel)]="vp.settings.phoneNumber" class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Creation Location</label>
                                    <div class="col-8">
                                        <input tabIndex="7" [attr.disabled]="fieldPermissions('creationLocation') ? '' : null" [(ngModel)]="vp.settings.creationLocation" class="form-control"/>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>




                    <div class="card" style="margin-top:1em;">
                        <div class="row">

                            <div class="col-6">
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Account History</label>
                                    <div class="col-8">
                                        <textarea tabIndex="8" [attr.disabled]="fieldPermissions('accountHistory') ? '' : null" [(ngModel)]="vp.settings.accountHistory"
                                                  class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Additional Information</label>
                                    <div class="col-8">
                                        <textarea tabIndex="9" [attr.disabled]="fieldPermissions('additionalInformation') ? '' : null" [(ngModel)]="vp.settings.additionalInformation"
                                                  class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <h2 style="margin-top:1em;">Twitter Information</h2>
                    <div class="card" style="margin-top:1em;">
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Twitter Handle</label>
                                    <div class="col-8">
                                        <input tabIndex="10" [attr.disabled]="fieldPermissions('twitterHandle') ? '' : null" [(ngModel)]="vp.settings.twitterHandle" class="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Number of Tweets</label>
                                    <div class="col-8" style="padding-top:7px;">
                                        <twitterProfileStat [profile]="vp.settings.twitterHandle" field="statuses_count"></twitterProfileStat>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Bio</label>
                                    <div class="col-8">
                                        <input tabIndex="13" [attr.disabled]="fieldPermissions('bio') ? '' : null" [(ngModel)]="vp.settings.bio" class="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Language</label>
                                    <div class="col-8">
                                        <autocompleteSelect tabIndex="16" [disabled]="fieldPermissions('language')" [(ngModel)]="vp.settings.language" >
                                            <option *ngFor="let language of languages" value="{{language.key}}">{{language.value}}</option>
                                        </autocompleteSelect><!--[(ngModel)]="vp.settings.language"  ngDefaultControl-->
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Twitter Password</label>
                                    <div class="col-8">
                                        <input tabIndex="11" [attr.disabled]="fieldPermissions('twitterPassword') ? '' : null" [(ngModel)]="vp.settings.twitterPassword" class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Following</label>
                                    <div class="col-8" style="padding-top:7px;">
                                        <twitterProfileStat [profile]="vp.settings.twitterHandle" field="following"></twitterProfileStat>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Profile Pic URL</label>
                                    <div class="col-8">
                                        <input tabIndex="14" [attr.disabled]="fieldPermissions('profilepic') ? '' : null" [(ngModel)]="vp.settings.profilepic" class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Location</label>
                                    <div class="col-8" style="padding-top:7px;">
                                        <twitterProfileStat [fromExtras]="true" [profile]="vp.settings.twitterHandle" field="location"></twitterProfileStat>
                                    </div>
                                </div>

                            </div>

                            <div class="col-4">

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Name</label>
                                    <div class="col-8">
                                        <input tabIndex="12" [attr.disabled]="fieldPermissions('name') ? '' : null" [attr.disabled]="fieldPermissions('name') ? '' : null" [(ngModel)]="vp.name" class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Followers</label>
                                    <div class="col-8" style="padding-top:7px;">
                                        <twitterProfileStat [profile]="vp.settings.twitterHandle" field="followers_count"></twitterProfileStat>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Background Pic URL</label>
                                    <div class="col-8">
                                        <input tabIndex="15" [attr.disabled]="fieldPermissions('backgroundpic') ? '' : null" [(ngModel)]="vp.settings.backgroundpic" class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Twitter Creation Date</label>
                                    <div class="col-8">
                                        <input tabIndex="17" [attr.disabled]="fieldPermissions('twitterCreationDate') ? '' : null" type="date" [(ngModel)]="vp.settings.twitterCreationDate" class="form-control"/>
                                        <!--<p-calendar [disabled]="fieldPermissions('twitterCreationDate') ? '' : null" [(ngModel)]="vp.settings.twitterCreationDate"></p-calendar>-->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <h2 style="margin-top:1em;">Email Information</h2>
                    <div class="card" style="margin-top:1em;">
                        <div class="row">

                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Email</label>
                                    <div class="col-8">
                                        <input tabIndex="18" [attr.disabled]="fieldPermissions('email') ? '' : null" [(ngModel)]="vp.settings.email" class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Recovery Info</label>
                                    <div class="col-8">
                                        <textarea tabIndex="21" [attr.disabled]="fieldPermissions('recoveryInfo') ? '' : null" [(ngModel)]="vp.settings.recoveryInfo"
                                                  class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Email Password</label>
                                    <div class="col-8">
                                        <input tabIndex="19" [attr.disabled]="fieldPermissions('emailPassword') ? '' : null" [(ngModel)]="vp.settings.emailPassword" class="form-control"/>
                                    </div>
                                </div>

                            </div>

                            <div class="col-4">

                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Email Name</label>
                                    <div class="col-8">
                                        <input tabIndex="20" [attr.disabled]="fieldPermissions('emailCreationName') ? '' : null" [(ngModel)]="vp.settings.emailCreationName" class="form-control"/>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>


                    <div style="margin-top:1em;margin-bottom:3em;">

                        <div class="row">
                            <div class="col-12" *ngIf="hasVpRole">
                                <hr/>


                                <button *ngIf="!showDelete" (click)="showDelete=true" type="button"
                                        class="btn btn-xs btn-dafault" style="margin-left:0.5em;">
                                    <i class="fa fa-times"></i>
                                    Delete VP
                                </button>
                                <span *ngIf="showDelete"
                                      style="display:inline-block;padding-left:1em;">Delete this VP?</span>
                                <button *ngIf="showDelete" (click)="showDelete=false" type="button"
                                        class="btn btn-xs btn-link" style="margin-left:0.5em;">
                                    Cancel
                                </button>
                                <button *ngIf="showDelete" (click)="deleteVp()" type="button"
                                        class="btn btn-xs btn-danger" style="margin-left:0.5em;">
                                    Delete
                                </button>




                                <input class="btn btn-primary btn-xs pull-right ml-1 {{(canEdit())?'':'disabled'}}" type="button" (click)="save()" value="Save">
                                <input class="btn btn-link btn-xs pull-right ml-1" type="button" (click)="discard()" value="Cancel">
                            </div>
                            <div class="col-12" *ngIf="!hasVpRole">
                                <hr/>
                                <div class="pull-right" style="color:#f00;">This user cannot modify VPs{{ hasVpRole ? '' : ' because no VP role has been defined'}}</div>
                            </div>
                        </div>


                    </div>

                </div>

                <div *ngIf="vp.id" class="col-3">
                    <div class="card">


                        <ul class="nav nav-tabs" style="width:100%!important;margin-bottom:1em;">
                            <li class="nav-item" style="width:50%;">
                                <a (click)="currentLog=1" class="nav-link {{(currentLog==1)?'active':''}}" href="javascript:;">Log</a>
                            </li>
                            <li class="nav-item" style="width:50%;">
                                <a (click)="currentLog=0" class="nav-link {{(currentLog==0)?'active':''}}" href="javascript:;">Maintenance Checklist</a>
                            </li>
                        </ul>

                        <div *ngIf="currentLog==1"><!-- Audit Trail -->

                            <div *ngFor="let l of allAudit">

                                <div *ngIf="l.newValue">

                                    <div>
                                        <small>{{l.when}}</small>
                                    </div>
                                    <div><img style="margin-right:5px;"
                                              src="{{gravatarProvider.gravatar(userById(l.userId).email||'', 16)}}"/>{{userById(l.userId).email||'Unknown'}}
                                    </div>
                                    <div *ngFor="let v of generateAuditArray(l.newValue)">
                                        <ng-container *ngIf="v.value">
                                            <ng-container *ngIf="v.key!='projectTopicCombo'">
                                                <small><b style="color:#000;">{{v.key}}</b> changed to: </small>{{v.value|json}}
                                            </ng-container>
                                            <ng-container *ngIf="v.key=='projectTopicCombo' && allProjects && allTopics">
                                                <div *ngIf="v.value.project"><small><b style="color:#000;">Project</b> changed to: </small>{{projectNameById(v.value.project)}} ({{v.value.project}})</div>
                                                <div *ngIf="v.value.topic"><small><b style="color:#000;">Topic</b> changed to: </small>{{topicNameById(v.value.topic)}} ({{v.value.topic}})</div>
                                            </ng-container>
                                        </ng-container>
                                        <ng-container *ngIf="!v.value">
                                            <ng-container>
                                                <small><b style="color:#000;">{{v.key}}</b> deleted. </small>
                                            </ng-container>
                                        </ng-container>
                                    </div>
                                    <hr/>

                                </div>

                            </div>


                        </div><!-- Audit Trail -->




                        <div *ngIf="currentLog==0"><!-- Activity Log -->

                            <div class="dropdown {{(canEdit())?'':'disabled'}}" style="width:100%;margin-bottom:1.7em;">
                                <a style="width:100%;" class="btn btn-primary btn-xs dropdown-toggle" href="javascript:;"
                                   data-toggle="dropdown">
                                    Log Activity
                                </a>

                                <div style="width:100%;" class="dropdown-menu">

                                    <ng-container *ngFor="let action of allActions">
                                        <ng-container *ngIf="validActionForMaintenanceProfile(action)">
                                            <label class="dropdown-item"
                                                   onClick="event.stopPropagation();">
                                                <input type="checkbox" [(ngModel)]="logEntry[action.id]">
                                                {{action.caption}}
                                            </label>
                                        </ng-container>
                                    </ng-container>
                                    <hr/>

                                    <div style="padding:0 1em 0 1em;">
                                        <a style="width:100%;" href="javascript:;" (click)="logWork()"
                                           class="btn btn-xs btn-primary {{(buttonActive())?'':'disabled'}}">Log</a>
                                    </div>
                                </div>
                            </div>

                            <div *ngFor="let l of log">
                                <ng-container *ngIf="actionById(l.actionId)">
                                    <div><b>{{actionById(l.actionId).caption}}</b> -
                                        <small>{{l.when}}</small>
                                    </div>
                                    <div><img style="margin-right:5px;"
                                              src="{{gravatarProvider.gravatar(userById(l.userId).email, 16)}}"/>{{userById(l.userId).email}}
                                    </div>
                                    <hr/>
                                </ng-container>
                            </div>

                        </div><!-- Activity Log -->



                    </div>
                </div>

            </div>


        </ng-container>


    `
})
export class VpForm implements OnInit, HasSavableChanges {
    identity: any;

    public get hasChanges(): Observable<boolean> {
        return Observable.of(
            !this._navigated && !_.isEqual(this._origVp, this.vp)
        );
    }

    public confirmNavigation(): Observable<boolean> {
        return Observable.of(
            confirm("Unsaved changes will be lost, are you sure?")
        );
    }

    canEdit() {
        return (!this.vp.checkedOutTo || this.vp.checkedOutTo === this.identity.id);
    }

    moment: typeof moment;
    // launches: any;
    adminRole: any;
    private _navigated: boolean = false;
    private _origVp: any;
    private vp: any;
    private logEntry: object = {};
    private subscribe: any;
    private allActions: any;
    private allMaintenanceProfiles: any;
    private allConnections: any;
    private allUsers: any[];
    private log: any[];
    private languages;
    private showAppIntro: boolean;
    private vpRole: any;
    private show: boolean;
    private allAudit: any;
    currentLog = 1;

    allProjects: any;
    allTopics: any;
    hasVpRole: boolean | any;
    private regions: any;

    deleteVp(){
        console.warn(this.vp)


        this.genericApiProvider
            .type("vos")
            .delete(this.vp.id)
            .subscribe(() => {
                alertify.success("Deleted");
                this.router.navigateByUrl("/vp");
            });




    }

    projectNameById(id) {
        const project = (this.allProjects || []).filter(p => {
            return Number(p.id) == Number(id);
        });
        return project[0] ? project[0].name : "Unknown";
    }
    topicNameById(id) {
        const topic = this.allTopics.filter(t => {
            return Number(t.id) === Number(id);
        });
        return topic[0] ? topic[0].name : "Unknown";
    }

    generateAuditArray(obj) {
        if (!obj) return [];
        return Object.keys(obj).map(key => {
            return key === "currentLocation"
                ? {
                      key: "serviceTimezone",
                      value: `${
                          (
                              this.regions.find(
                                  r => r.region_key === obj["currentLocation"]
                              ) || {}
                          ).provider
                      } - ${
                          (
                              this.regions.find(
                                  r => r.region_key === obj["currentLocation"]
                              ) || {}
                          ).display_name
                      }`
                  }
                : { key: key, value: obj[key] };
        });
    }

    fieldPermissions(fieldName) {
        if (this.adminRole === "superadmin") return false;
        if (!this.vpRole || !this.vpRole.settings) return true;
        if (this.vpRole.settings[fieldName]) return false;
        return true;
    }

    constructor(
        private vpService: VpApiService,
        private http: Http,
        private gravatarProvider: GravatarProvider,
        private activatedRoute: ActivatedRoute,
        private genericApiProvider: GenericApiProvider,
        private router: Router,
        private appCacheProvider: AppCacheProvider
    ) {
        const identity =this.appCacheProvider.getIdentity();
        this.hasVpRole =
            identity.adminRole == "superadmin" ||
            (identity.meta.modules.vp && isNumber(identity.meta.vpRole));

    }

    /*
    registerLaunch() {
        if (this.vp.id) {
            this.genericApiProvider
                .type("voLaunches")
                .post({
                    when: new Date(),
                    voId: this.vp.id
                })
                .subscribe(r => {
                    r;
                });
            this.vpService.getLaunches(this.vp.id).then(r => {
                this.launches = r;
            });
        }
    }
*/

    buttonActive() {
        let rtValue = false;
        Object.keys(this.logEntry).forEach(item => {
            if (this.logEntry[item] === true) rtValue = true;
        });
        return rtValue;
    }

    validActionForMaintenanceProfile(action) {
        const currentMaintenanceProfile = this.allMaintenanceProfiles.filter(
            s => {
                return s.id === Number(this.vp.statusId);
            }
        )[0];

        if (!currentMaintenanceProfile) return false;

        const maintenanceProfileAction = this.allConnections.filter(c => {
            return (
                c.actionId === action.id &&
                c.statusId === currentMaintenanceProfile.id
            );
        })[0];

        if (maintenanceProfileAction) return true;

        return false;
    }

    logWork() {
        const logRecords = [];

        Object.keys(this.logEntry).forEach(entry => {
            if (this.logEntry[entry] === true) {
                logRecords.push({
                    userId: this.appCacheProvider.getIdentity().id,
                    when: new Date().toJSON(),
                    serviceTimezone: "",
                    ip: "",
                    voId: this.vp.id,
                    actionId: Number(entry)
                });
            }
        });

        this.genericApiProvider
            .type("voActionLogs")
            .post(logRecords)
            .subscribe(res => {
                this.getLog();
            });

        this.logEntry = {};
    }

    discard() {
        this._navigated = true;
        this.router.navigateByUrl("/vp");
    }

    save() {
        this._navigated = true;
        if (this.vp.id) {
            // put
            this.genericApiProvider
                .type("vos")
                .put(this.vp)
                .subscribe(() => {
                    this._origVp = _.cloneDeep(this.vp);
                    /*this.router.navigateByUrl("/vp");*/
                    alertify.success("Saved");
                });
        } else {
            // post
            this.genericApiProvider
                .type("vos")
                .post(this.vp)
                .subscribe(res => {
                    if (res.json().id) {
                        this.router.navigateByUrl("/vp/edit/" + res.json().id);
                    }

                    alertify.success("Saved");
                });
        }
    }

    getAllPermissions() {
        const user = this.appCacheProvider.getIdentity();

        this.genericApiProvider
            .type("voRole")
            .list()
            .subscribe(allRoles => {
                this.adminRole = user.adminRole;
                this.vpRole = allRoles.filter(r => {
                    return r.id === user.meta.vpRole;
                })[0];
                this.show = true;

                 console.warn(this.vpRole);
            });
    }

    getProjectsAndTopics() {
        this.genericApiProvider
            .type("projects")
            .list()
            .subscribe(projects => {
                this.allProjects = projects;
            });
        this.genericApiProvider
            .type("subprojects")
            .list()
            .subscribe(subprojects => {
                this.allTopics = subprojects;
            });
    }

    ngOnInit() {
        this.identity = this.appCacheProvider.getIdentity();
        this.moment = moment;

        this.http.get('/proxy/getSupportedRegions').subscribe(res => {
            this.regions = res.json();
        });

        (async () => {
            this.languages = Object.keys(Languages).map(k => {
                return {
                    key: k,
                    value: `${Languages[k].name} (${Languages[k].nativeName})`
                };
            });

            this.allActions = await this.vpService.getAllActions();
            this.getAllMaintenanceProfiles();
            this.getAllConnections();
            this.getAllUsers();
            this.getAllPermissions();
            this.getProjectsAndTopics();

            this.subscribe = this.activatedRoute.params.subscribe(params => {
                if (params.vpId) {
                    /*
                    this.vpService.getLaunches(params.vpId).then(r => {
                        this.launches = r;
                    });
*/
                    this.genericApiProvider
                        .type("vos")
                        .get(params.vpId) //, { include: ["launch"] }
                        .subscribe(vp => {
                            this.vp = vp;
                            this._origVp = _.cloneDeep(vp);
                            this.getLog();
                            this.getAudit();
                        });
                } else {
                    /*
                    this.launches = [];
*/
                    this.vp = { settings: {} };
                }
            });
        })();
    }

    getAudit() {
        this.genericApiProvider
            .type("voAudit")
            .list({
                order: ["id DESC"],
                limit: 15,
                where: { voId: this.vp.id }
            })
            .subscribe(audit => {
                // console.warn(audit);
                this.allAudit = audit;
            });
    }

    getAllMaintenanceProfiles() {
        this.genericApiProvider
            .type("voStatus")
            .list()
            .subscribe(maintenanceProfiles => {
                this.allMaintenanceProfiles = maintenanceProfiles;
            });
    }

    getAllConnections() {
        this.genericApiProvider
            .type("voActionStatus")
            .list()
            .subscribe(connections => {
                this.allConnections = connections;
            });
    }

    userById(id) {
        if (!id) {
            return {};
        }
        return (
            this.allUsers.filter(u => {
                return u.id === id;
            })[0] || {}
        );
    }

    actionById(id) {
        return this.allActions.filter(a => {
            return a.id === id;
        })[0];
    }

    getAllUsers() {
        this.genericApiProvider
            .type("users")
            .list()
            .subscribe(users => {
                this.allUsers = users;
            });
    }

    getLog() {
        this.genericApiProvider
            .type("vos")
            .getRelation(this.vp.id, "actionLog")
            .subscribe(log => {
                this.log = log.reverse();
            });
    }

    ngOnDestroy() {
        this.subscribe.unsubscribe();
    }
}
