import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    OnInit,
    Output
} from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { GenericApiProvider } from "../shared/providers/genericApiProvider";
import * as alertify from 'alertifyjs';

@Component({
    styles: [
        `
        .row {
            position: fixed;
            top: 135px;
            bottom: 0;
            left: 0;
            right: 0;
            padding: 0 1em;
        }

        .col-6 {
            height: 100%;
        }

        .card {
            position: absolute;
            top: 0.5em;
            right: 1.5em;
            bottom: 1.5em;
            left: 1.5em;
        }

        .row .col-6:nth-child(1) .card {
            right: 0.7em;
            left: 1.5em;
        }

        .row .col-6:nth-child(2) .card {
            right: 1.5em;
            left: 0.7em;
        }

        .deleteLink {
            cursor: pointer;
        }

        .active .deleteLink {
            color: #fff;
        }
    `
    ],
    template: `
        <vpSubNav></vpSubNav>
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <listEditor [disabled]="false"
                                (refresh)="getAllMaintenanceProfiles()"
                                [list]="allMaintenanceProfiles"
                                [(selected)]="selectedMaintenanceProfile"
                                [itemSingular]="'maintenance profile'"
                                [itemPlural]="'maintenance profiles'"
                                [type]="'voStatus'"
                                [nameProperty]="'caption'">
                        <li style="width:100%;position:relative;"
                            [ngClass]="{'active':(context.item().id==this.selectedMaintenanceProfile?.id)}" class="nav-item"
                            *tag="'item'; let context">
                            <a
                                [ngClass]="{'active':(context.item().id==this.selectedMaintenanceProfile?.id)}"
                                class="nav-link"
                                href="javascript:;"
                                (click)="selectedMaintenanceProfile=(selectedMaintenanceProfile && (context.item().id==this.selectedMaintenanceProfile?.id))?null:context.item(); updateActionsList()"
                            >
                                <div>
                                    {{context.item().caption}}
                                </div>
                            </a>
                            <div class="deleteLink" (click)="deleteMaintenanceProfile(context.item())"
                                 style="position:absolute;top:5px;right:5px;">&times;
                            </div>
                        </li>
                    </listEditor>
                </div>
            </div>
            <div class="col-6">
                <div class="card">

                    <listEditor [disabled]="false"
                                *ngIf="actionsListVisible"
                                (refresh)="getAllActions()"
                                [list]="allActions"
                                [(selected)]="selectedAction"
                                [itemSingular]="'action'"
                                [itemPlural]="'actions'"
                                [type]="'voAction'"
                                [caption]="(selectedMaintenanceProfile)?selectedMaintenanceProfile.caption:null"
                                [nameProperty]="'caption'">
                        <li style="width:100%;position:relative;" class="nav-item" *tag="'item'; let context">
                            <span
                                class="nav-link"
                                href="javascript:;"
                            >
                                <div>
                                    <input *ngIf="selectedMaintenanceProfile" type="checkbox"
                                           (click)="toggleAction(selectedMaintenanceProfile.id, context.item().id)"
                                           [checked]="checkedAction(selectedMaintenanceProfile.id, context.item().id)"/>
                                    {{context.item().caption}}
                                    <span class="pull-right">

                                        <inputTimeblock
                                            *ngIf="selectedMaintenanceProfile && checkedAction(selectedMaintenanceProfile.id, context.item().id)"
                                            placeholder="Every"
                                            value="{{getConnection(selectedMaintenanceProfile.id, context.item().id).actionInterval}}"
                                            (onChange)="timeBlockChanged($event, context.item().id)"
                                        ></inputTimeblock>

                                    </span>
                                </div>
                            </span>
                            <div class="deleteLink" *ngIf="!selectedMaintenanceProfile" (click)="deleteAction(context.item())"
                                 style="position:absolute;top:5px;right:5px;">&times;
                            </div>
                            <hr style="margin:4px 0 0 0;"/>
                        </li>
                    </listEditor>

                </div>
            </div>
        </div>
    `
})
export class VpSettings implements OnInit {
    allConnections: any;
    selectedMaintenanceProfile: any;
    allActions: any;
    allMaintenanceProfiles: Object[];
    router: any;
    private actionsListVisible: boolean;

    constructor(
        private genericApiProvider: GenericApiProvider,
        private changeDetectorRef: ChangeDetectorRef
    ) {
        this.getAllMaintenanceProfiles();
        this.getAllActions();
        this.getAllConnections();
    }

    ngOnInit() {}

    updateActionsList() {
        this.actionsListVisible = false;
        this.changeDetectorRef.detectChanges();
        this.actionsListVisible = true;
    }

    getAllConnections() {
        this.genericApiProvider
            .type("voActionStatus")
            .list()
            .subscribe(connections => {
                this.allConnections = connections;
            });
    }

    deleteMaintenanceProfile(item) {
        if (
            this.selectedMaintenanceProfile &&
            item.id === this.selectedMaintenanceProfile.id
        ) {
            this.selectedMaintenanceProfile = null;
        }
        this.genericApiProvider
            .type("voStatus")
            .delete(item)
            .subscribe(() => {
                this.getAllMaintenanceProfiles();
            });
    }

    getAllMaintenanceProfiles() {
        this.genericApiProvider
            .type("voStatus")
            .list()
            .subscribe(maintenanceProfiles => {
                this.allMaintenanceProfiles = maintenanceProfiles;
            });
    }

    deleteAction(item) {
        this.genericApiProvider
            .type("voAction")
            .delete(item)
            .subscribe(() => {
                this.getAllActions();
            });
    }

    getAllActions() {
        this.genericApiProvider
            .type("voAction")
            .list()
            .subscribe(actions => {
                this.allActions = actions;
            });
    }

    checkedAction(maintenanceProfileId, actionId) {
        const connection = this.getConnection(maintenanceProfileId, actionId);
        return connection ? true : false;
    }

    toggleAction(maintenanceProfileId, actionId) {
        let connection = this.getConnection(maintenanceProfileId, actionId);
        if (connection) {
            this.genericApiProvider
                .type("voActionStatus")
                .delete(connection)
                .subscribe(() => {
                    this.getAllConnections();
                });
        } else {
            this.genericApiProvider
                .type("voActionStatus")
                .post({ statusId: maintenanceProfileId, actionId })
                .subscribe(() => {
                    this.getAllConnections();
                });
        }
    }

    timeBlockChanged(actionInterval, actionId) {
        const connection = this.getConnection(
            this.selectedMaintenanceProfile.id,
            actionId
        );

        connection.actionInterval = actionInterval;

        this.genericApiProvider
            .type("voActionStatus")
            .put(connection)
            .subscribe(
                res => {
                    console.warn(res);
                },
                err => {
                    alertify.error("Error saving new value.");
                }
            );
    }

    getConnection(maintenanceProfileId, actionId) {
        const connection = this.allConnections.filter(c => {
            return (
                c.statusId === maintenanceProfileId && c.actionId === actionId
            );
        })[0];
        return connection;
    }
}
