import {
    ApplicationRef,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewChild,
    OnDestroy
} from "@angular/core";
import { Http } from "@angular/http";
import * as alertify from "alertifyjs";
import { GenericApiProvider } from "../../shared/providers/genericApiProvider";
import { VpApiService } from "../services/vp.service";
import * as moment from "moment";
import { AppCacheProvider } from "../../shared/providers/app-cache-provider";
import { environment } from "../../../../environments/environment";
import { PushProvider } from "../../shared/providers/push-provider";

@Component({
    selector: "electron-launcher",
    styles: [
        `
            .dropdown-menu {
                max-height: 200px;
                overflow-y: auto;
                overflow-x: hidden;
            }
        `
    ],
    template: `
        <div style="display:inline-block; white-space: nowrap;" *ngIf="allUsers">
<!--{{_vp|json}}-->

            <span class="dropdown" *ngIf="launches; else noLaunchesYet">
                <a href="javascript:;" class="btn btn-default btn-xs mr-1 dropdown-toggle" data-toggle="dropdown">
                    <small>Last launch:</small> {{(launches[0])?moment(launches[0].when).fromNow():'Never'}}
                </a>
                <div *ngIf="launches.length > 0" class="dropdown-menu">
                    <div *ngFor="let launch of launches" style="padding:5px 17px;">
                        {{launch.when}}
                        ({{moment(launch.when).fromNow()}})
                    </div>
                </div>
                <div *ngIf="launches.length==0" class="dropdown-menu">
                    <div style="padding:0px 17px;">
                        No log items
                    </div>
                </div>
            </span>

            <ng-template #noLaunchesYet>
                <span class="dropdown">
                    <a href="javascript:;" class="btn btn-default btn-xs mr-1">
                        <small>Last launch:</small> <i class="fa fa-spinner fa-spin"></i>
                    </a>

                </span>
            </ng-template>

            <a *ngIf="canCheckout()" class="btn btn-info btn-xs" href="javascript:;" (click)="checkout()">Checkout</a>
            <a *ngIf="canLaunch()" class="btn btn-success btn-xs" href="javascript:;" type="button" value="Open App" (click)="launch()" #launchB>Launch App</a>
            <span *ngIf="isCheckedOutToSomeoneElse()">Checked-out to <b>{{getUser(_vp.checkedOutTo).email}}</b></span>
            <a *ngIf="canRelease()" class="btn btn-default btn-xs" href="javascript:;" (click)="release()">Release</a>





            <div *ngIf="showAppIntro" class="card" style="padding:16px;position:absolute;top:91px;right:30px;z-index:99999;box-shadow:1px 1px 10px rgba(0,0,0,0.5)">
                <a style="color:#000;position:absolute;top:2px;right:4px;" href="javascript:;" (click)="showAppIntro=false">&times;</a>
                <h4>Agent not active on your system</h4>
                To use the launch feature, you will need to install and run
                <div style="text-align:center;margin-top:17px;"><a class="btn btn-primary btn-xs" target="_blank" href="/assets/launcher-setup.exe">the Launcher</a></div>
            </div>

        </div>
    `
})
export class ElectronLauncher implements OnInit, OnDestroy {
    lastDate: any;
    _vp: any;
    log: any;
    allUsers: any;
    moment: typeof moment;
    private identity: any;

    private _subscribedChannels: string[] = [];

    @Input()
    userList?: any = null;

    @Input()
    set vp(val: any) {
        this._vp = val;
    }

    canCheckout() {
        if (this._vp.checkedOutTo !== null) {
            return false;
        }
        if (this.identity.adminRole === "superadmin") {
            return true;
        }
        if (
            this.identity.adminRole === "admin" &&
            this.identity.meta.modules.vp === true
        ) {
            return true;
        }
        if (
            (this.identity.adminRole === "user" ||
                this.identity.adminRole === null) &&
            this.identity.meta.modules.vp === true
        ) {
            return true;
        }
    }

    canLaunch() {
        return this._vp.checkedOutTo == this.identity.id;
    }

    isCheckedOutToSomeoneElse() {
        return (
            this._vp.checkedOutTo &&
            this._vp.checkedOutTo != this.identity.id
        );
    }

    canRelease() {
        return (
            this._vp.checkedOutTo !== null &&
            (this._vp.checkedOutTo == this.identity.id ||
                this.identity.adminRole === "superadmin")
        );
    }

    private regions: any;
    private showAppIntro: boolean;
    launches: any;

    @ViewChild("launchB")
    public launchButton: ElementRef;

    constructor(
        private http: Http,
        private genericApiProvider: GenericApiProvider,
        private vpService: VpApiService,
        private applicationRef: ApplicationRef,
        private appCacheProvider: AppCacheProvider,
        private pushProvider: PushProvider
    ) {}

    checkout() {
        this._vp.checkedOutTo = this.identity.id;
        this.saveVp("VP Checked-Out");
    }

    release() {
        this._vp.checkedOutTo = null;
        this.saveVp("VP Released");
    }

    saveVp(successMessage) {
        this.genericApiProvider
            .type("vos")
            .put(this._vp)
            .subscribe(() => {
                alertify.success(successMessage);
            });
    }

    getLaunches() {
        this.vpService.getLaunches(this._vp.id).then(r => {
            this.launches = null;
            this.applicationRef.tick();
            this.launches = r;
        });
    }

    ngOnInit() {
        this.http.get("/proxy/getSupportedRegions").subscribe(res => {
            this.regions = res.json();
        });

        this.identity = this.appCacheProvider.getIdentity();

        if (this.userList) {
            this.allUsers = this.userList;
        } else {
            this.vpService.getAllUsers().then(res => {
                this.allUsers = res;
            });
        }

        this.moment = moment;

        if (this._vp.id) {
            this.getLaunches();
        } else {
            this.launches = [];
        }
    }

    ngOnDestroy(): void {
        this.pushProvider.unsubscribe(...this._subscribedChannels);
    }

    getUser(userId) {
        return this.allUsers.filter(u => {
            return u.id === userId;
        })[0];
    }

    private static getRandomId(): string {
        const chars = "0123456789abcdefghijklmnopqrstuvwxyz";
        return Array.from(
            (function*() {
                for (let i = 0; i < 16; i++) {
                    yield chars[Math.floor(Math.random() * chars.length)];
                }
            })()
        ).join("");
    }

    registerLaunch() {
        this.lastDate = (this.launches[0] || {}).id;
        if (this._vp.id) {
            this.vpService.getLaunches(this._vp.id).then(r => {
                this.launches = r;
            });
        }
        // this.refreshOnInterval();
    }

    // refreshOnInterval() {
    // setTimeout(() => {
    // if (this._vp.id) {
    //     this.vpService.getLaunches(this._vp.id).then(r => {
    //         this.launches = r;
    //     });
    // }
    //     if ((this.launches[0] || {}).id === this.lastDate) {
    //         this.refreshOnInterval();
    //     }
    // }, 2000);
    // }

    launchAction() {
        const currentLocation = this.regions.find(region => {
            return region.region_key === this._vp.settings.currentLocation;
        });

        if (currentLocation) {
            (async () => {
                const launchId = ElectronLauncher.getRandomId();
                let launched = false;
                this.pushProvider
                    .subscribeOnce(`launch-${launchId}`)
                    .subscribe(mes => {
                        launched = true;
                        this.registerLaunch();
                    });
                this._subscribedChannels.push(`launch-${launchId}`);
                setTimeout(() => {
                    if (!launched) {
                        this.showAppIntro = true;
                        setTimeout(() => {
                            this.showAppIntro = false;
                        }, 5000);
                    }
                }, 10000);
                location.href = `cycurity:${encodeURIComponent(
                    JSON.stringify({
                        settings: {
                            id: this._vp.id,
                            name: this._vp.name,
                            email: this._vp.settings.email,
                            emailPassword: this._vp.settings.emailPassword,
                            twitterHandle: this._vp.settings.twitterHandle,
                            twitterPassword: this._vp.settings.twitterPassword
                        },
                        proxy: {
                            region: currentLocation.region_key,
                            accessToken: this.appCacheProvider.getToken()
                        },
                        env: environment.env,
                        launch: launchId
                    })
                )}`;
            })();
        } else {
            alertify.error("Service and Zone Not Defined!");
        }
    }

    launch(): void {
        if (
            !this.launches[0] ||
            moment().diff(this.launches[0].when, "days") > 0
        ) {
            this.launchAction();
        } else {
            alertify.confirm(
                "",
                "This VP has already been launched today. Do you wish to launch this VP again?",
                () => {
                    this.launchAction();
                },
                () => {}
            );
        }
    }
}
