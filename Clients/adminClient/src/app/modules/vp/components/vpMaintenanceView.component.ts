import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { GenericApiProvider } from "../../shared/providers/genericApiProvider";
import { GravatarProvider } from "../../shared/providers/gravatarProvider";
import { Router, ActivatedRoute } from "@angular/router";
import * as _ from "lodash";
import { VpApiService } from "../services/vp.service";
import { Languages } from "../../shared/providers/languageProvider";
import { SortController, ArraySortController, order } from "./sort-controller";
import { Observable } from "rxjs/Observable";
import { Http } from "../../../../../node_modules/@angular/http";

@Component({
    selector: "vpMaintenanceView",
    styles: [
        `
        tr td {
            vertical-align: middle;
        }
    `
    ],
    template: `
        <div class="card" style="margin-top:17px;padding:10px;margin-bottom:10px;">

            <div class="row">
                <div class="col-10">
                    <div class="row">
                        <div class="col-2">
                            <small>Management Profile</small>
                            <select class="form-control" *ngIf="allMaintenanceProfiles" [(ngModel)]="filter.maintenanceProfile" (ngModelChange)="updateState({'filter': filter})">
                                <option></option>
                                <option *ngFor="let maintenanceProfile of allMaintenanceProfiles" [value]="maintenanceProfile.id">{{maintenanceProfile.caption}}</option>
                            </select>
                        </div>
                        <div class="col-2">
                            <small>Project</small>
                            <select class="form-control" *ngIf="allProjects" [(ngModel)]="filter.project" (ngModelChange)="updateState({'filter': filter})">
                                <option></option>
                                <option *ngFor="let p of allProjects" [value]="p.id">
                                    <ng-container *ngIf="p.name?.trim() != ''; else unnamedProjectOption">
                                        {{p.name}}
                                    </ng-container>
                                    <ng-template #unnamedProjectOption>
                                        Unnamed
                                    </ng-template>
                                </option>
                            </select>
                        </div>
                        <div class="col-2">
                            <small>Topic</small>
                            <select class="form-control" *ngIf="allTopics" [(ngModel)]="filter.topic" (ngModelChange)="updateState({'filter': filter})">
                                <option></option>
                                <option *ngFor="let t of allowedTopics()" [value]="t.id">
                                    <ng-container *ngIf="t.name?.trim() != ''; else unnamedTopicOption">
                                        {{t.name}}
                                    </ng-container>
                                    <ng-template #unnamedTopicOption>
                                        Unnamed
                                    </ng-template>
                                </option>
                            </select>
                        </div>
                        <div class="col-2">
                            <small>Language</small>
                            <select class="form-control" *ngIf="languages" [(ngModel)]="filter.language" (ngModelChange)="updateState({'filter': filter})">
                                <option></option>
                                <option *ngFor="let t of languages" [value]="t.key">{{t.value}}</option>
                            </select>
                        </div>
                        <div class="col-2">
                            <small>Service/Timezone</small>
                            <select class="form-control" *ngIf="regions" [(ngModel)]="filter.serviceTimezone" (ngModelChange)="updateState({'filter': filter})">
                                <option></option>
                                <option *ngFor="let region of getAllRegions()" [value]="region.region_key">{{region.provider}} - {{region.display_name}}</option>
                            </select>
                        </div>
                        <div class="col-2">
                            <small>Free Text</small>
                            <input style="line-height:2.2em" type="text" [(ngModel)]="filter.freetext" (ngModelChange)="updateState({'filter': filter})"/>
                        </div>

                    </div>


                </div>

                <div class="col-2">
                    <a (click)="applyFilter()" href="javascript:;" class="btn btn-primary btn-xs pull-right" style="margin-top:24px;margin-right:6px;">Filter</a>
                    <a (click)="clearFilter();updateState({'filter': filter})" href="javascript:;" class="btn btn-default btn-xs pull-right" style="margin-top:24px;margin-right:6px;">Clear</a>
                </div>


            </div>


        </div>

        <div *ngIf="allVPs && allVPs.length===0" class="alert alert-info">There are currently no VPs defined <a [routerLink]="['/vp/new']" class="btn btn-xs btn-primary" href="">Create VP</a></div>

        <ng-container *ngIf="allVPs">
            <ng-container *ngIf="allVPs.length>0 && visibleVpsCount()>0; else emptySearchMessage">
                <table *ngIf="allVPs && allUsers && allProjects && allTopics" class="table table-hover">
                    <thead>
                    <tr>
                        <th (click)="sortController.sortBy('name')"><i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('name')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('name')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('handle')">Handle<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('handle')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('handle')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('language')">Language<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('language')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('language')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('project')">Project<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('project')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('project')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('topic')">Topic<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('topic')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('topic')==='desc'"></i></th>
                        <th>Required Actions</th>
                        <th (click)="sortController.sortBy('daysDelayed')">Days Delayed<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('daysDelayed')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('daysDelayed')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('lastAction')">Last Action Date<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('lastAction')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('lastAction')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('lastActionUser')">Last Action By<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('lastActionUser')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('lastActionUser')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('region')">Service and Zone<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('region')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('region')==='desc'"></i></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr *ngFor="let vp of allVPs">

                        <ng-container *ngIf="vp._display">

                            <td>
                                <img [src]="gravatarProvider.gravatar(vp.settings.email||'', 24)" alt=""/>
                                <a [routerLink]="['/vp/edit/',vp.id]">{{vp.name||'Unnamed'}}</a>
                            </td>

                            <td>{{vp.settings.twitterHandle}}</td>

                            <td>{{LanguagesObj[vp.settings.language]?.name}} <small>{{LanguagesObj[vp.settings.language]?.nativeName}}</small></td>

                            <td>
                                <ng-container *ngIf="getProject(vp.settings.projectTopicCombo?.project).name && getProject(vp.settings.projectTopicCombo?.project).name.trim() != ''; else unnamedProject">
                                    {{getProject(vp.settings.projectTopicCombo?.project).name}}
                                </ng-container>
                                <ng-template #unnamedProject>
                                    <i *ngIf="vp.settings.projectTopicCombo?.project">Unnamed</i>
                                </ng-template>
                            </td>

                            <td>
                                <ng-container *ngIf="getTopic(vp.settings.projectTopicCombo?.topic).name && getTopic(vp.settings.projectTopicCombo?.topic).name.trim() != ''; else unnamedTopic">
                                    {{getTopic(vp.settings.projectTopicCombo?.topic).name}}
                                </ng-container>
                                <ng-template #unnamedTopic>
                                    <i *ngIf="vp.settings.projectTopicCombo?.topic">Unnamed</i>
                                </ng-template>
                            </td>

                            <td>
                                <ul>
                                    <li *ngFor="let action of vp.missingActions">
                                        {{action.caption}}
                                    </li>
                                </ul>
                            </td>

                            <td>{{vpApiService.showDaysDelayed(vp.daysDelayed)}}</td>

                            <td>{{vp.lastActionDate}}</td>

                            <td>
                                <div *ngIf="getUser(vp.lastActionUserId)">
                                    <img [src]="gravatarProvider.gravatar(getUser(vp.lastActionUserId).email||'', 24)" alt=""/>
                                    {{getUser(vp.lastActionUserId).email}}
                                </div>
                            </td>

                            <td><ng-container *ngIf="regions">{{regions[vp.settings.currentLocation]?.provider}} - {{regions[vp.settings.currentLocation]?.display_name}}</ng-container></td>

                            <td style="text-align:right;">
                                <electron-launcher [userList]="allUsers" [vp]="vp"></electron-launcher>
                            </td>

                        </ng-container>

                    </tr>
                    </tbody>
                </table>
            </ng-container>
            <ng-template #emptySearchMessage>
                <div class="alert alert-info">Your query yielded 0 results, please adjust your filter</div>
            </ng-template>

        </ng-container>


    `
})
export class VpMaintenanceView implements OnInit {
    sortController: SortController;
    filter: any = {};
    allTopics: any;
    allProjects: any;
    allVPs: any;
    private allUsers: any;
    private allMaintenanceProfiles: any;
    private languages: any;
    LanguagesObj: any;
    private regions: any;

    @Input()
    vpState: Observable<{
        view: "maintenance" | "management";
        sortColumn: string;
        sortOrder: order;
        filter: any;
    }>;

    getAllRegions() {
        return this.regions && this.allVPs
            ? Object.keys(this.regions)
                //   .filter(key =>
                //       this.allVPs.some(
                //           vp => vp.settings.currentLocation === key
                //       )
                //   )
                  .map(k => this.regions[k])
            : null;
    }

    visibleVpsCount() {
        return this.allVPs.filter(vp => {
            return vp._display === true;
        }).length;
    }

    clearFilter() {
        this.filter = {};
        this.applyFilter();
    }

    updateState(value: { [field: string]: any }) {
        this.vpState.first().subscribe(state => {
            const cState = _.cloneDeep(state);
            Object.keys(value)
                .map(key => ({
                    [key]:
                        typeof value[key] === "object"
                            ? JSON.stringify(value[key])
                            : value[key]
                }))
                .forEach(kv => Object.assign(cState, kv));
            this.router.navigate([".", cState]);
        });
    }

    applyFilter() {
        this.allVPs.forEach(vp => {
            vp._display = false;

            if (
                !this.filter.maintenanceProfile &&
                !this.filter.project &&
                !this.filter.topic &&
                !this.filter.language &&
                !this.filter.serviceTimezone &&
                !this.filter.freetext
            ) {
                vp._display = true;
                return;
            }

            console.warn(
                Number(this.filter.maintenanceProfile),
                Number(vp.statusId)
            );

            if (
                (!this.filter.maintenanceProfile ||
                    Number(this.filter.maintenanceProfile) ===
                        Number(vp.statusId)) &&
                (!this.filter.project ||
                    Number(this.filter.project) ===
                        Number(
                            (vp.settings.projectTopicCombo || {}).project
                        )) &&
                (!this.filter.topic ||
                    Number(this.filter.topic) ===
                        Number((vp.settings.projectTopicCombo || {}).topic)) &&
                (!this.filter.language ||
                    this.filter.language === vp.settings.language) &&
                (!this.filter.serviceTimezone ||
                    this.filter.serviceTimezone == "" ||
                    (vp.settings.currentLocation &&
                        ("" + vp.settings.currentLocation).includes(
                            this.filter.serviceTimezone
                        ))) &&
                (!this.filter.freetext ||
                    this.filter.freetext == "" ||
                    vp.freetext.includes(this.filter.freetext))
            ) {
                vp._display = true;
            }
        });
    }

    constructor(
        private http: Http,
        private genericApiProvider: GenericApiProvider,
        private gravatarProvider: GravatarProvider,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private vpApiService: VpApiService
    ) {
        this.languages = Object.keys(Languages).map(k => {
            return {
                key: k,
                value: `${Languages[k].name} (${Languages[k].nativeName})`
            };
        });

        this.LanguagesObj = Languages;

    }

    async asyncInit() {
        await Promise.all([
            this.vpApiService.getAllUsers(),
            this.vpApiService.getAllProjects(),
            this.vpApiService.getAllTopics(),
            this.vpApiService.getAllMaintenanceProfiles(),
            this.vpApiService.getAllVPs(),
        ]).then((payload)=>{
            this.allUsers = payload[0];
            this.allProjects = payload[1];
            this.allTopics = payload[2];
            this.allMaintenanceProfiles = payload[3];
            this.allVPs = payload[4];
        });

/*
        this.allUsers = await this.vpApiService.getAllUsers();
        this.allProjects = await this.vpApiService.getAllProjects();
        this.allTopics = await this.vpApiService.getAllTopics();
        this.allMaintenanceProfiles = await this.vpApiService.getAllMaintenanceProfiles();

        this.allVPs = await this.vpApiService.getAllVPs();
*/

        this.allVPs = this.allVPs.map(vp => {
            vp.freetext = this.vpApiService.jsonToString(vp);
            return vp;
        });

        this.applyFilter();

        this.vpState.subscribe(state => {
            const sortState: any = { column: "name", order: "asc" };
            if (state["sortColumn"]) {
                sortState.column = state["sortColumn"];
            }
            if (state["sortOrder"]) {
                sortState.order = state["sortOrder"];
            }
            const curState = this.sortController.getState();
            this.sortController.setState(sortState);
            if (!curState ||
                `${curState["column"]}` != `${sortState["column"]}` ||
                `${curState["order"]}` != `${sortState["order"]}`
            ) {
                this.sortController.sortBy(
                    sortState["column"],
                    sortState["order"]
                );
            }
            if (state["filter"]) {
                this.filter =
                    (state.filter
                        ? typeof state.filter === "string"
                            ? JSON.parse(state.filter)
                            : state.filter
                        : null) || {};
            }
        });
    }

    ngOnInit() {
        this.http.get('/proxy/getSupportedRegions').subscribe(res => {
            this.regions = res.json().reduce((sum, cur) => Object.assign(sum, {[cur.region_key]: cur}, {}), {});
        });
        const caseInsensitiveComparer = (a, b) => {
            if (a && typeof a === "string") a = a.toLowerCase();
            if (b && typeof b === "string") b = b.toLowerCase();
            if (a == b) return 0;
            return a < b ? -1 : 1;
        };
        this.sortController = new ArraySortController(
            () => this.allVPs,
            {
                name: {
                    getValue: item => item.name,
                    compare: caseInsensitiveComparer
                },
                handle: {
                    getValue: item =>
                        item && item.settings
                            ? item.settings.twitterHandle
                            : null,
                    compare: caseInsensitiveComparer
                },
                language: {
                    getValue: item =>
                        this.LanguagesObj[item.settings.language]
                            ? this.LanguagesObj[item.settings.language].name
                            : null,
                    compare: caseInsensitiveComparer
                },
                project: {
                    getValue: item =>
                        item.settings.projectTopicCombo
                            ? this.getProject(
                                  item.settings.projectTopicCombo.project
                              ).name
                            : null,
                    compare: caseInsensitiveComparer
                },
                topic: {
                    getValue: item =>
                        item.settings.projectTopicCombo
                            ? this.getTopic(
                                  item.settings.projectTopicCombo.topic
                              ).name
                            : null,
                    compare: caseInsensitiveComparer
                },
                daysDelayed: {
                    getValue: item => item.daysDelayed,
                    compare: caseInsensitiveComparer
                },
                lastAction: {
                    getValue: item => item.lastActionDate,
                    compare: caseInsensitiveComparer
                },
                lastActionUser: {
                    getValue: item =>
                        this.getUser(item.lastActionUserId)
                            ? this.getUser(item.lastActionUserId).email
                            : null,
                    compare: caseInsensitiveComparer
                },
                region: {
                    getValue: item =>
                        this.regions[item.settings.currentLocation]
                            ? `${
                                  this.regions[item.settings.currentLocation]
                                      .provider
                              } - ${
                                  this.regions[item.settings.currentLocation]
                                      .display_name
                              }`
                            : null,
                    compare: caseInsensitiveComparer
                }
            },
            sorted => {
                this.allVPs = sorted;
                const state = this.sortController.getState();
                this.updateState({
                    sortColumn: state.column,
                    sortOrder: state.order
                });
            }
        );
        this.asyncInit();
    }

    allowedTopics() {
        return (this.allTopics || []).filter(t => {
            return t.projectId === Number(this.filter.project);
        });
    }

    getUser(userId) {
        return this.allUsers.filter(u => {
            return u.id === userId;
        })[0];
    }

    getProject(projectId) {
        return (
            this.allProjects.filter(p => {
                return p.id === Number(projectId);
            })[0] || {}
        );
    }

    getTopic(topicId) {
        return (
            this.allTopics.filter(t => {
                return t.id === Number(topicId);
            })[0] || {}
        );
    }
}
