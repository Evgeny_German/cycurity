import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { GenericApiProvider } from "../../shared/providers/genericApiProvider";
import { GravatarProvider } from "../../shared/providers/gravatarProvider";
import { Router } from "@angular/router";
import { VpApiService } from "../services/vp.service";
import { Languages } from "../../shared/providers/languageProvider";
import { Observable } from "rxjs/Observable";
import { order, SortController, ArraySortController } from "./sort-controller";
import * as alertify from "alertifyjs";
import * as _ from 'lodash';
import { Http } from "../../../../../node_modules/@angular/http";

@Component({
    selector: "vpManagementView",
    styles: [
        `
            tr td {
                vertical-align: middle;
            }

            tr.no-hover:hover,
            tr.no-hover tr:hover {
                background-color: transparent;
            }

            tr.no-hover label {
                color: #bbb;
                font-size: 0.8em;
                display: block;
            }
        `
    ],
    template: `
        <vp-edit-popup *ngIf="editPopupOpen" [allProjects]="allProjects" [allTopics]="allTopics" [allMaintenanceProfiles]="allMaintenanceProfiles" [languages]="languages" (done)="doneEdit($event)"></vp-edit-popup>
        <div class="card" style="margin-top:17px;padding:10px;margin-bottom:10px;">
            <div class="row">
                <div class="col-10">
                    <div class="row">
                        <div class="col-2">
                            <small>Management Profile</small>
                            <select class="form-control" *ngIf="allMaintenanceProfiles" [(ngModel)]="filter.maintenanceProfile" (ngModelChange)="updateState({'filter': filter})">
                                <option></option>
                                <option *ngFor="let maintenanceProfile of allMaintenanceProfiles" [value]="maintenanceProfile.id">{{maintenanceProfile.caption}}</option>
                            </select>
                        </div>
                        <div class="col-2">
                            <small>Project</small>
                            <select class="form-control" *ngIf="allProjects" [(ngModel)]="filter.project" (ngModelChange)="updateState({'filter': filter})">
                                <option></option>
                                <option *ngFor="let p of allProjects" [value]="p.id">
                                    <ng-container *ngIf="p.name?.trim() != ''; else unnamedProjectOption">
                                        {{p.name}}
                                    </ng-container>
                                    <ng-template #unnamedProjectOption>
                                        Unnamed
                                    </ng-template>
                                </option>
                            </select>
                        </div>
                        <div class="col-2">
                            <small>Topic</small>
                            <select class="form-control" *ngIf="allTopics" [(ngModel)]="filter.topic" (ngModelChange)="updateState({'filter': filter})">
                                <option></option>
                                <option *ngFor="let t of allowedTopics()" [value]="t.id">
                                    <ng-container *ngIf="t.name?.trim() != ''; else unnamedTopicOption">
                                        {{t.name}}
                                    </ng-container>
                                    <ng-template #unnamedTopicOption>
                                        Unnamed
                                    </ng-template>
                                </option>
                            </select>
                        </div>
                        <div class="col-2">
                            <small>Language</small>
                            <select class="form-control" *ngIf="languages" [(ngModel)]="filter.language" (ngModelChange)="updateState({'filter': filter})">
                                <option></option>
                                <option *ngFor="let t of languages" [value]="t.key">{{t.value}}</option>
                            </select>
                        </div>
                        <div class="col-2">
                            <small>Service/Timezone</small>
                            <select class="form-control" *ngIf="regions" [(ngModel)]="filter.serviceTimezone" (ngModelChange)="updateState({'filter': filter})">
                                <option></option>
                                <option *ngFor="let region of getAllRegions()" [value]="region.region_key">{{region.provider}} - {{region.display_name}}</option>
                            </select>
                        </div>
                        <div class="col-2">
                            <small>Free Text</small>
                            <input style="line-height:2.2em" type="text" [(ngModel)]="filter.freetext" (ngModelChange)="updateState({'filter': filter})"/>
                        </div>

                    </div>


                </div>

                <div class="col-2">
                    <a (click)="applyFilter()" href="javascript:;" class="btn btn-primary btn-xs pull-right" style="margin-top:24px;margin-right:6px;">Filter</a>
                    <a (click)="clearFilter();updateState({'filter': filter})" href="javascript:;" class="btn btn-default btn-xs pull-right" style="margin-top:24px;margin-right:6px;">Clear</a>
                    <a (click)="editPopupOpen = true" href="javascript:;" class="btn btn-xs pull-right" [ngClass]="{disabled: !areAllVpsSelected()}" style="margin-top:24px;margin-right:6px;">Edit</a>
                </div>


            </div>
        </div>

        <div *ngIf="show && allVPs.length===0" class="alert alert-info">There are currently no VPs defined <a [routerLink]="['/vp/new']" class="btn btn-xs btn-primary" href="">Create VP</a></div>

        <ng-container *ngIf="show">
            <ng-container *ngIf="allVPs.length>0 && visibleVpsCount()>0; else emptySearchMessage">
                <table *ngIf="allVPs && allUsers && allProjects && allTopics" class="table table-hover">
                    <thead>
                    <tr>
                        <th (click)="sortController.sortBy('name')"><i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('name')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('name')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('handle')">Handle<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('handle')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('handle')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('maintenanceProfile')">Maintenance Profile<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('maintenanceProfile')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('maintenanceProfile')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('language')">Language<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('language')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('language')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('project')">Project<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('project')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('project')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('topic')">Topic<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('topic')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('topic')==='desc'"></i></th>
                        <th (click)="sortController.sortBy('region')">Service and Zone<i class="fa fa-angle-up" *ngIf="sortController && sortController.isSortedBy('region')==='asc'"></i><i class="fa fa-angle-down" *ngIf="sortController && sortController.isSortedBy('region')==='desc'"></i></th>
                        <th style="position: relative;"><span style="position: absolute; display:block; z-index:1; top: 4px; left: 23px;" class="badge badge-info" *ngIf="selectedVpCount()">{{selectedVpCount()}}</span><input type="checkbox" [ngModel]="areAllVpsSelected()" (ngModelChange)="selectAllVps($event)"/></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                        <ng-container *ngFor="let vp of allVPs">
                            <ng-container *ngIf="vp._display">

                                <tr>

                                    <td>
                                        <img [src]="gravatarProvider.gravatar(vp.settings.email||'', 24)" alt=""/>
                                        <a [routerLink]="['/vp/edit/',vp.id]">{{vp.name||'Unnamed'}}</a>
                                    </td>

                                    <td>{{vp.settings.twitterHandle}}</td>

                                    <td>{{getMaintenanceProfile(vp.statusId).caption}}</td>

                                    <td><ng-container *ngIf="languagesObj">{{languagesObj[vp.settings.language]?.name}} <small>{{languagesObj[vp.settings.language]?.nativeName}}</small></ng-container></td>

                                    <td>
                                        <ng-container *ngIf="getProject(vp.settings.projectTopicCombo?.project).name && getProject(vp.settings.projectTopicCombo?.project).name.trim() != ''; else unnamedProject">
                                            {{getProject(vp.settings.projectTopicCombo?.project).name}}
                                        </ng-container>
                                        <ng-template #unnamedProject>
                                            <i *ngIf="vp.settings.projectTopicCombo?.project">Unnamed</i>
                                        </ng-template>
                                    </td>
                                    <td>
                                        <ng-container *ngIf="getTopic(vp.settings.projectTopicCombo?.topic).name && getTopic(vp.settings.projectTopicCombo?.topic).name.trim() != ''; else unnamedTopic">
                                            {{getTopic(vp.settings.projectTopicCombo?.topic).name}}
                                        </ng-container>
                                        <ng-template #unnamedTopic>
                                            <i *ngIf="vp.settings.projectTopicCombo?.topic">Unnamed</i>
                                        </ng-template>
                                    </td>

                                    <td><ng-container *ngIf="regions">{{regions[vp.settings.currentLocation]?.provider}} - {{regions[vp.settings.currentLocation]?.display_name}}</ng-container></td>

                                    <td><input type="checkbox" [ngModel]="isVpSelected(vp)" (ngModelChange)="selectVp(vp, !!$event)"/></td>

                                    <td><a onclick="event.stopPropagation();" (click)="vp._expanded=!vp._expanded" title="more" class="btn btn-primary btn-sm"><i class="fa fa-chevron-down text-light"></i></a></td>

                                </tr>

                                <tr class="no-hover" *ngIf="vp._expanded">
                                    <td></td>
                                    <td colspan="5">
                                        <div class="card" style="padding:1em 1em 0 1em;position:relative;top:-1em;box-shadow:1px 1px 10px rgba(0,0,0,0.1);">
                                            <table class="table">
                                                <tr>
                                                    <td><label>Followers</label>
                                                        <twitterProfileStat [profile]="vp.settings.twitterHandle" field="followers_count"></twitterProfileStat>
                                                    </td>
                                                    <td><label>Following</label>
                                                        <twitterProfileStat [profile]="vp.settings.twitterHandle" field="following"></twitterProfileStat>
                                                    </td>
                                                    <td><label>Name</label> {{vp.name && vp.name.trim()!=''?vp.name:'-'}}</td>
                                                    <td><label>Email Address</label> {{vp.settings.email||'-'}}</td>
                                                </tr>
                                                <tr>
                                                    <td><label>Phone Number</label> {{vp.settings.phoneNumber||'-'}}</td>
                                                    <td><label>Twitter Creation Date</label> {{vp.settings.TwitterCreationDate||'-'}}</td>
                                                    <td><label>Last Action Date</label> {{vp.lastActionDate||'-'}}</td>
                                                    <td><label>Last User Acting</label> {{vp.lastUserActing||'None'}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>

                            </ng-container>
                        </ng-container>

                    </tbody>
                </table>
            </ng-container>

            <ng-template #emptySearchMessage>
                <div class="alert alert-info">Your query yielded 0 results, please adjust your filter</div>
            </ng-template>

        </ng-container>


    `
})
export class VpManagementView implements OnInit {
    sortController: SortController;

    editPopupOpen: boolean;

    async doneEdit(value: {
        projectId?: number;
        topicId?: number;
        maintenanceProfileId?: number;
        regionId?: number;
        languageId?: number;
    }) {
        if (
            value &&
            !confirm(
                `You are about to make changes to ${
                    this._selectedVp.length
                } VPs. Are you sure you want to proceed?`
            )
        )
            return;
        this.editPopupOpen = false;
        if (value) {
            let success = false;
            try {
                if ("projectId" in value && !("topicId" in value)) {
                    if (value.projectId) {
                        for (let vp of this._selectedVp) {
                            if (
                                vp.settings.projectTopicCombo &&
                                vp.settings.projectTopicCombo.topicId
                            ) {
                                const topic = this.allTopics.find(
                                    p => p.id == value.topicId
                                );
                                if (
                                    !topic ||
                                    topic.projectId != value.projectId
                                ) {
                                    delete vp.settings.projectTopicCombo[
                                        "topicId"
                                    ];
                                }
                            }
                        }
                    } else {
                        for (let vp of this._selectedVp) {
                            if (
                                vp.settings.projectTopicCombo &&
                                vp.settings.projectTopicCombo.topicId
                            ) {
                                delete vp.settings.projectTopicCombo["topicId"];
                            }
                        }
                    }
                }
                await this.vpApiService.updateVps(this._selectedVp, value);
                success = true;
            } catch (error) {
                alertify.error(`Error: ${error}`);
            }
            if (success) {
                this.allVPs = await this.vpApiService.getAllVPs();
                this._selectedVp = this._selectedVp
                    .map(vp => this.allVPs.find(vp2 => vp2.id === vp.id))
                    .filter(vp => !!vp);
                this.applyFilter();
                alertify.success("Saved");
            }
        }
    }

    regions: any;
    show: boolean;

    @Input()
    vpState: Observable<{
        view: "maintenance" | "management";
        sortColumn: string;
        sortOrder: order;
        filter: any;
    }>;

    private _selectedVp: any[] = [];

    selectedVpCount() {
        return this._selectedVp.length;
    }

    selectAllVps(value: boolean = null) {
        if (!this.allVPs) return;
        if (value) {
            this.allVPs
                .filter(vp => vp._display)
                .forEach(vp => this.selectVp(vp, true));
            return;
        }
        if (value == false) {
            this.allVPs
                .filter(vp => vp._display)
                .forEach(vp => this.selectVp(vp, false));
            return;
        }
    }

    areAllVpsSelected(): "all" | "some" | false {
        if (!this.allVPs) return false;
        if (
            this._selectedVp.length ===
            this.allVPs.filter(vp => vp._display).length
        ) {
            return "all";
        }
        if (!this._selectedVp.length) {
            return false;
        }
        return "some";
    }

    selectVp(vp: any, value: boolean = null) {
        const selected = this.isVpSelected(vp);
        if (vp && value) {
            if (!selected) this._selectedVp.push(vp);
            return;
        }
        if (vp && value == false) {
            const id = this._selectedVp.findIndex(v => v === vp);
            if (id > -1) this._selectedVp.splice(id, 1);
            return;
        }
        this.selectVp(vp, !selected);
    }

    isVpSelected(vp: any): boolean {
        return this._selectedVp.includes(vp);
    }

    filter: any = {};
    allTopics: any;
    allProjects: any;
    allVPs: any;
    private allUsers: any;
    languagesObj: any;
    private allMaintenanceProfiles: any;
    private languages: any;

    getAllRegions() {
        return this.regions && this.allVPs
            ? Object.keys(this.regions)
                //   .filter(key =>
                //       this.allVPs.some(
                //           vp => vp.settings.currentLocation === key
                //       )
                //   )
                  .map(k => this.regions[k])
            : null;
    }

    visibleVpsCount() {
        return this.allVPs.filter(vp => {
            return vp._display === true;
        }).length;
    }

    getMaintenanceProfile(id) {
        return (
            this.allMaintenanceProfiles.filter(profile => {
                return Number(profile.id) === Number(id);
            })[0] || {}
        );
    }

    clearFilter() {
        this.filter = {};
        this.applyFilter();
    }

    updateState(value: { [field: string]: any }) {
        this.vpState.first().subscribe(state => {
            const cState = _.cloneDeep(state);
            Object.keys(value)
                .map(key => ({
                    [key]:
                        typeof value[key] === "object"
                            ? JSON.stringify(value[key])
                            : value[key]
                }))
                .forEach(kv => Object.assign(cState, kv));
            this.router.navigate([".", cState]);
        });
    }

    applyFilter() {
        const anyFilterPresent = function(filter): boolean {
            if (
                !filter.maintenanceProfile &&
                !filter.project &&
                !filter.topic &&
                !filter.language &&
                !filter.serviceTimezone &&
                !filter.freetext
            ) {
                return false;
            } else {
                return true;
            }
        };

        this.prepFreeText();

        this.allVPs.forEach(vp => {
            vp._display = false;

            if (!anyFilterPresent(this.filter)) {
                vp._display = true;
                return;
            }

            if (
                (!this.filter.maintenanceProfile ||
                    Number(this.filter.maintenanceProfile) ===
                        Number(vp.statusId)) &&
                (!this.filter.project ||
                    Number(this.filter.project) ===
                        Number(
                            (vp.settings.projectTopicCombo || {}).project
                        )) &&
                (!this.filter.topic ||
                    Number(this.filter.topic) ===
                        Number((vp.settings.projectTopicCombo || {}).topic)) &&
                (!this.filter.language ||
                    this.filter.language === vp.settings.language) &&
                (!this.filter.serviceTimezone ||
                    this.filter.serviceTimezone == "" ||
                    (vp.settings.currentLocation &&
                        ("" + vp.settings.currentLocation).includes(
                            this.filter.serviceTimezone
                        ))) &&
                (!this.filter.freetext ||
                    this.filter.freetext == "" ||
                    vp.freetext.includes(this.filter.freetext))
            ) {
                vp._display = true;
            }
        });
    }

    constructor(
        private http: Http,
        private genericApiProvider: GenericApiProvider,
        private gravatarProvider: GravatarProvider,
        private router: Router,
        private vpApiService: VpApiService
    ) {
        this.languages = Object.keys(Languages).map(k => {
            return {
                key: k,
                value: `${Languages[k].name} (${Languages[k].nativeName})`
            };
        });

        // this.asyncInit();
    }

    prepFreeText() {
        this.allVPs = this.allVPs.map(vp => {
            vp.freetext = this.vpApiService.jsonToString(vp);
            return vp;
        });
    }

    async asyncInit() {
        await Promise.all([
            this.vpApiService.getAllUsers(),
            this.vpApiService.getAllProjects(),
            this.vpApiService.getAllTopics(),
            this.vpApiService.getAllMaintenanceProfiles(),
            this.vpApiService.getAllVPs()
        ]).then(payload => {
            this.allUsers = payload[0];
            this.allProjects = payload[1];
            this.allTopics = payload[2];
            this.allMaintenanceProfiles = payload[3];
            this.allVPs = payload[4];
        });

        /*
        this.allUsers = await this.vpApiService.getAllUsers();
        this.allProjects = await this.vpApiService.getAllProjects();
        this.allTopics = await this.vpApiService.getAllTopics();
        this.allMaintenanceProfiles = await this.vpApiService.getAllMaintenanceProfiles();

        this.allVPs = await this.vpApiService.getAllVPs();
*/

        this.prepFreeText();

        this.show = true;

        this.vpState.subscribe(state => {
            const sortState: any = { column: "name", order: "asc" };
            if (state["sortColumn"]) {
                sortState.column = state["sortColumn"];
            }
            if (state["sortOrder"]) {
                sortState.order = state["sortOrder"];
            }
            const curState = this.sortController.getState();
            this.sortController.setState(sortState);
            if (!curState ||
                `${curState["column"]}` != `${sortState["column"]}` ||
                `${curState["order"]}` != `${sortState["order"]}`
            ) {
                this.sortController.sortBy(
                    sortState["column"],
                    sortState["order"]
                );
            }
            if (state["filter"]) {
                this.filter =
                    (state.filter
                        ? typeof state.filter === "string"
                            ? JSON.parse(state.filter)
                            : state.filter
                        : null) || {};
            }
        });

        this.applyFilter();
    }

    ngOnInit() {
        this.http.get('/proxy/getSupportedRegions').subscribe(res => {
            this.regions = res.json().reduce((sum, cur) => Object.assign(sum, {[cur.region_key]: cur}, {}), {});
        });
        const caseInsensitiveComparer = (a, b) => {
            if (a && typeof a === "string") a = a.toLowerCase();
            if (b && typeof b === "string") b = b.toLowerCase();
            if (a == b) return 0;
            return a < b ? -1 : 1;
        };
        this.sortController = new ArraySortController(
            () => this.allVPs,
            {
                name: {
                    getValue: item => item.name,
                    compare: caseInsensitiveComparer
                },
                handle: {
                    getValue: item =>
                        item && item.settings
                            ? item.settings.twitterHandle
                            : null,
                    compare: caseInsensitiveComparer
                },
                maintenanceProfile: {
                    getValue: item =>
                        this.getMaintenanceProfile(item.statusId).caption,
                    compare: caseInsensitiveComparer
                },
                language: {
                    getValue: item =>
                        this.languagesObj[item.settings.language]
                            ? this.languagesObj[item.settings.language].name
                            : null,
                    compare: caseInsensitiveComparer
                },
                project: {
                    getValue: item =>
                        item.settings.projectTopicCombo
                            ? this.getProject(
                                  item.settings.projectTopicCombo.project
                              ).name
                            : null,
                    compare: caseInsensitiveComparer
                },
                topic: {
                    getValue: item =>
                        item.settings.projectTopicCombo
                            ? this.getTopic(
                                  item.settings.projectTopicCombo.topic
                              ).name
                            : null,
                    compare: caseInsensitiveComparer
                },
                region: {
                    getValue: item =>
                        this.regions[item.settings.currentLocation]
                            ? `${
                                  this.regions[item.settings.currentLocation]
                                      .provider
                              } - ${
                                  this.regions[item.settings.currentLocation]
                                      .display_name
                              }`
                            : null,
                    compare: caseInsensitiveComparer
                }
            },
            sorted => {
                this.allVPs = sorted;
                const state = this.sortController.getState();
                this.updateState({
                    sortColumn: state.column,
                    sortOrder: state.order
                });
            }
        );
        this.asyncInit();
    }

    allowedTopics() {
        return (this.allTopics || []).filter(t => {
            return t.projectId === Number(this.filter.project);
        });
    }

    getUser(userId) {
        return this.allUsers.filter(u => {
            return u.id === userId;
        })[0];
    }

    getProject(projectId) {
        return (
            this.allProjects.filter(p => {
                return p.id === Number(projectId);
            })[0] || {}
        );
    }

    getTopic(topicId) {
        return (
            this.allTopics.filter(t => {
                return t.id === Number(topicId);
            })[0] || {}
        );
    }
}
