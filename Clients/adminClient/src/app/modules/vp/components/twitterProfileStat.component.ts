import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {VpApiService} from "../services/vp.service";

@Component({
    selector: 'twitterProfileStat',
    template: `
        <ng-container *ngIf="value; else noValue">{{value}}</ng-container>
        <ng-template #noValue><i class="fa fa-spinner fa-spin"></i></ng-template>
    `
})
export class TwitterProfileStat implements OnInit {

    @Input() profile: string;
    @Input() field: string;
    @Input() fromExtras?: boolean;
    value: any;

    constructor(private vpApiService: VpApiService) {
    }

    async asyncInit(){
        if(this.profile && this.profile!='') {
            const hydratedProfile = await this.vpApiService.profile(this.profile);
            if(!this.fromExtras){
                this.value = hydratedProfile.profile[this.field]||'-';
            }else{
                const extras = hydratedProfile.profile['extras']||[];
                this.value = (extras.filter((extra)=>{
                    return extra.key == this.field
                })[0]||{}).value||'-';
            }
        } else {
            this.value = '-';
        }
    }

    ngOnInit() {
        this.asyncInit();
    }
}


