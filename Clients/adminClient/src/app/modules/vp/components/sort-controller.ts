export type order = "asc" | "desc" | null;

export interface SortController {
    sortBy(key: string, order?: order): void;
    isSortedBy(key: string): order;
    setState(value: any): void;
    getState(): any;
}

export class ArraySortController implements SortController {
    private state: {
        column?: string;
        order?: order;
    };

    constructor(
        private array: () => any[],
        private columns: {
            [key: string]: {
                compare?: (x, y) => number;
                getValue(item: any): any;
            };
        },
        private onSorted: (array: any[]) => void
    ) {}
    getState(): {
        column?: string;
        order?: order;
    } {
        return this.state;
    }
    setState(value: { column?: string; order?: order }): void {
        this.state = value;
    }
    sortBy(key: string, order?: order): void {
        if (!this.columns[key]) return;
        if (!this.state) {
            this.state = {};
        }
        if (order) {
            this.state.column = key;
            this.state.order = order;
            const compare =
                this.columns[key].compare ||
                ((x, y) => (x == y ? 0 : x > y ? -1 : 1));
            this.onSorted(
                this.array().sort((a, b) =>
                    compare(
                        this.columns[key].getValue(a),
                        this.columns[key].getValue(b)
                    )
                )
            );
            return;
        }
        this.state.column = key;
        this.state.order =
            this.state.order === "asc"
                ? "desc"
                : this.state.order === "desc"
                    ? "asc"
                    : "asc";
        const compare =
            this.columns[key].compare ||
            ((x, y) => (x == y ? 0 : x > y ? -1 : 1));
        this.onSorted(
            this.array().sort((a, b) => {
                if (this.state.order === "asc") {
                    return compare(
                        this.columns[key].getValue(a),
                        this.columns[key].getValue(b)
                    );
                } else {
                    return -compare(
                        this.columns[key].getValue(a),
                        this.columns[key].getValue(b)
                    );
                }
            })
        );
    }
    isSortedBy(key: string): order {
        return this.state && this.state.order && this.state.column === key
            ? this.state.order
            : null;
    }
}
