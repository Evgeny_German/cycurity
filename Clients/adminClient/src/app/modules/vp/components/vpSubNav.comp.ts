import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {RouterModule, Routes} from "@angular/router";
import {AppCacheProvider} from "../../shared/providers/app-cache-provider";
import {isNumber} from "util";

@Component({
    selector:'vpSubNav',
    template: `
        <h1 class="pull-left">Controller</h1>
        <ul class="nav nav-pills justify-content-end" style="position:relative;top:4px;">
            <li class="nav-item">
                <a class="nav-link" [routerLink]="['/vp']" [ngClass]="{'active': checkActive('vp')}">VPs</a>
            </li>
            <li class="nav-item" *ngIf="hasVpRole">
                <a class="nav-link" [routerLink]="['/vp/new']" [ngClass]="{'active': checkActive('vp/new')}">New VP</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" [routerLink]="['/vp/settings']" [ngClass]="{'active': checkActive('vp/settings')}">Settings</a>
            </li>
        </ul>
        <div class="clearfix"></div>
    `
})
export class VpSubNav implements OnInit {
    private hasVpRole: boolean;

    checkActive(testedPathSegment: string) {
        return (this.router.isActive(testedPathSegment,true)) ;
    }

    constructor(private router:Router, private appCacheProvider:AppCacheProvider) {
        this.hasVpRole = (this.appCacheProvider.getIdentity().adminRole=="superadmin" || isNumber(this.appCacheProvider.getIdentity().meta.vpRole));
    }

    ngOnInit() {

    }
}


