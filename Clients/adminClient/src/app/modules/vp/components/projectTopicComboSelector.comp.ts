import {Component, OnInit, Input, OnDestroy, ElementRef, ApplicationRef, ViewChild} from '@angular/core';
import {GenericApiProvider} from "../../shared/providers/genericApiProvider";

import {
    NG_VALUE_ACCESSOR, NgModel,
} from '@angular/forms';

@Component({
    selector: 'projectTopicComboSelector',
    styles: [`
        [disabled]{
            background-color:#f5f5f5;
        }
    `],
    template: `
        <div class="row">
            <div class="col-6">
                <select tabIndex="1" [attr.disabled]="disabled ? '' : null" class="form-control" placeholder="Select Project" [(ngModel)]="selectedProject" (change)="update(true)">
                    <option [ngValue]="null">Select a Project</option>
                    <option *ngFor="let p of allProjects" [value]="p.id">{{p.name}}</option>
                </select>
            </div>
            <div class="col-6">
                <select tabIndex="2" [attr.disabled]="disabled ? '' : null" class="form-control" placeholder="Select Topic" [(ngModel)]="selectedTopic" (change)="update()">
                    <option [ngValue]="null">Select a Topic</option>
                    <option *ngFor="let t of allowedTopics()" [value]="t.id">{{t.name}}</option>
                </select>
            </div>
        </div>
    `,
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: ProjectTopicComboSelector, multi: true}
    ]
})

export class ProjectTopicComboSelector implements OnInit, OnDestroy {
    selectedTopic: any;
    selectedProject: any;
    select: any;
    selector: any;

    @Input() disabled?:boolean;

    @ViewChild(NgModel) model: NgModel;
    private _innerValue: any;

    private changed = new Array<(value) => void>();
    private touched = new Array<() => void>();
    private allProjects: any;
    private allTopics: any;

    constructor(private el: ElementRef, private applicationRef: ApplicationRef, private genericApiProvider:GenericApiProvider) {
        console.warn('construct')
    }

    update(resetTopic?){

        if(resetTopic) this.selectedTopic = null;
        console.warn('update')
        this.innerValue = {
            project: this.selectedProject,
            topic: this.selectedTopic
        }
    }

    allowedTopics(){
        return (this.allTopics||[]).filter((t)=>{
            return t.projectId==this.selectedProject;
        });
    }

    get innerValue() {
        console.warn('getInnerValue')
        return this._innerValue;
    }

    set innerValue(value) {
        console.warn('setInnerValue', value)
        if (this._innerValue !== value) {
            this._innerValue = value;
            this.changed.forEach(f => f(value));
        }
    }

    writeValue(value) {
        console.warn('writeValue', value)
        this._innerValue = value||{};
        this.selectedProject = this._innerValue.project;
        this.selectedTopic = this._innerValue.topic;
    }

    registerOnChange(fn: (value) => void) {
        this.changed.push(fn);
    }

    registerOnTouched(fn: () => void) {
        this.touched.push(fn);
    }

    ngAfterViewInit() {
    }

    ngOnInit() {
        console.warn('ngOnInit')
        this.genericApiProvider.type('projects').list().subscribe((projects)=>{
            this.allProjects = projects;
            console.warn(projects);
        });
        this.genericApiProvider.type('subprojects').list().subscribe((subprojects)=>{
            this.allTopics = subprojects;
            console.warn(subprojects);
        });
    }

    ngOnDestroy(): void {
    }

}
