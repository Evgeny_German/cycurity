import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'inputTimeblock',
    template: `
        <span style="display:inline-block">
            <small *ngIf="inputVal && inputVal.length">{{placeholder}}</small>
            <input
                [(ngModel)]="inputVal"
                (blur)="updateValue()"
                (keyup.enter)="updateValue()"
                style="width:56px;font-size:11px;display:inline-block"
                class="form-control-sm"
                placeholder="{{placeholder}}"
                type="text"
            />
            <select [(ngModel)]="inputTimeUnit" (blur)="updateValue()" (change)="updateValue()" class="form-control-sm" style="font-size:11px;">
                <option [value]="'d'">d</option>
                <option [value]="'w'">w</option>
                <option [value]="'m'">m</option>
                <option [value]="'y'">y</option>
            </select>
            <div *ngIf="this.errorMessage" class="alert alert-danger" style="position:absolute;top:2.4em;right:1em;z-index:3;">
                {{errorMessage}}
            </div>
        </span>
    `
})
export class InputTimeBlock implements OnInit {

    public inputVal: string;
    public inputTimeUnit: string;

    @Input() value: string;
    @Input() style: string;
    @Input() placeholder: string;

    @Output() public onChange = new EventEmitter();

    private errorMessage: string;

    private letterValues = {
        // Seems like you're always stuck in second gear
        // And that it hasn't been your
        'd': 86400,
        // or
        'w': 604800,
        // or
        'm': 2592000,
        // or even your
        'y': 31536000
    }


    constructor() {
    }

    updateValue() {

        this.errorMessage = null;

        let localInputVal = this.inputVal + this.inputTimeUnit;

        if (localInputVal ==='') localInputVal = '0d';

        // validate
        const validator = /^\d+[d,w,m,y]$/;
        const valid = validator.test(localInputVal)
        if (!valid) {
            this.errorMessage = 'Please use the format [<unit><unitName>] ie: 10d, 3w, 2m, 1y';
            return;
        }

        const lastChar = localInputVal.slice(-1);
        const numericValue = Number(localInputVal.substring(0, localInputVal.length - 1));
        if (!this.letterValues[lastChar]) {
            this.errorMessage = 'Unit must be one of [d, w, m, y]';
            return;
        }

        // translate to ms & emit change
        if(!this.errorMessage){
            this.onChange.emit(numericValue * this.letterValues[lastChar]);
        }
    }

    ngOnInit() {
        // translate value to string
        var returnValue;

        ['y', 'm', 'w', 'd'].forEach((unit) => {
            if (!returnValue && Number(this.value) % this.letterValues[unit] === 0) {
                returnValue = {
                    numeric: Number(this.value) / this.letterValues[unit],
                    unit: unit
                }
            }
        });

        if(returnValue.numeric===0 && returnValue.unit==='y'){
            console.warn(1)
            this.inputVal = '';
            this.inputTimeUnit = '';
        }else{
            console.warn(2)
            this.inputVal = returnValue.numeric;
            this.inputTimeUnit = returnValue.unit;
        }
        console.warn(this.inputVal, this.inputTimeUnit)
    }
}


