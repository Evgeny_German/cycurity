import { Component, Input, EventEmitter, Output, OnInit } from "@angular/core";
import { Http } from "@angular/http";

@Component({
    selector: "vp-edit-popup",
    styles: [
        `
            td {
                white-space: nowrap;
            }
        `
    ],
    template: `
        <div
            style="position: fixed; top:0px; left: 0px; right: 0px; bottom: 0px;background-color: rgba(0,0,0,.5); z-index: 9999;"
            (click)="onDone(false)">
            <div
                style="position: absolute; top: 50%; left: 50%; margin-top: -200px; margin-left: -200px; width: 400px; height: 314px; background-color: white;"
                (click)="$event.stopPropagation()">
                <div style="padding:2em;">

                    <table>
                        <tr>
                            <td><input class="checkboxCell" type="checkbox" [(ngModel)]="changeProject" (ngModelChange)="handleProjectChange($event)"/></td>
                            <td>Project</td>
                            <td><select class="form-control" [(ngModel)]="selectedProjectId">
                                <option [value]="null"></option>
                                <option *ngFor="let project of allProjects" [value]="project.id">{{project.name}}</option>
                            </select></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" [(ngModel)]="changeTopic" (ngModelChange)="handleTopicChange($event)"/></td>
                            <td>Topic</td>
                            <td><select class="form-control" [(ngModel)]="selectedTopicId">
                                <option [value]="null"></option>
                                <option *ngFor="let topic of getAllowedTopics()" [value]="topic.id">{{topic.name}}
                                </option>
                            </select></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" [(ngModel)]="changeRegion"/></td>
                            <td>Service and Zone</td>
                            <td><select class="form-control" [(ngModel)]="selectedRegionId">
                                <option [value]="null"></option>
                                <option *ngFor="let region of regions" [value]="region.region_key">{{region.provider}} -
                                    {{region.display_name}}
                                </option>
                            </select></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" [(ngModel)]="changeMaintenanceProfile"/></td>
                            <td>Maintenance Profile</td>
                            <td><select class="form-control" [(ngModel)]="selectedMaintenanceProfileId">
                                <option [value]="null"></option>
                                <option *ngFor="let maintenanceProfile of allMaintenanceProfiles"[value]="maintenanceProfile.id">{{maintenanceProfile.caption}}</option>
                            </select></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" [(ngModel)]="changeLanguage"/></td>
                            <td>Language</td>
                            <td><select class="form-control" [(ngModel)]="selectedLanguageId">
                                <option [value]="null"></option>
                                <option *ngFor="let language of languages" [value]="language.key">{{language.value}}</option>
                            </select></td>
                        </tr>
                    </table>
                    <hr/>
                    <a (click)="onDone(true)" class="btn btn-primary pull-right"
                       [ngClass]="{disabled:!changeProject && !changeTopic && !changeRegion && !changeMaintenanceProfile && !changeLanguage}">Save</a>
                    <a (click)="onDone(false)" class="btn btn-default pull-right">Cancel</a>

                </div>


            </div>
        </div>
    `
})
export class VpEditPopup implements OnInit {

constructor(private http: Http) { }

    ngOnInit(): void {
        this.http.get('/proxy/getSupportedRegions').subscribe(res => {
            this.regions = res.json();
        });
    }

    @Input() allProjects: any[];

    @Input() allTopics: any[];

    @Input() allMaintenanceProfiles: any[];

    regions: any[];

    @Input() languages: any[];

    changeProject: boolean;
    selectedProjectId: any;

    changeTopic: boolean;
    selectedTopicId: any;

    changeMaintenanceProfile: boolean;
    selectedMaintenanceProfileId: any;

    changeRegion: boolean;
    selectedRegionId: any;

    changeLanguage: boolean;
    selectedLanguageId: any;

    handleProjectChange(changeProject: boolean) {
        if (!changeProject) {
            this.changeTopic = false;
        }
    }

    handleTopicChange(changeTopic: boolean) {
        if (changeTopic) {
            this.changeProject = true;
        }
    }

    getAllowedTopics(): any[] {
        if (!this.allTopics || !this.selectedProjectId) {
            if (this.selectedTopicId) this.selectedTopicId = null;
            return null;
        }
        return this.allTopics.filter(
            topic => topic.projectId == this.selectedProjectId
        );
    }

    onDone(ok: boolean): void {
        if (!ok) {
            this.done.emit(null);
        } else {
            const res: any = {};
            if (this.changeProject) {
                res.projectId = this.selectedProjectId;
            }
            if (this.changeTopic) {
                res.topicId = this.selectedTopicId;
            }
            if (this.changeMaintenanceProfile) {
                res.maintenanceProfileId = this.selectedMaintenanceProfileId;
            }
            if (this.changeRegion) {
                res.regionId = this.selectedRegionId;
            }
            if (this.changeLanguage) {
                res.languageId = this.selectedLanguageId;
            }
            this.done.emit(res);
        }
    }

    @Output()
    done = new EventEmitter<{
        projectId?: number;
        topicId?: number;
        maintenanceProfileId?: number;
        regionId?: number;
        languageId?: number;
    }>();
}
