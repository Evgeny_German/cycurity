import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { GenericApiProvider } from "../../shared/providers/genericApiProvider";
import * as _ from "lodash";
import { AppCacheProvider } from "../../shared/providers/app-cache-provider";

@Injectable()
export class VpApiService {
    private profiles: {};

    constructor(
        private http: Http,
        private genericApiProvider: GenericApiProvider,
        private appCacheProvider: AppCacheProvider
    ) {
        this.profiles = {};
    }

    async getAllActions() {
        return new Promise((resolve, reject) => {
            this.genericApiProvider
                .type("voAction")
                .list()
                .subscribe(actions => {
                    resolve(actions);
                });
        });
    }

    async getLaunches(id) {
        console.warn("getLaunches");
        return new Promise((resolve, reject) => {
            this.genericApiProvider
                .type("vos")
                .getRelation(id, "launch")
                .subscribe(launches => {
                    resolve(launches.reverse());
                });
        });
    }

    showDaysDelayed(days) {
        let displayValue = Math.floor(days || 0);
        return displayValue < 365 ? displayValue + " days" : "> 1 Year";
    }

    async getAllVPs() {
        return new Promise((resolve, reject) => {
            this.genericApiProvider
                .type("vos")
                .list()
                .subscribe(allVPs => {
                    allVPs = allVPs.map(vp => {
                        vp.daysDelayed = vp.daysDelayed ? vp.daysDelayed : 0;
                        return vp;
                    });
                    allVPs = _.sortBy(allVPs, "daysDelayed").reverse();
                    resolve(allVPs);
                });
        });
    }

    async updateVps(vps: any[], value: any) {
        return new Promise((resolve, reject) => {
            this.http
                .post(`/vo/vos/updateVos`, { vos: vps.map(vp => vp.id), value })
                .subscribe(
                    res => {
                        resolve(res);
                    },
                    err => reject(err)
                );
        });
    }

    async getAllUsers() {
        return new Promise((resolve, reject) => {
            this.genericApiProvider
                .type("users")
                .list()
                .subscribe(users => {
                    resolve(users);
                });
        });
    }

    async getAllProjects() {
        return new Promise((resolve, reject) => {
            this.genericApiProvider
                .type("projects")
                .list()
                .subscribe(projects => {
                    resolve(projects);
                });
        });
    }

    async getAllTopics() {
        return new Promise((resolve, reject) => {
            this.genericApiProvider
                .type("subprojects")
                .list()
                .subscribe(subprojects => {
                    resolve(subprojects);
                });
        });
    }

    async getAllMaintenanceProfiles() {
        return new Promise((resolve, reject) => {
            this.genericApiProvider
                .type("voStatus")
                .list()
                .subscribe(maintenanceProfiles => {
                    resolve(maintenanceProfiles);
                });
        });
    }

    async profile(profileId) {
        return new Promise<any>((resolve, reject) => {
            if (this.profiles[profileId]) {
                resolve(this.profiles[profileId]);
            } else {
                return this.http
                    .get(
                        `/scrape/profile?userName=${profileId}&ownerId=${
                            this.appCacheProvider.getIdentity().id
                        }`
                    )
                    .subscribe(result => {
                        resolve(result.json());
                    }, reject);
            }
        });
    }

    jsonToString(j) {
        // TODO: Extract only values from the JSON document
        return JSON.stringify(j).toLowerCase();
    }
}
