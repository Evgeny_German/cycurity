import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {AuthenticationCallbackActivateGuard} from "../shared/auth/auth-guard-provider";
import {SharedModule} from "../shared/shared.module";

import {VpMain} from "./vp.comp";
import {VpForm} from "./vpForm.comp";
import {VpSettings} from "./vpSettings.comp";
import {VpSubNav} from './components/vpSubNav.comp';
import {ElectronLauncher} from './components/electronLauncher.comp';
import {InputTimeBlock} from './components/inputTimeblock.comp';
import {TwitterProfileStat} from "./components/twitterProfileStat.component";
import {ProjectTopicComboSelector} from './components/projectTopicComboSelector.comp';
import {VpApiService} from "./services/vp.service";

import {VpMaintenanceView} from './components/vpMaintenanceView.component';
import {VpManagementView} from './components/vpManagementView.component';
import { SaveChangesGuard } from '../shared/save-guard';
import { VpEditPopup } from './components/vpEditPopup.comp';



export const VP_MAIN_URL = 'vp';

export class VpSaveGuard extends SaveChangesGuard<VpForm> {
}

const vpRoutes: Routes = [
    {
        path: VP_MAIN_URL, component: VpMain, canActivate: [AuthenticationCallbackActivateGuard]
    },
    {
        path: VP_MAIN_URL+'/new', component: VpForm, canActivate: [AuthenticationCallbackActivateGuard], canDeactivate: [VpSaveGuard]
    },
    {
        path: VP_MAIN_URL+'/edit/:vpId', component: VpForm, canActivate: [AuthenticationCallbackActivateGuard], canDeactivate: [VpSaveGuard]
    },
    {
        path: VP_MAIN_URL+'/settings', component: VpSettings, canActivate: [AuthenticationCallbackActivateGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(vpRoutes), SharedModule],
    exports: [VpMain, VpForm, VpSettings, VpSubNav, InputTimeBlock, TwitterProfileStat, VpMaintenanceView, VpManagementView, ElectronLauncher, ProjectTopicComboSelector],
    declarations: [VpMain, VpForm, VpSettings, VpSubNav, InputTimeBlock, TwitterProfileStat, VpMaintenanceView, VpManagementView, ElectronLauncher, ProjectTopicComboSelector, VpEditPopup],
    providers: [VpApiService, VpSaveGuard],
})

export class VpModule {
}
