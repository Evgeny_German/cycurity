/**
 * Created by odedl on 16-Jul-17.
 */
import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {CONVERSATION_MAIN_URL} from "../conversation/conversation.module";
import {LoginActions} from "./store/loginActions";
import {IAppState, ILoggedUserState} from "../shared/store/store";
import {NgRedux} from "@angular-redux/store";

@Component({
  selector: 'login-main',
  styles:[`
      div.formContainer{
          background-color:#fff;
          position:fixed;
          top:0;
          right:0;
          bottom:0;
          left:0;
      }
    form{
      top:50%;
      left:50%;
      position:fixed;
      width:200px;
      margin-left:-100px;
      margin-top:-130px;
    }
    img{
      width:100%;
    }
    a{
      display:block;
    }
  `],
    template: `        
<div class="formContainer">
    <form class="form">
        <div class="form-group">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYUAAACCCAMAAACejyR2AAAAwFBMVEX///8eM2SbkG7O0doAHlr29fIAIlsbMWMULGAXLmHz9PccNGP8/P0AIFqUiGIAG1cAJFooPm83R3BeaYinrr1kb4/a3uXk5+2vtMO1u8qjmXqIkKdKV3vl4trw7ur6+ffRzL7Z1cm+xNGPgllUYYPGwK6tpInq6OK3r5ilm322rpfKxbXp6+/d2c+Xi2iepbjT198AAFEAFFVxfJl9h6BEUXclO2yTmq40SHV1fpi/uKU4SnZpdJEAC1MAElZNW33w4A5DAAATUklEQVR4nO1daZuquBJWRAg2Itru7b7gvoytttrO7f//r25IVSAgKnrOjM8zJ+8XFUKo1JtUqioBEwkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQmJPxyVdbW0O+yTyeT+sCsV15VXC/Tnwawev4lGiK1pWU2zCf3+faxlXi3WH4Xyx1617WyONLZfq+Lqa9sguaxtq/sPOSD+LZS3tk1y38fqXDiYqR53OWKTY/llcv1R+MrbRG/05hcn5r0f3bb3K/MFQv1hKA9UsjmnrpxNlVSiDtb/qkR/IIpJojV6Nwr0fuicXfzX5PkjsVIN7UPwhNrpVnOxaLbabe9Q5oMY9tcLZPtj8LEhyZr3a9QZKxaiPuym+fHanqjbl8j3R2BrkwJ3gdoLR1kuFQ/0+3SE5yoFon28Ssj/Oooq2XPPqOkIDHhEjJGHyo6oq5fJ+Z9GTSUFJCE9ti44AB5mUICOBrv6OlH/uyjnSRLNUVO5HAicBwemh3LSII/Hb+XUdlc4HAq7z57o7Zophov6ynBcjE/Mde9zVygcCo2PUHkoG4pysOZMoD4f69AtoQDkBzLhwilBysxtiZ/P9AxIFnt3N9j9l+L0oCwnLVaoapPSg+Fb9WiotqFTGCRrn6ve5am/VIpcPqTCTDLnHv/Ld5zNr3dbJW4VhmGr+6MQ1vT+55b9O+S89VjNf6PHkWf1Ccjuz4ERfdjQg5t39v3jbzUKm51fby4fSuhU8mGJH0Q1Z3/Ct46v9KWlTE+n09hZWt7BpQI0bLXcQ2FD75AletIHyea5Es2k4R6xS4ELzLPtHjW8ucpc6RoRatDFALLGCtshp6Gmukc1VHVevD9WkdsJVBZcOcg76x6fJFyYwdi5Z+ffIPE5eLt3do1RuMw7xMP8QHZw7cLyu/1whHFCe3HyrBSOhvmOFOLn9uYl27hokL3FoZvSXf3odmDGL9pMaTbvWOtv+0KLhPB8yhUWsrdZcKUw/M5U0GOykOhp7JcaGEpFxnlSu5Z5uIsvTYVh2/L7fCctlmidPBocdqCqarGDt3LBjmqS3UAatqxRui3QWtGBBByhidpblFp07Qg0PM1CUs95IVJ8FqhXz64VbVJ5r0fJEB90KAxY7W3uoVrjdLjQaILnlkP2m7qrMaehXtJrkkFccHVoOLeYOzZSUAiGAYzuHd5ipfFrdFYDH1n6BrrCAyyACLQKrFFPcrsWYEHDYgHBCSmAiJkDCUlsNuDI4elsZ1HLwsCfcRJmEaXaU04Dixt6xI4XNKz3qDPdNgal4/E82INx0bO8G65hhPupkVUWDuDornISdC0/OB+PpYYBVZADqDA+C/sBxzdWSgZYXGSh94Ol8nDjBvz64VmDHo5VTwVfMDqST9ujxIHk2Wc72NvD4DQsp+4vc0cacQaD+Y39iejbNbsgU/7KuzITn8UPO9At18HRjT8pjftVGapYb90Bpus4bcRmwWhUMohKFbsHQc2JLCSwkFkizPSs8bcvMVhR7rCvgdLs8ym28gZN/BC1fBKVKIyw9gTnhqb7a6Xl4hAPRp92/JJgRM0Pm2yEoZSB8Wwc4CwQR3YJ8Sc14V8C7ZVjjngCPMCCWEUBfB3s4AEWOJCFi9iAW9GfjCC/aFMfxZe2ZzdJ10HHDk+hZno/BYrzmtc9EifoMokzE/VwYg7P5alGwNOtwGQBCSoc3Xts+srGGaEWrKL443nmz7GQSDG3xviGXw+xkEiJEuNs/UQoy5F5t4HQLnipVhNP9HYqC5FI7sy7MR8trrdqluwY1MM0G+E6hKzZl+ZZh1QuKY7ujI72+yJA8e/+JAtmAwYDVPQYC2iT2NyVgkZqv5Bdq+wxReqInijtghvPx7e56UxjsrvDxPjrPvcoH/m5K8YObFI+kykYgUlzBa29mcd9kgUcdZunWMigH3fIZPYYd/7CanBKVdnIboFBWi7gcFGMkgwDxWinGZjNysznd2+7ZZLGcR1wCibbIwnYI97Y71uuwLMsMK9AV59iIbEGZ9feoj3a/8oWFTrLssubYPQnECnMNYEEV7inclQmuIfk837RxApiTwNGT5aP7jKatJuj/VmLxKZYPfkcC54VhW6j3Ukz92q3uiLtD0wyyCCBG8rnGw96NnaOai3cbb1hV8dypvgUAkr3cjRFbOrNXvAkC72caPseZiHxEylxJOZnYhvb6434xEwC5CiWHXYwcwjF++T95k08mJ9vtlHid1uhVx1rIM39fJ++964oYRR989LnWFijp4oFHmcBsxZgMW/bo6329vamfVTK5UoU5gOcVSAmQw8J/RRhMBTMTAyYqyy9m33Ee0M0RkrXZAti5U1FwiISujG3neJnWDBreQMYxyTo4yxwK+rGMrdtRaWgU728kdw1GOST3RhcJAuWNatZrv43RPJqBSJUwgrjfIeTc9xNG5jNFpJ4FBAdabcbGZsF/e0bcdCywLmXon+CBcxm+5HfNVQKTJGGLULzYetRLKA1FlmwY8FghTfY48DfiZlwojYJRrjREFL0yMLtmeWBbJ5uAPiwU7dc7c+wgDbJKNzxFs0fVzFk0KsJqPqo7ezzpUXq8aHmYc8vKN7EuzsYeDTKWYidXYEwOeBtFH7rWLiA5jv5z7CAEYdWu3aeI2XYhn24Hl+d7QHruWOYnbvs4Dy8InBnevRQoZOzrfOeC54WOd68RECVlQ/4YzgWbg+nJ1nQA0mYp1ioMqORve8ElrfnjxurcFs7z1iAtPZyzA5CLlHsM3GtSuXjvPVkxoxQPm5QWbxkgefJbl73iEVigK/Jgai9f5aFO/iyVcbRAnJEEzgaMklPBoYgY3IT9+IIFmDVS9dvXhfNQi+ChbcDxTekHIwgtS9loadCUIUZDAWTeSVNJGFz1/BFYq4+tgwYwQJ2B/umANEsgIsR9FQh7KmgwxTYf/5SFio6WhvM5oFJclOtPgk8lZYeOwxs8ij+vN/L5mHSUo+7UyCChQrml95vWbUeC/hIKHwFP5lP7MgCeG8YyAQueCkLmR8bOgiud1q45Dwv5XB1xVa5kwPJ7yU4Ukd1dzcmhonhrjvNEcGCeQYp1Ftpmgpm/ILTH+sCOl82DbCA6xkbcTC8lAU6PR9YV8UNGDyTRHvYmYZhNKzzZts0rrWxuaOyj6HbCvjld1NdiAgWMNlzJy+bhCWXgNlKwTjkGgyw4EX1gs5fy0IvhwkD3O2CqSQKc15JpeZ+BxtjAWaQapgRv40tXygLlc18RRmpKBZ4zswI74UrCwugmG0SgycTd2nxpaggC+iK28KtXsuCmSMN9mXEt4Q1owvO8PyELS8MSD5Gkq7iOYiB0G3dyB0iaIhkgS+aknygq/eMnO/j4D4tcYPfFyRhPBc7yAIPZYTlp9ey4G4Kg1r41i8rkoYhkmCxdaCUHs/zqeWQBu27hxowU58bkiT7yyfkIlngGwiSujrgm4czvQatF+NN9/cAFga1H6y0XAIS/OxsiAV0k4S84YtZKO8xCcqdVUXpXBRKc4rQiToTLd5a95H7WgbZfRZ7veq2oePq5kUsGc1Cxlt5IHrjWO31ip87WFqxvRx6je8u2peqqVT1nMdL/GgzxALODMbh1zIYv4+FRMlW4TbeNtWl0woWWUy8LZLMh1qrcfPVmYEXeRhEU9Wst2fVvvA+o1lIVGxhd182m9W8nXU5z87xfKzu3kPjSxXk4FUSZqECeXTf93o1CxVCGnDzjkeDNW16DxW2FnX/wRKWdKVT31vcbR+Zz/BiBSp0czG7X2EhUdlF7nQVrVqmEVGECJNPmAU+GDz39tUs0JmBb5TzdgUry+VkPOwuFp3TtC5sp4ds38p+4Nk298HQCA3lL8PhaywkKufgOjh0e7shTC2VwQUNfAMlwwULfA8UH04vZyGTNLiVPwmPUy2XS8sKPEdiAQllqsNHtunX9tnwCmruJ6JlV1lImF9aeOs8yX0GnLT5ey5Atq4GViEvWMAZ61fW2n4vCzTC8R5r6wg0hLCsg/OUORD1wfuuDjl/m7RBNoPIMK6YdRdgolcP519E8zdrG/bm80KE6rdKeL6UZPPBbWR7Qqv2vaqEG3e6hwwVR/WBFQizYNODZH+VhWLuusSPg3a1M97fn4jDJOBjbZmzTR7epj9fD3ZvsLy3b7yXo0ON9a5BcS3rNK/97AgsEB4a20pEYilT+ynAQmKhUQzd4uxWvQtELR/sdruVWCDUsC9WpHQ1Mkqx843f9JYWs6RpPLeVHloRPFie+/qpPbcNzVzXqsVitVa+frHp4kYVmVStSKvoXc8OznvsHhEFIuo2A8cib35PosS984/hx9Y8o9ka1gNPFbpTdQfPZd61OBtUJZ7CfGDbA29kpbvTyZJDccZN/nBP+d2O9+CCxFOg4RURE5ftxaLboVgshIRGKm9nB5KEfxKfNvX+bi3IzD9zRH14YpZ4DNU9sQ+ra109syrYZC/fvfCPozJQiXb4iorI5l/fWZIb/CafTOImaoMcoaa/ug64Qevqe9YmauO5TQASDyNTa+g2ye4H79uau8G4XNu+D5JZYus7+UrVfxOprZ6jAaqdZTuAs+6+1hzZ/p5ciUR8mOvVsbEnGkvm7xvH1VrGaa9Bhlqj8tp97EEaIgkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJiX8dJu6qw4dz4M2D8BJCOCN8Nf3Xa6e9Z0ja/sHWyHvER6ygHSpn+uXa/tsOo5H2FvECAgYr5N/aUBp/tlujdLCNifQo9AySgHaUIvj7GEOywEl+RUBZIdFcnZiJi6rC6CpwXGEPBaadjvtxqtcnk3rdPdOinxRs/91C8eqoe+/a7tbxy8hRlooDG/Wa7KI6E3YGT7rN+DtAF6wcvGhpzIpFvzKawcF3ViZMeKNAwvEKD71vM9wt3lKYVFP24t0WvQ3WvKiDajt178ZRikhcKoJe4oI9451Wup4s7D4zeClIq+6qCP55ojNlTe7wtjanvk6GrKpx5M07fWRh6ZZNg3ab3Y5y6nZdEltL+qULfwXW7XtdyfJVYGErrHpn0Z30od39jnsVFOgPQQjoE93+pEvLwfO4Tn1Bi11TDBXEe3+r6cBLRBXejHRd4cPiVMfeMXQfP531WcHJpNlE5rvwcq2mdRotnCvPCVNFwCf8kwcSmhgqbkPYNel+B4u26+wt16cl+9XqD/22Wky8GTK66NdpWx3ovGNlwauKuDmywNhM8z6e5hvhW8uuV7bb98aCz0LHwgtYD8SxuPD5oiKxB9WHMGZG1piVAw06TuImHP52MpcF9sycx8LM36s/5CwknKXZxFfisw8kHiqZubRd3abgs+AqrVWHymcTzwKKLLDWn0DXrb6v2Rm8Cx5ZoDpxBUODOK4nroKzsBw6k7TPQstjwYrHQifwFHSAhcnQlRNZGFqiZbzDQqvfdVDrpjOdusrmLJj103DCh4DHwkgZT/DlQfWJJwKysLBumD6PBevk0P70EAsLr5JZfebqC1no9MVRHouFWVtxolhQnPF0CkzcYuE0EbW7sKb0qhFviGspkAUnIIuj0GInoX+2FuLp04RWD8o0nVN7QtXDWeguWyM+TH0W3BeiYMtH9f6pyQ/CsWG/PhM9gXRX+OWxMGwvpz4LrpldMAGvsmC5GgK5Z0q6Yy04C1gAMWZtjfZE/LHgdqQIFpbO6TSGe9xnYerAk9ALa3w6nZAFOkk79dYVFmgxsYdOxSHFxiH+04BJB0VLmXosTOjhKXZ7gYWE/8cE7Vkd/yqCs5Bojpd1oXsOhV4ssJBoKqc0Z2FZVxSo/joLU6ohEJuyQLU9usICbet9FqgdOU0uWXjEIp0cHPx+ScZCezIZgspOlijHhUVqip11aHW6Cwd04LKQWCxnMDG6M223e4K3PVxhwX1fE5hsjwV3up/650dDYfwKLNCGDr2xMGq1YDRezs5R80KaSlo/QVebWQ9bJFetHYu/8eVhFlrwFpImZ0GYF1yHtaUsgYWRNXXHN87iN+eFNHPtJvCfiIwFqk/U8lSh7uFEAZVGssDcbFAcZ8EVMGg5A4oQWjbjipj5xX0WEhNXoSbOQGEWqNwWKDy9BI8FdBGbBdpTL8fCsEnBemi3v+BfrTH9xtrW4Z7qst5pLsacha57FUjGwoYRskDLO91mZwJ/puFMRrTYlVBqRr026ktBlwMWqHqYlpu0b9BTC5j+hvUFv5nPgjIetU4gDbLQVbqtphLtsIdYSIzh7wtEgtN0nkFZF27znSXcstWf+W1lTmlLqQs66ToT9FRHXGcX6HqzM7T1cnZeun+pgCObAmyp+8C5xTpyZ4k1Nev0kAKGr8n+h6EPURsEb50JTsJdtxw+Gu241fTF/5gRgC82GTEvjc7O7NeUHcR+ZYJOh+xmwKXHQsf9EzloDbLQmlqKNbkWPQdZaKMixLFgUWH7IFNXoU1FI9Gy/Nt3IDRY1L3Hkt22gt08MUVeGfw8+scPL7fAz7eFuNv/2vYzD0K0vvCYFqN1rGoklAtmMKIF8yrGfAG/wj/kGTbxZr7DNfIsBW9M+9qwE24XrDkhTFNtUdamb4ZittW8l62RkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJB4BP8HOPECUFMaHI0AAAAASUVORK5CYII="/>
            <input (keyup.enter)="login()" class="form-control" type="text" placeholder="Username" name="uname" [(ngModel)]="username" required>
            <input (keyup.enter)="login()" class="form-control" type="password" placeholder="Password" name="psw" required [(ngModel)]="password">
            <a href="javascript:;" class="btn btn-primary" (click)="login()" >Login</a>
            <div *ngIf="errorMessage" class="alert alert-danger" style="margin-top:10px;">
                {{errorMessage}}
            </div>
        </div>
    </form>    
</div>        

  `
})
export class LoginMain implements OnInit {
    errorMessage: string;
    username: string;
    password: string;

    constructor(private redux: NgRedux<IAppState>,
                private loginActions: LoginActions,
                private router: Router) {

        this.initStateFromRedux(this.redux.getState().loggedUserState);
        this.redux.subscribe(()=>{
            this.initStateFromRedux(this.redux.getState().loggedUserState);
        })
    }

    initStateFromRedux=(loggedState:ILoggedUserState)=>{
        this.errorMessage = loggedState.loginError

    }

    ngOnInit() {
    }

    login = () => {
        this.loginActions.loginUser(this.username, this.password)
    }
}
