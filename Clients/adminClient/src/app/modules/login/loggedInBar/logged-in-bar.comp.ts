import { Component, OnInit } from "@angular/core";
import { LoginActions } from "../store/loginActions";
import { AppCacheProvider } from "../../shared/providers/app-cache-provider";
import { GravatarProvider } from "../../shared/providers/gravatarProvider";

@Component({
  selector: "logged-in-bar",
  template: `
        <ul class="nav navbar-nav">
            <li>
                <a href="#" (click)="logout()">
                    <span style="color:white">
                        <ng-container *ngIf="identity?.email">
                            <img src="{{gravatarProvider.gravatar(identity.email, 14)}}"/>
                            {{identity.email}}
                        </ng-container>
                        <span class="btn btn-primary btn-xs">
                            Log out
                        </span>
                    </span>
                </a>
            </li>
        </ul>
    `
})
export class LoggedInBar implements OnInit {
  private identity: any | { meta: { modules: {} } };

  constructor(
    private loginActions: LoginActions,
    private appCacheProvider: AppCacheProvider,
    private gravatarProvider: GravatarProvider
  ) {}

  ngOnInit() {
    this.identity = this.appCacheProvider.getIdentity();
    console.warn(this.identity.email);
  }

  logout = () => {
    this.loginActions.logoutUser();
  };
}
