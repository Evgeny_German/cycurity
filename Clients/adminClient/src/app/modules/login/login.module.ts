/**
 * Created by odedl on 16-Jul-17.
 */
import {NgModule} from '@angular/core';
import {LoginMain} from "./login.comp";
import {SharedModule} from "../shared/shared.module";
import {RouterModule, Routes} from "@angular/router";
import {LoginApiService} from "./services/login-api.service";
import {LoginActions} from "./store/loginActions";
import {LoginEpics} from "./store/loginEpics";
import {LoggedInBar} from "./loggedInBar/logged-in-bar.comp";

export const LOGIN_MAIN_URL = 'login';

const loginRoutes: Routes = [
    {
        path: LOGIN_MAIN_URL, component: LoginMain
    }
];

@NgModule({
    imports: [RouterModule.forChild(loginRoutes), SharedModule],
    exports: [LoggedInBar],
    declarations: [LoginMain,LoggedInBar],
    providers: [LoginApiService,LoginActions,LoginEpics],
})
export class LoginModule {
}
