import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {USER_LOGIN, USER_LOGIN_COMPLETED, USER_LOGOUT, USER_LOGOUT_COMPLETED} from "./loginActions";
import {ActionsObservable} from "redux-observable";
import {Action} from "redux";
import {Observable} from "rxjs/Observable";
import {LoginApiService} from "../services/login-api.service";
import {AppCacheProvider} from "../../shared/providers/app-cache-provider";


@Injectable()
export class LoginEpics {
    epics = [];

    constructor(private router: Router,
                private appCacheProvider: AppCacheProvider,
                private loginApiService: LoginApiService) {

        this.epics = [this.loginUser, this.logoutUser]
    }

    loginUser = (action$: ActionsObservable<Action>) => {
        return action$.ofType(USER_LOGIN)
            .mergeMap(action => {
                return this.loginApiService.login((<any>action).payload.email, (<any>action).payload.password).map(res => {
                    console.warn(res);
                    if (res && res.token && res.token!=='') {

                        if(res.token && res.token.user.meta && res.token.user.meta.modules){
                            let selectedModule;
                            ['reports','projects','users','settings','vp'].forEach((module)=>{
                                if(!selectedModule && res.token.user.meta.modules[module]) selectedModule = module;
                            });
                            if(selectedModule){
                                this.router.navigate([`/${selectedModule}`]);
                            }else{
                                this.router.navigate([`/reports`]);
                            }
                        }else{
                            this.router.navigate([`/reports`]);
                        }
/*
                        if(res.token && res.token.user.meta && res.token.user.meta.modules && res.token.user.meta.modules.reports){
                            this.router.navigate([`/reports`]);
                        }else{
                            this.router.navigate([`/home`]);
                        }
*/

                        this.appCacheProvider.saveToken(res.token.token);
                        this.appCacheProvider.saveIdentity(res.token.user);
                        return {
                            type: USER_LOGIN_COMPLETED,
                            payload: {loginSucceed: true, email: (<any>action).payload.email}
                        };
                    } else {
                        this.appCacheProvider.clearLocalStorage();
                      return {type: USER_LOGIN_COMPLETED, payload: {loginSucceed: false, error: res.errorMessage||res.user}};
                    }

                },err => {
                    this.appCacheProvider.clearLocalStorage();
                    return Observable.of({type: "ERROR"});

                });
            });
    }

    logoutUser = (action$: ActionsObservable<Action>) => {
        return action$.ofType(USER_LOGOUT)
            .mergeMap(action => {
                this.appCacheProvider.clearLocalStorage();
                this.router.navigate(["/login"]);
                //return Observable.empty();
                return Observable.of({type: USER_LOGOUT_COMPLETED}); //this is mainly done to cause a redux publish so that the app.comp will know that the user is not logged in
            });
    }

}



