/**
 * Created by odedl on 16-Jul-17.
 */
import {Injectable} from '@angular/core';
import {NgRedux} from "@angular-redux/store";
import {IAppState} from "../../shared/store/store";

export const USER_LOGIN:string="USER_LOGIN";
export const USER_LOGIN_COMPLETED:string="USER_LOGIN_COMPLETED";

export const USER_LOGOUT:string="USER_LOGOUT";
export const USER_LOGOUT_COMPLETED:string="USER_LOGOUT_COMPLETED";

@Injectable()
export class LoginActions {

  constructor(private redux :NgRedux<IAppState>) {
  }

  loginUser=(email?:string,password?:string)=>{
    this.redux.dispatch(<any>{type:USER_LOGIN,payload:{email,password}})
  }

  logoutUser=()=>{
      this.redux.dispatch(<any>{type:USER_LOGOUT})
  }

}
