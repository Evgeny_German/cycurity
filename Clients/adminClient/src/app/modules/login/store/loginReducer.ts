/**
 * Created by odedl on 16-Jul-17.
 */
import {Reducer, Action} from "redux";

import {USER_LOGIN, USER_LOGIN_COMPLETED, USER_LOGOUT_COMPLETED} from "./loginActions";
import {ILoggedUserState} from "../../shared/store/store";
import {IUserInfo} from "../../shared/dataModels/userInfo";

const defaultLoggedUsersState: ILoggedUserState = {
    userInfo: null,
    loginError:""
}

export const loggedUserReducer: Reducer<ILoggedUserState> = (state: ILoggedUserState = defaultLoggedUsersState, action: Action) => {
    let newState: ILoggedUserState = Object.assign({}, state);
    switch (action.type) {
        case USER_LOGIN_COMPLETED:
            if((<any>action).payload.loginSucceed) {
                let email = (<any>action).payload.email;
                newState.userInfo = <IUserInfo>{email: email}
                newState.loginError = "";
            }else{
                newState.userInfo = null;
                newState.loginError = (<any>action).payload.error
            }
            return newState;
        case USER_LOGOUT_COMPLETED:
            newState.userInfo = null;
            newState.loginError = "";
            return newState;
        default:
            return state;
    }

}
