import {Injectable} from '@angular/core';
import {Http} from "@angular/http";

@Injectable()
export class LoginApiService {

    constructor(private http: Http) {

    }

    private AUTH_URL = "/userManage/login";

    login = (email: string, password: string) => {
        return this.http.post(this.AUTH_URL, {email, password}).map((res) => {
            return res.json();
        })
    }
}
