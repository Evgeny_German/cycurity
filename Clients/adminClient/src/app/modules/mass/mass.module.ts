import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { RouterModule } from "@angular/router";
import { MassComponent } from "./mass.component";

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: "mass",
                component: MassComponent
            }
        ])
    ],
    declarations: [MassComponent],
    exports: [MassComponent]
})
export class MassModule {}
