import { Component } from "@angular/core";
import * as alertify from 'alertifyjs';
import { Http } from "@angular/http";

@Component({
    template: `
    <div style="width: 300px; position: absolute; top:50%; left: 50%; margin-top: 50px; margin-left: -150px;">
        <div>
            Mail:<br/>
            <input class="form-control" type="text" [(ngModel)]="mail" />
        </div>
        <div>
            Twitter User names: <small>(Please add comma-separated handles without @)</small><br/>
            <textarea class="form-control" [(ngModel)]="userNames"></textarea>
        </div>
        <div>
            <input class="form-control btn btn-primary" type="button" value="Go" (click)="go()"/>
        </div>
    </div>
    `
})
export class MassComponent {
    public mail: string;
    public userNames: string;

constructor(private http: Http){}

    public go(): void {
        const names = this.userNames
            .split(",")
            .map(n => n.trim())
            .filter(n => n);
        this.http.post(`/scrape/mail?mail=${encodeURIComponent(this.mail)}`, names).subscribe(res => {
            this.mail = "";
            this.userNames = "";
            alertify.success("Sent.");
        }, err => {
            alertify.error("Failed.");
        });
    }
}
