/**
 * Created by odedl on 16-Jul-17.
 */
import {Injector, NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {AppCacheProvider} from "./providers/app-cache-provider";
import {NgRedux, NgReduxModule} from "@angular-redux/store";
import {AuthenticationCallbackActivateGuard} from "./auth/auth-guard-provider";
import {HttpInterceptor, provideInterceptor} from "./interceptors/httpInterceptor";
import {Http} from "@angular/http";
import {Spinner} from "./components/spinner.comp";
import {listEditor} from "./components/listEditor.comp";
import {HtmlEditor} from "./components/htmlEditor.comp";
import {AutocompleteSelect} from "./components/autocompleteSelect.comp";
import {KolEditor} from "./components/kolEditor.comp";
import {SaveButton} from "./components/saveButton.comp";
import {Autolinker} from './pipes/autolinker';
import {RoundPipe} from './pipes/round';
import {DocumentProvider} from './providers/documentProvider';
import {GenericApiProvider} from './providers/genericApiProvider';
import {PermissionsProvider} from './providers/permissionsProvider';
import {LinkPreviewComponent} from "./components/linkPreview.comp";
import {PreviewProvider} from "./providers/previewProvider";
import {GravatarProvider} from "./providers/gravatarProvider";
import {SortableDirective} from './directives/sortable.directive';
import {TagDirective} from "./directives/tag.directive";
import {VirtualScrollModule} from 'angular2-virtual-scroll';
import { NumberParserPipe, NumberFormatterPipe } from './pipes/num-parser';
import { PushProvider } from './providers/push-provider';
import { CoreModule } from '../core/core.module';

@NgModule({
    imports: [FormsModule, CommonModule, NgReduxModule, VirtualScrollModule, BrowserAnimationsModule, CoreModule.forRoot()],
    exports: [
        FormsModule,
        CommonModule,
        NgReduxModule,
        Spinner,
        listEditor,
        HtmlEditor,
        AutocompleteSelect,
        KolEditor,
        SaveButton,
        Autolinker,
        RoundPipe,
        TagDirective,
        LinkPreviewComponent,
        SortableDirective,
        VirtualScrollModule,
        NumberParserPipe,
        NumberFormatterPipe
    ],
    declarations: [Spinner, listEditor, HtmlEditor, AutocompleteSelect, KolEditor, SaveButton, Autolinker, RoundPipe, TagDirective, LinkPreviewComponent, SortableDirective, NumberParserPipe, NumberFormatterPipe],
    providers: [AppCacheProvider, AuthenticationCallbackActivateGuard, DocumentProvider, GenericApiProvider, PermissionsProvider, PreviewProvider,GravatarProvider, {
        provide: Http,
        useFactory: provideInterceptor
    }],
})
export class SharedModule {

}
