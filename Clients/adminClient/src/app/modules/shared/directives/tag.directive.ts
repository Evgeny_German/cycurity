import {Directive, forwardRef, Inject, Input, QueryList, TemplateRef, ViewContainerRef} from "@angular/core";
import {InterceptorComponent} from "../components/interceptor.component";

@Directive({
  selector: '[tag]'
})
export class TagDirective {
  @Input()
  public tag: any;

  public get template(): TemplateRef<any> {
    return this._template;
  }

  public get viewContainerRef(): ViewContainerRef {
    return this._viewContainerRef;
  }

  constructor(private _template: TemplateRef<any>, private _viewContainerRef: ViewContainerRef) {
  }
}
