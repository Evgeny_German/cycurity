import { Observable } from "rxjs/Observable";

export interface HasSavableChanges {
    hasChanges: Observable<boolean>;
    confirmNavigation(): Observable<boolean>;
}
