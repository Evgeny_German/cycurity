import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { HasSavableChanges } from "./has-savable-changes";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

@Injectable()
export class SaveChangesGuard<T extends HasSavableChanges>
    implements CanDeactivate<T> {
    canDeactivate(
        component: T,
        currentRoute: ActivatedRouteSnapshot,
        currentState: RouterStateSnapshot,
        nextState?: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        return new Promise(resolve => {
            component.hasChanges.first().subscribe(hasChanges => {
                if (!hasChanges) {resolve(true);return;}
                component.confirmNavigation().first().subscribe(resolve);
            });
        });
    }
}
