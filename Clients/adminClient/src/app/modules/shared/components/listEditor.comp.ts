import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FlagActions} from "../store/flagsActions";
import {GenericApiProvider} from "../providers/genericApiProvider";
import {Observable} from "rxjs/Observable";
import {template} from "./template";
import {InterceptorComponent} from "./interceptor.component";

@Component({
    selector: 'listEditor',
    styles: [`
        h5, .listEmptyMessage {
            text-transform: capitalize;
        }

        input:not(:focus) {
            cursor: pointer;
        }

        .disabled {
            opacity: 0.3;
            pointer-events: none;
        }
    `],
    template: `
        <div class="spinner" *ngIf="!list"><i class="fa fa-circle-o-notch fa-spin text-primary"></i></div>

        <div *ngIf="list"
             class="{{(disabled)?'disabled':''}}"
             style="position:absolute;top:0px;bottom:0;left:0;right:0;overflow-y:auto;/*border:11px solid #f00;*/">

            <h5 style="padding:17px 10px 0 10px">
                {{itemPlural}}
                <small *ngIf="caption">: {{caption}}</small>
            </h5>
            <hr/>
            <div>

                <div *ngIf="list.length==0" style="margin:9px 6px 0 4px" class="listEmptyMessage alert alert-danger">
                    No {{itemPlural}} Currently Available
                </div>

                <div *ngIf="list.length>0">

                    <ul class="nav nav-pills" style="margin-bottom:5px;">
                        <ng-container *ngFor="let item of list; let i = index">
                            ${template(`'item'`, {$implicit: '_contexts[i]'})}
                        </ng-container>
                    </ul>
                </div>

            </div>

            <div *ngIf="canAdd" style="padding:1px 5px 0 5px;">
                <div class="form-group">
                    <input
                        (keyup.enter)="createListItem()"
                        placeholder="+ Create {{itemSingular}}"
                        class="form-control"
                        [(ngModel)]="searchText"
                    />
                </div>
            </div>

        </div>

    `
})

export class listEditor extends InterceptorComponent implements OnInit {
    searchText: any;

    constructor(changeDetector: ChangeDetectorRef, private flagActions: FlagActions, private genericApiProvider: GenericApiProvider,) {
        super(changeDetector);
    }

    @Input() canAdd?: boolean;

    @Output() public refresh = new EventEmitter();

    createListItem() {
        if (!this.searchText || this.searchText.length == 0) return

        this.flagActions.setFlag('loading', true);

        var newItem = this.genericApiProvider.type(this.type).dispense();
        Object.assign(newItem, this.additionalParams || {});

        newItem[this.nameProperty] = this.searchText;

        this.genericApiProvider.type(this.type).post(newItem)
            .catch((err) => {
                this.flagActions.setFlag('loading', false);
                return Observable.of(err);
            })
            .subscribe((res) => {
                this.flagActions.setFlag('loading', false);
                this.refresh.emit(res);
                this.searchText = '';
            });


    }

    ngOnInit() {
        if(this.canAdd!==false && this.canAdd!==true){
            this.canAdd = true;
        }
    }

    private _contexts: any[];
    private _list: any[];

    @Input() itemSingular: string;
    @Input() itemPlural: string;

    @Input()
    get list(): any[] {
        return this._list;
    }

    set list(value: any[]) {
        const prev = this._list;
        this._list = value;

        if (prev !== value) {
            this._contexts = value ? value.map(item => ({
                item(): any {
                    return item;
                }
            })) : [];
        }
    }

    @Input() selected: Object;
    @Input() type: string;
    @Input() nameProperty: string;
    @Input() additionalParams: string;
    @Input() disabled: boolean;
    @Input() caption?: string;

}
