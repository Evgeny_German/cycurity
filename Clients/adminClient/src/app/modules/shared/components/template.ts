export function template(name: string, contextBinding: any, defaultTemplate?: string): string {
  return defaultTemplate ? `
        <ng-container *ngIf="templates[${name}]">
            <ng-container *ngTemplateOutlet="templates[${name}]; context: {${Object.keys(contextBinding).map(key => `'${key}': ${contextBinding[key]}`).join(', ')}}"></ng-container>
        </ng-container>
        <ng-container *ngIf="!templates[${name}]">
            ${defaultTemplate}
        </ng-container>` : `
        <ng-container *ngIf="templates[${name}]">
            <ng-container *ngTemplateOutlet="templates[${name}]; context: {${Object.keys(contextBinding).map(key => `'${key}': ${contextBinding[key]}`).join(', ')}}"></ng-container>
        </ng-container>`;
}
