import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    SimpleChange
} from "@angular/core";
import { kolEditorConfig } from "../dataModels/kol-config";
import { KolService, toStartCase } from "../../projects/services/kolService";

@Component({
    selector: "kolEditor",
    styles: [
        `
            input[type="number"]::-webkit-inner-spin-button,
            input[type="number"]::-webkit-outer-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            .select-placeholder {
                position: absolute;
                left: 28px;
                top: 10px;
                color: #ccc;
                font-size: 1rem;
            }
        `
    ],
    template: `
        <div class="container">

            <div *ngFor="let row of kolEditorConfig" class="form-group row">


                <div class="col-2" style="padding-top:5px;">
                    <span *ngIf="!hasImportance(row.id)" class="select-placeholder">{{defaults[row.id]?.importance||0}}</span>
                    <select [ngModel]="params[row.id]?.importance" (ngModelChange)="setValue(row.id, 'importance', $event); validate(); emitParamsChanged();" class="custom-select">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    <a *ngIf="hasImportance(row.id)" href="javascript:;" (click)="deleteImportance(row.id); validate(); emitParamsChanged();">&times;</a>
                </div>

                <label class="col-3 col-form-label" style="margin-top:7px;">{{toStartCase(row.id)}}</label>

                <div *ngIf="row.type=='range'" class="col-7">

                    <div class="row">

                        <div class="col-5">
                            <input step="1" min="0" max="{{params[row.id]?.to||99999999999999999999999999}}" [ngModel]="params[row.id]?.from" (ngModelChange)="setValue(row.id, 'from', $event); validate(); emitParamsChanged();" placeholder="{{defaults[row.id]?.from}}" type="number" class="form-control"/>
                        </div>

                        <div class="col-2" style="padding-top:7px;text-align:center;"><label class="col-form-label">to</label></div>

                        <div class="col-5">
                            <input step="1" min="0" [ngModel]="params[row.id]?.to" placeholder="{{defaults[row.id]?.to}}" (ngModelChange)="setValue(row.id, 'to', $event); validate(); emitParamsChanged();" type="number" class="form-control"/>
                        </div>

                    </div>

                </div>

                <div *ngIf="row.type=='string'" class="col-7">

                    <input [ngModel]="params[row.id]?.value" (ngModelChange)="setValue(row.id, 'value', $event); validate(); emitParamsChanged();" placeholder="{{defaults[row.id]?.value}}" class="form-control"/>

                </div>



            </div>

        </div>
 `
})
export class KolEditor implements OnInit {
    toStartCase = toStartCase;

    private kolEditorConfig = kolEditorConfig;

    constructor(private kolService: KolService) {}

    ngOnInit() {
        this.kolEditorConfig.forEach(kolComponent => {
            if (!this.params[kolComponent.id])
                this.params[kolComponent.id] = {};
            if (!this.defaults[kolComponent.id])
                this.defaults[kolComponent.id] = {};
        });
    }

    @Input()
    defaults: any;
    @Input()
    params: any;

    @Output()
    paramsChange = new EventEmitter<any>();

    @Output()
    validationErrors = new EventEmitter<{
        errors: string[];
        warnings: string[];
    }>();

    emitParamsChanged() {
        this.paramsChange.emit(this.params);
    }

    hasImportance(rowId: string): boolean {
        return this.params[rowId] && "importance" in this.params[rowId];
    }

    deleteImportance(rowId: string) {
        delete this.params[rowId]["importance"];
    }

    setValue(rowId: string, field: string, value: string) {
        if (this.params[rowId]) {
            this.params[rowId][field] =
                kolEditorConfig.find(row => row.id === rowId).type === "string"
                    ? value
                    : Number(value);
        } else {
            this.params[rowId] = {
                [field]:
                    kolEditorConfig.find(row => row.id === rowId).type ===
                    "string"
                        ? value
                        : Number(value)
            };
        }
    }

    validate() {
        const final = {};
        for (let item of kolEditorConfig) {
            final[item.id] = Object.assign(
                this.defaults[item.id] || {},
                this.params[item.id] || {}
            );
        }
        this.validationErrors.emit(this.kolService.validate(final));
    }

    sanitizeNumericValues(rowId, field) {
        if (typeof this.params[rowId][field] !== "number") return;

        // No fractions
        this.params[rowId][field] = Math.floor(this.params[rowId][field]);

        // No negative numbers
        this.params[rowId][field] = Math.abs(this.params[rowId][field]);

        // 'From' should be lower or equal to 'To'
        // 'To' should be higher or equal to 'From'
        if (field == "from") {
            const toValue = this.params[rowId].to || this.defaults[rowId].to;
            if (this.params[rowId]["from"] > toValue) {
                this.params[rowId]["from"] = toValue;
            }
        } else {
            const fromValue =
                this.params[rowId].from || this.defaults[rowId].from;
            if (this.params[rowId]["to"] < fromValue) {
                this.params[rowId]["to"] = fromValue;
            }
        }
    }
}
