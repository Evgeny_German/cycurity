import {
  ChangeDetectorRef, ContentChildren, ElementRef, QueryList, TemplateRef, ViewChildren,
  ViewContainerRef
} from "@angular/core";
import {TagDirective} from "../directives/tag.directive";

export class InterceptorComponent {

  constructor(private changeDetector: ChangeDetectorRef) {
  }

  protected templates = {};

  public findTemplate(tag: any): TemplateRef<any> {
    if (this.contentTags) {
      const ret = this.contentTags.find(t => t.tag === tag);
      if (ret) return ret.template;
    }
    if (this.viewTags) {
      const ret = this.viewTags.find(t => t.tag === tag);
      if (ret) return ret.template;
    }
    return undefined;
  }

  protected get tags(): TagDirective[] {
    return [...this._tags];
  }

  protected viewTags: TagDirective[];

  protected contentTags: TagDirective[];

  private _tags: TagDirective[];

  private __viewTags: QueryList<TagDirective>;

  @ViewChildren(TagDirective, <any>{descendants: false})
  private get _viewTags(): QueryList<TagDirective> {
    return this.__viewTags;
  }

  private set _viewTags(value: QueryList<TagDirective>) {
    const prevValue = this.__viewTags;
    this.__viewTags = value;

    if (prevValue !== value) {
      this.viewTags = value.toArray();
      this.changeDetector.detectChanges();
      this.updateTags();
    }
  }

  private __contentTags: QueryList<TagDirective>;

  @ContentChildren(TagDirective)
  private get _contentTags(): QueryList<TagDirective> {
    return this.__contentTags;
  }

  private set _contentTags(value: QueryList<TagDirective>) {
    const prevValue = this.__contentTags;
    this.__contentTags = value;

    if (prevValue !== value) {
      this.contentTags = value.toArray();
      this.changeDetector.detectChanges();
      this.updateTags();
    }
  }

  private updateTags(): void {
    this._tags = [...(this.__viewTags ? this.__viewTags.toArray() : []), ...(this.__contentTags ? this.__contentTags.toArray() : [])];
    const temp = {};
    this._tags.forEach(t => temp[t.tag] = t.template);
    if (this.templates && Object.keys(this.templates).every(k => this.templates[k] === temp[k]) && Object.keys(temp).every(k => this.templates[k] === temp[k])) return;
    this.templates = temp;
  }
}


