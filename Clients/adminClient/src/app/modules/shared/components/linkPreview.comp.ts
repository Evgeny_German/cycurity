import {ChangeDetectorRef, Component, Input} from "@angular/core";
import {InterceptorComponent} from "./interceptor.component";
import {template} from "./template";

@Component({
    selector: 'link-preview',
    template: template('preview', {$implicit: 'context'}, `
        <ul class="list-group" style="margin-top:7px;">
            <li class="list-group-item" style="padding:0;" *ngFor="let obj of context().data?.objects">
                <a target="_blank"
                   [href]="context().data ? getUrl(context().data) : 'javascript:;'"
                   style="text-decoration:none;padding:0;/*max-height:48px;overflow:hidden;*/">

                    <div class="media">
                        <div class="d-flex mr-0" *ngIf="obj.images && obj.images[0]" style="display:inline-block;">
                            <div
                                [ngStyle]="{'background-image': 'url('+(obj.images[0].url||obj.images[0].value.url)+')' }"
                                style="width:48px;height:48px;background-size:cover;background-position:center;"></div>
                        </div>

                        <div class="media-body" style="padding:4px 4px 4px 4px;">
                            <h5 class="mt-0"
                                style="height:1.2em;overflow:hidden;font-size:12px;font-weight:bold;margin:0;">
                                {{obj.title || '...'}}</h5>
                            <div style="font-size:12px;color:#000;line-height:1.1em;">
                                {{(obj.text || obj.description?.value)?.substring(0, 70)}}...
                            </div>

                        </div>
                    </div>

                </a>
            </li>
        </ul>`)
})
export class LinkPreviewComponent extends InterceptorComponent {

    constructor(changeDetector: ChangeDetectorRef) {
     super(changeDetector);
    }

    private getUrl(data: any): string {
        return data['resolvedPageUrl'] || data['pageUrl'] || data['request'] ? data['request']['resolvedPageUrl'] || data['request']['pageUrl'] : null;
    }

    @Input()
    public data: any;

    public get context(): () => any {
        const self = this;
        return () => ({
            data: self.data
        });
    }

}
