import {Component, OnInit, Input, OnDestroy, ElementRef, ApplicationRef} from '@angular/core';

declare var MediumEditor, AutoList;

@Component({
    selector: 'htmlEditor',
    styles: [`
        .editablePrecursor {
            padding: 10px;
        }
    `],
    template: `
        <div
            *ngIf="!editing"
            (click)="editOn()"
            class="editablePrecursor wBox medium-editor-element"
            [innerHTML]="binding[field]">
        </div>

        <textarea
            *ngIf="editing"
            [innerHTML]="binding[field]"
            style="background-color:#fff;padding:10px;"
            class="wBox editable">
                {{binding[field]}}
        </textarea>
    `
})

export class HtmlEditor implements OnInit, OnDestroy {
    @Input() binding: any;
    @Input() field: string;

    editor;
    editing: boolean = false;

    constructor(private el: ElementRef, private applicationRef: ApplicationRef) {
    }

    ngOnInit() {
    }

    editOn() {
        this.editing = true;
        this.applicationRef.tick();

        let autolist = new AutoList();
        this.editor = new MediumEditor('.editable', {
            extensions: {
                'autolist': autolist
            },
            toolbar: {
                buttons: ['bold', 'italic', 'underline', 'anchor', 'quote', 'unorderedlist', 'orderedlist'],
            }
        }).subscribe('editableClick', function (e) {
            if (e.target.href) {
                window.open(e.target.href)
            }
        }).subscribe('editableInput', (event) => {
            this.binding[this.field] = this.editor.elements[0].innerHTML;
        }).subscribe('blur', (event) => {
            if(event.type=='click'){
                this.editOff();
            }
        });
        this.editor.setContent(this.binding[this.field]);

        var editorElement = this.el.nativeElement.querySelector('.editable');
        editorElement.focus();
    }

    editOff() {
        console.warn('editOff');
        this.editor.destroy();
        this.editing = false;
    }

    ngOnDestroy(): void {
        //this.editor.destroy();
    }

}
