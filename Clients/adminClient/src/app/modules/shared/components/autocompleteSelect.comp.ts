import {Component, OnInit, Input, OnDestroy, ElementRef, ApplicationRef, ViewChild} from '@angular/core';
import * as UUID from 'uuid/v4';
//import * as selectize from 'selectize';

declare let $: any;

import {
    NG_VALUE_ACCESSOR, NgModel,
} from '@angular/forms';

@Component({
    selector: 'autocompleteSelect',
    styles: [`
        :host{
            padding:0;
        }
        
        div.containerBox{
            position:relative;
        }
        
        .disabled{
            z-index:2;
            position:absolute;
            top:0;
            right:0;
            bottom: 4px;
            left:0;
            background-color:rgba(0,0,0,0.06);
            cursor: not-allowed;
        }
        
    `],
    template: `
        <div class="containerBox">
            <div class="disabled" *ngIf="disabled"></div>
            <select id="{{uid}}" class="form-control"><!-- [(ngModel)]="innerValue"-->
                <option></option>
                <ng-content></ng-content>
            </select> 
        </div> 
    `,
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: AutocompleteSelect, multi: true}
    ]
})

export class AutocompleteSelect implements OnInit, OnDestroy {
    select: any;
    selector: any;

    @Input() disabled?:boolean;

    @ViewChild(NgModel) model: NgModel;
    private _innerValue: any;

    private uid = 'autocompleteSelect' + UUID();

    private changed = new Array<(value) => void>();
    private touched = new Array<() => void>();

    constructor(private el: ElementRef, private applicationRef: ApplicationRef) {
    }

    get innerValue() {
        return this._innerValue;
    }

    set innerValue(value) {
        if (this._innerValue !== value) {
            this._innerValue = value;
            this.changed.forEach(f => f(value));
        }
    }

    writeValue(value) {
        this._innerValue = value;

        if(this.select){
            var selectize = this.select[0].selectize;
            selectize.setValue(value);
        }
    }

    registerOnChange(fn: (value) => void) {
        this.changed.push(fn);
    }

    registerOnTouched(fn: () => void) {
        this.touched.push(fn);
    }

    ngAfterViewInit() {
        this.select = $('select#' + this.uid).selectize({
            onChange: (value) => {
                this.innerValue = value;
            }
        });

    }

    ngOnInit() {
    }

    ngOnDestroy(): void {
    }

}
