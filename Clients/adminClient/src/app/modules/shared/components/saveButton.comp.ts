import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

export interface SaveButtonWarning {
    alert: boolean,
    message: String
}

@Component({
    selector: 'saveButton',
    styles: [`
    `],
    template: `
        <div>
            <ng-container *ngIf="warnings.length==0">
                <a (click)="clickEvent()" href="javascript:;" class="btn btn-xs btn-primary {{(active)?'':'disabled'}}">{{caption}}</a>
            </ng-container>
            <ng-container *ngIf="warnings.length>0">

                <span class="dropdown">
                    <a href="javascript:;"
                       data-toggle="dropdown"
                       class="btn btn-xs btn-primary">
                       {{caption}}
                       <span style="position:absolute;top:-3px;right:-3px;"class="badge badge-pill badge-danger">
                           {{warnings.length}}
                       </span>
                    </a>
                    
                    <ul class="dropdown-menu" style="margin-top:-22px;margin-right:15px;">
                        <li
                            *ngFor="let err of warnings"
                            style="background-color:#fff;"
                            class="dropdown-item">
                                <i class="text-{{(err.alert)?'danger':'warning'}} fa fa-exclamation-triangle"
                                   aria-hidden="true"></i>
                                <span [innerHTML]="err.message"></span>
                        </li>
                        <!--<li *ngFor="let err of validationWarnings" style="background-color:#fff;" class="dropdown-item"><i class="text-warning fa fa-exclamation-triangle" aria-hidden="true"></i> <span [innerHTML]="err"></span></li>-->
                        <li *ngIf="alertCount()==0" style="text-align:right;">
                            <hr/>
                            <input
                                type="button"
                                class="btn btn-sm btn-primary"
                                style="margin-right:20px;"
                                value="{{caption}}"
                                (click)="clickEvent()"
                            />
                        </li>
                    </ul>
                </span>

            </ng-container>
        </div>
    `
})

export class SaveButton implements OnInit {
    constructor() {
    }

    ngOnInit() {
    }

    alertCount() {
        let returnValue = this.warnings.filter((warning) => {
            return warning.alert
        }).length;
        console.warn(returnValue)
        return returnValue;
    }

    clickEvent() {
        console.warn(this.warnings)
        this.action.emit();
    }

    @Input() caption: String;
    @Input() warnings: Array<SaveButtonWarning>;
    @Input() active: boolean;

    @Output() action = new EventEmitter<number>();

}
