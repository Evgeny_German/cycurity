import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'spinner',
    styles: [`
      .spinner {
          position:fixed;
          top:0;
          right:0;
          bottom:0;
          left:0;
          background-color:rgba(255,255,255,0.5);
          z-index: 9999999;
          font-size:4em;
      }
      i{
          position:absolute;
          top:50%;
          left:50%;
          margin-top:-1em;
          margin-left:-0.5em;
      }
  `],
    template: `
    <div class="spinner" *ngIf="display"><i class="fa fa-circle-o-notch fa-spin text-primary"></i></div>
 `
})

export class Spinner implements OnInit {
    constructor() {
    }

    ngOnInit() {
    }

    @Input()
    display:boolean

}
