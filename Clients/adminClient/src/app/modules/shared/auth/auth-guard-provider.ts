/**
 * Created by odedl on 16-Jul-17.
 */
import {Injectable} from "@angular/core";
import {CanActivate, Router, ActivatedRouteSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {IAppState} from "../store/store";
import {Location} from "@angular/common";
import {NgRedux} from "@angular-redux/store";
import {AppCacheProvider} from "../providers/app-cache-provider";
import {Http} from "@angular/http";

@Injectable()
export class AuthenticationCallbackActivateGuard implements CanActivate {
    constructor(private http: Http,
                private router: Router,
                private location: Location,
                private appCacheProvider: AppCacheProvider,
                private redux: NgRedux<IAppState>) {
    }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        let token: any;
        if ((token = this.hasAccessTokenInQueryParams())) {
            console.log("token found in query param. Permitting user");
            return this.http
                .get(
                    `/userManage/tokenLogin?token=${encodeURIComponent(token)}`
                )
                .catch(err => {
                    this.router.navigate(["/login"]);
                    return Observable.of(false);
                })
                .map(res => {
                    if (!res) {
                        return false;
                    }
                    const user = res['json']();
                    this.appCacheProvider.saveIdentity(user);
                    if (!['admin','superadmin'].includes(user.adminRole) && route.url && route.url.length && ['projects', 'users', 'settings', 'vp', 'reports', 'search', 'mass', 'proxy'].includes(route.url[0].path)) {
                        return user.meta && user.meta.modules && user.meta.modules[route.url[0].path];
                    }
                    return true;
                });
        } else if ((token = this.appCacheProvider.getToken())) {
            console.log("token found in appCache. Permitting user");
            return this.http
                .get(
                    `/userManage/tokenLogin?token=${encodeURIComponent(token)}`
                )
                .catch(err => {
                    this.router.navigate(["/login"]);
                    return Observable.of(false);
                })
                .map(res => {
                    console.warn(res);
                    if (!res) {
                        return false;
                    }
                    const user = res['json']();
                    this.appCacheProvider.saveIdentity(user);
                    if (!['admin','superadmin'].includes(user.adminRole) && route.url && route.url.length && ['projects', 'users', 'settings', 'vp', 'reports', 'search', 'mass', 'proxy'].includes(route.url[0].path)) {
                        return user.meta && user.meta.modules && user.meta.modules[route.url[0].path];
                    }
                    return true;
                });
        } else {
            this.router.navigate(["/login"]);
        }
    }

    //validate if the query string has an access token param
    //this is relevant when the server wants to print a page using headless browser and is not logged in
    private hasAccessTokenInQueryParams = () => {
        let parsedObj: any = this.getParsedQueryString();
        if (parsedObj && parsedObj.access_token) {
            //save the token so that the call for the conversation will use this token
            this.appCacheProvider.saveToken(parsedObj.access_token);
            return parsedObj.access_token;
        }
        return null;
    };

    private getParsedQueryString = () => {
        let pathFragments = this.location.path(true);
        return this.parseQueryString(pathFragments);
    };

    //gets a query string and returns an object of key values
    parseQueryString = queryString => {
        var params = {},
            queries,
            temp,
            i,
            l;
        // Split into key/value pairs
        queries = queryString.split(";");
        // Convert the array of strings into an object
        for (i = 0, l = queries.length; i < l; i++) {
            temp = queries[i].split("=");
            params[temp[0]] = temp[1];
        }
        return params;
    };
}
