import {Observable} from "rxjs/Observable";
import {ConnectionBackend, Http, RequestOptions, RequestOptionsArgs,Request,Response,Headers} from "@angular/http";
import {Injectable, Injector} from "@angular/core";
import {AppCacheProvider} from "../providers/app-cache-provider";
import {Router} from "@angular/router";
import * as alertify from 'alertifyjs';
/**
 * Created by odedl on 16-Jul-17.
 */
@Injectable()
export class HttpInterceptor extends Http {
  //Cannot request to get AppCacheProvider and AuthorizationStratagy during constructer since it would lead to
  //a cyclic dependency. Therefore added getter that creates them for first time form injector
   private _cacheProvider:AppCacheProvider;
  // private _authStratagy:AuthorizationStratagy;
  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions, private injector:Injector) {
    super(backend, defaultOptions);
  }

  private get cacheProvider(){
    if(!this._cacheProvider){
      this._cacheProvider = this.injector.get(AppCacheProvider);
    }
    return this._cacheProvider;
  }

  private getRouter=():Router=>{
      return this.injector.get(Router);
  }
  // private get authStratagy(){
  //   if(!this._authStratagy){
  //     this._authStratagy = this.injector.get(AuthorizationStratagy);
  //   }
  //   return this._authStratagy;
  // }
  request(url: string|Request, options?: RequestOptionsArgs): Observable<any>  {
    return super.request(url, options).catch(this.errHandler);
  }

  get(url: string, options?: RequestOptionsArgs): Observable<any> {
    return super.get(url,this.getRequestOptionArgs(options)).catch(this.errHandler);
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
    return super.post(url, body, this.getRequestOptionArgs(options)).catch(this.errHandler);
  }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<any> {
        return super.put(url, body, this.getRequestOptionArgs(options)).catch(this.errHandler);
    }

    patch(url: string, body: string, options?: RequestOptionsArgs): Observable<any> {
        return super.patch(url, body, this.getRequestOptionArgs(options)).catch(this.errHandler);
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<any> {
    return super.delete(url, this.getRequestOptionArgs(options)).catch(this.errHandler);
  }

  getRequestOptionArgs(options?: RequestOptionsArgs) : RequestOptionsArgs {
    if (options == null) {
      options = new RequestOptions();
    }
    if (options.headers == null) {
      options.headers = new Headers();
    }
    options.headers.append('Content-Type', 'application/json');
    if(this.cacheProvider) {
      let token = this.cacheProvider.getToken();
      let AuthString = '';
      if (token) {
        AuthString = "Bearer " + token;
        options.headers.append("Authorization", AuthString);
      }
    }
    return options;
  }

  errHandler=(err:any,source:Observable<Response>):any=>{
      if (err.status  == 401 || err.status  == 403){
          alertify.error("unauthorized");
          return Observable.throw(err);
        }else{
          return Observable.throw(err);
      }

    // if (err.status  == 401 ){
    //   this.authStratagy.onUnAuthorized();
    //   return Observable.empty();
    // }else if(err.status==403){
    //   this.authStratagy.onForbidden();
    //   return Observable.empty();
    // }else{
    //   return Observable.throw(err);
    // }
  }

  // intercept(observable: Observable<Response>): Observable<Response> {
  //     return observable.catch((err, source) => {
  //         //if not permitted - route to login page
  //         if (err.status  == 401 || err.status  == 403){   //&& !err.url.endsWith( 'api/auth/login')) {
  //             this._router.navigate(['/login']);
  //             return Observable.empty();
  //         } else {
  //             //If any other error - keep throwing for app to handle
  //             return Observable.throw(err);
  //         }
  //     });
  //
  // }
}


export function provideInterceptor(): Function {
    return (injector: Injector) => injector.get(HttpInterceptor);
}
