import {IUserInfo} from "../dataModels/userInfo";
import {combineReducers} from "redux";
import {loggedUserReducer} from "../../login/store/loginReducer";
import {searchReducer} from "../../search/store/searchReducer";
import {conversationReducer} from "../../conversation/store/conversationReducer";
import {flagsReducer} from "./flagsReducer";

/**
 * Created by odedl on 16-Jul-17.
 */


export interface IAppState {
    loggedUserState: ILoggedUserState,
    searchState: ISearchResults,
    conversationState: IConversation,
    flags: IFlags
}

export interface IFlags {
    loading: boolean
}

export interface ILoggedUserState {
    userInfo: IUserInfo,
    loginError:string //error returned after wrong login
}


export interface ISearchResults {
    data: any;
}

export interface IConversation {
    type: number,
    documentId: string,
    data: any
}

export const rootReducer = combineReducers<IAppState>({
    loggedUserState: loggedUserReducer,
    searchState: searchReducer,
    conversationState: conversationReducer,
    flags: flagsReducer
});
