import {Reducer, Action} from "redux";
import {IConversation, IFlags} from "./store";
import {SET_FLAG, SET_PROGRESS} from './flagsActions';

export const flagsReducer: Reducer<IFlags> = ( state: IFlags = null, action: Action ) => {
    let newState: IFlags = state;
    if(action.type==SET_FLAG){
        let localFlags = Object.assign({}, newState);
        localFlags[action['payload'].key] = action['payload'].value;
        return localFlags;
    }
    if(action.type==SET_PROGRESS){
        let localFlags = Object.assign({}, newState);
        localFlags['progressCurrent'] = action['payload'].current;
        localFlags['progressTotal'] = action['payload'].total;
        return localFlags;
    }
    return state;
}
