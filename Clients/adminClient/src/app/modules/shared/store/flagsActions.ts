import {Injectable} from '@angular/core';
import {NgRedux} from "@angular-redux/store";
import {IAppState, IFlags} from "../../shared/store/store";

export const SET_FLAG: string = "SET_FLAG";
export const SET_PROGRESS: string = "SET_PROGRESS";

@Injectable()
export class FlagActions {

    constructor(private redux: NgRedux<IAppState>) {
    }

    setFlag = (flagKey: string, flagValue: boolean) => {
        this.redux.dispatch(<any>{type: SET_FLAG, payload: {key: flagKey, value: flagValue}})
    }

    setProgress = (current: any, total: any) => {
        this.redux.dispatch(<any>{type: SET_PROGRESS, payload: {current: current, total: total}})
    }

}
