export const kolEditorConfig = [
    { type: "range", id: "following" },
    { type: "range", id: "followers" },
    { type: "range", id: "favorites" },
    { type: "range", id: "followersToFollowingRate" },
    { type: "range", id: "averageTweetsPerDay" },
    { type: "range", id: "averageRepliesPerDay" },
    { type: "range", id: "repliesToTweetRate" },
    { type: "range", id: "averageRetweets" },
    { type: "range", id: "averageReplies" },
    { type: "range", id: "averageFavorites" },
    { type: "string", id: "location" },
    { type: "string", id: "language" },
    { type: "string", id: "bioKeywords" },
    { type: "string", id: "tweetsKeywords" }
];
