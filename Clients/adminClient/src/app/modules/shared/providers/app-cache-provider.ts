/**
 * Created by odedl on 16-Jul-17.
 */
import {Injectable} from '@angular/core';

@Injectable()
export class AppCacheProvider {

  constructor() {
  }

  public getToken=():string=>{
    let token = localStorage.getItem("token");
    return token;
  }

  public isLoggedIn=():boolean=>{
      return !!this.getToken()
  }

  public saveToken=(token:string)=>{
    localStorage.setItem("token",token);
  }

  public clearLocalStorage=()=>{
    localStorage.clear();
  }

    saveIdentity(user) {
        localStorage.setItem("user",JSON.stringify(user));
    }
    getIdentity() {
      try{
          const identity = JSON.parse(localStorage.getItem("user"));
          if(!identity.meta)identity.meta = {};
          if(!identity.meta.modules)identity.meta.modules = {};
          return identity;
      }catch(E){
          return {meta:{modules:{}}};
      }
    }
}
