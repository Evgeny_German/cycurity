import {DocumentBaseClass} from './documentBaseClass'

export class ReportDoctype<T> extends DocumentBaseClass {

    constructor(fields?: object) {
        super(fields);
    }
    public static typeId = 2;
    title: string;
    conversations: Array<any>;
}
