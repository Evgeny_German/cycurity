export abstract class LoopbackBaseClass {
    public id: string;
    public typeId: number;

    constructor(fields?: object) {
        if (fields) Object.assign(this, fields);
    }

}
