import {LoopbackBaseClass} from './loopbackBaseClass'

export class ProjectsDoctype extends LoopbackBaseClass {
    constructor(fields?: object) {
        super(fields);
    }
}
