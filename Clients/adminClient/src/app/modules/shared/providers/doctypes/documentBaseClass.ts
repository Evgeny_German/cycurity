export abstract class DocumentBaseClass {
    public id: string;
    public typeId: number;

    constructor(fields?: object) {
        if (fields) Object.assign(this, fields);
    }

}
