import { Injectable } from "@angular/core";
import { AppCacheProvider } from "./app-cache-provider";
import { Http } from "@angular/http";
import { DocumentProvider } from "./documentProvider";

@Injectable()
export class PermissionsProvider {
    identity: any;

    constructor(
        private appCacheProvider: AppCacheProvider,
        private documentProvider: DocumentProvider,
        private http: Http
    ) {
        this.identity = this.appCacheProvider.getIdentity();
    }

    _isAdmin() {
        return this.identity.adminRole === "admin";
    }

    _isSuperAdmin() {
        return this.identity.adminRole === "superadmin";
    }

    _hasModule(moduleName) {
        return !!this.identity.meta.modules[moduleName];
    }

    _vpRole() {
        this.identity.meta.vpRole;
    }

    async reportPermissions(
        ownerId: string,
        reportId: string
    ): Promise<"READ" | "EDIT" | "NONE"> {
        if (this._isSuperAdmin()) return "EDIT";
        // if (this._hasModule("reports") && ownerId == this.identity.id) {
        //     return "EDIT";
        // }
        if (
            !this._isSuperAdmin() &&
            !this._isAdmin() &&
            !this._hasModule("reports")
        ) {
            return "NONE";
        }
        if (this._isAdmin()) {
            if (this._hasModule("reports")) {
                return "EDIT";
            }
            return "READ";
        }
        try {
            const report = (await this.documentProvider
                .get(ownerId, 2, reportId)
                .first()
                .toPromise()).json().data;
            if (!report.data.projectId || report.data.projectId == 0)
                return "EDIT";
            const projects = (await this.http
                .get("/projectManagement/projects/getSharedProjects")
                .first()
                .toPromise()).json();
            if (projects.editor.find(p => p.id == report.data.projectId))
                return "EDIT";
            if (projects.member.find(p => p.id == report.data.projectId))
                return "READ";
        } catch (error) {}
        return "NONE";
    }

    booleanModulePermissions(module){
        if (this._isSuperAdmin() || this._hasModule(module)) return true;
        if (this._isAdmin()) return true;
        return false;
    }

    modulePermissions(module) {
        if (this._isSuperAdmin() || this._hasModule(module)) return "EDIT";
        if (this._isAdmin()) return "READ";
        return "NONE";
    }
}
