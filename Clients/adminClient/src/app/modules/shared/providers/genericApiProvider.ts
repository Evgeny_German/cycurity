import {Injectable} from '@angular/core';
import {Http, RequestOptions, ResponseContentType, Headers} from "@angular/http";

import {ProjectsDoctype} from './doctypes/ProjectsDoctype';
import {SubprojectsDoctype} from './doctypes/SubprojectsDoctype';

import {error} from "util";

@Injectable()
export class GenericApiProvider {

    constructor(private http: Http) {
    }

    public types = {
        projects: {apiPath: 'projectManagement/projects', doctype: ProjectsDoctype},
        sharedProjects: {apiPath: 'projectManagement/projects/getSharedProjects', doctype: ProjectsDoctype},
        subprojects: {apiPath: 'projectManagement/subprojects', doctype: SubprojectsDoctype},
        globalKolConfigs: {apiPath: 'projectManagement/globalKolConfigs/global', doctype: Object},
        getInboxes: {apiPath: 'projectManagement/subprojects/engagorInboxes', doctype: Object},
        voStatus: {apiPath:'vo/voStatus', doctype: Object},
        voAction: {apiPath:'vo/voActions', doctype: Object},
        voActionStatus: {apiPath:'vo/voActionStatus', doctype: Object},
        vos: {apiPath:'vo/vos', doctype: Object},
        voRole: {apiPath:'userManage/voRoles', doctype: Object},
        voActionLogs: {apiPath:'vo/voActionLogs', doctype: Object},
        voAudit: {apiPath:'vo/voAudits', doctype: Object},
        users: {apiPath:'userManage/cy_users', doctype: Object},
        projectUser: {apiPath:'projectManagement/projectUsers', doctype: Object},
        voLaunches: {apiPath:'vo/voLaunches', doctype: Object}
    };

    type(typeName) {

        if (!this.types[typeName]) {
            console.error(`Type ${typeName} does not exist`);
            return;
        }

        const self = this;

        return {

            list(filter?) {
                var url = self.types[typeName].apiPath;
                url += (filter) ? '?filter=' + encodeURIComponent(JSON.stringify(filter)) : '';
                return self.http.get(url).map((r) => {
                    return r.json()
                });
            },

            get(id, filter?) {
                var url = `${self.types[typeName].apiPath}/${id}`;
                url += (filter) ? '?filter=' + encodeURIComponent(JSON.stringify(filter)) : '';
                return self.http.get(url).map((r) => {
                    return r.json()
                });
            },

            getRelation(id, relation) {
                var url = `${self.types[typeName].apiPath}/${id}/${relation}`;
                return self.http.get(url).map((r) => {
                    return r.json()
                });
            },

            post(item) {
                return self.http.post(self.types[typeName].apiPath, item);
            },

            put(item) {
                return self.http.put(self.types[typeName].apiPath + `/${item.id}`, item);
            },

            delete(item) {
                if(typeof(item)==='object'){
                    item = item.id;
                }
                return self.http.delete(self.types[typeName].apiPath + `/${item}`);
            },

            dispense() {
                return new self.types[typeName].doctype;
            }

        }
    }

}

