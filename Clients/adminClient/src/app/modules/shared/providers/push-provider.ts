import { Injectable } from "@angular/core";
import { Subject, Observable, BehaviorSubject } from "rxjs";
import { AppCacheProvider } from "./app-cache-provider";

@Injectable()
export class PushProvider {
    private _connectionId: number;

    private _socket: WebSocket;

    private connecting = false;

    private connected = false;

    private connectingSubject = new Subject<WebSocket>();

    private connectingObservable = this.connectingSubject.asObservable();

    private get socket(): Observable<WebSocket> {
        if (this.connected && this._socket) {
            return Observable.of(this._socket);
        } else if (this.connecting) {
            return this.connectingObservable.first();
        } else {
            this.connect();
            return this.connectingObservable.first();
        }
    }

    private timeoutTimeout: any;

    private keepAliveTimeout: any;

    private lastSeen: Date;

    private _subscribedChannels: string[] = [];

    private _subscribedOnceChannels: string[] = [];

    private messageSubject = new BehaviorSubject<any>(null);

    private _messageObservable = this.messageSubject.asObservable();

    private async connect(): Promise<void> {
        this.connected = false;
        this.connecting = true;
        this._socket = new WebSocket(
            `${window.location.protocol === "http:" ? "ws" : "wss"}://${
                window.location.hostname
            }:3002/?access_token=${encodeURIComponent(
                this.cacheProvider.getToken()
            )}${this._connectionId ? `&reconnect=${this._connectionId}` : ""}`
        );
        this._socket.addEventListener("error", err => {
            if (this.connecting) {
                setTimeout(() => {
                    this.connect();
                }, 5000);
            }
        });
        this._socket.addEventListener("close", ev => {
            if (this.keepAliveTimeout) {
                clearTimeout(this.keepAliveTimeout);
                this.keepAliveTimeout = undefined;
            }
            if (this.timeoutTimeout) {
                clearTimeout(this.timeoutTimeout);
                this.timeoutTimeout = undefined;
            }
            if (this.connected) {
                this.connect();
            }
        });
        this._socket.addEventListener("message", message => {
            this.seen();
            if (message.data === "pong") {
                return;
            }
            if (message.data === "ping") {
                this._socket.send("pong");
                return;
            }
            if (message.data.startsWith("id-")) {
                this._connectionId = message.data.split("-")[1];
                return;
            }
            const parsedMessage = JSON.parse(message.data);
            this._socket.send(`ack-${parsedMessage.id}`);
            this.messageSubject.next(parsedMessage);
        });
        await new Promise((resolve, reject) => {
            this._socket.addEventListener("open", () => {
                this.seen();
                resolve();
            });
        });
        this.connected = true;
        this.connecting = false;
        this.connectingSubject.next(this._socket);
    }

    private disconnect(): void {
        this.connected = false;
        this.connecting = false;
        if (this._socket) {
            try {
                this._socket.close();
            } catch (e) {}
        }
    }

    private seen(): void {
        this.lastSeen = new Date();
        if (this.keepAliveTimeout) {
            clearTimeout(this.keepAliveTimeout);
            this.keepAliveTimeout = undefined;
        }
        if (this.timeoutTimeout) {
            clearTimeout(this.timeoutTimeout);
            this.timeoutTimeout = undefined;
        }
        this.keepAliveTimeout = setTimeout(() => {
            if (
                this.connected &&
                new Date().valueOf() - this.lastSeen.valueOf() > 5000
            ) {
                try {
                    this._socket.send("ping");
                } catch (e) {}
            }
        }, 5000);
        this.timeoutTimeout = setTimeout(() => {
            if (
                this.connected &&
                new Date().valueOf() - this.lastSeen.valueOf() > 10000
            ) {
                this.disconnect();
                this.connect();
            }
        }, 10000);
    }

    public get message(): Observable<any> {
        return this._messageObservable;
    }

    public channels(...names: string[]): Observable<any> {
        return this.message
            .filter(
                message =>
                    message && names.some(channel => channel == message.channel)
            )
            .map(message => message.data);
    }

    constructor(private cacheProvider: AppCacheProvider) {
        console.warn("!!!!!!!!!!!!!!!!!Push provider ctor");
    }

    public unsubscribe(...channels: string[]): void {
        channels.forEach(channel => {
            let index = this._subscribedChannels.indexOf(channel);
            let once = false;
            if (index < 0) {
                index = this._subscribedOnceChannels.indexOf(channel);
                if (index < 0) {
                    return Observable.of(undefined);
                }
                once = true;
            }
            if (once) {
                this._subscribedOnceChannels.splice(index, 1);
            } else {
                this._subscribedChannels.splice(index, 1);
            }
        });

        if (this.connected) {
            try {
                this._socket.send(
                    JSON.stringify({ type: "unsubscribe", channels })
                );
            } catch (e) {}
        }

        if (
            !(
                this._subscribedChannels.length +
                this._subscribedOnceChannels.length
            )
        ) {
            this.disconnect();
        }
    }

    public subscribe(...channels: string[]): Observable<any> {
        const newChannels = channels.filter(
            channel => !this._subscribedChannels.some(c => c == channel)
        );
        if (newChannels.length) {
            newChannels.forEach(channel =>
                this._subscribedChannels.push(channel)
            );
        }
        this.socket.first().subscribe(socket => {
            socket.send(
                JSON.stringify({ type: "subscribe", channels: newChannels })
            );
        });
        return this.channels(...channels);
    }

    public subscribeOnce(...channels: string[]): Observable<any> {
        const newChannels = channels.filter(
            channel => !this._subscribedOnceChannels.some(c => c == channel)
        );
        if (newChannels.length) {
            newChannels.forEach(channel =>
                this._subscribedOnceChannels.push(channel)
            );
            this.socket.first().subscribe(socket => {
                socket.send(
                    JSON.stringify({ type: "subscribe", channels: newChannels })
                );
            });
        }
        return this.channels(...channels)
            .first()
            .do(m => {
                channels.forEach(channel => {
                    const index = this._subscribedOnceChannels.indexOf(channel);
                    if (index >= 0) {
                        this._subscribedOnceChannels.splice(index, 1);
                    }
                });
                if (this.connected) {
                    try {
                        this._socket.send(
                            JSON.stringify({ type: "unsubscribe", channels })
                        );
                    } catch (e) {}
                }
                if (
                    !(
                        this._subscribedChannels.length +
                        this._subscribedOnceChannels.length
                    )
                ) {
                    this.disconnect();
                }
            });
    }
}
