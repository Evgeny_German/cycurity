import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class PreviewProvider {
    constructor(private http: Http) {
    }
    public analyze(token: string, url: string): Observable<any> {
        return this.http.get(`/utils/preview?token=${token}&url=${encodeURIComponent(url)}`).map(result => result.json());
    }
}
