import { Injectable } from "@angular/core";
import {
    Http,
    RequestOptions,
    ResponseContentType,
    Headers
} from "@angular/http";
import * as fileSaver from "file-saver";
import { Observable } from "rxjs/Observable";
import * as UUID from "uuid";

import { ReportDoctype } from "./doctypes/reportDoctype";
import { AppCacheProvider } from "./app-cache-provider";

@Injectable()
export class DocumentProvider {
    public types = {
        report: ReportDoctype
    };

    constructor(private http: Http, private cache: AppCacheProvider) {}

    purge = document => {
        const STORE_DOCUMENTS_URL = `/documentStorage/purge?type=${
            document.type
        }&documentId=${document.documentId}&ownerId=${document.userId}`;
        return this.http.delete(STORE_DOCUMENTS_URL, document);
    };

    store = (document, ownerId, typeId, documentId) => {
        const STORE_DOCUMENTS_URL = `/documentStorage/saveLatest?type=${
            typeId
        }&ownerId=${ownerId}&documentId=${documentId}`;
        return this.http.post(STORE_DOCUMENTS_URL, document);
    };

    get = (ownerId, typeId, documentId) => {
        const STORE_DOCUMENTS_URL = `/documentStorage/getLatest?type=${
            typeId
        }&ownerId=${ownerId}&documentId=${documentId}`;
        return this.http.get(STORE_DOCUMENTS_URL);
    };

    list = (documentType, filter?: object) => {
        const GET_DOCUMENTS_URL = `/documentStorage/search?type=${
            this.types[documentType].typeId
        }`;
        return this.http.get(GET_DOCUMENTS_URL).map(res => {
            return res.json().data.map(x => {
                return x;
            });
        });
    };

    dispense = (doctype, fields?: object) => {
        const newDocument = new this.types[doctype]();
        Object.assign(newDocument, fields);
        newDocument.documentId = UUID();
        newDocument.typeId = this.types[doctype].typeId;
        newDocument.ownerId = this.cache.getIdentity().id;
        return newDocument;
    };
}
