import { Pipe, PipeTransform } from "@angular/core";

export const NumberSymbols = {
    K: 1000,
    M: 1000000,
    G: 1000000000
};

@Pipe({
    name: "parseNumber"
})
export class NumberParserPipe implements PipeTransform {
    private static parseNumber(value: string): number {
        value = value.trim();
        const s = Object.keys(NumberSymbols).map(n => ({
            value: NumberSymbols[n],
            key: n
        }));
        const v = s.find(
            y =>
                value.endsWith(y.key.toUpperCase()) ||
                value.endsWith(y.key.toLowerCase())
        );
        if (v) {
            const m = Number(value.substring(0, value.length - 1));
            return m * v.value;
        }
        return value ? Number(value) : 0;
    }
    transform(value: any, ...args: any[]) {
        if (typeof value === "string") {
            return NumberParserPipe.parseNumber(value);
        }
        return value ? Number(value) : 0;
    }
}

@Pipe({
    name: "formatNumber"
})
export class NumberFormatterPipe implements PipeTransform {
    private static formatNumber(value: number): string {
        const s = Object.keys(NumberSymbols).map(n => ({
            value: NumberSymbols[n],
            key: n
        }));
        if (!value && value !== 0) {
            return "";
        }
        const v = s.findIndex(y => y.value > value);
        const m = v === 0 ? null : s[v > -1 ? v - 1 : 0];
        return m
            ? `${(value / m.value).toFixed(1)}${m.key.toUpperCase()}`
            : `${value}`;
    }
    transform(value: any, ...args: any[]) {
        return NumberFormatterPipe.formatNumber(value);
    }
}
