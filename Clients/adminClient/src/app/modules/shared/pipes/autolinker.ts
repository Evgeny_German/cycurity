import { Pipe, PipeTransform } from '@angular/core';
import * as autolinkerLib from 'autolinker';

@Pipe({name: 'autolinker'})

export class Autolinker implements PipeTransform {
    transform(value: string) {
        return new autolinkerLib({ mention:'twitter', hashtag: 'twitter' }).link(value);
    }
}
