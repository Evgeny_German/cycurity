import {NgModule} from '@angular/core';
import {ConversationMain} from "./conversation.comp";
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../shared/shared.module";
import {ConversationActions} from "./store/conversationActions";
import {ConversationApiService} from "./services/conversation-api.service";
import {ConversationEpics} from "./store/conversationEpics";

import {ProfileBox} from './components/profileBox';
import {RootTweet} from './components/rootTweet';
import {ResponseTweet} from './components/responseTweet';
import {ConversationLegend} from './components/conversationLegend';

import {MetaComment} from './components/editors/metaComment';
import {MetaFoldUnfold} from './components/editors/metaFoldUnfold'
import {MetaAvatarCircle} from './components/editors/metaAvatarCircle'
import {AuthenticationCallbackActivateGuard} from "../shared/auth/auth-guard-provider";
import {MetaAnnotate} from './components/editors/metaAnnotate'
import {MetaCircle} from './components/editors/metaCircle'
import {TweetTemplate} from "./components/tweetTemplate";

export const CONVERSATION_MAIN_URL = 'conversation';

const conversationRoutes: Routes = [
    {
        path: CONVERSATION_MAIN_URL, component: ConversationMain,canActivate:[AuthenticationCallbackActivateGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(conversationRoutes), SharedModule],
    exports: [ConversationMain, ConversationLegend, RootTweet, ResponseTweet, ProfileBox, MetaComment, MetaFoldUnfold, MetaAvatarCircle, MetaAnnotate, MetaCircle, TweetTemplate],
    declarations: [ConversationMain, ConversationLegend, RootTweet, ResponseTweet, ProfileBox, MetaComment, MetaFoldUnfold, MetaAvatarCircle, MetaAnnotate, MetaCircle, TweetTemplate],
    providers: [ConversationActions, ConversationApiService, ConversationEpics]
})
export class ConversationModule {
}
