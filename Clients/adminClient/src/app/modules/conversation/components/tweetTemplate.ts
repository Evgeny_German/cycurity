import {
    AfterContentInit, Component, ContentChildren, ElementRef, Input, QueryList,
    SecurityContext
} from "@angular/core";
import { DomSanitizer } from '@angular/platform-browser';

export type tweetNode = { type: 'text' | 'link' | 'hashtag' | 'mention', text: string };

@Component({
    selector: 'tweet-template',
    template: `
        <ng-container *ngFor="let node of nodes">
            <ng-container [ngSwitch]="node.type">
                <ng-container *ngSwitchCase="'text'">
                    {{node.text}}
                </ng-container>
                <a *ngSwitchCase="'link'" [href]="node['href']">
                    {{node.text}}
                </a>
                <a *ngSwitchCase="'hashtag'" [href]="hashtag(node['href'])">
                    {{node.text}}
                </a>
                <a *ngSwitchCase="'mention'" [href]="user(node['href'])">
                    {{node.text}}
                </a>
            </ng-container>
        </ng-container>
        <div>
            <div class="tweetMedia" *ngFor="let media of tweet.tweetMedia" style="position: relative;">
                <img style="width: 100%; " [src]="media.src"/>
                <a
                    *ngIf="media.type === 'video'"
                    target="_blank"
                    [href]="tweetUrl"
                    style="position: absolute; left:50%; top:50%;margin-top:-70px;margin-left:-45px;display: block;color:#fff;font-size:100px;opacity:0.7;"
                >
                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <link-preview *ngFor="let url of previews" [data]="url"></link-preview>

        <div *ngIf="tweet.quote" class="quote-box card mt-2 p-2">
            <a class="user" target="_blank" href="http://www.twitter.com/{{tweet.quote.userName}}">{{tweet.quote.fullName}}</a>
            <small>@{{tweet.quote.userName}}</small>
            <span class="text">{{tweet.quote.tweetText}}</span>
            <ng-container *ngIf="tweet.quote.videoPreview"><img style="width:100%;" src="{{tweet.quote.videoPreview}}"></ng-container>
            <ng-container *ngIf="tweet.quote.imagePreview"><img style="width:100%;" src="{{tweet.quote.imagePreview}}"></ng-container>
            <!--{{tweet.quote.fullName}}-->
        </div>


    `
})
export class TweetTemplate{

    private tweetUrl: string;

    private previews: any[];

    private _tweet: any;

    @Input()
    public get tweet(): any {
        return this._tweet;
    }

    constructor(private domSanitizer:DomSanitizer){}

    public set tweet(value: any) {
         if(value.tweetMedia) value.tweetMedia.forEach((m)=>{
             m.src = m.src.replace(/"/g,'');
         });

        //console.warn(value.tweetMedia)

        let prevValue = this._tweet;
        this._tweet = value;

        if (value && prevValue !== value) {
            this.tweetUrl = `https://twitter.com/${encodeURIComponent(value.userName)}/status/${value.tweetId}`;
            this.nodes = value.tweetData;
            this.previews = value.linkPreviews;
        }
    }

    private nodes: tweetNode[];

    private hashtag(href: string): string {
        return `https://twitter.com/hashtag/${encodeURIComponent(href)}?src=hash`;
    }

    private user(href: string): string {
        return `https://twitter.com/${encodeURIComponent(href)}`;
    }

}
