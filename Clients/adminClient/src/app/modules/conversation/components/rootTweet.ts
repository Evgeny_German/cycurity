import {Component, Input, OnInit} from '@angular/core';
import {CircleTypes} from './editors/metaAvatarCircle';
import {AnnotationTypes} from './editors/metaAnnotate';
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";
import * as _ from 'lodash';

@Component({
    selector: 'rootTweet',
    styles: [`
        .tweet-container {
            margin: 20px 0 20px 0;
        }

        .media {
            padding: 15px 0 0 0;
            /*font-size: 1.5em;*/
        }

        img.avatar {
            width: 48px;
            height: 48px;
            border-radius: 50%;
            border: 4px solid #fff;
            z-index: 1;
            box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.4);
        }

        .user {
            font-weight: bold;
        }

        .statistics div {
            white-space: nowrap;
            padding-right: 70px;
        }

        .statistics span {
            font-weight: bold;
        }

        .metaFoldUnfold {
            position: absolute;
            top: 7px;
            right: 10px;
            padding: 1px 4px;
            font-size: 11px;
            z-index: 1;
        }

        .metaAvatarCircle {
            position: absolute;
            top: 11px;
            left: 7px;
            padding: 1px 4px;
            font-size: 11px;
        }

        .metaAnnotate {
            position: absolute;
            top: 11px;
            left: -18px;
            padding: 1px 4px;
            font-size: 11px;
        }

        .annotationContainer {
            position: absolute;
            top: 0;
            left: 0px;
            width: 20px;
            bottom: 0;
        }

        .annotation {
            color: #fff;
            width: 20px;
            display: inline-block;
            position: relative;
            float: left;
            border-right: 1px solid rgba(255, 255, 255, 0.2);
            overflow: hidden;
        }

        .annotation .label {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .annotation .label span {
            position: absolute;
            bottom: -0.5em;
            left: -1px;
            display: inline-block;
            transform-origin: top left;
            transform: rotate(-90deg);
            -webkit-transform: rotate(-90deg);
        }

        /*
        .annotation .label {
            border:1px solid #f00;
            position:absolute;
            bottom:30px;
            left:-17px;
            right:-17px;
            line-height:1em;
            overflow:hidden;
            transform: rotate(-90deg); !* added translateX *!
            -webkit-transform: rotate(-90deg);

        }
        */

        .highlight {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-color: rgba(247, 246, 54, 0.58);
        }


        .threeChar{
            position: absolute;
            bottom: 5px;
            line-height: 1em;
            overflow: hidden;
            transform: rotate(-90deg); /* added translateX */
            -webkit-transform: rotate(-90deg);
            font-family: monospace;
            width: 1ch;
            white-space: nowrap;
            left: 6px;
        }

        .timeSpan {
            display: inline-flex;
            font-size: 0.8125rem;
        }
        .quote-box{
            display:block;
            margin:20px 10px;
            padding:5px;
            border:1px solid rgba(0,0,0,0.1);
            border-radius: 5px;
        }
        .quote-box .text{
            display:block
        }

        .dont-break-out-of {
            overflow-wrap: break-word;
            word-wrap: break-word;
            -ms-word-break: break-all;
            word-break: break-all;
            word-break: break-word;
            -ms-hyphens: auto;
            -moz-hyphens: auto;
            -webkit-hyphens: auto;
            hyphens: auto;
        }
    `],
    template: `

        <div *ngIf="editMode||(!editMode && !tweet.meta.folded)" class="tweet-container wBox relative"
             [ngClass]="{'eBox': this.editMode}">

            <div class="row" style="margin-right:0px;margin-left:0px;">

            <metaFoldUnfold class="metaFoldUnfold" [binding]="tweet" [editMode]="editMode"></metaFoldUnfold>
            <!--<a *ngIf="editMode" style="" href="javascript:;" (click)="tweet.meta.folded = !tweet.meta.folded" class="btn btn-sm btn-primary"><i class="fa" [ngClass]="{'fa-caret-down':tweet.meta.folded,'fa-caret-up':!tweet.meta.folded}"></i></a>-->

            <div *ngIf="tweet.meta.folded" style="padding:10px;">Hidden Tweet</div>


            <div *ngIf="tweet.meta.highlight" class="highlight"></div>

            <div *ngIf="!tweet.meta.folded" class="col-md-{{(editMode || lengthOfComment(tweet.meta.comment||'')>0)?8:12}}">


                <div class="media">
                    <img class="avatar d-flex mr-3"
                         [ngStyle]="{'border-color':circleTypes[tweet.meta.circle||'CIRCLE_NONE'].color}"
                         src="{{image(tweet.userName, 'avatar')}}"
                         alt=""><!-- "https://abs.twimg.com/sticky/default_profile_images/default_profile_bigger.png" -->

                    <metaAvatarCircle class="metaAvatarCircle" [binding]="tweet"
                                      [editMode]="editMode"></metaAvatarCircle>

                    <metaAnnotate class="metaAnnotate" [binding]="tweet" [editMode]="editMode"></metaAnnotate>

                    <!--                    <div
                                            *ngIf="tweet.meta.annotation && tweet.meta.annotation!='ANNOTATION_NONE'"
                                            class="annotation"
                                            [ngStyle]="{'background-color':annotationTypes[tweet.meta.annotation].color}"
                                        >
                                            <div class="label">{{ annotationTypes[tweet.meta.annotation].name }}</div>
                                        </div>-->

                    <div class="annotationContainer">
                        <span *ngFor="let annotation of annotationTypes">
                        <div
                            *ngIf="tweet.meta.annotation && tweet.meta.annotation[annotation.id]==true"
                            class="annotation"
                            [ngStyle]="{'background-color':annotation.color, height:widthOfAnnotation()+'%'}"
                        >

                            <div class="label" [ngClass]="{'label': widthOfAnnotation() == '100', 'threeChar': widthOfAnnotation() < '100'}"><span>{{annotation.name}}</span></div>
                        </div>
                        </span>
                    </div>


                    <div class="media-body">
                        <a class="user" target="_blank" href="http://www.twitter.com/{{tweet.userName}}">{{tweet.fullName}}</a>
                        @{{tweet.userName}}
                        - <span class="timeSpan">{{tweet.timestamp | date:'MMM dd, yyyy, HH:mm'}}</span>
                        <p class="dont-break-out-of">
                            <tweet-template [tweet]="tweet"></tweet-template>

                            <span class="row statistics">
                                <div class="col-md-2">
                                    <metaCircle [circleId]="'replyCount'" [binding]="tweet.meta" [editMode]="editMode">
                                        <i class="fa fa-comment-o"></i> <span>{{tweet.replyCount | parseNumber | formatNumber}}</span>
                                    </metaCircle>
                                </div>
                                <div class="col-md-2">
                                    <metaCircle [circleId]="'retweetCount'" [binding]="tweet.meta"
                                                [editMode]="editMode">
                                        <i class="fa fa-retweet"></i> <span>{{tweet.retweetCount | parseNumber | formatNumber}}</span>
                                    </metaCircle>
                                </div>
                                <div class="col-md-2">
                                    <metaCircle [circleId]="'favoriteCount'" [binding]="tweet.meta"
                                                [editMode]="editMode">
                                        <i class="fa fa-heart-o"></i> <span>{{tweet.favoriteCount | parseNumber | formatNumber}}</span>
                                    </metaCircle>
                                </div>
                            </span>
                        </p>

<!--
                        &lt;!&ndash; Quote box &ndash;&gt;
                        <div *ngIf="tweet.quote" class="quote-box">
                            <a class="user" target="_blank" href="http://www.twitter.com/{{tweet.quote.userName}}">{{tweet.quote.fullName}}</a>
                             <small>@{{tweet.quote.userName}}</small>
                             <span class="text">{{tweet.quote.tweetText}}</span>
                            &lt;!&ndash;{{tweet.quote.fullName}}&ndash;&gt;
                        </div>
-->
                    </div>
                </div>

            </div>
            <ng-container *ngIf="(editMode || lengthOfComment(tweet.meta.comment||'')>0)">
                <div *ngIf="!tweet.meta.folded" class="col-4 rootTweetComment" style="padding-top:43px;padding-bottom:10px;">
                    <metaComment [editMode]="editMode" [binding]="tweet"></metaComment>
                </div>
            </ng-container>
            </div>

        </div>
    `
})
export class RootTweet implements OnInit {

    image(
        userName: string,
        type:
            | "avatar"
            | "profile_image"
            | "profile_background_image"
            | "profile_image_https"
            | "profile_background_image_https"
    ): string {
        return `getProfileImage?username=${encodeURIComponent(
            `${userName.toLowerCase()} ${type}`
        )}`;
    }


    @Input()
    public tweet: any;

    circleTypes: any;
    annotationTypes: any;

    @Input() public profile: any;
    @Input() public editMode: any;

    constructor(private sanitizer: DomSanitizer) {
    }

    public get profileBannerUrl(): SafeUrl {
        return this.profile && this.profile.profile_banner_url ?
            this.sanitizer.bypassSecurityTrustStyle(`url('${this.profile.profile_banner_url}')`)
            :
            null
            ;
    }

    lengthOfComment(html){
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent.trim().length || tmp.innerText.trim().length || 0;
    }

    ngOnInit() {
        this.circleTypes = _.keyBy(CircleTypes, 'id');
        this.annotationTypes = AnnotationTypes;//_.keyBy(AnnotationTypes, 'id');
    }

    widthOfAnnotation() {
        const metaObject = (this.tweet.meta || {}).annotation || {};

        var rt = 100 / Object.keys(
                metaObject
            ).filter(
                (k) => {
                    return metaObject[k]
                }
            ).length;

        return rt;
    }
}
