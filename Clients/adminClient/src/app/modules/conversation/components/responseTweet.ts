import {Component, Input, OnInit} from '@angular/core';
import {CircleTypes} from './editors/metaAvatarCircle';
import {AnnotationTypes} from './editors/metaAnnotate';
import * as _ from 'lodash';
import {PreviewProvider} from "../../shared/providers/previewProvider";
import * as Autolinker from 'autolinker';

@Component({
    selector: 'responseTweet',
    styles: [`
        .media {
            padding: 15px;
        }

        div.avatarContainer {
            width: 52px;
            height: 52px;
            padding:4px;
            border-radius:50%;
            overflow:hidden;
            // border: 4px solid #fff;
            box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.4);
        }

        img.avatar {
            width: 44px;
            height: 44px;
            border-radius:50%;
            /*
            border: 4px solid #fff;
            box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.4);
            */
        }

        .user {
            font-weight: bold;
        }

        .statistics span {
            font-weight: bold;
        }

        .statistics div {
            white-space: nowrap;
        }

        .metaFoldUnfold {
            position: absolute;
            top: 7px;
            right: 10px;
            padding: 1px 4px;
            font-size: 11px;
            z-index: 1;
        }

        .metaAvatarCircle {
            position: absolute;
            top: 11px;
            left: 23px;
            padding: 1px 4px;
            font-size: 11px;
        }

        .metaAnnotate {
            position: absolute;
            top: 11px;
            left: -2px;
            padding: 1px 4px;
            font-size: 11px;
        }

        .annotationContainer {
            position: absolute;
            top: 0;
            left: 15px;
            width: 20px;
            bottom: 0;
        }

        .annotation {
            color: #fff;
            width: 20px;
            display: inline-block;
            position: relative;
            float: left;
            border-right: 1px solid rgba(255, 255, 255, 0.2);
            overflow: hidden;
        }

        .annotation .label {
            position: absolute;
            bottom: 35px;
            left: -14px;
            line-height: 1em;
            transform: rotate(-90deg); /* added translateX */
            -webkit-transform: rotate(-90deg);
            transform-origin: 30px 100%;
        }

        .annotation .threeChar {
            position: absolute;
            bottom: 5px;
            line-height: 1em;
            overflow: hidden;
            transform: rotate(-90deg); /* added translateX */
            -webkit-transform: rotate(-90deg);
            font-family: monospace;
            width: 1ch;
            white-space: nowrap;
            left: 6px;

        }

        .highlight {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-color: rgba(247, 246, 54, 0.58);
        }

        .timeSpan {
            display: inline-flex;
            font-size: 0.8125rem;
        }

        .dont-break-out-of {
            overflow-wrap: break-word;
            word-wrap: break-word;
            -ms-word-break: break-all;
            word-break: break-all;
            word-break: break-word;
            -ms-hyphens: auto;
            -moz-hyphens: auto;
            -webkit-hyphens: auto;
            hyphens: auto;
        }
    `],
    template: `
        <div *ngIf="editMode||(!editMode && !tweet.meta.folded)" class="wBox relative"
             [ngClass]="{'eBox': this.editMode}" style="margin-bottom:6px;">

            <metaFoldUnfold class="metaFoldUnfold" [binding]="tweet" [editMode]="editMode"></metaFoldUnfold>

            <div *ngIf="tweet.meta.folded" style="padding:10px;">Hidden Tweet</div>

            <div class="row" *ngIf="!tweet.meta.folded">

                <div *ngIf="tweet.meta.highlight" class="highlight"></div>

                <div class="col-md-{{(editMode || (tweet.meta.comment && lengthOfComment(tweet.meta.comment||'')>0))?8:12}}">

                    <div class="media">

                        <div
                            class="avatarContainer d-flex mr-3"
                            style="z-index:2;"
                            [ngStyle]="{'background-color':circleTypes[tweet.meta.circle||'CIRCLE_NONE'].color}"
                        >
                            <img
                                class="avatar"
                                src="{{image(tweet.userName, 'avatar')}}"
                                alt=""
                            />
                        </div>

                        <metaAvatarCircle class="metaAvatarCircle" [binding]="tweet"
                                          [editMode]="editMode"></metaAvatarCircle>

                        <metaAnnotate class="metaAnnotate" [binding]="tweet" [editMode]="editMode"></metaAnnotate>

                        <!--                        <div
                                                    *ngIf="tweet.meta.annotation && tweet.meta.annotation!='ANNOTATION_NONE'"
                                                    class="annotation"
                                                    [ngStyle]="{'background-color':annotationTypes[tweet.meta.annotation].color}"
                                                >
                                                    <div class="label">{{ annotationTypes[tweet.meta.annotation].name }}</div>
                                                </div>-->

                        <div class="annotationContainer">
                            <span *ngFor="let annotation of annotationTypes">
                                <div
                                    *ngIf="tweet.meta.annotation && tweet.meta.annotation[annotation.id]==true"
                                    class="annotation"
                                    [ngStyle]="{'background-color':annotation.color, height:widthOfAnnotation()+'%'}"
                                >
                                    <div
                                        [ngClass]="{'label': widthOfAnnotation() == '100', 'threeChar': widthOfAnnotation() < '100'}">{{annotation.name}}</div>
                                </div>
                            </span>
                        </div>

                        <div class="media-body">
                            <a target="blank" class="user" href="http://www.twitter.com/{{tweet.userName}}">{{tweet.fullName}}</a>
                            @{{tweet.userName}}
                            - <span class="timeSpan">{{tweet.timestamp | date:'MMM dd, yyyy, HH:mm'}}</span>
                            <p style="margin-bottom:3px;" class="dont-break-out-of">
                                <tweet-template [tweet]="tweet"></tweet-template>
                            </p>
                            <div class="row statistics">
                                <div class="col-md-2">
                                    <metaCircle [circleId]="'replyCount'" [binding]="tweet.meta" [editMode]="editMode">
                                        <i class="fa fa-comment-o"></i> <span>{{tweet.replyCount | parseNumber | formatNumber}}</span>
                                    </metaCircle>
                                </div>
                                <div class="col-md-2">
                                    <metaCircle [circleId]="'retweetCount'" [binding]="tweet.meta"
                                                [editMode]="editMode">
                                        <i class="fa fa-retweet"></i> <span>{{tweet.retweetCount | parseNumber | formatNumber}}</span>
                                    </metaCircle>
                                </div>
                                <div class="col-md-2">
                                    <metaCircle [circleId]="'favoriteCount'" [binding]="tweet.meta"
                                                [editMode]="editMode">
                                        <i class="fa fa-heart-o"></i> <span>{{tweet.favoriteCount | parseNumber | formatNumber}}</span>
                                    </metaCircle>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <ng-container *ngIf="(editMode || lengthOfComment(tweet.meta.comment||'')>0)">
                    <div class="col-md-4" style="padding:23px 30px 10px 10px;">
                        <metaComment [editMode]="editMode" [binding]="tweet"></metaComment>
                    </div>
                </ng-container>
            </div>
        </div>

    `
})
export class ResponseTweet implements OnInit {

    image(
        userName: string,
        type:
            | "avatar"
            | "profile_image"
            | "profile_background_image"
            | "profile_image_https"
            | "profile_background_image_https"
    ): string {
        if (!userName) return null;
        return `getProfileImage?username=${encodeURIComponent(
            `${userName.toLowerCase()} ${type}`
        )}`;
    }

    circleTypes: any;
    annotationTypes: any;

    @Input()
    public tweet: any;

    @Input() public editMode: any;

    constructor() {
    }

    lengthOfComment(html){
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent.trim().length || tmp.innerText.trim().length || 0;
    }

    ngOnInit() {
        this.circleTypes = _.keyBy(CircleTypes, 'id');
        this.annotationTypes = AnnotationTypes;//_.keyBy(AnnotationTypes, 'id');

    }

    widthOfAnnotation() {
        const metaObject = (this.tweet.meta || {}).annotation || {};

        var rt = 100 / Object.keys(
                metaObject
            ).filter(
                (k) => {
                    return metaObject[k]
                }
            ).length;

        return rt;
    }

}
