import {Component, Input, OnInit} from '@angular/core';
import {CircleTypes} from './editors/metaAvatarCircle';
import {AnnotationTypes} from './editors/metaAnnotate';
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";
import * as _ from 'lodash';

@Component({
    selector: 'conversationLegend',
    styles: [`
        .legend-container {
            margin: 20px 0 0 0;
            padding-top:20px;
            padding-bottom:20px;
        }

        .legendItem{
            position:relative;
            display:inline-block;
            padding-right:20px;
            width:110px;
        }

        .annotation {
            width: 18px;
            height: 20px;
            display:inline-block;
            border-top:1px solid #ccc;
            border-right:1px solid #ccc;
            border-bottom:1px solid #ccc;
            border-left:5px solid #fff;
            position:relative;
            top:4px;
            box-shadow:1px 1px 10px rgba(0,0,0,0.3);
        }
        
        .circle {
            width: 18px;
            height: 20px;
            display:inline-block;
            border:3px solid #fff;
            border-radius:50%;
            position:relative;
            top:4px;
            box-shadow:1px 1px 10px rgba(0,0,0,0.3);
        }

        label{
            display:inline-block;
            padding-left:30px;
            font-weight:bold;
        }
        
        .translucent{
            opacity:0.4;
        }
        div{
        }
    `],
    template: `

        <div class="hidden-xs-down legend-container container wBox relative" [ngClass]="{'translucent': this.editMode}" style="min-width:100%;padding-top:10px;padding-bottom:5px;">

            <div class="row">

                <div style="width:100%;">

                    <div class="row" style="white-space:nowrap;">
                        <label class="col-3">Annotations</label>
                        <div class="col-9">
                            <div class="legendItem" *ngFor="let annotation of annotationTypes">
                                <div class="annotation" [ngStyle]="{'border-left-color': annotation.color}"></div>
                                {{annotation.name}}
                            </div>
                        </div>
                    </div>

                    <div class="row" style="white-space:nowrap;">
                        <label class="col-3">Image Borders</label>
                        <div class="col-9">
                            <div class="legendItem" *ngFor="let circle of circleTypes">
                                <div class="circle" [ngStyle]="{'border-color': circle.color}"></div>
                                {{circle.name}}
                            </div>
                        </div>
                    </div>
                   
                </div>

            </div>
        </div>
    `
})
export class ConversationLegend implements OnInit {

    @Input()
    public tweet: any;

    circleTypes: any;
    annotationTypes: any;

    @Input() public editMode: any;

    constructor(private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        this.circleTypes = CircleTypes;
        this.annotationTypes = AnnotationTypes;
    }
}
