import {Component, Input, OnInit, Pipe, PipeTransform} from '@angular/core';
import {NgRedux} from "@angular-redux/store";
import {IAppState, ISearchResults} from "../../../shared/store/store";
import {ActivatedRoute, Router} from "@angular/router";
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";

@Component({
    selector: 'metaCircle',
    styles: [`
        .circle {
            border: 2px solid #f00;
            border-radius:50%;
            position:absolute;
            top:-5px;
            right:10px;
            bottom:-5px;
            left:10px;
        }

        .btn {
            position: absolute;
            top: 0;
            left: 0;
            padding: 0 2px 0 2px;
            font-size: 15px;
        }
    `],
    template: `
        <div style="display:inline-block;">
            <div *ngIf="binding.circles[circleId]" class="circle"></div>
            <button
                (click)="toggleCircle()"
                *ngIf="editMode"
                style="z-index:3;"
                class="btn btn-sm btn-primary"
                type="button">
                    <i class="fa fa-circle-o"></i>
            </button>
            <ng-content></ng-content>
        </div>
    `
})
export class MetaCircle implements OnInit {

    @Input() public binding: any;
    @Input() public editMode: any;
    @Input() public circleId: string;

    constructor(private redux: NgRedux<IAppState>,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        if (!this.binding.circles) {
            this.binding.circles = {};
        }
    }

    toggleCircle() {
        this.binding.circles[this.circleId] = !this.binding.circles[this.circleId];
    }
}
