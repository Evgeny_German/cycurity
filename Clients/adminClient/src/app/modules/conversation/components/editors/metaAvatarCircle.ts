import {Component, Input, OnInit, Pipe, PipeTransform} from '@angular/core';
import {NgRedux} from "@angular-redux/store";
import {IAppState, ISearchResults} from "../../../shared/store/store";
import {ActivatedRoute, Router} from "@angular/router";
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";

export const CircleTypes = [
    {id:'CIRCLE_NONE',  color: '#fff',       name:'None'},
    {id:'CIRCLE_US',    color: '#314778',       name:'Us'},
    {id:'CIRCLE_PKOL',  color: '#569fbe',    name:'PKOL'},
    {id:'CIRCLE_KOL',   color: '#45c462',    name:'KOL'}
];

@Component({
    selector: 'metaAvatarCircle',
    styles:[`
        .circle {
            display:inline-block;
            width:20px;
            height:20px;
            border-width:2px;
            border-style:solid;
            border-radius:50%;
            position:relative;
            top:4px;
            box-shadow:1px 1px 4px rgba(0,0,0,0.2);
        }
        .btn{
            padding:0 3px 0 0;
            font-size:15px;
        }
    `],
    template: `
        <div *ngIf="editMode" class="dropdown">
            <button style="z-index:3;" class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"></button>
            <div class="dropdown-menu">
                <div *ngFor="let circle of CircleTypes">
                    <a (click)="setColor(circle)" class="dropdown-item" href="javascript:;">
                        <div class="circle" [ngStyle]="{'border-color': circle.color}"></div>
                        {{circle.name}}
                    </a>
                </div>
            </div>
        </div>        
    `
})
export class MetaAvatarCircle implements OnInit {

    @Input() public binding:any;
    @Input() public editMode:any;

    public CircleTypes = CircleTypes;

    constructor(
        private redux: NgRedux<IAppState>,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private sanitizer: DomSanitizer
    ){
    }

    ngOnInit() {
    }

    setColor(circle) {
        this.binding.meta.circle = circle.id;
    }
}
