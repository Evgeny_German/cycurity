import {Component, Input, OnInit, Pipe, PipeTransform} from '@angular/core';
import {NgRedux} from "@angular-redux/store";
import {IAppState, ISearchResults} from "../../../shared/store/store";
import {ActivatedRoute, Router} from "@angular/router";
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";

export const AnnotationTypes = [
    {id: 'ANNOTATION_CONTACTED', color: '#314778', name: 'Contacted'},
    {id: 'ANNOTATION_ENGAGED', color: '#569fbe', name: 'Engaged'},
    {id: 'ANNOTATION_TARGETED', color: '#16dbd8', name: 'Targeted'},
    {id: 'ANNOTATION_INFLUENCED', color: '#45c462', name: 'Influenced'}
];

@Component({
    selector: 'metaAnnotate',
    styles: [`
        .annotation {
            display: inline-block;
            width: 20px;
            height: 20px;
            border-width: 0 0 0 4px;
            border-style: solid;
            position: relative;
            top: 4px;
            box-shadow: 1px 1px 4px rgba(0, 0, 0, 0.2);
        }

        .highlight .checkbox_container {
            padding-left: 4px;
            display: inline-block;
            width: 20px;
            height: 20px;
            position: relative;
            top: 4px;
            box-shadow: 1px 1px 4px rgba(0, 0, 0, 0.2);
        }

        .btn {
            padding: 0 3px 0 0;
            font-size: 15px;
        }

        .highlight span {
            position: relative;
            top: 5px;
            background-color: #f7f636;
        }
    `],
    template: `
        <div *ngIf="editMode" class="dropdown">
            <button style="z-index:3;" class="btn btn-sm btn-primary dropdown-toggle" type="button"
                    id="dropdownMenuButton" data-toggle="dropdown"></button>
            <div class="dropdown-menu">
                <div *ngFor="let annotation of AnnotationTypes">
                    <a (click)="toggleAnnotation(annotation)" class="dropdown-item" href="javascript:;">
                        <div class="annotation checkbox_container" [ngStyle]="{'border-color': annotation.color}">
                            <i *ngIf="(binding.meta.annotation||{})[annotation.id]" class="fa fa-check"></i>
                        </div>

                        {{annotation.name}}
                    </a>
                </div>
                <hr/>
                <div>
                    <a (click)="highlightToggle()" class="dropdown-item highlight" href="javascript:;">
                        <div class="checkbox_container">
                            <i *ngIf="binding.meta.highlight" class="fa fa-check"></i>
                        </div>
                        <span>Highlight</span>
                    </a>
                </div>
            </div>
        </div>
    `
})
export class MetaAnnotate implements OnInit {

    @Input() public binding: any;
    @Input() public editMode: any;

    public AnnotationTypes = AnnotationTypes;

    constructor(private redux: NgRedux<IAppState>,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
    }

    setColor(annotation) {
        this.binding.meta.annotation = annotation.id;
    }

    highlightToggle() {
        this.binding.meta.highlight = !this.binding.meta.highlight;
    }

    toggleAnnotation(annotation) {
        var _a = this.binding.meta.annotation||{};
        _a[annotation.id]=!(_a[annotation.id]);
        this.binding.meta.annotation = _a;
    }
}
