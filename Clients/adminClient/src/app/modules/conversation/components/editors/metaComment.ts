import {Component, Input, OnInit, Pipe, PipeTransform} from '@angular/core';
import {NgRedux} from "@angular-redux/store";
import {IAppState, ISearchResults} from "../../../shared/store/store";
import {ActivatedRoute, Router} from "@angular/router";
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";

@Component({
    selector: 'metaComment',
    styles:[`
        .stickyNote{
            /*background-color:#f7ecb5;*/
            background-color:#f5f5f5;
            position:relative;
            box-shadow:0px 0px 5px rgba(0,0,0,0.2);
            word-wrap: break-word;
            border:1px solid #ddd;

        }

    `],
    template: `
        <div *ngIf="editMode"><htmlEditor [binding]="binding.meta" [field]="'comment'"></htmlEditor></div>
        <ng-container *ngIf="!editMode">
            <div class="stickyNote">
                <div class="form-control" style="padding:15px;line-height:1em;min-width:100%;min-height:20px;height:100%;" [innerHTML]="binding.meta.comment"></div>
            </div>
        </ng-container>
    `
})
export class MetaComment implements OnInit {

    @Input() public binding:any;
    @Input() public editMode:any;

    constructor(
        private redux: NgRedux<IAppState>,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private sanitizer: DomSanitizer
    ){
    }

    ngOnInit() {
            if( this.binding.meta.comment == undefined || this.binding.meta.comment == "undefined" || this.binding.meta.comment == null ){
                this.binding.meta.comment = "";
            }

    }

}
