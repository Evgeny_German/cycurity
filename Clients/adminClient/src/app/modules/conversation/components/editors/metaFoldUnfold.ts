import {Component, Input, OnInit, Pipe, PipeTransform} from '@angular/core';
import {NgRedux} from "@angular-redux/store";
import {IAppState, ISearchResults} from "../../../shared/store/store";
import {ActivatedRoute, Router} from "@angular/router";
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";

@Component({
    selector: 'metaFoldUnfold',
    styles: [`
        a {
            padding: 1px 4px;
            font-size: 15px;
            z-index: 99999;
        }
    `],
    template: `
        <a *ngIf="editMode" href="javascript:;" (click)="foldUnfold()" class="btn btn-sm btn-primary">
            <i class="fa" [ngClass]="{'fa-caret-down':binding.meta.folded,'fa-caret-up':!binding.meta.folded}"></i>
        </a>
    `
})
export class MetaFoldUnfold implements OnInit {

    @Input() public binding: any;
    @Input() public editMode: any;

    constructor(private redux: NgRedux<IAppState>,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
    }

    foldUnfold() {
        this.binding.meta.folded = !this.binding.meta.folded;
    }
}
