import { Component, Input, OnInit } from "@angular/core";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import * as _ from "lodash";
import { CircleTypes } from "./editors/metaAvatarCircle";

const extrasIcons = {
    location: "map-marker",
    url: "link",
    joinDate: "calendar",
    birthdate: "calendar"
};

@Component({
    selector: "profileBox",
    providers: [],
    styles: [
        `
        .profile-container {
            padding: 10px 10px;
        }

        .profile-header {
            padding: 15px;
            background-size: cover;
        }

        .profile-header .avatarContainer{
            display:inline-block;
            width: 110px;
            height:110px;
            background-size:cover;
            border-radius: 50%;
            border: 6px solid #fff;
            box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.4);
        }

        a {
            font-weight: bold;
        }

        .statistic {
            text-align: center;
            white-space:nowrap;
        }

        .statistic span {
            font-weight: bold;
        }

        .metaFoldUnfold {
            position: absolute;
            top: 7px;
            right: 6px;
            padding: 1px 4px;
            font-size: 11px;
            z-index: 1;
        }

        .extraData i {
            display: inline-block;
            width: 24px;
        }

        .extraData th {
            padding-right: 15px;
        }

        .metaAvatarCircle {
            position: absolute;
            top: 21px;
            left: 23px;
            padding: 1px 4px;
            font-size: 11px;
        }
    `
    ],
    template: `
        <div *ngIf="editMode||(!editMode && !profile.meta.folded)" class="profile-container wBox relative"
             [ngClass]="{'eBox': this.editMode}">
            <metaFoldUnfold class="metaFoldUnfold" [binding]="profile" [editMode]="editMode"></metaFoldUnfold>
            <div *ngIf="profile.meta.folded">Hidden Profile</div>

            <div *ngIf="!profile.meta.folded">

                <div style="margin:-10px -10px 0px -10px;" class="profile-header" [style.background-image]="profileBannerUrl" [style.background-color]="'#1DA1F2'">
                    <metaAvatarCircle class="metaAvatarCircle" [binding]="profile" [editMode]="editMode"></metaAvatarCircle>
                    <a
                        class="avatarContainer"
                        target="_blank"
                        href="http://www.twitter.com/{{profile.screen_name}}"
                        [style.background-image]="profileImageUrl"
                        [ngStyle]="{'border-color':circleTypes[profile.meta.circle||'CIRCLE_NONE'].color}">
                    </a>
                </div>
                <div class="row">
                    <h5 class="col-md-12" style="margin-top:8px;">
                        <a target="_blank" href="http://www.twitter.com/{{profile.screen_name}}">{{profile.name}}</a>
                        <a target="_blank" href="http://www.twitter.com/{{profile.screen_name}}">
                            @{{profile.screen_name}}
                        </a>
                    </h5>
                </div>
                <div class="row">
                    <div class="col-md-{{(editMode || lengthOfComment(profile.meta.comment||'')>0)?8:12}}"
                         style="padding:0px 20px">
                        <p style="overflow-wrap: break-word;word-wrap: break-word;" [innerHtml]="profile.description|autolinker"></p>

                        <div class="extraData">
                            <div *ngFor="let extra of profile.extras">
                                <span style="position:relative;display:inline-block;padding:0px 26px 0px 26px;margin:6px 0 6px -6px;" *ngIf="extrasIcons[extra.key] && extra.value">
                                    <metaCircle [circleId]="extra.key" [binding]="profile.meta" [editMode]="editMode">
                                        <b style="inline-block;margin-right:10px;">
                                            <i class="fa fa-{{extrasIcons[extra.key]}}"></i>{{toStartCase(extra.key)}}
                                        </b>
                                        <span *ngIf="extrasIcons[extra.key] && extra.value" [innerHtml]="extra.value|autolinker"></span>
                                    </metaCircle>
                                </span>
                            </div>
                        </div>

                        <hr/>
                        <span class="row" style="flex-wrap: nowrap;">

                            <div class="col-sm-3 statistic">
                                <small>Tweets</small>

                                <metaCircle [circleId]="'statuses_count'" [binding]="profile.meta"
                                            [editMode]="editMode">
                                    <span>{{profile.statuses_count | parseNumber | formatNumber}}</span>
                                </metaCircle>

                            </div>

                            <div class="col-sm-3 statistic">
                                <small>Following</small>

                                <metaCircle [circleId]="'friends_count'" [binding]="profile.meta" [editMode]="editMode">
                                    <span>{{profile.friends_count  | parseNumber | formatNumber}}</span>
                                </metaCircle>

                            </div>

                            <div class="col-sm-3 statistic">
                                <small>Followers</small>

                                <metaCircle [circleId]="'followers_count'" [binding]="profile.meta"
                                            [editMode]="editMode">
                                    <span>{{profile.followers_count | parseNumber | formatNumber}}</span>
                                </metaCircle>

                            </div>


                            <div class="col-sm-3 statistic">
                                <small>Likes</small>

                                <metaCircle [circleId]="'favourites_count'" [binding]="profile.meta"
                                            [editMode]="editMode">
                                    <span>{{profile.favourites_count | parseNumber | formatNumber}}</span>
                                </metaCircle>

                            </div>
                        </span>
                    </div>
                    <div *ngIf="(editMode || lengthOfComment(profile.meta.comment||'')>0)" class="col-md-4">
                        <metaComment [editMode]="editMode" [binding]="profile"></metaComment>
                    </div>
                </div>


            </div>

        </div>
    `
})
export class ProfileBox implements OnInit {
    image(
        userName: string,
        type:
            | "avatar"
            | "profile_image"
            | "profile_banner"
            | "profile_background_image"
            | "profile_image_https"
            | "profile_background_image_https"
    ): string {
        return `getProfileImage?username=${encodeURIComponent(
            `${userName.toLowerCase()} ${type}`
        )}`;
    }

    circleTypes: any;
    extrasIcons: {};

    @Input() public profile: any;
    @Input() public editMode: any;

    constructor(private sanitizer: DomSanitizer) {}

    lengthOfComment(html) {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return (
            tmp.textContent.trim().length || tmp.innerText.trim().length || 0
        );
    }

    public get profileBannerUrl(): SafeUrl {
        return this.profile && this.profile.profile_banner_url
            ? this.sanitizer.bypassSecurityTrustStyle(
                  `url('${this.image(
                      this.profile.screen_name,
                      "profile_banner"
                  )}')`
              )
            : null;
    }

    public get profileImageUrl(): SafeUrl {
        console.warn(this.profile);
        return this.profile
            ? this.sanitizer.bypassSecurityTrustStyle(
                  `url('${this.image(
                      this.profile.screen_name,
                      "profile_image_https"
                  )}'), url('${this.image(
                    this.profile.screen_name,
                    "profile_image"
                )}')`
              )
            : null;
    }

    ngOnInit() {
        this.extrasIcons = extrasIcons;
        this.circleTypes = _.keyBy(CircleTypes, "id");
    }

    shorthand(value: number) {
        return value > 999 ? (value / 1000).toFixed(1) + "k" : value;
    }

    toStartCase(string) {
        return string.replace(/([A-Z])/g, " $1").replace(/^./, function(str) {
            return str.toUpperCase();
        });
    }
}
