import {
    Component,
    EventEmitter,
    OnInit,
    Output,
    OnDestroy
} from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { NgRedux } from "@angular-redux/store";
import { IAppState } from "../shared/store/store";
import { FlagActions } from "../shared/store/flagsActions";
import * as alertify from "alertifyjs";
import { ConversationApiService } from "./services/conversation-api.service";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";

import * as _ from "lodash";
import { VirtualScrollComponent } from "angular2-virtual-scroll";
import { AppCacheProvider } from "../shared/providers/app-cache-provider";
import { PermissionsProvider } from "../shared/providers/permissionsProvider";
import { PushProvider } from "../shared/providers/push-provider";

@Component({
    selector: "conversation-main",
    styles: [
        `
            nav {
                z-index: 4;
                top: 80px;
                height: 38px;
                padding-top: 15px;
            }

            .line {
                border-left: 3px solid #71b6ff;
                position: absolute;
                top: 38px;
                left: 38px;
                bottom: 48px;
                z-index: 1;
            }

            .navbar .btn-xs {
                padding: 6px 9px;
                font-size: 12px;
            }
        `
    ],
    template: `
        <nav *ngIf="!errorResponse" class="navbar fixed-top navbar-toggleable-md navbar-light bg-faded wBox">
            <div class="collapse navbar-collapse">

                <ul class="navbar-nav mr-auto">
                    <li *ngIf="!editMode" class="menu-item">
                        <a (click)="back()" href="javascript:;" class="btn btn-xs btn-default" style="margin-right:0.5em;">
                            <i class="fa fa-chevron-left"></i> Back
                        </a>
                    </li>
                    <li *ngIf="!editMode" class="menu-item">
                        <a (click)="refreshConversation(true)" *ngIf="permission==='EDIT'" href="javascript:;" class="btn btn-xs btn-default">
                            <i class="fa fa-pencil-square-o"></i> Refresh Conversation
                        </a>
                    </li>
                </ul>
                <ul *ngIf="permission!=='EDIT'">
                    <div style="position: relative; right: 2px; top: 8px;" class="text-danger">Read Only</div>
                </ul>

                <ul *ngIf="!editMode" class="navbar-nav my-2 my-lg-0">
                    <li class="menu-item" *ngIf="exportJob">
                        <a
                            href="/s3/{{exportJob.output?.output}}"
                            download="report.docx"
                            class="{{(exportJob.output?.output)?'':'disabled'}} btn btn-xs btn-{{(exportJob.status==='error')?'danger':(exportJob.output?.output)?'success':'default'}} mr-1">
                            <ng-container *ngIf="exportJob.status==='error'; else spinnerAndDownload">Export
                                Failed
                            </ng-container>
                            <ng-template #spinnerAndDownload>
                                <ng-container *ngIf="exportJob.output?.output"><span
                                    title="Started {{exportJob.start_date}}">Download Report</span></ng-container>
                                <ng-container *ngIf="!exportJob.output?.output"><i
                                    class="fa fa-circle-o-notch fa-spin"></i> Generating Report
                                </ng-container>
                            </ng-template>
                        </a>
                    </li>
                     <li class="menu-item">
                        <a (click)="exportToWord()" *ngIf="permission==='EDIT'" href="javascript:;" class="btn btn-xs btn-primary"
                           style="display:inline-block;margin-right:0.5em;">
                            <i class="fa fa-pencil-square-o"></i> Export to Word
                        </a>
                    </li>
                    <li class="menu-item">
                        <a (click)="enterEditMode()" *ngIf="permission==='EDIT'" href="javascript:;" class="btn btn-xs btn-info">
                            <i class="fa fa-pencil-square-o"></i> Edit Conversation
                        </a>
                    </li>
                </ul>
                <ul *ngIf="editMode" class="navbar-nav my-2 my-lg-0">
                    <li class="menu-item">
                        <a (click)="discardChanges()" style="margin-left:0.5em;margin-right:0.5em;" href="javascript:;"
                           class="btn btn-xs btn-default">
                            <i class="fa fa-ban"></i> Discard Changes
                        </a>
                    </li>
                    <li (click)="saveDocument()" class="menu-item">
                        <a href="javascript:;" class="btn btn-xs btn-primary">
                            <i class="fa fa-floppy-o"></i>
                            Save
                        </a>
                    </li>
                </ul>
            </div>
        </nav>

        <div *ngIf="errorResponse" class="alert alert-danger" style="margin-top:102px;">
            <h1>Error <b>{{errorResponse.status}}</b>: {{errorResponse.statusText}}</h1>
            <hr/>
            <h5><b>When reporting this error, please provide the following information to the technical staff:</b></h5>
            {{errorResponse | json}}
        </div>

        <div class="row" style="padding-top:57px;">
            <div style="position:fixed;top:125px;right:0;bottom:0;left:0;" class="conversationRow">


                <div *ngIf="conversation" style="position:absolute;top:0;right:0;bottom:0;left:0;">
                    <!--<div *ngIf="editMode" style="margin-bottom:20px;">-->
                    <!--<div>-->
                    <!--<htmlEditor [binding]="conversation.meta" [field]="'coverText'"></htmlEditor>-->
                    <!--</div> -->
                    <!--</div>-->
                    <!--
                                        <div *ngIf="!editMode">
                                            <div [innerHTML]="conversation.meta.coverText"
                                                 *ngIf="conversation.meta && conversation.meta.coverText!=''"
                                                 style="margin-bottom:20px;"></div>
                                        </div>
                    -->

                    <div id="conversationContainer" style="position:absolute;top:0;right:0;bottom:0;left:0; overflow-x: hidden; overflow-y: scroll; margin-right: -17px;">
                        <!--
                                                <profileBox [editMode]="editMode" [profile]="conversation.profile"></profileBox>

                                                <conversationLegend [editMode]="editMode"></conversationLegend>
                        -->
                        <virtual-scroll #scroll style="margin-right: -17px; min-height: 100%;" *ngIf="!printMode && scrollableItems" [items]="scrollableItems" [childHeight]="54">
                             <div style="width:100%;max-width:800px;margin:0 auto;">

                                <!-- childHeight should be minimum possible item height  -->
                                <ng-container *ngFor="let item of scroll.viewPortItems">
                                    <profileBox *ngIf="item.type==='profile' && item.model" [editMode]="editMode" [profile]="item.model"></profileBox>
                                    <rootTweet *ngIf="item.type==='rootTweet' && item.model" [editMode]="editMode" [tweet]="item.model" [profile]="conversation.profile"></rootTweet>
                                    <responseTweet *ngIf="item.type==='replyTweet' && item.model" [editMode]="editMode" [tweet]="item.model"></responseTweet>
                                    <conversationLegend *ngIf="item.type==='legend' && item.model" [editMode]="editMode"></conversationLegend>
                                </ng-container>

                            </div>
                        </virtual-scroll>
                        <div *ngIf="printMode && scrollableItems" style="width:100%;max-width:800px;margin:0 auto;">
                            <ng-container *ngFor="let item of scrollableItems">
                                <profileBox *ngIf="item.type==='profile' && item.model" [editMode]="editMode" [profile]="item.model"></profileBox>
                                <rootTweet *ngIf="item.type==='rootTweet' && item.model" [editMode]="editMode" [tweet]="item.model" [profile]="conversation.profile"></rootTweet>
                                <responseTweet *ngIf="item.type==='replyTweet' && item.model" [editMode]="editMode" [tweet]="item.model"></responseTweet>
                                <conversationLegend *ngIf="item.type==='legend' && item.model" [editMode]="editMode"></conversationLegend>
                            </ng-container>

                        </div>
                        <!--
                                                <div class="tweetsContainer" style="margin-top:20px;position:relative;">
                                                    <div *ngIf="!editMode" class="line"></div>
                                                    <div *ngFor="let response of conversation.replies">
                                                        <responseTweet *ngIf="response.timestamp<=conversation.tweet.timestamp" [editMode]="editMode" [tweet]="response"></responseTweet>
                                                    </div>
                                                </div>

                                                <rootTweet [editMode]="editMode" [tweet]="conversation.tweet"
                                                           [profile]="conversation.profile"></rootTweet>

                                                <div class="tweetsContainer" style="margin-top:20px;position:relative;">
                                                    <div *ngIf="!editMode" class="line"></div>
                                                    <div *ngFor="let response of conversation.replies">
                                                        <responseTweet *ngIf="response.timestamp>conversation.tweet.timestamp" [editMode]="editMode" [tweet]="response"></responseTweet>
                                                    </div>
                                                </div>
                        -->



                    </div>

                </div>

            </div>
        </div>
    `
})
export class ConversationMain implements OnInit, OnDestroy {
    userName: string;
    tweetId: string;
    reportId: string;
    ownerId: string;
    editMode: boolean;

    scrollableItems: {
        type: "rootTweet" | "legend" | "profile" | "responseTweet";
        model: any;
    }[] = [];

    private _subscribedChannels: string[] = [];
    exportJob: any;

    permission: string;
    conversation: any;
    conversationSnapshot: any;
    errorResponse: any;
    printMode: boolean;

    constructor(
        private cache: AppCacheProvider,
        private flagActions: FlagActions,
        private activatedRoute: ActivatedRoute,
        private conversationApiService: ConversationApiService,
        private permissionsProvider: PermissionsProvider,
        private redux: NgRedux<IAppState>,
        private router: Router,
        private http: Http,
        private pushProvider: PushProvider
    ) {}

    back() {
        if (this.reportId) {
            this.router.navigate([`/reports/${this.reportId}`]);
        } else {
            history.back();
        }
    }

    ngOnDestroy(): void {
        this.pushProvider.unsubscribe(...this._subscribedChannels);
    }

    // private setExportTick() {
    //     if (this.exportTick) clearInterval(this.exportTick);

    //     this.exportTick = <any>setInterval(() => {
    private handleExportCallback() {
        // if (
        //     this.exportJob.status === "error" ||
        //     (this.exportJob && this.exportJob.status === "finished")
        // ) {
        //     clearInterval(this.exportTick);
        //     return;
        // }

        this.http.get(`/job/jobs/${this.exportJob.id}`).subscribe(res => {
            this.exportJob = res.json();
            // if (
            //     this.exportJob.status === "finished" ||
            //     this.exportJob.status === "error"
            // ) {
            //     clearInterval(this.exportTick);
            //     this.exportTick = null;
            // }
        });
    }
    //     }, 2000);
    // }

    ngOnInit() {
        this.activatedRoute.params.delay(0).subscribe((params: Params) => {
            this.userName = params["userName"];
            this.tweetId = params["tweetId"];
            this.reportId = params["reportId"];
            this.ownerId = params["ownerId"];

            this.refreshConversation();

            // Feature not available from interface, here for dev ease:
            // queryParam ';editMode=true' will load the page in edit mode, without the need to click 'edit'
            this.printMode = !!params["printMode"];
            let editMode = params["editMode"];
            this.editMode = !!editMode;
        });
    }

    enterEditMode() {
        this.conversationSnapshot = JSON.parse(
            JSON.stringify(this.conversation)
        );
        this.editMode = true;
    }

    discardChanges() {
        this.conversation = JSON.parse(
            JSON.stringify(this.conversationSnapshot)
        );
        this.editMode = false;
    }

    exportToWord = () => {
        let path = window.location.href;
        //this.flagActions.setFlag('loading', true);
        this.conversationApiService
            .exportToWord(`Conversation: ${this.tweetId}`, this.ownerId, path)
            .subscribe(
                res => {
                    this.exportJob = res.response;
                    this.pushProvider
                        .subscribeOnce(res.response.id)
                        .subscribe(mes => {
                            this.handleExportCallback();
                        });
                    this._subscribedChannels.push(res.response.id);
                    // this.setExportTick();
                    alertify.success("Export in progress...");
                    //this.flagActions.setFlag('loading', false);
                },
                err => {
                    alertify.error("Error: ", err.message);
                }
            );
    };

    saveDocument() {
        this.flagActions.setFlag("loading", true);
        const docId = `https://twitter.com/${this.userName}/status/${
            this.tweetId
        }?reportId=${this.reportId}`;
        return this.http
            .post(
                `/scrape/saveConversation?documentId=${encodeURIComponent(
                    docId
                )}&ownerId=${this.ownerId}&type=1`,
                this.conversation
            )
            .catch((err, caught) => {
                return Observable.of(err);
            })
            .subscribe(res => {
                this.flagActions.setFlag("loading", false);
                this.editMode = false;
                if (res.ok) {
                    alertify.success("Saved");
                } else {
                    alertify.error(res.statusText);
                }
            });
    }

    refreshConversation(hard?: boolean) {
        this.flagActions.setFlag("loading", true);

        const previousConversation = this.conversation;

        this.conversation = null;
        this.errorResponse = null;

        this.conversationApiService
            .getConversation(
                this.userName,
                this.tweetId,
                this.reportId,
                +this.ownerId,
                hard
            )
            .catch((err, caught) => {
                this.flagActions.setFlag("loading", false);
                return Observable.of(err);
            })
            .subscribe(res => {
                this.flagActions.setFlag("loading", false);
                if (!res.ok) {
                    if (previousConversation) {
                        this.conversation = previousConversation;
                        if (res.json) {
                            res = res.json();
                        }
                        alertify.error(
                            `Error loading conversation: ${res.message ||
                                res.statusText ||
                                res.status ||
                                res}`
                        );
                    } else {
                        this.errorResponse = res;
                    }
                } else {
                    this.conversation = res.json();
                    if (
                        this.conversation.profile &&
                        (this.userName || "").toLowerCase() !=
                            (
                                this.conversation.profile.screen_name || ""
                            ).toLowerCase()
                    ) {
                        this.router.navigate([
                            ".",
                            {
                                userName: this.conversation.profile.screen_name,
                                tweetId: this.tweetId,
                                reportId: this.reportId,
                                ownerId: this.ownerId
                            }
                        ]);
                    }
                    // this.conversation.replies = _.sortBy(
                    //     this.conversation.replies,
                    //     "timestamp"
                    // );

                    window["printHelper"].meta = {
                        title: `${this.conversation.tweet.fullName}: ${this
                            .conversation.tweet.tweetText || "--no text--"}`
                    };
                    this.conversation.meta = this.conversation.meta || {};
                    this.conversation.tweet.meta =
                        this.conversation.tweet.meta || {};
                    if (this.conversation.profile) {
                        this.conversation.profile.meta =
                            this.conversation.profile.meta || {};
                    }
                    this.conversation.legend = this.conversation.legend || {};
                    this.conversation.replies.forEach((reply: object) => {
                        reply["meta"] = reply["meta"] || {};
                    });
                    (async () => {
                        this.permission = await this.permissionsProvider.reportPermissions(
                            this.ownerId,
                            this.reportId
                        );
                    })();
                    let orderedTweets: any[];
                    const self = this;
                    if ("index" in this.conversation.tweet) {
                        orderedTweets = Array.from(
                            (function*() {
                                for (
                                    let i = 0;
                                    i < self.conversation.tweet.index;
                                    i++
                                ) {
                                    yield {
                                        type: "replyTweet",
                                        model: self.conversation.replies[i]
                                    };
                                }
                                yield {
                                    type: "replyTweet",
                                    model: self.conversation.tweet
                                };
                                for (
                                    let i = self.conversation.tweet.index;
                                    i < self.conversation.replies.length;
                                    i++
                                ) {
                                    yield {
                                        type: "replyTweet",
                                        model: self.conversation.replies[i]
                                    };
                                }
                            })()
                        );
                    } else {
                        orderedTweets = Array.from(
                            (function*() {
                                yield {
                                    model: self.conversation.tweet,
                                    type: "replyTweet"
                                };
                                for (
                                    let i = 0;
                                    i < self.conversation.replies.length;
                                    i++
                                ) {
                                    yield {
                                        type: "replyTweet",
                                        model: self.conversation.replies[i]
                                    };
                                }
                            })()
                        );
                    }

                    this.scrollableItems = [
                        { type: "profile", model: this.conversation.profile },
                        { type: "legend", model: this.conversation.legend },
                        ...orderedTweets
                    ];
                }

                this.conversationSnapshot = JSON.parse(
                    JSON.stringify(this.conversation)
                );
                //console.warn(this.conversation);
            });
    }
}
