import { Injectable } from "@angular/core";
import {
    Http,
    RequestOptions,
    ResponseContentType,
    Headers
} from "@angular/http";
import * as fileSaver from "file-saver";
import { Observable } from "rxjs/Observable";
import * as Url from "url";

@Injectable()
export class ConversationApiService {
    //EXPORT_PDF_URL="http://localhost:3007/export/pdf"
    EXPORT_PDF_URL = "/export/pdf";
    EXPORT_ZIP_URL = "/export/zip";
    EXPORT_WORD_URL = "/export/word";

    constructor(private http: Http) {}

    getConversation = (userName: string, tweetId: string, reportId: string, ownerId: number, force?: boolean) => {
        const GET_CONVERSATION_URL = `/scrape/conversationByTweet?${
            force ? "forceRescrape=true&" : ""
        }userName=${userName}&tweetId=${tweetId}&reportId=${reportId}&ownerId=${ownerId}`;
        return this.http.get(GET_CONVERSATION_URL);
    };

    exportToWord = (name, ownerId, ...urls: string[]) => {
        let fullUrl = `${this.EXPORT_WORD_URL}`;
        return this.http
            .post(this.EXPORT_WORD_URL, {
                urls: urls.map(
                    url => `${Url.parse(url).href};ownerId=${ownerId}`
                ),
                name: name
            })
            .map(res => res.json());
    };

    _exportToWord = (...urls: string[]) => {
        let fullUrl = `${this.EXPORT_WORD_URL}`;
        //add headers for supporting pdf
        let headers = new Headers({
            Accept:
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        });

        let options = new RequestOptions();
        // Ensure you set the responseType to Blob.
        options.responseType = ResponseContentType.Blob;
        options.headers = headers;
        return this.http
            .post(this.EXPORT_WORD_URL, { urls: urls }, options)
            .map(res => {
                //convert the response to blob
                let fileBlob = res.blob();
                let blob = new Blob([fileBlob], {
                    type:
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document" // must match the Accept type
                });
                let filename = "report.docx";
                //use file saver for saving blob to file
                fileSaver.saveAs(blob, filename);
            });
    };
}
