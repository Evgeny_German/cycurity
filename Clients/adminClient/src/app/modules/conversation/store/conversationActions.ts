import {Injectable} from '@angular/core';
import {NgRedux} from "@angular-redux/store";
import {IAppState} from "../../shared/store/store";

@Injectable()
export class ConversationActions {

  constructor(private redux :NgRedux<IAppState>) {
  }

}
