import { Component, OnInit } from "@angular/core";
import { Http } from "@angular/http";
import * as alertify from "alertifyjs";
import { ActivatedRoute, Router } from "@angular/router";
import { UsersApiService } from "../users/services/users-api.service";

@Component({
    template: `

        <div class="card" style="padding:1em 1em 0.2em 1em;position:relative;">
            <label><input type="checkbox" [ngModel]="onlyRunning" (ngModelChange)="setOnlyRunning($event)"/> Show only running instances</label>

            <span style="position:absolute;right:10px;top:8px;">
                <div class="dropdown">
                    <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
                        New Proxy
                    </button>
                    <div class="dropdown-menu">
                        <a *ngFor="let region of regions" class="dropdown-item" (click)="launchProxy(region)" href="javascript:;"><span class="badge badge-primary">{{region.provider}}</span>  {{region.display_name}}</a>
                    </div>
                </div>
            </span>

        </div>
        <table class="table" *ngIf="proxyManageList && proxyManageList.length>0; else noProxies">
            <thead>
                <tr>
                    <!--<td>ID</td>-->
                    <th>Provider</th>
                    <th>Region</th>
                    <th>User</th>
                    <th>Startup time</th>
                    <th>Shutdown time</th>
                    <!--<td>Key-pair name</td>-->
                    <th>Status</th>
                    <!--<td>Image</td>-->
                    <!--<td>Instance name</td>-->
                    <th>Instance ID</th>
                    <!--<td>Proxy port</td>-->
                    <th>IP address</th>
                    <!--<td>Pending image</td>-->
                    <th></th>
                </tr>
            </thead>
            <tr *ngFor="let item of proxyManageList">
                <!--<td>{{item.id}}</td>-->
                <td style="text-transform:uppercase;font-weight:bold;">{{item.provider}}</td>
                <td>{{regionName(item.region)}} <small>({{item.region}})</small></td>
                <td>{{userName(item.user_id)}}</td>
                <td>{{item.startup_time | date: 'medium'}}</td>
                <td>{{item.shutdown_time | date: 'medium'}}</td>
                <!--<td>{{item.key_pair_name}}</td>-->
                <td>{{item.state}}</td>
                <!--<td>{{imageName(item.image_id)}}</td>-->
                <!--<td>{{item.instance_name}}</td>-->
                <td>{{item.instance_id}}</td>
                <!--<td>{{item.proxy_port}}</td>-->
                <td>{{item.public_ip}}</td>
                <!--<td>{{item.is_pending_image}}</td>-->
                <td><input *ngIf="item.state!=='terminated'" style="margin-top:-5px;" class="btn btn-primary btn-xs" type="button" value="Terminate" (click)="terminate(item)"/></td>
            </tr>
        </table>
        
        <ng-template #noProxies>
            <div class="mt-3 alert alert-info">No Proxy Servers to show</div>
        </ng-template>
    `
})
export class ProxyComponent implements OnInit {
    onlyRunning = false;

    users: any[];
    images: any[];
    regions: any[];
    proxyManageList: any[];

    setOnlyRunning(value: boolean) {
        this.router.navigate([".", { onlyRunning: value }]);
    }

    constructor(
        private http: Http,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private usersApiService: UsersApiService
    ) {}

    ngOnInit(): void {
        this.activatedRoute.params.subscribe(params => {
            const onlyRunningUrlParam = typeof(params["onlyRunning"]) === 'boolean'? params["onlyRunning"] : params["onlyRunning"]==='true' ;
            this.onlyRunning = onlyRunningUrlParam;
            this.getProxyManageList();
        });
        this.http.get("/proxy/getSupportedRegions").subscribe(
            res => {
                this.regions = res.json();
            },
            err => {
                alertify.error(
                    `Error loading region data: ${err.message || err}`
                );
            }
        );
        this.http.get("/proxy/getImages").subscribe(
            res => {
                this.images = res.json();
            },
            err => {
                alertify.error(
                    `Error loading image data: ${err.message || err}`
                );
            }
        );
        (async () => {
            this.users = <any>await this.usersApiService.list();
        })();
        this.getProxyManageList();
    }

    launchProxy(event) {
        alertify.success(`New Proxy Requested`);
        this.http
            .post(`/proxy/createInstance`, {
                "region": event.region_key,
                "provider": event.provider,
                "instanceName": "proxy_instance_"+this.makeid()
            })
            .subscribe(
                res => {
                    alertify.success(`New Proxy Started`);
                },
                err => {
                    alertify.error(`Error: ${err.message || err}`);
                }
            );
    }

    makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    getProxyManageList() {
        this.http
            .post(`/proxy/getProxyManageList`, {
                limit: 100,
                onlyRunning: this.onlyRunning
            })
            .subscribe(
                res => {
                    this.proxyManageList = res.json();
                },
                err => {
                    alertify.error(`Error loading data: ${err.message || err}`);
                }
            );
    }

    terminate(item: any) {
        this.http
            .post("/proxy/terminateInstance", {
                region: item.region,
                provider: item.provider,
                instanceId: item.instance_id
            })
            .subscribe(
                res => {
                    alertify.success(
                        `Instance '${item.instance_name}' terminated.`
                    );
                    this.getProxyManageList();
                },
                err => {
                    alertify.error(
                        `Error terminating instance '${
                            item.instance_name
                        }', ${err.message || err}.`
                    );
                }
            );
    }

    regionName(regionId: string): string {
        if (
            !regionId ||
            !regionId.length ||
            !this.regions ||
            !this.regions.length
        )
            return regionId;
        const region = this.regions.find(r => r.region_key === regionId);
        return region ? region.display_name : regionId;
    }

    imageName(imageId: string): string {
        if (!imageId || !imageId.length || !this.images || !this.images.length)
            return imageId;
        const image = this.images.find(r => r.image_id === imageId);
        return image ? image.image_name : imageId;
    }

    userName(userId: number): string {
        if (!userId || !this.users || !this.users.length) return <any>userId;
        const user = this.users.find(u => u.id === userId);
        return user ? user.email : <any>userId;
    }
}
