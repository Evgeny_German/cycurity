import { NgModule } from "@angular/core";
import { ProxyComponent } from "./proxy.component";
import { SharedModule } from "../shared/shared.module";
import { RouterModule } from "@angular/router";

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild([
            {
                path: "proxy",
                component: ProxyComponent
            }
        ])
    ],
    declarations: [ProxyComponent],
    exports: [ProxyComponent]
})
export class ProxyModule {}
