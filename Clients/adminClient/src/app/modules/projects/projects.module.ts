import {NgModule} from '@angular/core';
import {ProjectsMain} from "./projects.comp";
import {RouterModule, Routes} from "@angular/router";
import {AuthenticationCallbackActivateGuard} from "../shared/auth/auth-guard-provider";
import {SharedModule} from "../shared/shared.module";

import {ProjectsSidebar} from './components/projectsSidebar';
import {ProjectsEditor} from './components/projectsEditor';
import {KolService} from "../projects/services/kolService";
import {UserManager} from "./components/userManager";
import {GeneralSettings} from "./components/generalSettings.comp";


export const PROJECTS_MAIN_URL = 'projects';

const projectsRoutes: Routes = [
    {
        path: PROJECTS_MAIN_URL, component: ProjectsMain,canActivate:[AuthenticationCallbackActivateGuard]
    },
    {
        path: PROJECTS_MAIN_URL+'/:projectId', component: ProjectsMain,canActivate:[AuthenticationCallbackActivateGuard]
    },
    {
        path: PROJECTS_MAIN_URL+'/:projectId/:subprojectId', component: ProjectsMain,canActivate:[AuthenticationCallbackActivateGuard]
    }
];

@NgModule({
  imports: [RouterModule.forChild(projectsRoutes),SharedModule],
  exports: [ProjectsMain, ProjectsSidebar, ProjectsEditor, UserManager, GeneralSettings],
  declarations: [ProjectsMain, ProjectsSidebar, ProjectsEditor, UserManager, GeneralSettings],
  providers: [KolService],
})

export class ProjectsModule {
}
