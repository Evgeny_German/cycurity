import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { FlagActions } from "../../shared/store/flagsActions";
import * as alertify from "alertifyjs";
import { GenericApiProvider } from "../../shared/providers/genericApiProvider";
import { Observable } from "rxjs/Observable";
import { KolService } from "../../projects/services/kolService";
import { kolEditorConfig } from "../../shared/dataModels/kol-config";
import { PermissionsProvider } from "../../shared/providers/permissionsProvider";

@Component({
    selector: "general-settings",
    template: `
    <div class="row">

        <div class="container-fluid row" style="position:absolute;top:0;right:0;bottom:0;left:0;">
            <div  class="col-12">

                <ol style="margin-top:7px;margin-right:-28px;" class="breadcrumb">
                    <li class="breadcrumb-item">
                        <b>Default Settings</b>
                    </li>
                    <li *ngIf="permissions=='EDIT'">
                        <a *ngIf="!(validationErrors.length + validationWarnings.length); else errors" (click)="saveConfig()" href="javascript:;"
                           class="pull-right btn btn-sm btn-primary" style="padding:5px 8px;font-size:12px;margin-right:-7px;margin-top:-1px;"><i class="fa fa-floppy-o"></i> Save</a>

                        <ng-template #errors>
                            <span class="dropdown" style="position:absolute;top:17px;right:9px;">

                                <a href="javascript:;"
                                   data-toggle="dropdown"
                                   class="btn btn-xs btn-primary {{(actionsDisabled)?'disabled':''}}">
                                    <i class="fa fa-floppy-o"></i> Save
                                    <span style="position:absolute;top:-3px;right:-3px;"
                                          class="badge badge-pill badge-danger">{{(validationErrors.length+validationWarnings.length)||0}}</span>
                                </a>

                                <ul class="dropdown-menu" style="margin-top:-20px;margin-right:25px;">
                                    <li *ngFor="let err of validationErrors" style="background-color:#fff;" class="dropdown-item"><i class="text-danger fa fa-exclamation-triangle" aria-hidden="true"></i> <span [innerHTML]="err"></span></li>
                                    <li *ngFor="let err of validationWarnings" style="background-color:#fff;" class="dropdown-item"><i class="text-warning fa fa-exclamation-triangle" aria-hidden="true"></i> <span [innerHTML]="err"></span></li>
                                    <li *ngIf="!validationErrors.length" style="text-align:right;">
                                        <hr/>
                                        <input type="button" class="btn btn-sm btn-primary" style="margin-right:20px;" value="Save Anyway" (click)="saveConfig()"/>
                                    </li>
                                </ul>

                            </span>
                        </ng-template>
                    </li>
                </ol>

            </div>

            <div *ngIf="kolParams" class="col-12" style="position:absolute;top:60px;right:0px;bottom:0;left:0;overflow-y:scroll;margin-right:-10px;">
                <div [ngClass]="{'disabled': permissions!='EDIT'}">
                    <kolEditor [params]="kolParams.kol_filter_config" (validationErrors)="validationErrors = $event.errors; validationWarnings = $event.warnings" [defaults]="kolDefaults"></kolEditor>
                </div>
            </div>



        </div>



    </div>
  `
})
export class GeneralSettings implements OnInit {
    permissions: string;
    validationErrors: string[] = [];
    validationWarnings: string[] = [];

    private kolParams;
    private kolDefaults = {};

    constructor(
        private genericApiProvider: GenericApiProvider,
        private kolService: KolService,
        private permissionsProvider: PermissionsProvider
    ) {
        this.permissions = permissionsProvider.modulePermissions("settings");

        kolEditorConfig.forEach(item => {
            this.kolDefaults[item.id] = { importance: 0 };
        });

        this.genericApiProvider
            .type("globalKolConfigs")
            .list()
            .subscribe(res => {
                res.kol_filter_config = kolService.postLoad(
                    res.kol_filter_config
                );
                this.kolParams = res;
            });
    }

    ngOnInit() {}

    saveConfig() {
        var kolParams = this.kolParams;
        kolParams.kol_filter_config = this.kolService.preSave(
            kolParams.kol_filter_config
        );

        this.genericApiProvider
            .type("globalKolConfigs")
            .post(kolParams)
            .subscribe(res => {
                this.validationErrors = [];
                this.validationWarnings = [];
                alertify.success("Saved");
            }, err => {
                alertify.error(err);
            });
    }
}
