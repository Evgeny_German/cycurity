import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ChangeDetectorRef
} from "@angular/core";
import { Router } from "@angular/router";
import { FlagActions } from "../../shared/store/flagsActions";
import * as alertify from "alertifyjs";
import { GenericApiProvider } from "../../shared/providers/genericApiProvider";
import { KolService } from "../services/kolService";

import * as _ from "lodash";
import { AppCacheProvider } from "../../shared/providers/app-cache-provider";

@Component({
    selector: "projectEditor",
    styles: [
        `
            nav {
                z-index: 4;
                top: 80px;
                height: 38px;
                padding-top: 15px;
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
            }

            div.disabled {
                cursor: not-allowed;
                pointer-events: none;
            }
        `
    ],
    template: `

        <!-- Toolbar -->

        <div class="row" *ngIf="!project" style="height:100%;">
            <div class="col-md-12" style="height:100%;">
                <div style="position:absolute;top:50%;left:50%;margin-top:-40px;margin-left:-50px;">
                    Please select a project
                </div>
            </div>
        </div>

        <!-- Conversation List -->

        <ul *ngIf="project && rootLevelKolDefaults" class="navbar-nav mr-auto">
            <li class="menu-item">
                <div style="width:100%;padding-top:10px;position:relative;">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <small>Project:</small>
                            <b>{{project.name}}</b>
                        </li>
                        <li *ngIf="subproject" class="breadcrumb-item active">
                            <small>Topic:</small>
                            <b>{{subproject.name}}</b>
                        </li>
                    </ol>







                    <ng-container *ngIf="canEdit; else readOnlyMessage">

                        <a (click)="discard()" href="javascript:;"
                           class="btn btn-xs btn-default {{(actionsDisabled)?'disabled':''}}"
                           style="position:absolute;top:17px;right:79px;">
                            <i class="fa fa-ban"></i> Discard Changes
                        </a>

                        <span *ngIf="!subproject" class="dropdown" style="position:absolute;top:17px;right:228px;z-index:1;">
                            <a (click)="discard()" href="javascript:;"
                               class="btn btn-xs btn-default"
                               data-toggle="dropdown">
                                <i class="fa fa-user-plus"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" style="width:430px" onclick="event.stopPropagation()">
                                <li>
                                    <userManager [project]="project"></userManager>
                                </li>
                            </ul>
                        </span>

                        <ng-container *ngIf="!(validationErrors.length + validationWarnings.length)">
                            <a (click)="save()" href="javascript:;"
                               class="btn btn-xs btn-primary {{(actionsDisabled)?'disabled':''}}"
                               style="position:absolute;top:17px;right:9px;">
                                <i class="fa fa-floppy-o"></i> Save
                            </a>
                        </ng-container>

                    </ng-container>

                    <ng-template #readOnlyMessage>
                        <div style="position:absolute;top:20px;right:9px;color:#f00;">Read Only!</div>
                    </ng-template>






                    <ng-container *ngIf="validationErrors.length + validationWarnings.length >0">
                        <span class="dropdown" style="position:absolute;top:17px;right:9px;">

                            <a href="javascript:;"
                               data-toggle="dropdown"
                               class="btn btn-xs btn-primary {{(actionsDisabled)?'disabled':''}}">
                                <i class="fa fa-floppy-o"></i> Save
                                <span style="position:absolute;top:-3px;right:-3px;"
                                      class="badge badge-pill badge-danger">{{validationErrors.length + validationWarnings.length}}</span>
                            </a>

                            <ul class="dropdown-menu" style="margin-top:-20px;margin-right:25px;">
                                <li *ngFor="let err of validationErrors" style="background-color:#fff;" class="dropdown-item"><i class="text-danger fa fa-exclamation-triangle" aria-hidden="true"></i> <span [innerHTML]="err"></span></li>
                                <li *ngFor="let err of validationWarnings" style="background-color:#fff;" class="dropdown-item"><i class="text-warning fa fa-exclamation-triangle" aria-hidden="true"></i> <span [innerHTML]="err"></span></li>
                                <li *ngIf="validationErrors.length==0" style="text-align:right;">
                                    <hr/>
                                    <input type="button" class="btn btn-sm btn-primary" style="margin-right:20px;" value="Save Anyway" (click)="save()"/>
                                </li>
                            </ul>

                        </span>
                    </ng-container>


                </div>

            </li>
        </ul>

        <div *ngIf="project && showEditor"
             style="position:absolute;top:60px;right:0;bottom:0;left:0;overflow-y:scroll;">
            <div *ngIf="subproject" class="form-group row" style="padding-top:10px;">
                <div class="col-2"></div>
                <div class="col-3"><label style="padding-left:10px;padding-top:5px;">Inbox</label></div>
                <div class="col-7">
                    <select [disabled]="!canEdit" value="{{subproject.inboxId}}" *ngIf="inboxes" (change)="inboxChanged($event)"
                            class="custom-select" style="width:100%;">
                        <option value=""></option>
                        <ng-container *ngFor="let inbox of inboxes">
                            <option *ngIf="!inbox.subprojectId || inbox.subprojectId==subproject.id"
                                    value="{{inbox.id}}">{{inbox.name}}
                            </option>
                        </ng-container>
                    </select>
                    <i *ngIf="!inboxes" class="fa fa-circle-o-notch fa-spin"></i>
                </div>
                <hr style="width:100%;"/>
            </div>

            <div class="{{(!canEdit)?'disabled':''}}">
                <kolEditor *ngIf="kolDefaults" (paramsChange)="paramsChanged($event)" (validationErrors)="validationErrors = $event.errors; validationWarnings = $event.warnings" [params]="kolParams" [defaults]="kolDefaults"></kolEditor>
            </div>

        </div>



    `
})
export class ProjectsEditor implements OnInit {
    [x: string]: any;
    rootLevelKolDefaults: any;

    actionsDisabled: boolean;
    validationErrors: string[];
    validationWarnings: string[];

    kolParams: any;
    kolDefaults: any;
    showEditor: boolean = true;

    // private originalProject;
    // private originalSubProject;

    private _project: any;
    private _subproject: any;

    @Output()
    discardEvent = new EventEmitter<boolean>();
    @Output()
    saveEvent = new EventEmitter<boolean>();
    @Output()
    changeEvent = new EventEmitter<boolean>();

    @Input()
    inboxes: any;

    @Input()
    public get project(): any {
        return this._project;
    }

    public set project(value: any) {
        // if (value) this.originalProject = JSON.parse(JSON.stringify(value));

        const prevValue = this._project;
        this._project = value;

        if (prevValue !== value) {
            this.calcDefaults();
        }

        this.canEdit = false;
        if (value) {
            const userId = this.identity.id;
            const ownerId = value.owner_id;
            const adminRole = this.identity.adminRole;
            const projectUsers = value.projectUser.filter(u => {
                return u.user_id == userId && u.role == "editor";
            });

            /*
            console.warn('userId',userId)
            console.warn('ownerId',ownerId)
            console.warn('adminRole',adminRole)
            console.warn('projectUsers',projectUsers)
*/

            this.canEdit =
                userId == ownerId ||
                adminRole == "superadmin" ||
                projectUsers.length > 0;
        }
    }

    @Input()
    public get subproject(): any {
        return this._subproject;
    }

    public set subproject(value: any) {
        // if (value) this.originalSubProject = JSON.parse(JSON.stringify(value));

        const prevValue = this._subproject;
        this._subproject = value;

        if (prevValue !== value) {
            this.calcDefaults();
        }
    }

    private calcDefaults() {
        if (!this.project || !this.rootLevelKolDefaults) return;

        this.actionsDisabled = true;

        // this.showEditor = false;
        const _defaults = JSON.parse(
            JSON.stringify(this.rootLevelKolDefaults.kol_filter_config)
        );

        this.kolParams = this.subproject
            ? this.subproject.kol_filter_config
            : this.project.kol_filter_config;

        if (this.subproject) {
            Object.keys(this.project.kol_filter_config).forEach(key => {
                _defaults[key] = Object.assign(
                    _defaults[key] || {},
                    this.project.kol_filter_config[key]
                );
            });
        }
        this.kolDefaults = _defaults;

        // this.changeDetectorRef.detectChanges();
        // this.showEditor = true;
    }

    constructor(
        private appCacheProvider: AppCacheProvider,
        private changeDetectorRef: ChangeDetectorRef,
        private genericApiProvider: GenericApiProvider,
        private router: Router,
        private flagActions: FlagActions,
        private kolService: KolService
    ) {
        this.genericApiProvider
            .type("globalKolConfigs")
            .list()
            .subscribe(res => {
                res.kol_filter_config = kolService.postLoad(
                    res.kol_filter_config
                );
                this.rootLevelKolDefaults = res;
                this.calcDefaults();
            });

        this.identity = appCacheProvider.getIdentity();
    }

    save() {
        this.showEditor = false;

        alertify.success("Saved...");
        this.actionsDisabled = true;
        this.saveEvent.emit();

        this.changeDetectorRef.detectChanges();
        this.showEditor = true;
        this.validationErrors = [];
        this.validationWarnings = [];
    }

    discard() {
        this.showEditor = false;

        this.actionsDisabled = true;
        // this.project.kol_filter_config = this.originalProject.kol_filter_config;
        // this.kolParams = this.project.kol_filter_config;
        // if (this.subproject)
        //     this.subproject.kol_filter_config = this.originalSubProject.kol_filter_config;
        this.discardEvent.emit();

        this.changeDetectorRef.detectChanges();
        this.showEditor = true;
        this.validationErrors = [];
        this.validationWarnings = [];
    }

    ngOnInit() {
        this.actionsDisabled = true;
        this.validationErrors = [];
        this.validationWarnings = [];
    }

    paramsChanged(event) {
        // this.validateForm();
        this.changeEvent.emit();
        this.calcDefaults();
        this.actionsDisabled = false;
    }

    //     validateForm() {
    //         this.validationWarnings = [];
    //         if (this.subproject) {
    //             var kolFinal = {};
    //             Object.keys(this.kolDefaults).forEach(key => {
    //                 kolFinal[key] = Object.assign(
    //                     this.kolDefaults[key],
    //                     this.subproject.kol_filter_config[key]
    //                 );
    //             });

    //             // Values found in 0 importance fields
    //             Object.keys(kolFinal).forEach(key => {
    //                 const importance = kolFinal[key].importance || 0;

    //                 if (importance == 0) {
    //                     if (kolFinal[key].from || kolFinal[key].to) {
    //                         this.validationWarnings.push(
    //                             `<b>${this.toStartCase(
    //                                 key
    //                             )}</b> has min/max, but <b>Importance</b> is 0`
    //                         );
    //                     }
    //                 }

    //                 /*
    //                 console.warn(kolFinal[key]);
    //                 acc += kolFinal[key].importance||0;
    // */
    //             });
    //         }
    //     }

    toStartCase(string) {
        return string.replace(/([A-Z])/g, " $1").replace(/^./, function(str) {
            return str.toUpperCase();
        });
    }

    inboxChanged(event) {
        this.subproject.inboxId = event.target.value;
        this.subproject.inboxName = this.inboxes.filter(
            inbox => inbox.id == this.subproject.inboxId[0] || {}
        ).name;
        this.paramsChanged(event);
    }
}
