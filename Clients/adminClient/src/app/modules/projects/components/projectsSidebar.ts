import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
//import {DocumentProvider} from '../../shared/providers/documentProvider';
import {Observable} from "rxjs/Observable";
import {GenericApiProvider} from "../../shared/providers/genericApiProvider";
import {FlagActions} from "../../shared/store/flagsActions";
import * as moment from 'moment';

@Component({
    selector: 'projectsSidebar',
    styles: [`
        .nav-link small{
            color:#777;
        }
        
        .nav-link.active small{
            color:#8dd!important;
        }

    `],
    template: `

        <div *ngIf="projects" style="position:absolute;top:0px;bottom:0;left:0;right:0;overflow-y:auto;/*border:11px solid #f00;*/">
            
            <h5 style="padding:17px 10px 0 10px">Projects</h5>
            
            <div>

                <div *ngIf="projects.length==0" style="margin:9px 6px 0 4px" class="alert alert-danger">No Projects Currently Available</div>

                <div *ngIf="projects.length>0">
                    <h3 style="margin-top:3px;">Project <a href="" class="btn btn-sm btn-primary pull-right" style="padding:2px 4px 1px 4px;font-size:11px;margin:6px 7px 0 0;"><i class="fa fa-plus"></i></a></h3>

                    <div class="form-group">
                        <select class="form-control" style="padding:0 4px 0 4px;">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>                    
                    
                    <ul class="nav nav-pills" style="margin-bottom:5px;display:none;">
                        <li style="width:100%;position:relative;" class="nav-item" *ngFor="let project of projects">
                            <a
                                [ngClass]="{'active':(project.documentId==selected?.documentId)}"
                                class="nav-link active"
                                href="javascript:;"
                                (click)="selectProject(project)"
                            >
                                <div>{{project.title}}</div>
                                <div>
                                    <small>{{moment(project.createdDate).format("MMMM Do YYYY, h:mm:ss a")}}</small>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>

            <div style="padding:1px 5px 0 5px;">
                <div class="form-group">
                    <input
                        (keyup.enter)="createProject()"
                        placeholder="+ Create a project"
                        class="form-control"
                        [(ngModel)]="searchText"
                    />
                </div>
            </div>








            
            
            
            
            
            
            
            

        </div>

    `
})
export class ProjectsSidebar implements OnInit {
    moment: typeof moment;
    @Input() public projects: any[];

    @Input() public selected: object;
    @Output() public selectedChange = new EventEmitter();

    searchText: String = '';

    constructor(private genericApiProvider: GenericApiProvider, private flagActions: FlagActions, private router: Router) {
        this.moment = moment;
    }

    ngOnInit() {
    }

    selectProject(project) {
        this.router.navigate(['projects', project.documentId]);
        //this.selectedChange.emit(project);
    }

    createProject() {

        if(!this.searchText || this.searchText.length==0)return

        this.flagActions.setFlag('loading', true);

        var newProject = this.genericApiProvider.type('projects').dispense();

        newProject.name = this.searchText;

        this.genericApiProvider.type('projects').post( newProject ).subscribe((res)=>{});

        this.flagActions.setFlag('loading', false);

/*        const documentProvider = this.documentProvider;
        const project = documentProvider.dispense('project', {
            title: this.searchText,
            conversations: [],
            createdDate: new Date().getTime()
        });

        documentProvider.store(project)
            .catch((err, caught) => {
                this.flagActions.setFlag('loading', false);
                console.warn(err);
                return Observable.of(err);
            })
            .subscribe((res)=>{
                this.flagActions.setFlag('loading', false);
                this.projects.unshift(project);
                this.selectProject(project);
                this.searchText = '';
            });*/

    }
}
