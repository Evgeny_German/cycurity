import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    SimpleChanges
} from "@angular/core";
import { UsersApiService } from "../../users/services/users-api.service";
import { GravatarProvider } from "../../shared/providers/gravatarProvider";
import { GenericApiProvider } from "../../shared/providers/genericApiProvider";

@Component({
    selector: "userManager",
    styles: [
        `
            .deleteButton {
                position: absolute;
                top: 1px;
                right: 5px;
                text-decoration: none;
                color: #000;
            }

            .editor .deleteButton {
                color: #fff;
            }
        `
    ],
    template: `

        <div style="padding:1em;width:430px;" *ngIf="show">

            <div *ngIf="owner">
                Owner:
                <img src="{{gravatarProvider.gravatar(owner.email, 18)}}"/>
                {{owner.email}}
                <hr/>
            </div>

            <h6>Share this Project</h6>

            <div class="row">
                <div class="col-6">
                    <select [(ngModel)]="userToAdd" class="form-control">
                        <option *ngFor="let user of availableMembers" [value]="user.id">{{user.email}}</option>
                    </select>
                </div>
                <div class="col-3" style="padding-right:0;padding-left:0;">
                    <select [(ngModel)]="role" class="form-control">
                        <option value="member">View</option>
                        <option value="editor">Edit</option>
                    </select>
                </div>
                <div class="col-3">
                    <a href="javascript:;" (click)="addMember()" class="btn btn-primary"
                       style="padding:8px 0 8px 0;margin-top:1px;width:100%;">Add</a>
                </div>
            </div>

            <hr *ngIf="members.length>0"/>

            <div *ngFor="let member of members"
                 class="btn btn-xs btn-{{(member.role==='editor')?'primary':'default'}} {{member.role}}"
                 style="position:relative;padding:0 17px 0 0;margin-right:5px;margin-bottom:7px;">
                <img src="{{gravatarProvider.gravatar(userById(member.user_id).email, 18)}}"/>
                {{userById(member.user_id).email}}
                <a *ngIf="member.id" href="javascript:;" class="deleteButton" (click)="removeMember(member.id)">&times;</a>
            </div>
            <br/>
            <br/>
        </div>

    `
})
export class UserManager implements OnInit {
    members: any;
    show: boolean = false;
    owner: any;
    usersList: any;
    availableMembers: any;

    userToAdd: any;
    role: any = "member";

    @Input()
    project;

    constructor(
        private gravatarProvider: GravatarProvider,
        private usersApiService: UsersApiService,
        private genericApiProvider: GenericApiProvider,
        private changeDetectorRef: ChangeDetectorRef
    ) {
        this.init();
    }

    async init() {
        this.usersList = await this.usersApiService.list();
        this.members = await this.getMembers();
        this.calcAvailableMembers();
        this.owner = this.userById(this.project.owner_id);
        this.show = true;
    }

    async getMembers() {
        return new Promise((resolve, reject) => {
            if (!this.project) {
                reject();
            } else {
                this.genericApiProvider
                    .type("projects")
                    .getRelation(this.project.id, "projectUser")
                    .subscribe(res => {
                        resolve(
                            res.filter(r =>
                                this.usersList.some(u => u.id == r.user_id)
                            )
                        );
                    });
            }
        });
    }

    ngOnInit() {}

    ngOnChanges(changes: SimpleChanges) {
        this.show = false;
        this.init();
    }

    calcAvailableMembers() {
        const currentMembers = this.members.map(member => {
            return member.user_id;
        });
        if (this.project.owner_id) currentMembers.push(this.project.owner_id);
        const availableMembers = this.usersList.filter(user => {
            return currentMembers.indexOf(user.id) === -1;
        });
        this.availableMembers = availableMembers;
    }

    addMember() {
        if (!Number(this.userToAdd)) return;

        const membership = {
            id: null,
            user_id: Number(this.userToAdd),
            role: this.role,
            project_id: this.project.id
        };

        this.genericApiProvider
            .type("projectUser")
            .post(membership)
            .subscribe(res => {
                membership.id = res.json().id;
                this.members.push(membership);
                this.userToAdd = null;
                this.calcAvailableMembers();
            });
    }

    removeMember(id) {
        this.members = this.members.filter(member => {
            return !(member.id === id);
        });
        this.genericApiProvider
            .type("projectUser")
            .delete(id)
            .subscribe(res => {
                console.warn(res);
            });
        this.calcAvailableMembers();
    }

    userById(userId) {
        return this.usersList.find(u => {
            return u.id === Number(userId);
        });
    }
}
