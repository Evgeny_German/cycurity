import { Injectable } from "@angular/core";
import { kolEditorConfig as kolConfig } from "../../shared/dataModels/kol-config";

export const arrayElements = [
    "location",
    "language",
    "bioKeywords",
    "tweetsKeywords"
];

export function toStartCase(string) {
    return string.replace(/([A-Z])/g, " $1").replace(/^./, function(str) {
        return str.toUpperCase();
    });
}

@Injectable()
export class KolService {
    postLoad(kolFilterConfig: any) {
        if (!kolFilterConfig) kolFilterConfig = {};

        arrayElements.forEach(entry => {
            if (kolFilterConfig[entry] && kolFilterConfig[entry].value) {
                try {
                    kolFilterConfig[entry].value = kolFilterConfig[
                        entry
                    ].value.join(", ");
                } catch (error) {
                    console.error("Error post load: ", error);
                }
            }
        });
        return kolFilterConfig;
    }

    validate(params): { errors: string[]; warnings: string[] } {
        const validationErrors = [];
        const validationWarnings = [];
        if (!params) {
            return { errors: [], warnings: [] };
        }
        for (let key of Object.keys(params)) {
            const type = kolConfig.find(c => c.id === key).type;
            const importance = (params[key] ? params[key].importance : 0) || 0;
            if (type === "range") {
                const from = (params[key] ? params[key].from : 0) || 0;
                const to = (params[key] ? params[key].to : 0) || 0;
                // if (
                //     importance &&
                //     !isNaN(importance) &&
                //     !(importance % 1) &&
                //     importance > 0
                // ) {
                let fromValid = true;
                let toValid = true;
                if (isNaN(from) || from % 1 || from < 0) {
                    fromValid = false;
                    validationErrors.push(
                        `<b>${toStartCase(
                            key
                        )}</b> min value must be a positive integer.`
                    );
                }
                if (isNaN(to) || to % 1 || to < 1) {
                    toValid = false;
                    validationErrors.push(
                        `<b>${toStartCase(
                            key
                        )}</b> max value must be a positive integer.`
                    );
                }
                if (fromValid && toValid && from >= to) {
                    validationErrors.push(
                        `<b>${toStartCase(key)}</b> min must be less than max.`
                    );
                }
                // }

                if (!importance && (from || to)) {
                    validationWarnings.push(
                        `<b>${toStartCase(
                            key
                        )}</b> has min/max, but <b>Importance</b> is 0`
                    );
                }
            } else if (type === "string") {
                const value = params[key] ? params[key].value : null;
                if (!importance && value) {
                    validationWarnings.push(
                        `<b>${toStartCase(
                            key
                        )}</b> has value, but <b>Importance</b> is 0`
                    );
                }
            }
        }

        if (
            kolConfig
                .map(row => row.id)
                .reduce(
                    (sum, key) =>
                        sum +
                        ((params[key] && "importance" in params[key]
                            ? params[key].importance
                            : 0) || 0),
                    0
                ) <= 0
        ) {
            validationErrors.push(
                `At least one <b>Importance</b> score should be higher than 0`
            );
        }

        return {
            errors: validationErrors,
            warnings: validationWarnings
        };
    }

    preSave(kolFilterConfig: object) {
        for (let key of Object.keys(kolFilterConfig).filter(
            entry => !arrayElements.includes(entry)
        )) {
            for (let key2 of Object.keys(kolFilterConfig[key])) {
                if (
                    !kolFilterConfig[key][key2] ||
                    (typeof kolFilterConfig[key][key2] === "string" &&
                        !kolFilterConfig[key][key2].trim().length)
                ) {
                    delete kolFilterConfig[key][key2];
                }
            }
            if (!Object.keys(kolFilterConfig[key]).length) {
                delete kolFilterConfig[key];
            }
        }

        arrayElements.forEach(entry => {
            if (kolFilterConfig[entry] && kolFilterConfig[entry].value) {
                if (!Array.isArray(kolFilterConfig[entry].value)) {
                    kolFilterConfig[entry].value = kolFilterConfig[
                        entry
                    ].value.toString();
                    if (kolFilterConfig[entry].value.length == 0) {
                        kolFilterConfig[entry] = null;
                    } else {
                        kolFilterConfig[entry].value = kolFilterConfig[
                            entry
                        ].value
                            .split(",")
                            .map(e => {
                                return e.trim();
                            })
                            .filter(v => {
                                return v != "";
                            });
                    }
                }
            }
        });
        return kolFilterConfig;
    }
}
