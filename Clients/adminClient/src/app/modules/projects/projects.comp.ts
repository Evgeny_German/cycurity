import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { FlagActions } from "../shared/store/flagsActions";
import { GenericApiProvider } from "../shared/providers/genericApiProvider";
import { KolService } from "./services/kolService";
import { AppCacheProvider } from "../shared/providers/app-cache-provider";
import * as Alertify from "alertifyjs";
import { PermissionsProvider } from "../shared/providers/permissionsProvider";
import * as _ from "lodash";

@Component({
    selector: "projects-main",
    styles: [
        `
            .firstColumn {
                width: 100%;
            }

            .selectedProject .firstColumn {
                width: 50%;
            }

            .message {
                display: none;
                height: 100%;
                line-height: 100%;
            }

            .message div {
                position: absolute;
                top: 50%;
                width: 100%;
                text-align: center;
            }

            .message.visible {
                display: block;
            }

            .deleteLink {
                cursor: pointer;
            }

            .active .deleteLink {
                color: #fff;
            }
        `
    ],
    template: `
        <h1>
            <small>Settings:</small> Projects
        </h1>

        <ul class="nav nav-tabs">
            <li *ngIf="permissionsProvider.booleanModulePermissions('users')" class="nav-item">
                <a class="nav-link" href="javascript:;" [routerLinkActive]="'active'" [routerLink]="['/users']"
                   [routerLinkActiveOptions]="{exact: true}">Users</a>
            </li>
            <li *ngIf="permissionsProvider.booleanModulePermissions('users')" class="nav-item">
                <a class="nav-link" href="javascript:;" [routerLinkActive]="'active'" [routerLink]="['/users/vp']"
                   [routerLinkActiveOptions]="{exact: true}">VP</a>
            </li>
            <li *ngIf="permissionsProvider.booleanModulePermissions('projects')" class="nav-item">
                <a class="nav-link" href="javascript:;" [routerLinkActive]="'active'" [routerLink]="['/projects']" [routerLinkActiveOptions]="{exact: true}">Projects</a>
            </li>
        </ul>

        <div *ngIf="errorResponse" class="alert alert-danger" style="margin-top:10px;">
            <h1>Error <b>{{errorResponse.status}}</b>: {{errorResponse.statusText}}</h1>
            <hr/>
            <h5><b>When reporting this error, please provide the following information to the technical staff:</b></h5>
            {{errorResponse | json}}
        </div>


        <div *ngIf="!errorResponse" class="fullscreenInterface" style="top:182px;border-top:1px solid #ddd;">
            <div class="row" style="height:100%;">
                <div class="col-md-{{(selectedProject)?'4':'2'}} {{(selectedProject)?'selectedProject':''}}"
                     style="border-right:1px dashed #777;">

                    <div style="position:absolute;top:0;right:0;bottom:0;left:0;" class="message {{(changed)?'visible':''}}">
                        <div>Please Save or Discard your changes.</div>
                    </div>

                    <div style="position:absolute;top:0;left:0;right:0;bottom:0;">
                        <div class="firstColumn" style="position:absolute;top:0;bottom:0;left:0;">
                            <listEditor [canAdd]="canAdd" [disabled]="changed" (refresh)="getAllProjects()" [list]="allProjects" [(selected)]="selectedProject"
                                        [itemSingular]="'project'" [itemPlural]="'projects'" [type]="'projects'"
                                        [nameProperty]="'name'">
                                <li style="width:100%;position:relative;" class="nav-item" *tag="'item'; let context">
                                    <a
                                        [ngClass]="{'active':(context.item().id==this.selectedProject?.id)}"
                                        class="nav-link active"
                                        href="javascript:;"
                                        (click)="selectProject(context.item())"
                                    >
                                        <div>{{showName(context.item().name)}}</div>
                                        <div class="deleteLink" onclick="event.stopPropagation()" (click)="deleteProject(context.item())"
                                             style="position:absolute;top:5px;right:5px;">&times;
                                        </div>
                                    </a>
                                </li>
                            </listEditor>
                        </div>
                        <div *ngIf="selectedProject"
                             style="position:absolute;top:0;width:50%;bottom:0;left:50%;border-left:1px dashed #aaa;">
                            <listEditor [canAdd]="canAdd" [disabled]="changed" (refresh)="getSubProjects()" [additionalParams]="{projectId:selectedProject.id}"
                                        [list]="allSubProjects" [(selected)]="selectedSubProject"
                                        [itemSingular]="'topic'" [itemPlural]="'topics'"
                                        [type]="'subprojects'" [nameProperty]="'name'">
                                <li style="width:100%;position:relative;" class="nav-item" *tag="'item'; let context">
                                    <a
                                        [ngClass]="{'active':(context.item().id==this.selectedSubProject?.id)}"
                                        class="nav-link active"
                                        href="javascript:;"
                                        (click)="selectSubProject(context.item())"
                                    >
                                        <div>{{showName(context.item().name)}}</div>
                                        <div class="deleteLink" onclick="event.stopPropagation()" (click)="deleteTopic(context.item())"
                                             style="position:absolute;top:5px;right:5px;">&times;
                                        </div>
                                    </a>
                                </li>
                            </listEditor>
                        </div>
                    </div>
                </div>
                <div class="col-md-{{(selectedProject)?'8':'10'}}">
                    <projectEditor
                        *ngIf="selectedProject"
                        [inboxes]="inboxes"
                        (saveEvent)="saveEvent()"
                        (changeEvent)="changeEvent()"
                        (discardEvent)="discardEvent($event)"
                        [project]="selectedProject"
                        [subproject]="selectedSubProject">
                    </projectEditor>

                    <general-settings
                        *ngIf="!selectedProject">
                    </general-settings>
                </div>
            </div>

        </div>
    `
})
export class ProjectsMain implements OnInit {
    canAdd: boolean;
    inboxes: any;
    changed: boolean;
    change: any;
    allProjects: any;
    allSubProjects: any;
    originalProject: any;
    originalSubproject: any;
    selectedProject: any;
    selectedSubProject: any;
    errorResponse: any;

    constructor(
        private flagActions: FlagActions,
        private permissionsProvider: PermissionsProvider,
        private appCacheProvider: AppCacheProvider,
        private kolService: KolService,
        private genericApiProvider: GenericApiProvider,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.getAllProjects();
        this.getInboxes();
    }

    deleteProject(item) {
        if (this.selectedProject && item.id === this.selectedProject.id) {
            this.selectedProject = null;
        }
        this.genericApiProvider
            .type("projects")
            .delete(item)
            .subscribe(() => {
                this.getAllProjects();
            });
    }

    deleteTopic(item) {
        if (this.selectedSubProject && item.id === this.selectedSubProject.id) {
            this.selectedSubProject = null;
        }
        this.genericApiProvider
            .type("subprojects")
            .delete(item)
            .subscribe(() => {
                this.getSubProjects();
            });
    }

    showName(name: string) {
        //console.warn(name)
        name = name.trim();
        //console.warn(name)
        if (name != "") {
            return name;
        } else {
            return "Unnamed";
        }
    }

    getAllProjects() {
        this.flagActions.setFlag("loading", true);

        this.genericApiProvider
            .type("projects")
            .list({ order: "name ASC", include: "projectUser" })
            .catch((err, caught) => {
                this.errorResponse = err;
                return Observable.of(err);
            })
            .subscribe(response => {
                this.flagActions.setFlag("loading", false);
                this.allProjects = response;
                this.allProjects.forEach(p => {
                    if (!p.kol_filter_config) p.kol_filter_config = {};
                });
                // this.selectedProject = this.allProjects[0];
                if (this.selectedProject) this.getSubProjects();
                this.prepLoad(this.allProjects);
            });
    }

    getInboxes() {
        this.inboxes = null;
        this.genericApiProvider
            .type("getInboxes")
            .list()
            .subscribe(
                response => {
                    this.inboxes = response;
                },
                err => {
                    console.error(JSON.stringify(err));
                    // this.errorResponse = err;
                    Alertify.error(
                        "An error has occured while retreiving Inboxes. See the developer console (F12) for more details."
                    );
                }
            );
    }

    getSubProjects() {
        this.genericApiProvider
            .type("subprojects")
            .list({
                order: "name ASC",
                where: { projectId: this.selectedProject.id }
            })
            .catch((err, caught) => {
                this.errorResponse = err;
                return Observable.of(err);
            })
            .subscribe(response => {
                this.flagActions.setFlag("loading", false);
                this.allSubProjects = response;
                this.allSubProjects.forEach(p => {
                    if (!p.kol_filter_config) p.kol_filter_config = {};
                });
                this.prepLoad(this.allSubProjects);
            });
    }

    ngOnInit() {
        const identity = this.appCacheProvider.getIdentity();
        const canAdd =
            identity.adminRole === "superadmin" ||
            identity.meta.modules.projects;
        this.canAdd = canAdd;
    }

    selectSubProject(subproject) {
        if (
            subproject &&
            !this.selectedSubProject ||
            this.selectedSubProject.id != subproject.id
        ) {
            this.selectedSubProject = this.allSubProjects.find(sp => sp.id == subproject.id);
            this.originalSubproject = _.cloneDeep(subproject);
        } else {
            this.selectedSubProject = null;
            this.originalSubproject = _.cloneDeep(subproject);
        }
    }

    selectProject(project) {
        this.selectedSubProject = null;
        if (!this.selectedProject || this.selectedProject.id != project.id) {
            this.selectedProject = project;
            this.originalProject = _.cloneDeep(project);
            this.getSubProjects();
        } else {
            this.originalProject = null;
            this.selectedProject = null;
        }
    }

    discardEvent() {
        if ((this.selectedProject = this.originalProject)) {
            const index = this.allProjects.findIndex(
                p => p.id == this.originalProject.id
            );
            this.allProjects[index] = this.originalProject;
        }
        if (this.selectedSubProject = this.originalSubproject) {
            const index = this.allSubProjects.findIndex(
                p => p.id == this.originalSubproject.id
            );
            this.allSubProjects[index] = this.originalSubproject;
        }
        this.changed = false;
    }

    prepLoad(list) {
        list.forEach(item => {
            item.kol_filter_config = this.kolService.postLoad(
                item.kol_filter_config
            );
        });
    }

    prepSave(doc) {
        if (!doc) return;
        doc.kol_filter_config = this.kolService.preSave(doc.kol_filter_config);
    }

    saveEvent() {
        this.prepSave(this.selectedProject);
        this.prepSave(this.selectedSubProject);

        this.flagActions.setFlag("loading", true);

        if (!this.selectedSubProject) {
            // Project
            this.genericApiProvider
                .type(`projects`)
                .put(this.selectedProject)
                .catch((err, caught) => {
                    this.flagActions.setFlag("loading", false);
                    this.errorResponse = err;
                    return Observable.of(err);
                })
                .subscribe(response => {
                    this.originalProject = _.cloneDeep(this.selectedProject);
                    this.flagActions.setFlag("loading", false);
                });
        } else {
            // Sub-Project
            this.genericApiProvider
                .type(`subprojects`)
                .put(this.selectedSubProject)
                .catch((err, caught) => {
                    this.flagActions.setFlag("loading", false);
                    this.errorResponse = err;
                    return Observable.of(err);
                })
                .subscribe(response => {
                    this.getInboxes(); // TODO: Manage this locally to reduce number of Engagor calls
                    this.originalSubproject = _.cloneDeep(
                        this.selectedSubProject
                    );
                    this.flagActions.setFlag("loading", false);
                });
        }

        this.changed = false;
    }

    changeEvent() {
        this.changed = true;
    }
}
