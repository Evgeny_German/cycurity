import {NgModule} from '@angular/core';
import {SearchMain} from "./search.comp";
import {SearchBox} from "./components/searchBox.comp";
import {ResultsBox} from "./components/resultsBox.comp";
import {RouterModule, Routes} from "@angular/router";
import {AuthenticationCallbackActivateGuard} from "../shared/auth/auth-guard-provider";
import {SharedModule} from "../shared/shared.module";
import {SearchApiService} from "./services/search-api.service";
import {SearchActions} from "./store/searchActions";
import {SearchEpics} from "./store/searchEpics";
import {SearchResult} from "./components/searchResult";

export const REPORT_MAIN_URL = 'search';

const homeRoutes: Routes = [
  {
    path: REPORT_MAIN_URL , component: SearchMain ,canActivate:[AuthenticationCallbackActivateGuard]
  }
];


@NgModule({
  imports: [RouterModule.forChild(homeRoutes),SharedModule],
  exports: [SearchBox, ResultsBox, SearchResult],
  declarations: [SearchMain, SearchBox, ResultsBox, SearchResult],
  providers: [SearchApiService, SearchActions, SearchEpics],
})
export class SearchModule {
}
