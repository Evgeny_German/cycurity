import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'search-main',

  template: `
      <div class="alert alert-danger"><b>Notice</b>: This page has been deprecated</div>
      <searchBox (onSearch)="searchValueChanged($event)"></searchBox>
      <resultsBox *ngIf="showResults" [searchTerm]="searchTerm"></resultsBox>
  `
})
export class SearchMain implements OnInit {

    private searchTerm: string;
    private showResults: boolean;

    constructor(private router: Router) {
    }
    searchValueChanged(searchTerm:string) {
        this.searchTerm = searchTerm;
        this.showResults = !!searchTerm;
    }
    ngOnInit() {
        // this.router.navigate(['/home']);
    }
}
