import {Component, OnInit, Input, EventEmitter, Output, AfterViewInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import "rxjs/add/operator/delay";
import * as _ from 'lodash';

@Component({
    selector: 'searchBox',
    styles: [`

        @media screen and (min-width: 576px) {
            .card {
                box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.1);
            }

            .card:not(.pinned) {
                position: fixed;
                top: 50%;
                left: 50%;
                margin-top: -50px;
                margin-left: -285px;
                width: 575px;
            }
        }

        @media screen and (max-width: 700px) {
            a {
                display: block;
                width: 100%;
            }

            input {
                display: block;
                width: 100%;
            }
        }

    `],
    template: `
        <div class="card {{(searchTermActive)?'pinned':''}}">
            <div class="card-block" style="padding-right:0;">
                <div class="row" style="width:100%;">
                    <div class="col-sm-10">
                        <input (keyup.enter)="search()" style="width:100%;" [(ngModel)]="searchTerm" name="searchTerm"
                               class="form-control mr-sm-2" type="text" placeholder="Enter search term or tweet URL">
                    </div>
                    <div class="col-sm-2" style="padding:0;">
                        <a href="javascript:;" style="width:100%;" (click)="search()" class="btn btn-primary">Search</a>
                    </div>
                </div>
            </div>
        </div>
    `
})
export class SearchBox implements OnInit {

    private searchTerm: string;
    private searchTermActive: boolean = false;

    @Output()
    public onSearch = new EventEmitter<string>();

    constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    }

    search() {
        this.searchTerm = this.searchTerm.replace(/http:\/\/twitter.com\//i, 'https://twitter.com/');
        if (_.startsWith(this.searchTerm, 'https://twitter.com/')) {
            this.router.navigate(['conversation', {q: this.searchTerm || ''}]);

        }
        else {
            this.router.navigate([this.activatedRoute.snapshot.url[0].path, {q: this.searchTerm || ''}]);
        }
    }

    ngOnInit() {
        this.activatedRoute.params.delay(0).subscribe((params: Params) => {

            let q = params['q'];

            this.searchTerm = q; // entered URL with q param

            this.onSearch.emit(q);
            this.searchTermActive = !!q; // true if field has value
        });
    }
}
