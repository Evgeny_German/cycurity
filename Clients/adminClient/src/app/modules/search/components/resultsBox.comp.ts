import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FlagActions} from "../../shared/store/flagsActions";
import {SearchApiService} from "../services/search-api.service"
import {Observable} from "rxjs/Observable";

@Component({
    selector: 'resultsBox',
    styles: [`
    `],
    template: `
        <div class="container-fluid" style="margin-top:1em;">

            <div *ngIf="errorResponse" class="alert alert-danger">
                <h1>Error <b>{{errorResponse.status}}</b>: {{errorResponse.statusText}}</h1>
                <hr/>
                <h5><b>When reporting this error, please provide the following information to the technical staff:</b>
                </h5>
                {{errorResponse | json}}
            </div>

            <div class="row" *ngIf="searchResults">
                <div class="col-12">

                    <h4>{{searchResults.length}} Result<span *ngIf="searchResults.count>1">s</span> Found</h4>

                    <div class="row">
                        <table class="table table-hover col-12">
                            <tbody>
                            <tr *ngFor="let result of searchResults">
                                <td style="padding:0;">
                                    <searchResult [result]="result"></searchResult>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    `
})
export class ResultsBox implements OnInit {
    errorResponse: any;

    searchResults: any;
    q: any;

    @Input()
    public searchTerm: string;

    goToConversation(twitterUrl) {
        this.router.navigate(['conversation', {q: twitterUrl}]);
    }

    constructor(private searchApiService: SearchApiService, private flagActions: FlagActions, private router: Router, private activatedRoute: ActivatedRoute) {

        this.activatedRoute.params.delay(0).subscribe((params: Params) => {
            this.q = params['q'];
            if (!this.q) {
                this.searchResults = null;
            } else {

                this.flagActions.setFlag('loading', true);
                this.searchApiService.search(this.q)
                    .catch((err, caught) => {
                        this.flagActions.setFlag('loading', false);
                        return Observable.of(err);
                    })
                    .subscribe((res) => {
                        this.flagActions.setFlag('loading', false);
                        if (!res.ok) {
                            this.errorResponse = res;
                        } else {
                            this.searchResults = res.json();
                        }
                    })
                ;
            }
        });
    }

    ngOnInit() {
    }
}
