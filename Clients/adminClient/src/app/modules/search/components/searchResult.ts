import {Component, Input, OnInit} from '@angular/core';
import * as _ from 'lodash';
import {Router} from "@angular/router";

@Component({
    selector: 'searchResult',
    styles: [`
        .media {
            padding: 15px;
            cursor:pointer;
        }
        
        img.avatar {
            width: 32px;
            height: 32px;
            border-radius: 50%;
            border: 3px solid #fff;
            box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.4);
            margin-right:10px;
        }

        .user {
            font-weight: bold;
        }

        .statistics span {
            font-weight: bold;
        }

        .statistics div {
            white-space: nowrap;
        }
    `],
    template: `
        <div>

            <div (click)="navigate()" class="media" style="padding:10px 0 0 10px;">
                <img class="avatar" src="{{result.tweet.avatar||'https://abs.twimg.com/sticky/default_profile_images/default_profile_bigger.png'}}" alt="">
                <div class="media-body">
                    <a class="user" href="{{result.tweet.userName}}">{{result.tweet.fullName}}</a>
                    <small>@{{result.tweet.userName}}</small>
                    <p>{{result.tweet.tweetText}}</p>
                </div>
            </div>
            
            

        </div>

    `
})
export class SearchResult implements OnInit {

    @Input() public result: any;

    navigate() {
        const tweetUrl = `https://twitter.com/${this.result.tweet.userName}/status/${this.result.tweet.tweetId}`;
        this.router.navigate(['conversation', {q:tweetUrl||''}]);
    }

    constructor(private router: Router) {
    }

    ngOnInit() {
    }
}
