import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {ActionsObservable} from "redux-observable";
import {Action} from "redux";
import {Observable} from "rxjs/Observable";
import {SearchApiService} from "../services/search-api.service";
import {AppCacheProvider} from "../../shared/providers/app-cache-provider";

@Injectable()
export class SearchEpics {
    epics = [];

    constructor(
        private router: Router,
        private appCacheProvider:AppCacheProvider,
        private searchApiService: SearchApiService
    ) {
        this.epics = []
    }
}



