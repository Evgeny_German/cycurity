import {Injectable} from '@angular/core';
import {Http} from "@angular/http";

@Injectable()
export class SearchApiService {

    constructor(private http: Http) {

    }

    //private MENTIONS_URL = "/documentStorage/search";
    private SEARCH_URL = "/scrape/searchConversation";

    search = (freeText:string) => {
        return this.http.get(`${this.SEARCH_URL}?freeText=${freeText}` );
    }
}
