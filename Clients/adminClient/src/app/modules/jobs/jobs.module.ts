import {NgModule} from '@angular/core';
import {JobsMain} from "./jobs.comp";
import {RouterModule, Routes} from "@angular/router";
import {AuthenticationCallbackActivateGuard} from "../shared/auth/auth-guard-provider";
import {SharedModule} from "../shared/shared.module";

export const JOBS_MAIN_URL = 'jobs';

const jobsRoutes: Routes = [
  {
      path: JOBS_MAIN_URL, component: JobsMain,canActivate:[AuthenticationCallbackActivateGuard]

  }
];

@NgModule({
  imports: [RouterModule.forChild(jobsRoutes),SharedModule],
  exports: [JobsMain],
  declarations: [JobsMain],
  providers: [],
})

export class JobsModule {
}
