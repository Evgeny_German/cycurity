import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { Http } from "@angular/http";
import { AppCacheProvider } from "../shared/providers/app-cache-provider";

export const status = {
  started: { color: "" },
  finished: { color: "" },
  canceled: { color: "" }
};

@Component({
  selector: "jobs",
  template: `
        <div>
            <h1>JOBS</h1>
            
            <div class="alert alert-info">This page has been deprecated</div>

            <div *ngIf="jobs && !jobs.length" class="alert alert-info">There are currently no jobs on your list</div>

            <table *ngIf="jobs && jobs.length" class="table">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Title</th>
                        <th>Started</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <ng-container *ngFor="let job of jobs">
                    <tr *ngIf="job.type===1">
                        <td>Export</td>
                        <td>{{job?.payload?.name}}</td>
                        <td>{{job.start_date}}</td>
                        <td title="{{job.status}}">
                        <ng-container *ngIf="job.status=='queued'"><i class="fa fa-circle-o-notch fa-spin"></i> <small>Queued</small></ng-container>
                        <ng-container *ngIf="job.status=='started'"><i class="fa fa-circle-o-notch fa-spin"></i> <small>Started</small></ng-container>
                        <ng-container *ngIf="job.status=='finished'"><i style="color:#070;" class="fa fa-check"></i> <small>Finished</small></ng-container>
                            <ng-container *ngIf="job.status=='canceled'"><i style="color:#f00;" class="fa fa-ban"></i> <small>Canceled</small></ng-container>
                        </td>
                        <td>
                            <ng-container *ngIf="job.output">
                                <div *ngIf="job.output.success"><a href="/static/{{job.output.output}}" download="report.docx">Download</a></div>
                                <div *ngIf="!job.output.success" class="text-danger">Job Failed <a href="javascript:;" title="{{job.output|json}}"><small>Why?</small></a></div>
                            </ng-container>
                        </td>
                    </tr>
                    <tr *ngIf="job.type===2">
                        <td>Scrape</td>
                        <td>https://twitter.com/{{job?.payload?.userName}}/status/{{job?.payload?.tweetId}}</td>
                        <td>{{job.start_date}}</td>
                        <td title="{{job.status}}">
                            <ng-container *ngIf="job.status=='queued'"><i class="fa fa-circle-o-notch fa-spin"></i> <small>Queued</small></ng-container>
                            <ng-container *ngIf="job.status=='started'"><i class="fa fa-circle-o-notch fa-spin"></i> <small>Started</small></ng-container>
                            <ng-container *ngIf="job.status=='finished'"><i style="color:#070;" class="fa fa-check"></i> <small>Finished</small></ng-container>
                            <ng-container *ngIf="job.status=='canceled'"><i style="color:#f00;" class="fa fa-ban"></i> <small>Canceled</small></ng-container>
                            <ng-container *ngIf="job.status=='scraped'"><i style="color:#f0f;" class="fa fa-check"></i> <small>Scraped</small></ng-container>
                            <ng-container *ngIf="job.status=='error'"><i style="color:#f00;" class="fa fa-times"></i> <small>Failed</small></ng-container>
                        </td>
                        <td></td>
                    </tr>
                </ng-container>
            </table>
        </div>
    `
})
export class JobsMain implements OnInit, OnDestroy {
  tick: any;

  ngOnDestroy(): void {
    clearInterval(this.tick);
    this.subscriber.unsubscribe();
  }

  subscriber: any;
  identity: any;
  jobs: any;

  constructor(private http: Http, private appCacheProvider: AppCacheProvider) {
    this.identity = this.appCacheProvider.getIdentity();
  }

  ngOnInit() {
    this.updateJobs();
    this.tick = setInterval(() => {
      this.updateJobs();
    }, 20000);
  }

  updateJobs() {
    if (this.subscriber) {
      this.subscriber.unsubscribe();
    }
    this.subscriber = this.http
      .get(
      `/job/jobs?filter=${encodeURIComponent(
        JSON.stringify({
            where: { user_id: this.identity.id },
            order: "start_date DESC",
            limit: 50
        })
      )}`
      )
      .subscribe(res => {
        const r = res.json();
        this.jobs = r ? r : null;
      });
    /*
        .sort((x, y) =>
            x.start_date === y.start_date
              ? 0
              : x.start_date > y.start_date
                ? -1
                : 1)
    * */
  }
}
