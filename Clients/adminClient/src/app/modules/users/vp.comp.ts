import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { GenericApiProvider } from "../shared/providers/genericApiProvider";
import * as alertify from "alertifyjs";
import { PermissionsProvider } from "../shared/providers/permissionsProvider";
// import {FlagActions} from "../shared/store/flagsActions";
// import {Http} from "@angular/http";
// import {GravatarProvider} from "../shared/providers/gravatarProvider";
// import {UsersApiService} from "./services/users-api.service";

// export const modules = ['search', 'reports', 'projects', 'users', 'settings'];

@Component({
    selector: "vp-users-main",
    styles: [
        `
            tr:hover {
                cursor: pointer;
            }

            tr td {
                vertical-align: middle;
            }

            .deleteLink {
                cursor: pointer;
            }

            .active .deleteLink {
                color: #fff;
            }
        `
    ],
    template: `
        <div>
            <h1>
                <small>Settings:</small>
                VP
            </h1>

            <ul class="nav nav-tabs">
                <li *ngIf="permissionsProvider.booleanModulePermissions('users')" class="nav-item">
                    <a class="nav-link" href="javascript:;" [routerLinkActive]="'active'" [routerLink]="['/users']"
                       [routerLinkActiveOptions]="{exact: true}">Users</a>
                </li>
                <li *ngIf="permissionsProvider.booleanModulePermissions('users')" class="nav-item">
                    <a class="nav-link" href="javascript:;" [routerLinkActive]="'active'" [routerLink]="['/users/vp']"
                       [routerLinkActiveOptions]="{exact: true}">VP</a>
                </li>
                <li *ngIf="permissionsProvider.booleanModulePermissions('projects')" class="nav-item">
                    <a class="nav-link" href="javascript:;" [routerLinkActive]="'active'" [routerLink]="['/projects']" [routerLinkActiveOptions]="{exact: true}">Projects</a>
                </li>
            </ul>


            <div class="row"
                 style="margin:0;position:fixed;top:183px;left:0;bottom:0;right:0;border-top:1px solid #ddd;">

                <div class="col-2" style="height:100%;overflow-y:auto;border-right:1px dashed #777;">
                    <listEditor [disabled]="false"
                                [canAdd]="permission === 'EDIT'"
                                (refresh)="getAllVoRoles()"
                                [list]="allVoRoles"
                                [(selected)]="selectedVoRole"
                                [itemSingular]="'vp role'"
                                [itemPlural]="'VP roles'"
                                [type]="'voRole'"
                                [nameProperty]="'name'">
                        <li style="width:100%;position:relative;"
                            [ngClass]="{'active':(context.item().id==this.selectedVoRole?.id)}" class="nav-item"
                            *tag="'item'; let context">
                            <a
                                [ngClass]="{'active':(context.item().id==this.selectedVoRole?.id)}"
                                class="nav-link"
                                href="javascript:;"
                                (click)="selectedVoRole=(selectedVoRole && (context.item().id==this.selectedVoRole?.id))?null:context.item()"
                            >
                                <div>
                                    {{context.item().name}}
                                </div>
                            </a>
                            <div *ngIf="permission === 'EDIT'" class="deleteLink" (click)="deleteVoRole(context.item())"
                                 style="position:absolute;top:5px;right:5px;">&times;
                            </div>
                        </li>
                    </listEditor>
                </div>
                <div class="col-10" style="height:100%;overflow-y:auto;">
                    <voEditor [role]="selectedVoRole"></voEditor>
                </div>
            </div>

        </div>
    `
})
export class VpUserMain implements OnInit {
    permission: "EDIT" | "READ" | "NONE";

    allVoRoles: any;
    selectedVoRole: any;

    constructor(
        private genericApiProvider: GenericApiProvider,
        private permissionsProvider: PermissionsProvider
    ) {}

    ngOnInit() {
        this.permission = this.permissionsProvider.modulePermissions("users");
        this.getAllVoRoles();
    }

    getAllVoRoles() {
        this.genericApiProvider
            .type("voRole")
            .list({ order: "name ASC" })
            .subscribe(maintenanceProfiles => {
                this.allVoRoles = maintenanceProfiles;
            });
    }

    deleteVoRole(item) {
        if (this.selectedVoRole && item.id === this.selectedVoRole.id) {
            this.selectedVoRole = null;
        }
        this.genericApiProvider
            .type("voRole")
            .delete(item)
            .subscribe(
                () => {
                    this.getAllVoRoles();
                },
                err => {
                    alertify.error(`Error: ${err.message || err}`);
                }
            );
    }
}
