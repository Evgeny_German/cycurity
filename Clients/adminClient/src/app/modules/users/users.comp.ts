import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { FlagActions } from "../shared/store/flagsActions";
import { Http } from "@angular/http";
import { GravatarProvider } from "../shared/providers/gravatarProvider";
import { UsersApiService } from "./services/users-api.service";
import { PermissionsProvider } from "../shared/providers/permissionsProvider";

export const modules = {
    reports: "Reports",
    projects: "Projects",
    users: "Users",
    settings: "Settings",
    vp: "VP Management",
    mass: "Mass Scrape"
};

@Component({
    selector: "users-main",
    styles: [
        `
            tr:hover {
                cursor: pointer;
            }

            tr td {
                vertical-align: middle;
            }
        `
    ],
    template: `
        <div>
            <h1>
                <small>Settings:</small> Users
                <a *ngIf="permission === 'EDIT'" style="margin-top:8px;" [routerLink]="['/users/create']" class="btn btn-xs btn-primary pull-right">Create
                    User</a>
            </h1>

            <ul class="nav nav-tabs">
                <li *ngIf="permissionsProvider.booleanModulePermissions('users')" class="nav-item">
                    <a class="nav-link" href="javascript:;" [routerLinkActive]="'active'" [routerLink]="['/users']" [routerLinkActiveOptions]="{exact: true}">Users</a>
                </li>
                <li *ngIf="permissionsProvider.booleanModulePermissions('users')" class="nav-item">
                    <a class="nav-link" href="javascript:;" [routerLinkActive]="'active'" [routerLink]="['/users/vp']" [routerLinkActiveOptions]="{exact: true}">VP</a>
                </li>
                <li *ngIf="permissionsProvider.booleanModulePermissions('projects')" class="nav-item">
                    <a class="nav-link" href="javascript:;" [routerLinkActive]="'active'" [routerLink]="['/projects']" [routerLinkActiveOptions]="{exact: true}">Projects</a>
                </li>
            </ul>

            <table class="table table-hover">
                <tbody>
                <tr (click)="router.navigateByUrl('/users/modify/'+user.id)" *ngFor="let user of usersList">
                    <td style="width:48px;" scope="row">
                        <img class="d-flex mr-3" [src]="gravatar(user.email, 48)" alt=""/>
                    </td>
                    <td style="width:25%;font-weight:bold;">{{user.email}}</td>
                    <td>Modules:
                        <small style="text-transform: capitalize;">{{moduleList(user)}}</small>
                    </td>
                </tr>
                </tbody>
            </table>


        </div>
    `
})
export class UsersMain implements OnInit {
    permission: string;
    usersList: any;

    constructor(
        private flagActions: FlagActions,
        private http: Http,
        private router: Router,
        private gravatarProvider: GravatarProvider,
        private usersApiService: UsersApiService,
        private permissionsProvider: PermissionsProvider
    ) {
        this.permission = permissionsProvider.modulePermissions("users");
        this.getList();
    }

    async getList() {
        this.flagActions.setFlag("loading", true);
        this.usersList = await this.usersApiService.list({
            order: "email ASC"
        });
        this.flagActions.setFlag("loading", false);
    }

    gravatar(email, size?) {
        return this.gravatarProvider.gravatar(email, size);
    }

    ngOnInit() {}

    moduleList(user) {
        var activeModules = [];
        if (user.meta && user.meta.modules) {
            for (let mod in modules) {
                if (user.meta.modules[mod] || user.adminRole === "superadmin") {
                    activeModules.push(modules[mod]);
                }
            }
        }

        if (activeModules.length == 0) {
            activeModules.push("None");
        }

        return activeModules.join(", ");
    }
}
