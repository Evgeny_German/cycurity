import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { GenericApiProvider } from "../../shared/providers/genericApiProvider";
import * as alertify from "alertifyjs";
import { PermissionsProvider } from "../../shared/providers/permissionsProvider";

@Component({
    selector: "user-projects",
    styles: [
        `
    `
    ],
    template: `
        <div>
            <div *ngIf="!projects || !allProjects || !sharedProjects">
                <i class="fa fa-circle-o-notch fa-spin text-light"></i>
            </div>

            <ng-container *ngIf="projects">
                <div *ngIf="projects.length==0" class="alert alert-info">
                    This user has no projects
                </div>
                <div *ngIf="projects.length>0">

                    <div class="row">
                        <div class="col-12">
                            <ul class="nav flex-column">
                                <li *ngFor="let project of projects" class="nav-item" style="position:relative;">
                                    <ng-container *ngIf="isNoOwner(project) || isOwner(project) || isEditor(project) || isMember(project)">
                                        <a class="nav-link" [routerLink]="['/projects']">
                                            <ng-container *ngIf="project.name?.trim() != ''; else unnamedProject">
                                                {{project.name}}
                                            </ng-container>
                                            <ng-template #unnamedProject>
                                                <i>Unnamed</i>
                                            </ng-template>
                                            &nbsp;<!-- <small>{{project.last_update}}</small>-->
                                            <span class="pull-right" style="display:inline-block; margin-right: 20px; color: black;" *ngIf="!isNoOwner(project) && !isOwner(project) && isEditor(project)">Edit</span>
                                            <span class="pull-right" style="display:inline-block; margin-right: 20px; color: black;" *ngIf="!isNoOwner(project) && !isOwner(project) && isMember(project)">View</span>
                                        </a>
                                        <a *ngIf="!isOwner(project) && !isNoOwner(project)" (click)="removeUserFromProject(project.id)" href="javascript:;" style="position:absolute;right:10px;top:3px;color:#000;text-decoration:none;" onclick="event.stopPropagation()">&times;</a>
                                        <i *ngIf="isOwner(project) || isNoOwner(project)" class="fa fa-lock" style="position:absolute;right:10px;top:3px;color:#ddd;text-decoration:none;"></i>
                                    </ng-container>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>


                <hr/>

                <div class="row" style="padding:0 12px 0 15px;">
                    <div class="col-6">
                        <select [ngModel]="selectedProject||''" (ngModelChange)="selectedProject = $event==''?null:$event" class="form-control">
                            <option value="">Select Project</option>
                            <option *ngFor="let project of allAvailableProjects()" [value]="project.id">
                                <ng-container *ngIf="project.name?.trim() != ''; else unnamedProjectOption">
                                    {{project.name}}
                                </ng-container>
                                <ng-template #unnamedProjectOption>
                                    Unnamed
                                </ng-template>
                            </option>
                        </select>
                    </div>

                    <div class="col-3">
                        <select [ngModel]="selectedRole||''" (ngModelChange)="selectedRole = $event==''?null:$event" class="form-control">
                            <option value="">Select Role</option>
                            <option value="member">View</option>
                            <option value="editor">Edit</option>
                        </select>
                    </div>

                    <div class="col-3">
                        <button [disabled]="!selectedProject || !selectedRole" (click)="addUserToProject()" style="width:100%;" class="btn btn-primary">Add</button>
                    </div>
                </div>

            </ng-container>
        </div>
    `
})
export class UserProjects implements OnInit {
    allProjects: any;
    editableProjects: number[];
    selectedProject = null;
    selectedRole = null;

    isNoOwner(project): boolean {
        return !project.owner_id || project.owner_id == "0";
    }

    isOwner(project, userId?): boolean {
        if (!userId) userId = this.userId;
        return project.owner_id == userId;
    }

    isEditor(project, userId?): boolean {
        if (!userId) userId = this.userId;
        return project.projectUser
            ? project.projectUser.some(
                  pu => pu.user_id == userId && pu.role === "editor"
              )
            : false;
    }

    isMember(project, userId?): boolean {
        if (!userId) userId = this.userId;
        return project.projectUser
            ? project.projectUser.some(
                  pu => pu.user_id == userId && pu.role === "member"
              )
            : false;
    }

    @Input() userId: number;

    private projects: any[];

    addUserToProject() {
        this.genericApiProvider
            .type("projectUser")
            .post({
                id: null,
                user_id: Number(this.userId),
                role: this.selectedRole,
                project_id: this.selectedProject
            })
            .subscribe(
                res => {
                    this.getUserProjects();
                    this.selectedProject = null;
                    this.selectedRole = null;
                },
                err => {
                    if (err.json) {
                        err = err.json();
                    }

                    alertify.error(
                        `Error: ${
                            err.status == 401
                                ? "Unauthorized"
                                : err.message
                                    ? err.message
                                    : err.status
                        }`
                    );
                }
            );
    }

    removeUserFromProject(projectId) {
        // console.warn(projectId, this.userId);
        const filter = {
            where: {
                project_id: Number(projectId),
                user_id: Number(this.userId)
            }
        };
        this.genericApiProvider
            .type("projectUser")
            .list(filter)
            .subscribe(response => {
                if (!response || !response.length) {
                    alertify.error(`Error: Unauthorized`);
                    return;
                }
                const membershipId = response[0].id;
                this.genericApiProvider
                    .type("projectUser")
                    .delete(membershipId)
                    .subscribe(
                        () => {
                            this.getUserProjects();
                        },
                        err => {
                            if (err.json) {
                                err = err.json();
                            }

                            alertify.error(
                                `Error: ${
                                    err.status == 401
                                        ? "Unauthorized"
                                        : err.message
                                            ? err.message
                                            : err.status
                                }`
                            );
                        }
                    );
            });
    }

    constructor(
        private http: Http,
        private genericApiProvider: GenericApiProvider,
        private permissionsProvider: PermissionsProvider
    ) {
        this.projects = null;
    }
    getUserProjects() {
        this.http
            .get(
                "/projectManagement/projectUsers/projects?user_id=" +
                    this.userId
            )
            .catch((error: any, source: Observable<any>) => {
                this.projects = [];
                return Observable.throw(error);
            })
            .subscribe(res => {
                this.projects = res.json();
            });
    }

    ngOnInit() {
        this.getAllProjects();
        this.getUserProjects();
    }

    allAvailableProjects() {
        if (!this.allProjects || !this.projects) return [];
        const userId = this.permissionsProvider.identity.id;
        const allCurrentIds = this.projects.map(project => {
            return project.id;
        });
        return this.allProjects.filter(project => {
            return (
                (this.isNoOwner(project) ||
                    this.isOwner(project, userId) ||
                    this.isEditor(project, userId)) &&
                allCurrentIds.indexOf(project.id) < 0
            );
        });
    }

    getAllProjects() {
        this.genericApiProvider
            .type("projects")
            .list({ include: "projectUser" })
            .catch((err, caught) => {
                return Observable.of(err);
            })
            .subscribe(response => {
                this.allProjects = response;
            });
    }
}
