import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ChangeDetectorRef,
    SimpleChange,
    OnDestroy,
    SimpleChanges
} from "@angular/core";
import * as alertify from "alertifyjs";
import { Router } from "@angular/router";
import { GenericApiProvider } from "../shared/providers/genericApiProvider";
import { PermissionsProvider } from "../shared/providers/permissionsProvider";

@Component({
    selector: "voEditor",
    styles: [
        `
            h5 input {
                font-size: 23px;
            }

            .form-check {
            }

            input[type="checkbox"]:checked:before,
            .checkbox input[type="checkbox"]:checked:before,
            .checkbox-inline input[type="checkbox"]:checked:before {
                height: 17px;
            }

            input[type="checkbox"] {
                width: 18px;
            }

            input[type="checkbox"]:focus {
                box-shadow: 0 0 12px rgba(55, 155, 255, 0.8);
            }
        `
    ],
    template: `

        <div class="row" *ngIf="!role" style="height:100%;">
            <div class="col-md-12" style="height:100%;">
                <div style="position:absolute;top:50%;left:50%;margin-top:-40px;margin-left:-50px;">
                    Please select a role
                </div>
            </div>
        </div>

        <div class="row" *ngIf="role">

            <div class="col-12">
                <h5><input [disabled]="permission !== 'EDIT'" class="form-control" [(ngModel)]="role.name"/></h5>

                <div class="card" style="width:100%;padding:1em;">
                    <div class="row">

                        <div class="col-4">

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="1" [(ngModel)]="role.settings.projectTopicCombo" class="form-check-input"
                                           type="checkbox">
                                    Project &amp; Topic
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="4" [(ngModel)]="role.settings.currentLocation" class="form-check-input"
                                           type="checkbox">
                                    Service and Zone
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="7" [(ngModel)]="role.settings.twitterCreationDate" class="form-check-input"
                                           type="checkbox">
                                    Creation Date
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="10" [(ngModel)]="role.settings.statusId" class="form-check-input"
                                           type="checkbox">
                                    M. Profile
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="13" [(ngModel)]="role.settings.phoneNumber" class="form-check-input"
                                           type="checkbox">
                                    Phone Number
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="16" [(ngModel)]="role.settings.creationLocation" class="form-check-input"
                                           type="checkbox">
                                    Creation Location
                                </label>
                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="2" [(ngModel)]="role.settings.accountHistory" class="form-check-input"
                                           type="checkbox">
                                    Account History
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="5" [(ngModel)]="role.settings.additionalInformation" class="form-check-input"
                                           type="checkbox">
                                    Additional Information
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="8" [(ngModel)]="role.settings.twitterHandle" class="form-check-input"
                                           type="checkbox">
                                    Twitter Handle
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="11" [(ngModel)]="role.settings.bio" class="form-check-input" type="checkbox">
                                    Bio
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="14" [(ngModel)]="role.settings.language" class="form-check-input"
                                           type="checkbox">
                                    Language
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="17" [(ngModel)]="role.settings.twitterPassword" class="form-check-input"
                                           type="checkbox">
                                    Twitter Password
                                </label>
                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="3" [(ngModel)]="role.settings.profilepic" class="form-check-input"
                                           type="checkbox">
                                    Profile Pic URL
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="6" [(ngModel)]="role.settings.name" class="form-check-input" type="checkbox">
                                    VP Identifier
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="9" [(ngModel)]="role.settings.backgroundpic" class="form-check-input"
                                           type="checkbox">
                                    Background Pic URL
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="12" [(ngModel)]="role.settings.email" class="form-check-input" type="checkbox">
                                    Email
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="15" [(ngModel)]="role.settings.recoveryInfo" class="form-check-input"
                                           type="checkbox">
                                    Recovery Info
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="18" [(ngModel)]="role.settings.emailPassword" class="form-check-input"
                                           type="checkbox">
                                    Email Password
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input [disabled]="permission !== 'EDIT'" tabIndex="21" [(ngModel)]="role.settings.emailCreationName" class="form-check-input"
                                           type="checkbox">
                                    Creation Name
                                </label>
                            </div>


                        </div>



                    </div>

                </div>
            </div>

            <div class="col-12">
                <hr/>
                <input *ngIf="permission==='EDIT'" class="btn btn-primary btn-xs pull-right ml-1" type="button" (click)="save()" value="Save"/>
            </div>

        </div>

    `
})
export class VpRoleForm implements OnInit {

    permission: "EDIT" | "READ" | "NONE";

    @Input()
    role;

    constructor(
        private router: Router,
        private genericApiProvider: GenericApiProvider,
        private permissionProvider: PermissionsProvider
    ) {}

    save() {
        this.genericApiProvider
            .type("voRole")
            .put(this.role)
            .subscribe(
                () => {
                    alertify.success("Saved");
                },
                err => {
                    alertify.error(`Error: ${err.message || err}`);
                }
            );
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.role) {
            if (!this.role.settings) this.role.settings = {};
            // this.backupRole = JSON.parse(JSON.stringify(this.role));
        }
    }

    ngOnInit(): void {
        this.permission = this.permissionProvider.modulePermissions("users");
    }
}
