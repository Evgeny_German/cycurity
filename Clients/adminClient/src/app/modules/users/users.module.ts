import {NgModule} from '@angular/core';

import {UsersMain} from "./users.comp";
import {UsersForm} from "./usersForm.comp";

import {RouterModule, Routes} from "@angular/router";
import {AuthenticationCallbackActivateGuard} from "../shared/auth/auth-guard-provider";
import {SharedModule} from "../shared/shared.module";

import {UsersApiService} from "./services/users-api.service";
import {VpUserMain} from "./vp.comp";
import {VpRoleForm} from "./vpForm.comp";

import {UserProjects} from "./components/user-projects.comp";

export const USERS_MAIN_URL = 'users';

const usersRoutes: Routes = [
    {
        path: USERS_MAIN_URL, component: UsersMain, canActivate: [AuthenticationCallbackActivateGuard]
    },
    {
        path: `${USERS_MAIN_URL}/create`, component: UsersForm, canActivate: [AuthenticationCallbackActivateGuard]
    },
    {
        path: `${USERS_MAIN_URL}/modify/:userId`,
        component: UsersForm,
        canActivate: [AuthenticationCallbackActivateGuard]
    },
    {
        path: `${USERS_MAIN_URL}/vp`, component: VpUserMain, canActivate: [AuthenticationCallbackActivateGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(usersRoutes), SharedModule],
    exports: [UsersMain, UsersForm, VpUserMain, VpRoleForm, UserProjects],
    declarations: [UsersMain, UsersForm, VpUserMain, VpRoleForm, UserProjects],
    providers: [UsersApiService],
})

export class UsersModule {
}
