import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FlagActions } from "../shared/store/flagsActions";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { SaveButtonWarning } from "../shared/components/saveButton.comp";
import * as alertify from "alertifyjs";
import { UsersApiService } from "./services/users-api.service";
import { GenericApiProvider } from "../shared/providers/genericApiProvider";
import { PermissionsProvider } from "../shared/providers/permissionsProvider";

@Component({
    selector: "usersForm-main",
    styles: [
        `

        .dim{
            opacity:0.2;
            cursor:not-allowed;
            pointer-events:none;
        }

        .card {
            padding: 10px;
            margin-bottom: 2em;
        }

        .permission_READ *{
            pointer-events: none;
        }
    `
    ],
    template: `
        <div *ngIf="userNotFound">
            <div class="alert alert-danger">User not found</div>
        </div>


        <div *ngIf="user" class="permission_{{permission}}">
            <h1>
                <span *ngIf="!userId">Create User</span>
                <span *ngIf="userId">Modify User</span>

                <saveButton
                    *ngIf="permission=='EDIT'; else readOnly"
                    class="pull-right"
                    caption="Save"
                    [warnings]="formWarnings"
                    (action)="formSave()"
                    [active]="formSaveActive">
                </saveButton>

                <ng-template #readOnly>
                    <div style="font-size:13px;position:relative;top:14px;left:-6px;" class="text-danger pull-right">Read Only</div>
                </ng-template>

            </h1>

            <div class="row">
                <div class="col-6">

                    <div class="card">

                        <h3>Settings</h3>

                        <div class="form-group row">
                            <label for="email" class="col-3 col-form-label">Email</label>
                            <div class="col-9"><input (change)="validate()" type="email" class="form-control"
                                                      [(ngModel)]="user.email"/></div>
                        </div>

                    </div><!-- Settings -->

                    <div class="card">


                        <h3>Password</h3>

                        <ng-container *ngIf="user.id">

                            <div class="form-group row">
                                <label for="oldPass" class="col-3 col-form-label">Old Password</label>
                                <div class="col-9"><input [(ngModel)]="user.oldPassword" (change)="validate()" type="password" class="form-control" placeholder="*****">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-3 col-form-label">New Password</label>
                                <div class="col-9"><input [(ngModel)]="user.password" (change)="validate()" type="password" class="form-control" placeholder="*****">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="passwordConfirm" class="col-3 col-form-label">Password (again)</label>
                                <div class="col-9"><input [(ngModel)]="user.passwordRepeat" (change)="validate()" type="password" class="form-control" placeholder="*****"></div>
                            </div>

                        </ng-container>


                        <ng-container *ngIf="!user.id">

                            <div class="form-group row">
                                <label for="password" class="col-3 col-form-label">New Password</label>
                                <div class="col-9"><input [(ngModel)]="user.password" (change)="validate()" type="password" class="form-control" placeholder="*****">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="passwordConfirm" class="col-3 col-form-label">Password (again)</label>
                                <div class="col-9"><input [(ngModel)]="user.passwordRepeat" (change)="validate()" type="password" class="form-control" placeholder="*****"></div>
                            </div>

                        </ng-container>


                    </div><!-- New Password -->

                    <div class="card">


                        <div class="row">
                                <div class="col-4">
                                    <h3>Role</h3>

                                    <div class="form-group">

                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input [(ngModel)]="user.adminRole" (change)="validate()" class="form-check-input" type="radio" name="roleRadio" id="exampleRadios3" value="superadmin">
                                                Superadmin
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input [(ngModel)]="user.adminRole" (change)="validate()" class="form-check-input" type="radio" name="roleRadio" id="exampleRadios2" value="admin">
                                                Admin
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input [(ngModel)]="user.adminRole" (change)="validate()" class="form-check-input" type="radio" name="roleRadio" id="exampleRadios1" [value]="null">
                                                User
                                            </label>
                                        </div>

                                    </div>

                                </div>

                                <div class="col-4">

                                    <h3>Modules</h3>

                                    <div class="form-group" [ngClass]="{'dim':user.adminRole==='superadmin'}">
                                        <div class=" col-sm-10">

                                            <div class="checkbox">
                                                <label class="checkbox-custom">
                                                    <input (change)="validate()" type="checkbox"
                                                           [(ngModel)]="user.meta.modules.reports">
                                                    <span class="checkbox-label">Reports</span>
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label class="checkbox-custom">
                                                    <input (change)="validate()" type="checkbox"
                                                           [(ngModel)]="user.meta.modules.projects">
                                                    <span class="checkbox-label">Projects</span>
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label class="checkbox-custom">
                                                    <input (change)="validate()" type="checkbox"
                                                           [(ngModel)]="user.meta.modules.users">
                                                    <span class="checkbox-label">Users</span>
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label class="checkbox-custom">
                                                    <input (change)="validate()" type="checkbox"
                                                           [(ngModel)]="user.meta.modules.settings">
                                                    <span class="checkbox-label">Settings</span>
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label class="checkbox-custom">
                                                    <input (change)="validate()" type="checkbox"
                                                           [(ngModel)]="user.meta.modules.vp">
                                                    <span class="checkbox-label">VP Management</span>
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label class="checkbox-custom">
                                                    <input (change)="validate()" type="checkbox"
                                                           [(ngModel)]="user.meta.modules.mass">
                                                    <span class="checkbox-label">Mass Scrape</span>
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label class="checkbox-custom">
                                                    <input (change)="validate()" type="checkbox"
                                                           [(ngModel)]="user.meta.modules.proxy">
                                                    <span class="checkbox-label">Proxy</span>
                                                </label>
                                            </div>

                                        </div>
                                    </div>

                                </div>



                            <div class="col-4">

                                <h3>VP Role</h3>

                                <div class="form-group" [ngClass]="{'dim':user.adminRole==='superadmin' || !user.meta.modules.vp}">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input [(ngModel)]="user.meta.vpRole" class="form-check-input" type="radio" name="vpRoleRadio" id="exampleRadios1" [value]="undefined">
                                            None
                                        </label>
                                    </div>
                                    <div class="form-check" *ngFor="let role of allVoRoles">
                                        <label class="form-check-label">
                                            <input [(ngModel)]="user.meta.vpRole" class="form-check-input" type="radio" name="vpRoleRadio" id="exampleRadios1" [value]="role.id">
                                            {{role.name}}
                                        </label>
                                    </div>
                                </div>

                            </div>


                        </div>




                    </div><!-- Available Modules -->

                </div>
                <div class="col-6">

                    <div class="card">
                        <h3>Projects</h3>
                        <user-projects [userId]="userId"></user-projects>
                    </div><!-- Projects -->


                </div>

            </div>

            <div class="row">
                <div class="col-12">

                    <hr/>

                    <button *ngIf="!showDelete && permission=='EDIT' && userId" (click)="showDelete=true" type="button"
                            class="btn btn-xs btn-dafault" style="margin-left:0.5em;">
                        <i class="fa fa-times"></i>
                        Delete User
                    </button>
                    <span *ngIf="showDelete"
                          style="display:inline-block;padding-left:1em;">Delete this user?</span>
                    <button *ngIf="showDelete" (click)="showDelete=false" type="button"
                            class="btn btn-xs btn-link" style="margin-left:0.5em;">
                        Cancel
                    </button>
                    <button *ngIf="showDelete" (click)="deleteUser()" type="button"
                            class="btn btn-xs btn-danger" style="margin-left:0.5em;">
                        Delete
                    </button>

                </div>
            </div>

        </div>
    `
})
export class UsersForm implements OnInit {
    private allVoRoles: any;
    private formWarnings: SaveButtonWarning[];
    private formSaveActive: boolean;

    private userNotFound: boolean;
    private user: any;
    private userId: any;
    private permission: string;

    constructor(
        private permissionsProvider: PermissionsProvider,
        private activatedRoute: ActivatedRoute,
        private http: Http,
        private flagActions: FlagActions,
        private router: Router,
        private genericApiProvider: GenericApiProvider
    ) {
        this.permission = permissionsProvider.modulePermissions("users");

        this.activatedRoute.params.subscribe(params => {
            this.flagActions.setFlag("loading", true);
            this.userNotFound = false;
            this.user = null;
            this.userId = params.userId;

            if (this.userId) {
                // Loading a user

                this.http
                    .get("/userManage/cy_users/" + this.userId)
                    .catch((error: any, source: Observable<any>) => {
                        this.userNotFound = true;
                        this.flagActions.setFlag("loading", false);
                        return Observable.throw(error);
                    })
                    .subscribe(res => {
                        this.userNotFound = false;
                        this.user = res.json();
                        if (!this.user.meta) this.user.meta = {};
                        if (!this.user.meta.modules)
                            this.user.meta.modules = {};
                        this.flagActions.setFlag("loading", false);
                    });
            } else {
                // Creating a new user
                this.user = { meta: { modules: {} }, adminRole: null };
                this.flagActions.setFlag("loading", false);
            }
        });
    }

    ngOnInit() {
        this.formSaveActive = true;
        this.formWarnings = [];
        this.getAllVoRoles();
    }

    getAllVoRoles() {
        this.genericApiProvider
            .type("voRole")
            .list({order: 'name ASC'})
            .subscribe(maintenanceProfiles => {
                this.allVoRoles = maintenanceProfiles;
            });
    }

    validate() {
        this.formWarnings = [];

        if (
            !this.user.email ||
            this.user.email.length == 0 ||
            !this.user.email.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.formWarnings.push({
                alert: true,
                message: "Email is invalid"
            });
        }

        if (!this.user.id) {
            if (!this.user.password || !this.user.password.length) {
                this.formWarnings.push({
                    alert: true,
                    message: "User must have a password"
                });
            }
        }

        if (
            this.user.password &&
            this.user.password.length &&
            this.user.password != this.user.passwordRepeat
        ) {
            if (this.user.password.length) {
                this.formWarnings.push({
                    alert: true,
                    message: "Password and repeat password are not identical"
                });
            }
        }

        if (
            this.user.adminRole !== "superadmin" &&
            !Object.keys(this.user.meta.modules)
                .map(m => {
                    return this.user.meta.modules[m];
                })
                .some(item => {
                    return item == true;
                })
        ) {
            this.formWarnings.push({
                alert: false,
                message: "User has no permissions"
            });
        }
    }

    deleteUser() {
        this.http
            .delete(`/userManage/cy_users/${this.user.id}`)
            .catch((error: any, source: Observable<any>) => {
                alertify.error(
                    JSON.parse(JSON.parse(error._body).response.text).error
                        .message
                );
                return Observable.throw(error);
            })
            .subscribe(res => {
                alertify.success("User Deleted...");
                this.router.navigate(["/users"]);
            });
    }

    formSave() {
        if (!this.user.password) delete this.user.password;

        if (this.user.id) {
            this.http
                .patch(`/userManage/cy_users/${this.user.id}`, this.user)
                .catch((error: any, source: Observable<any>) => {
                    alertify.error(
                        JSON.parse(JSON.parse(error._body).response.text).error
                            .message
                    );
                    return Observable.throw(error);
                })
                .subscribe(res => {
                    alertify.success("User Updated...");
                    this.router.navigate(["/users"]);
                });
        } else {
            this.http
                .post("/userManage/cy_users", this.user)
                .catch((error: any, source: Observable<any>) => {
                    alertify.error(
                        JSON.parse(JSON.parse(error._body).response.text).error
                            .message
                    );
                    return Observable.throw(error);
                })
                .subscribe(res => {
                    alertify.success("User Created...");
                    this.router.navigate(["/users"]);
                });
        }
    }
}
