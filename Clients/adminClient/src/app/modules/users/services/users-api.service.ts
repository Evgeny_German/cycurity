import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

@Injectable()
export class UsersApiService {
    constructor(private http: Http) {}

    list = (filter?: any) => {
        return new Promise((resolve, reject) => {
            this.http
                .get(
                    `/userManage/cy_users${
                        filter
                            ? `?filter=${encodeURIComponent(
                                  JSON.stringify(filter)
                              )}`
                            : ""
                    }`
                )
                .map(res => res.json())
                .subscribe(resolve, reject);
        });
    };
}
