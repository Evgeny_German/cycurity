/**
 * Created by odedl on 16-Jul-17.
 */
import {NgModule} from '@angular/core';
import {NavigationMain} from "./navigation.comp";
import {SharedModule} from "../shared/shared.module";
import {RouterModule, Routes} from "@angular/router";
import {LoginModule} from "../login/login.module";


@NgModule({
  imports: [SharedModule,RouterModule,LoginModule],
  exports: [NavigationMain],
  declarations: [
    NavigationMain
],
  providers: [],
})
export class NavigationModule {
}
