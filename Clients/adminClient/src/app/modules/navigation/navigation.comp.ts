/**
 * Created by odedl on 16-Jul-17.
 */
import {Component, OnInit} from '@angular/core';
import {NgClass} from '@angular/common';
import {ActivatedRoute, Router} from "@angular/router";
import {AppCacheProvider} from "../shared/providers/app-cache-provider";

@Component({
    selector: 'navigation-main',
    template: `

        <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-primary">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" [routerLink]="['/'+getFirstModule()]"><img src="./assets/brand.png"/></a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <!--
                            <li class="nav-item" [ngClass]="{'active': checkActive('home')}">
                                <a class="nav-link" [routerLink]="['/home']">Home</a>
                            </li>
                    -->
                    <!--
                            <li *ngIf="modules.search" class="nav-item" [ngClass]="{'active': checkActive('search')}">
                                <a class="nav-link" [routerLink]="['/search']">Search</a>
                            </li>
                    -->
                    <li *ngIf="modules.reports || role!=null" class="nav-item">
                        <a class="nav-link" [routerLink]="['/reports']" [ngClass]="{'active': checkActive('reports')}">Reports</a>
                    </li>
                    <li *ngIf="modules.vp || role!=null" class="nav-item">
                        <a class="nav-link" [routerLink]="['/vp']" [ngClass]="{'active': checkActive('vp')}">VP
                            Management</a>
                    </li>
<!--
                    <li *ngIf="modules.projects || role!=null" class="nav-item">
                        <a class="nav-link" [routerLink]="['/projects']"
                           [ngClass]="{'active': checkActive('projects')}">KOL-M</a>
                    </li>
-->
                    <li *ngIf="modules.users || modules.projects || role!=null" class="nav-item">
                        <a class="nav-link" [routerLink]="getBaseSettingsRoute()"
                           [ngClass]="{'active': checkActive(['users','projects'])}">Settings</a>
                    </li>
<!--
                    <li *ngIf="modules.settings || role!=null" class="nav-item">
                        <a class="nav-link" [routerLink]="['/settings']"
                           [ngClass]="{'active': checkActive('settings')}">Settings</a>
                    </li>
-->
<!--
                    <li class="nav-item">
                        <a class="nav-link" [routerLink]="['/jobs']"
                           [ngClass]="{'active': checkActive('jobs')}">Jobs</a>
                    </li>
-->
                    <li class="nav-item" *ngIf="modules.mass || role!=null">
                        <a class="nav-link" [routerLink]="['/mass']"
                           [ngClass]="{'active': checkActive('mass')}">Mass scrape</a>
                    </li>
                    <li class="nav-item" *ngIf="modules.proxy || role!=null">
                        <a class="nav-link" [routerLink]="['/proxy']"
                           [ngClass]="{'active': checkActive('mass')}">Proxy</a>
                    </li>

                    <!--        <li class="nav-item" [ngClass]="{'active': checkActive('kolpoc')}">
                                <a class="nav-link" [routerLink]="['/kolpoc']">KOL POC</a>
                            </li>-->
                </ul>

                <div class="navigation-right">
                    <logged-in-bar></logged-in-bar>
                </div>
            </div>
        </nav>
    `
})
export class NavigationMain implements OnInit {
    modules: any;
    role: any;

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private appCacheProvider: AppCacheProvider) {

        this.modules = appCacheProvider.getIdentity().meta.modules;
        this.role = appCacheProvider.getIdentity().adminRole;
        console.warn(this.modules)

    }

    getBaseSettingsRoute() {
        if (!this.modules.users && this.modules.projects){return ['/projects'];}
        return ['/users'];
    }

    ngOnInit() {
    }

    checkActive(testedPathSegment: any) {
        let returnValue;
        if(!Array.isArray(testedPathSegment)) {
            testedPathSegment = [testedPathSegment];
        }
        // console.warn(testedPathSegment);
        testedPathSegment.forEach((segment)=>{
            if (this.router.isActive(segment, false)) {
                returnValue = true;
            }
        });
        return returnValue;
    }

    getFirstModule() {
        let selectedModule;
        if (this.modules) {
            ['reports', 'projects', 'users', 'settings', 'vp', 'mass', 'proxy'].forEach((module) => {
                if (!selectedModule && this.modules[module]) selectedModule = module;
            });
        }
        return selectedModule || 'jobs';
    }

}
