import {NgModule} from '@angular/core';
import {SettingsMain} from "./settings.comp";
import {RouterModule, Routes} from "@angular/router";
import {AuthenticationCallbackActivateGuard} from "../shared/auth/auth-guard-provider";
import {SharedModule} from "../shared/shared.module";

export const SETTINGS_MAIN_URL = 'settings';

const settingsRoutes: Routes = [
  {
    path: SETTINGS_MAIN_URL, component: SettingsMain,canActivate:[AuthenticationCallbackActivateGuard]

  }
];

@NgModule({
  imports: [RouterModule.forChild(settingsRoutes),SharedModule],
  exports: [SettingsMain],
  declarations: [SettingsMain],
  providers: [],
})

export class SettingsModule {
}
