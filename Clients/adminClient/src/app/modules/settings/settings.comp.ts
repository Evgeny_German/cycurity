import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {FlagActions} from "../shared/store/flagsActions";
import alertify from 'alertifyjs';
import {GenericApiProvider} from "../shared/providers/genericApiProvider";
import {Observable} from "rxjs/Observable";
import {KolService} from "../projects/services/kolService";
import {kolEditorConfig} from "../shared/dataModels/kol-config";
import {PermissionsProvider} from "../shared/providers/permissionsProvider";

@Component({
  selector: 'settings-main',
  template: `
    <div class="row">

        <div class="container-fluid row">
            <div  class="col-12">

                <h1>Settings</h1>

                <ul class="nav nav-tabs" style="width:100%;">
                    <li class="nav-item">
                        <a class="nav-link disabled" href="javascript:;">General Settings</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="javascript:;">KOL Calculator Defaults</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="javascript:;">Coming Soon...</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="javascript:;">Coming Soon...</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="javascript:;">Coming Soon...</a>
                    </li>
                </ul>

            </div>





            <div *ngIf="kolParams" class="col-12" style="padding-top">
                <h5 style="margin:10px 0">KOL Calculator Defaults</h5>
                <div style="max-width:800px;" [ngClass]="{'disabled': permissions!='EDIT'}">
                    <kolEditor [params]="kolParams.kol_filter_config" [defaults]="kolDefaults"></kolEditor>
                    <div>
                        <a *ngIf="permissions=='EDIT'" href="javascript:;" (click)="saveConfig()" class="pull-right btn btn-primary">Save</a>
                    </div>
                </div>
            </div>



        </div>



    </div>
  `
})
export class SettingsMain implements OnInit {
    permissions: string;

    private kolParams;
    private kolDefaults={};

    constructor(private genericApiProvider:GenericApiProvider, private kolService: KolService, private permissionsProvider:PermissionsProvider) {

        this.permissions = permissionsProvider.modulePermissions('settings');

        kolEditorConfig.forEach((item)=>{
            this.kolDefaults[item.id] = {importance:0};
        });

        this.genericApiProvider.type('globalKolConfigs').list().subscribe((res)=>{
            res.kol_filter_config = kolService.postLoad(res.kol_filter_config);
            this.kolParams = res;
        });

    }

    ngOnInit() {
    }

    saveConfig() {
        var kolParams = this.kolParams;
        kolParams.kol_filter_config = this.kolService.preSave(kolParams.kol_filter_config);

        this.genericApiProvider.type('globalKolConfigs').post(kolParams)
        .catch((err, caught)=>{
            alertify.error(caught);
            return Observable.of(err);
        })
        .subscribe((res)=>{
            alertify.success('Saved');
        });
    }
}
