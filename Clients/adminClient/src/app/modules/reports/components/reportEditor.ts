import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ChangeDetectorRef,
    SimpleChange,
    OnDestroy
} from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as _ from "lodash";
import { ConversationApiService } from "../../conversation/services/conversation-api.service";
import { Observable } from "rxjs/Observable";
import * as async from "async";
import { DocumentProvider } from "../../shared/providers/documentProvider";
import { FlagActions } from "../../shared/store/flagsActions";
import * as alertify from "alertifyjs";
import { Http } from "@angular/http";
import * as Url from "url";
import { GenericApiProvider } from "../../shared/providers/genericApiProvider";
import { AppCacheProvider } from "../../shared/providers/app-cache-provider";
import { PermissionsProvider } from "../../shared/providers/permissionsProvider";
import { PushProvider } from "../../shared/providers/push-provider";

@Component({
    selector: "reportEditor",
    styles: [
        `
            nav {
                z-index: 4;
                top: 80px;
                height: 38px;
                padding-top: 15px;
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
            }

            .navbar .btn-xs {
                padding: 6px 9px;
                font-size: 12px;
            }

            .conversation {
                white-space: nowrap;
                overflow: hidden;
                margin-bottom: 10px;
                padding: 6px;
                color: #000;
                display: block;
                text-decoration: none;
                position: relative;
            }

            .conversation .avatar {
                display: inline-block;
                width: 24px;
                height: 24px;
                border: 2px solid #fff;
                border-radius: 50%;
                box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.4);
            }

            .conversation span {
                white-space: nowrap;
                overflow: hidden;
            }

            .conversation i {
                color: #bbb;
            }

            .conversation .grad {
                height: 36px;
                width: 136px;
                display: inline-block;
                position: absolute;
                right: 0;
                top: 0;
                background: -webkit-linear-gradient(
                    left,
                    rgba(255, 255, 255, 0),
                    rgba(255, 255, 255, 1)
                ); /* For Safari 5.1 to 6.0 */
                background: -o-linear-gradient(
                    right,
                    rgba(255, 255, 255, 0),
                    rgba(255, 255, 255, 1)
                ); /* For Opera 11.1 to 12.0 */
                background: -moz-linear-gradient(
                    right,
                    rgba(255, 255, 255, 0),
                    rgba(255, 255, 255, 1)
                ); /* For Firefox 3.6 to 15 */
                background: linear-gradient(
                    to right,
                    rgba(255, 255, 255, 0),
                    rgba(255, 255, 255, 1)
                ); /* Standard syntax (must be last) */
            }
        `
    ],
    template: `

        <div class="row" *ngIf="!report" style="height:100%;">
            <div class="col-md-12" style="height:100%;">
                <div style="position:absolute;top:50%;left:50%;margin-top:-40px;margin-left:-50px;">
                    Please select a report
                </div>
            </div>
        </div>

        <!-- Conversation List -->
        <div class="row" *ngIf="report && permission">
            <nav *ngIf="!errorResponse" class="navbar navbar-toggleable-md navbar-light bg-faded wBox">

                <div class="collapse navbar-collapse">

                    <ng-container *ngIf="userMayEditReport(); else readOnly">
                        <ul class="navbar-nav mr-auto">

                            <ng-container *ngIf="report && projects?.length">
                                <li class="menu-item">


                                    <select [disabled]="!userMayEditReport()" (change)="reportProjectChanged()" [(ngModel)]="report.data.projectId" class="form-control" style="margin-right:10px;display:inline-block;width:200px;height:32px;line-height:16px;">
                                        <option [value]="0">Choose a Project</option>
                                        <option *ngFor="let project of projects" value="{{project.id}}">
                                            {{project?.name}}
                                        </option>
                                    </select>


                                </li>
                            </ng-container>

                            <li class="menu-item">
                                <button
                                    type="button"
                                    class="btn btn-xs btn-info"
                                    data-toggle="modal"
                                    data-target="#addConversationModal"
                                >
                                    <i class="fa fa-plus"></i>
                                    Add Conversations
                                </button>
                            </li>
                            <li class="menu-item">
                                <button *ngIf="!showDelete" (click)="showDelete=true" type="button"
                                        class="btn btn-xs btn-dafault" style="margin-left:0.5em;">
                                    <i class="fa fa-times"></i>
                                    Delete Report
                                </button>
                                <span *ngIf="showDelete"
                                      style="display:inline-block;padding-left:1em;">Delete this report?</span>
                                <button *ngIf="showDelete" (click)="showDelete=false" type="button"
                                        class="btn btn-xs btn-link" style="margin-left:0.5em;">
                                    Cancel
                                </button>
                                <button *ngIf="showDelete" (click)="deleteReport()" type="button"
                                        class="btn btn-xs btn-danger" style="margin-left:0.5em;">
                                    Delete
                                </button>
                            </li>

                        </ul>

                        <ul class="navbar-nav my-2 my-lg-0">
                        </ul>
                        <ul class="navbar-nav my-2 my-lg-0" *ngIf="report.data.conversations.length>0">

                            <li class="menu-item" *ngIf="report.data.exportId && !report.data.downloadUrl && report.data.exportStatus!=='error'">
                                <small style="display: inline-block; margin-top: 7px; margin-right: 10px;"><a href="javascript:;" (click)="cancelExport()">Cancel</a></small>
                            </li>

                            <li class="menu-item" *ngIf="report.data.exportId">
                                <a
                                    href="/s3/{{report.data.downloadUrl}}"
                                    download="report.docx"
                                    class="{{(report.data.downloadUrl)?'':'disabled'}} btn btn-xs btn-{{(report.data.exportStatus==='error')?'danger':(report.data.downloadUrl)?'success':'default'}} mr-1">
                                    <ng-container *ngIf="report.data.exportStatus==='error'; else spinnerAndDownload">Export
                                        Failed
                                    </ng-container>
                                    <ng-template #spinnerAndDownload>
                                        <ng-container *ngIf="report.data.downloadUrl"><span
                                            title="Started {{report.data.start_date}}">Download Report</span></ng-container>
                                        <ng-container *ngIf="!report.data.downloadUrl"><i
                                            class="fa fa-circle-o-notch fa-spin"></i> Generating Report
                                        </ng-container>
                                    </ng-template>
                                </a>
                            </li>

                            <li class="menu-item">
                                <a (click)="exportReport()" href="javascript:;" class="btn btn-xs btn-primary {{(userMayEditReport())?null:'disabled'}}">
                                    <i class="fa fa-print"></i>
                                    Export Report
                                </a>
                            </li>
                        </ul>

                    </ng-container>
                    <ng-template #readOnly>
                        <div class="text-danger">Read Only</div>
                    </ng-template>
                </div>
            </nav>

            <div style="position:absolute;top:38px;left:0;right:0;bottom:0;overflow-y:auto;padding:0 17px;">

                <h1>
                    <small>Report:</small>
                    {{report.data.title}}
                </h1>

                <div *ngIf="report.data.conversations.length==0" class="alert alert-primary">There are currently no
                    conversations in this report.
                </div>

                <div *ngIf="showConversations" appsortable class="sortable" (orderChanged)="orderChanged($event)">

                    <a (click)="clickConversation(c)" href="javascript:;" class="wBox conversation"
                       *ngFor="let c of report.data.conversations">
                        <i class="fa fa-bars" aria-hidden="true"></i>

                        <ng-container *ngIf="c.avatar">
                            <img class="avatar" src="{{image(c.userName, 'avatar')}}"/>
                            <a
                                target="_blank"
                                href="https://twitter.com/{{c.userName}}/status/{{c.tweetId}}"
                               (click)="$event.stopPropagation();"
                            >
                                {{c.fullName}}
                                <small>@{{c.userName}}</small>
                            </a>
                            <span>{{c.tweetText}}</span>
                        </ng-container>
                        <a *ngIf="userMayEditReport()" style="z-index:999;display:inline-block;padding:4px;position:absolute;top:0px;right:4px;text-decoration:none;color:#000;"
                           href="javascript:;" (click)="$event.stopPropagation();deleteConversation(c)">&times;</a>

                        <ng-container *ngIf="!c.avatar">
                            <i *ngIf="c.status!=='error'" class="fa fa-spin fa-circle-o-notch"></i>
                            <i *ngIf="c.status==='error'" class="text-danger fa fa-exclamation-triangle"></i>
                            <span [ngClass]="{'text-danger':(c.status==='error')}">{{c.url}}</span>
                            <a *ngIf="c.status==='error' && userMayEditReport()"
                               style="z-index:999;display:inline-block;padding:4px;position:absolute;top:0px;right:4px;text-decoration:none;color:#000;"
                               href="javascript:;" (click)="$event.stopPropagation();deleteConversation(c)">&times;</a>
                        </ng-container>

                        <span class="grad"></span>
                    </a>

                </div>

            </div>

        </div>

        <!-- Modal -->
        <div id="addConversationModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="max-width:100%;!important; margin-left:10%;margin-right:10%;">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Conversations</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <textarea
                            [(ngModel)]="conversationsToAdd"
                            placeholder="Add Conversation URLs (one per line)"
                            style="min-height:300px;border:1px solid #ddd;white-space: nowrap;line-height:2em;"
                            class="form-control"
                        >
                        </textarea>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button (click)="addConversations()" type="button" class="btn btn-primary" data-dismiss="modal">
                            Add Conversations
                        </button>
                    </div>
                </div>

            </div>
        </div>

    `
})
export class ReportsEditor implements OnInit, OnDestroy {
    image(
        userName: string,
        type:
            | "avatar"
            | "profile_image"
            | "profile_background_image"
            | "profile_image_https"
            | "profile_background_image_https"
    ): string {
        return `getProfileImage?username=${encodeURIComponent(
            `${userName.toLowerCase()} ${type}`
        )}`;
    }
    ngOnDestroy(): void {
        this.pushService.unsubscribe(...this._subscribedChannels);
    }

    private _subscribedChannels: string[] = [];

    showConversations = true;
    conversationsToAdd: string;

    private _projects: any[];

    public get projects(): any[] {
        return this._projects;
    }

    cancelExport() {
        clearInterval(this.exportTick);
        delete this.report.data.exportId;
    }

    userMayEditReport() {
        return this._permission === "EDIT";
    }

    userRoleInModule() {
        return this._permission === "READ" || this._permission === "EDIT";
    }

    reportProjectChanged() {
        this.documentProvider
            .store(
                this.report.data,
                this.report.userId,
                2,
                this.report.documentId
            )
            .subscribe(
                res => {
                    this.flagActions.setFlag("loading", false);
                    this.showConversations = false;
                    this.changeDetectorRef.detectChanges();
                    this.showConversations = true;
                    alertify.success("Saved");
                },
                err => {
                    this.flagActions.setFlag("loading", false);
                    this.showConversations = false;
                    this.changeDetectorRef.detectChanges();
                    this.showConversations = true;
                    alertify.error(`Error: ${err}`);
                }
            );
    }

    private _permission: "EDIT" | "READ" | "NONE";

    public get permission(): "EDIT" | "READ" | "NONE" {
        return this._permission;
    }

    private _report: any;
    private tick: number;
    private exportTick: number;

    @Input() public sharedProjects: any;

    @Input()
    public get report(): any {
        return this._report;
    }

    // private setExportTick() {
    //     if (this.exportTick) clearInterval(this.exportTick);

    //     this.exportTick = <any>setInterval(() => {
    // if (
    //     this.report.data.exportStatus === "error" ||
    //     (this.report.data.exportId && this.report.data.downloadUrl)
    // ) {
    //     clearInterval(this.exportTick);
    //     return;
    // }
    handleExportCallback(): Observable<void> {
        return this.http
            .get(`/job/jobs/${this.report.data.exportId}`)
            .do(res => {
                const r = res.json();
                if (r && r.output && r.output.output) {
                    this.report.data.downloadUrl = r.output.output;
                    this.report.data.start_date = r.start_date;
                }

                if (this.report.data.exportStatus !== r.status) {
                    this.report.data.exportStatus = r.status;

                    this.documentProvider
                        .store(
                            this.report.data,
                            this.report.userId,
                            2,
                            this.report.documentId
                        )
                        .catch((err, caught) => {
                            return Observable.of(err);
                        })
                        .subscribe(res => {});
                }
            })
            .map(() => {});
    }
    //     }, 2000);
    // }

    // private setTick(report: any) {
    //     if (this.tick) clearInterval(this.tick);

    //     this.tick = <any>setInterval(() => {
    //         const unfinishedScrapesCount = report.data.conversations.filter(
    //             conversation => {
    //                 return (
    //                     !conversation.avatar && conversation.status !== "error"
    //                 );
    //             }
    //         ).length;

    //         if (unfinishedScrapesCount == 0) {
    //             clearInterval(this.tick);
    //             return;
    //         }
    refreshReport(report: any) {
        this.http
            .get(
                `scrape/fetchReport?reportId=${
                    report.data.documentId
                }&ownerId=${report.userId}`
            )
            .catch((err, caught) => {
                return Observable.of(err);
            })
            .subscribe(response => {
                report.data.conversations = response.json().data.data.conversations;

                report.data.conversations.forEach((conv, index) => {
                    if (!conv.url) {
                        report.data.conversations.splice(index, 1);
                        this.documentProvider
                            .store(
                                this.report.data,
                                this.report.userId,
                                2,
                                this.report.documentId
                            )
                            .catch((err, caught) => {
                                return Observable.of(err);
                            })
                            .subscribe(res => {
                                console.error("Removed blank conversation");
                            });
                        return;
                    }

                    if (!conv.tweetText) {
                        report.data.conversations
                            .filter(scrape => {
                                // Finished Scrapes
                                return !!scrape.tweetText;
                            })
                            .forEach(finishedScrape => {
                                if (finishedScrape.url === conv.url) {
                                    const removedUrl = conv.url;
                                    report.data.conversations.splice(index, 1);
                                    clearInterval(this.tick);
                                    // Save report
                                    this.documentProvider
                                        .store(
                                            this.report.data,
                                            this.report.userId,
                                            2,
                                            this.report.documentId
                                        )
                                        .catch((err, caught) => {
                                            return Observable.of(err);
                                        })
                                        .subscribe(res => {
                                            const message =
                                                "Duplicate Removed: " +
                                                removedUrl;
                                            alertify.error(message);
                                            console.error(message);
                                        });
                                }
                            });
                    }
                });

                this.showConversations = false;
                this.changeDetectorRef.detectChanges();
                this.showConversations = true;

                //console.warn(this._report, response.json().data.data)
                //this._report = response.json().data.data;
                //console.warn(response.json().data.data, this._report)
                //this._report = response.json().data.data;
                //debugger
            });
    }
    //     }, 2000);
    // }

    public set report(value: any) {
        this._report = value;
        if (this._report && !this._report.data.projectId)
            this._report.data.projectId = 0;

        if (value) {
            const unfinishedScrapesCount = value.data.conversations.filter(
                conversation => {
                    if (!conversation) return false;
                    return !conversation.avatar;
                }
            ).length;

            // if (unfinishedScrapesCount > 0) {
            //     // this.setTick(value);
            // } else {
            //     clearInterval(this.tick);
            // }

            if (this.report.data.exportId && !this.report.data.downloadUrl) {
                this.handleExportCallback().subscribe(() => {
                    if (
                        this.report.data.exportId &&
                        !this.report.data.downloadUrl
                    ) {
                        this.pushService
                            .subscribeOnce(this.report.data.exportId)
                            .subscribe(mes => {
                                this.handleExportCallback().subscribe();
                            });
                        this._subscribedChannels.push(
                            this.report.data.exportId
                        );
                    }
                });
            }
            // } else {
            //     clearInterval(this.exportTick);
            // }

            (async () => {
                this._permission = await this.permissionsProvider.reportPermissions(
                    this._report.userId,
                    this._report.documentId
                );
            })();
        }
    }

    private showDelete: boolean;

    @Output() deleteReportEvent = new EventEmitter<boolean>();

    constructor(
        private apiProvider: GenericApiProvider,
        private changeDetectorRef: ChangeDetectorRef,
        private conversationApiService: ConversationApiService,
        private documentProvider: DocumentProvider,
        private router: Router,
        private http: Http,
        private flagActions: FlagActions,
        private appCacheProvider: AppCacheProvider,
        private permissionsProvider: PermissionsProvider,
        private pushService: PushProvider
    ) {}

    ngOnChanges(report: SimpleChange) {
        this.showConversations = false;
        this.changeDetectorRef.detectChanges();
        this.showConversations = true;
    }

    orderChanged(order) {
        let safeGuard = false;
        this.flagActions.setFlag("loading", true);

        const newOrder = [];
        order.forEach((conversationIndex, index) => {
            if (!this.report.data.conversations[conversationIndex].url) {
                safeGuard = true;
            } else {
                newOrder.push(
                    this.report.data.conversations[conversationIndex]
                );
            }
        });

        if (safeGuard) {
            this.flagActions.setFlag("loading", false);
            alertify.alert("Something went wrong, please refresh this page");
            return;
        }

        this.report.data.conversations = newOrder;

        // Save report
        this.documentProvider
            .store(
                this.report.data,
                this.report.userId,
                2,
                this.report.documentId
            )
            .catch((err, caught) => {
                this.flagActions.setFlag("loading", false);
                this.showConversations = false;
                this.changeDetectorRef.detectChanges();
                this.showConversations = true;
                return Observable.of(err);
            })
            .subscribe(res => {
                this.flagActions.setFlag("loading", false);
                this.showConversations = false;
                this.changeDetectorRef.detectChanges();
                this.showConversations = true;
                alertify.success("Saved");
            });
    }

    ngOnInit() {
        this.apiProvider
            .type("projects")
            .list()
            .subscribe(res => {
                this._projects = res;
            });
    }

    clickConversation(conversation) {
        if (conversation.avatar) {
            this.router.navigate([
                "conversation",
                {
                    userName: conversation.userName.toLowerCase(),
                    tweetId: conversation.tweetId,
                    reportId: conversation.reportId,
                    ownerId: this.report.userId
                }
            ]);
        }
    }

    addConversations() {
        const inputText = this.conversationsToAdd;

        const messages = [];
        const conversations = [];

        // Convert all URLs to API calls
        // =============================

        let lines = inputText

            // Split input into lines
            .split("\n")

            // Make sure all lines are actual Twitter URLs
            .filter(line => {
                const isTwitterUrl =
                    _.startsWith(line, "https://twitter.com/") ||
                    _.startsWith(line, "http://twitter.com/");
                if (isTwitterUrl) {
                    return true;
                } else {
                    if (line !== "") {
                        messages.push({
                            type: 1,
                            message: `'${line}' is not a valid twitter URL`
                        });
                    }
                }
            })
            .map(conversation => {
                const { userName, tweetId } = this.parseUrl(conversation);
                return `https://twitter.com/${userName.toLowerCase()}/status/${tweetId}`;
            });

        // Remove duplicates
        // From provided lines
        lines = _.uniq(lines);
        // From existing lines

        const existingConversations = this.report.data.conversations.map(r => {
            return r.userName
                ? `https://twitter.com/${r.userName.toLowerCase()}/status/${
                      r.tweetId
                  }`
                : r.url;
        });

        lines = lines.filter(line => {
            const isAlreadyInReport = !(
                existingConversations.indexOf(line) < 0
            );
            if (isAlreadyInReport) {
                messages.push({
                    type: 3,
                    message: `'${line}' is already present in the report`
                });
            }
            return !isAlreadyInReport;
        });

        // Add conversation stubs to report
        lines.forEach(conversation => {
            this.report.data.conversations.push({
                url: conversation
                /* avatar, fullName ,userName ,tweetText ,tweetId: conversation.tweet.tweetId */
            });
        });

        messages.forEach(message => {
            if (message.type === 1) {
                alertify.error(message.message);
            }
            if (message.type === 2) {
                alertify.success(message.message);
            }
            if (message.type === 3) {
                alertify.success(message.message);
            }
        });

        this.showConversations = false;
        this.changeDetectorRef.detectChanges();
        this.showConversations = true;

        // Save report
        alertify.success("Saved");
        const report = this.report;
        this.documentProvider
            .store(report.data, report.userId, 2, report.documentId)
            .catch((err, caught) => Observable.of(err))
            .subscribe(res => {
                // Call scrapeReport (report_id, array_of_urls) on server

                this.http
                    .post("scrape/scrapeReport", {
                        ownerId: report.userId,
                        reportId: report.documentId,
                        urls: lines
                    })
                    .subscribe(
                        response => {
                            for (let job of response.json()) {
                                this.pushService
                                    .subscribeOnce(job.id)
                                    .subscribe(notification => {
                                        this.refreshReport(report);
                                    });
                                this._subscribedChannels.push(job.id);
                            }
                            // this.setTick(report);
                            console.warn(response);
                        },
                        err => {
                            alertify.error(`Error: ${err.message || err}`);
                        }
                    );
            });
    }

    parseUrl(url: string): { userName: string; tweetId: string } {
        const u = Url.parse(_.trim(url));
        return {
            userName: _
                .trim(u.path, "/")
                .split("/")[0]
                .toLowerCase(),
            tweetId: _.trim(u.path, "/").split("/")[2]
        };
    }

    exportReport() {
        const urls = this.report.data.conversations.map(c => {
            return `${window.location.protocol}//${
                window.location.host
            }/#/conversation;userName=${encodeURIComponent(
                c.userName.toLowerCase()
            )};tweetId=${c.tweetId};reportId=${c.reportId}`;
        });

        this.report.data.exportId = null;
        this.report.data.downloadUrl = null;
        this.report.data.exportStatus = null;

        // this.flagActions.setFlag('loading', true);
        this.conversationApiService
            .exportToWord(
                `Report: ${this.report.data.title}`,
                this.report.userId,
                ...urls
            )
            .subscribe(res => {
                /*
                        console.warn(res.response.id);
                        console.warn(this.report);
            */
                this.report.data.exportId = res.response.id;

                this.documentProvider
                    .store(
                        this.report.data,
                        this.report.userId,
                        2,
                        this.report.documentId
                    )
                    .subscribe(
                        res2 => {
                            this.pushService
                                .subscribeOnce(res.response.id)
                                .subscribe(exp => {
                                    this.handleExportCallback().subscribe();
                                });
                            this._subscribedChannels.push(res.response.id);

                            // this.setExportTick();
                            alertify.success("Export in progress...");
                        },
                        err => {
                            alertify.error(`Error: ${err.message || err}`);
                        }
                    );

                /*            this.http
                            .get(
                                `/job/jobs?filter=${encodeURIComponent(
                                    JSON.stringify({ where: { id:res.response.id } })
                                )}`
                            )
                            .subscribe(res => {
                                const r = res.json();
                                console.warn(r);
                            });*/

                // this.flagActions.setFlag('loading', false);
            });
    }

    deleteConversation(context) {
        const prevConversations = Array.from(this.report.data.conversations);

        this.report.data.conversations = this.report.data.conversations.filter(
            r => {
                if (r.tweetId) {
                    // Already scraped and stored
                    return r.tweetId !== context.tweetId;
                } else {
                    // Not scraped and stored
                    return r.url !== context.url;
                }
            }
        );

        // Save report
        this.documentProvider
            .store(
                this.report.data,
                this.report.userId,
                2,
                this.report.documentId
            )
            .subscribe(
                res => {},
                err => {
                    alertify.error(
                        `Error saving report: ${err.message || err}`
                    );
                    this.report.data.conversations = prevConversations;
                }
            );
    }

    deleteReport() {
        this.showDelete = false;
        this.deleteReportEvent.emit(this.report);
    }
}
