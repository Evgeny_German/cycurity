import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {DocumentProvider} from '../../shared/providers/documentProvider';
import {Observable} from "rxjs/Observable";
import {FlagActions} from "../../shared/store/flagsActions";
import * as moment from 'moment';
import {PermissionsProvider} from "../../shared/providers/permissionsProvider";

@Component({
    selector: 'reportsSidebar',
    styles: [`
        .nav-link small {
            color:#777;
        }

        .nav-link.active small {
            color:#8dd!important;
        }

    `],
    template: `

        <div *ngIf="reports" style="position:absolute;top:0px;bottom:0;left:0;right:0;">
            <div>

                <div *ngIf="reports.length==0" style="margin:9px 6px 0 4px" class="alert alert-danger">No Reports Currently Available</div>

                <div *ngIf="reports.length>0" style="position:absolute;top:0px;bottom:4.5em;left:0;right:0;overflow-y:auto;">
                    <h3 style="margin-top:3px;">Reports:</h3>
                    <ul class="nav nav-pills" style="margin-bottom:5px;">
                        <li style="width:100%;position:relative;" class="nav-item" *ngFor="let report of reports">

                            <a
                                [ngClass]="{'active':(report.data.documentId==selected?.data?.documentId)}"
                                class="nav-link active"
                                href="javascript:;"
                                (click)="selectReport(report)"
                            >
                                <div>
                                    <i style="color:#555;" title="Private, this report is not assigned to a project" *ngIf="!report.data.projectId || report.data.projectId==='0'" class="fa fa-lock"></i>
                                    {{report.data.title}}
                                </div>
                                <div>
                                    <small>{{moment(report.data.createdDate).format("MMMM Do YYYY, h:mm:ss a")}}</small>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>

            <div style="position: absolute; padding:1px 5px 0 5px; left: 0; bottom: 0; right: 0;">
                <div *ngIf="userRoleInModule()==='EDIT'" class="form-group">
                    <input
                        (keyup.enter)="createReport()"
                        placeholder="+ create a report"
                        class="form-control"
                        [(ngModel)]="searchText"
                    />
                </div>
            </div>

        </div>

    `
})
export class ReportsSidebar implements OnInit {
    moment: typeof moment;
    @Input() public reports: any[];
    @Input() public sharedProjects:any;


    @Input() public selected: object;
    @Output() public selectedChange = new EventEmitter();

    searchText: String = '';

    constructor(private permissionsProvider: PermissionsProvider, private documentProvider: DocumentProvider, private flagActions: FlagActions, private router: Router) {
        this.moment = moment;
    }

    ngOnInit() {
    }

    selectReport(report) {
        this.router.navigate(['reports', report.documentId]);
        this.selectedChange.emit(report);
    }

    userRoleInModule(){
        return this.permissionsProvider.modulePermissions('reports');
    }

    createReport() {

        if(!this.searchText || this.searchText.length==0)return

        this.flagActions.setFlag('loading', true);

        const documentProvider = this.documentProvider;
        let report = documentProvider.dispense('report', {
            title: this.searchText,
            conversations: [],
            createdDate: new Date().getTime()
        });

        documentProvider.store(report, report.ownerId, 2, report.documentId)
            .catch((err, caught) => {
                this.flagActions.setFlag('loading', false);
                console.warn(err);
                return Observable.of(err);
            })
            .subscribe((res)=>{
                this.flagActions.setFlag('loading', false);
                this.reports.unshift(report = res.json().data);
                this.selectReport(report);
                this.searchText = '';
            });

    }
}
