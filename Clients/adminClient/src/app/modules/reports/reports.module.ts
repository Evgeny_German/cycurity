import {NgModule} from '@angular/core';
import {ReportsMain} from "./reports.comp";
import {RouterModule, Routes} from "@angular/router";
import {AuthenticationCallbackActivateGuard} from "../shared/auth/auth-guard-provider";
import {SharedModule} from "../shared/shared.module";

import {ReportsSidebar} from './components/reportsSidebar';
import {ReportsEditor} from './components/reportEditor';


export const REPORTS_MAIN_URL = 'reports';

const reportsRoutes: Routes = [
    {
        path: REPORTS_MAIN_URL, component: ReportsMain,canActivate:[AuthenticationCallbackActivateGuard]
    },
    {
        path: REPORTS_MAIN_URL+'/:reportId', component: ReportsMain,canActivate:[AuthenticationCallbackActivateGuard]
    }
];

@NgModule({
  imports: [RouterModule.forChild(reportsRoutes),SharedModule],
  exports: [ReportsMain, ReportsSidebar, ReportsEditor],
  declarations: [ReportsMain, ReportsSidebar, ReportsEditor],
  providers: [],
})

export class ReportsModule {
}
