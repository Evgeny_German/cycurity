import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {FlagActions} from "../shared/store/flagsActions";
import {DocumentProvider} from "../shared/providers/documentProvider";
import * as _ from 'lodash';
import {GenericApiProvider} from "../shared/providers/genericApiProvider";

@Component({
    selector: 'reports-main',
    template: `

        <div *ngIf="errorResponse" class="alert alert-danger" style="margin-top:102px;">
            <h1>Error <b>{{errorResponse.status}}</b>: {{errorResponse.statusText}}</h1>
            <hr/>
            <h5><b>When reporting this error, please provide the following information to the technical staff:</b></h5>
            {{errorResponse | json}}
        </div>


        <div *ngIf="!errorResponse && sharedProjects" class="fullscreenInterface">
            <div class="row" style="height:100%;">
                <div class="col-md-2" style="border-right:1px dashed #777;">
                    <div style="position:absolute;top:0;left:0;right:0;bottom:0;">
                        <reportsSidebar [sharedProjects]="sharedProjects" [reports]="allReports" [(selected)]="selectedReport"></reportsSidebar>
                    </div>
                </div>
                <div class="col-md-10">
                    <reportEditor [sharedProjects]="sharedProjects" (deleteReportEvent)="deleteReportEvent($event)" [report]="selectedReport"></reportEditor>
                </div>
            </div>

        </div>
    `
})
export class ReportsMain implements OnInit {
    sharedProjects: any;
    allReports: any;
    selectedReport: object;
    errorResponse: any;

    constructor(
        private flagActions: FlagActions,
        private documentProvider: DocumentProvider,
        private route: ActivatedRoute,
        private genericApiProvider: GenericApiProvider
    ) {
        this.selectedReport = null;

        this.getAllReports()

    }

    deleteReportEvent(report){
        this.selectedReport = null;
        this.allReports = this.allReports.filter((r)=>{
            return r.documentId !== report.documentId;
        });
        this.documentProvider.purge(report)
            .catch((err, caught) => {
                return Observable.of(err);
            })
            .subscribe((res) => {
            });
    }

    getAllReports() {
        this.flagActions.setFlag('loading', true);

        this.documentProvider.list('report')
            .catch((err, caught) => {
                this.flagActions.setFlag('loading', false);
                this.errorResponse = err;
                return Observable.of(err);
            })
            .subscribe((res) => {
                this.flagActions.setFlag('loading', false);

                this.allReports = _.reverse(res);

                this.route.params.subscribe(params => {
                    if(params['reportId']){
                        this.selectedReport = this.allReports.filter((report)=>{
                            return report.documentId == params['reportId'];
                        })[0];
                    }else{
                        this.selectedReport = null;
                    }
                });

                //this.selectedReport = this.allReports[0];
            });
    }

    ngOnInit() {

        this.genericApiProvider.type('sharedProjects').list().subscribe((res)=>{
            this.sharedProjects = res;
        })

    }
}
