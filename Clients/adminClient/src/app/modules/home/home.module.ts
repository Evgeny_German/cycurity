/**
 * Created by odedl on 16-Jul-17.
 */
import {NgModule} from '@angular/core';
import {HomeMain} from "./home.comp";
import {RouterModule, Routes} from "@angular/router";
import {AuthenticationCallbackActivateGuard} from "../shared/auth/auth-guard-provider";
import {SharedModule} from "../shared/shared.module";

export const HOME_MAIN_URL = 'home';

const homeRoutes: Routes = [
  {
    path: HOME_MAIN_URL, component: HomeMain,canActivate:[AuthenticationCallbackActivateGuard]

  }
];

@NgModule({
  imports: [RouterModule.forChild(homeRoutes),SharedModule],
  exports: [HomeMain],
  declarations: [HomeMain],
  providers: [],
})

export class HomeModule {
}
