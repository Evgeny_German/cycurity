/**
 * Created by odedl on 16-Jul-17.
 */
import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";

@Component({
  selector: 'home-main',
  template: `
    <div>
        <h1>Home Dashboard</h1>
        <div class="alert alert-info">Coming Soon...</div>
        
    </div>
  `
})
export class HomeMain implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }
}
