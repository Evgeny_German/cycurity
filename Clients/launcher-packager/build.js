const path = require("path");
const fs = require("fs-extra");
const { execSync } = require("child_process");
const archiver = require("archiver");

const dist = path.join(__dirname, "./dist");

if (fs.existsSync(dist)) {
  fs.removeSync(dist);
}

fs.mkdirSync(dist);

[
  "package.json",
  "index.js",
  "ui/dist",
  "config"
].forEach(item => {
  fs.copySync(
    path.join(__dirname, `../launcher/${item}`),
    path.join(__dirname, `./dist/${item}`),
    { overwrite: true }
  );
});

  // fs.copySync(
  //   path.join(__dirname, `../launcher/config/${process.argv.length > 2?process.argv[2]:''}.env`),
  //   path.join(__dirname, `./dist/config/.env`),
  //   { overwrite: true }
  // );

// const out = path.join(__dirname, "./out");
// if (fs.existsSync(out)) {
//   fs.removeSync(out);
// }
// fs.mkdirSync(out);

// execSync(`npm run package`, { cwd: __dirname, stdio: "pipe" });

// const outStream = fs.createWriteStream(path.join(__dirname, "../launcher.zip"));
// const archive = archiver("zip");
// archive.pipe(outStream);
// archive.directory(out, false);
// archive.finalize();
