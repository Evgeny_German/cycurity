import {
  app,
  Tray,
  Menu,
  BrowserWindow,
  session,
  ipcMain,
  screen,
  dialog
} from "electron";

import * as express from "express";
import * as bodyparser from "body-parser";
import * as dotenv from "dotenv";
import * as superagent from "superagent";

import * as path from "path";

app.setAsDefaultProtocolClient(
  "cycurity",
  `${process.execPath}" "${__filename}`
);
app.commandLine.appendSwitch("ignore-certificate-errors");

const eapp = express();
eapp.use(bodyparser.json({ limit: "100mb" }));
eapp.use(express.static(path.join(__dirname, "./ui/dist/")));
eapp.listen(9999).on("error", err => {});

const data =
  process.argv.length &&
  process.argv.length > 2 &&
  process.argv[2].toLowerCase().startsWith("cycurity:")
    ? JSON.parse(
        decodeURIComponent(
          process.argv[2].substr(process.argv[2].indexOf(":") + 1)
        )
      )
    : null;

if (!data) {
  app.exit();
}

const config = dotenv.config({
  path: path.join(__dirname, `./config/${data.env}.env`)
});

if (
  process.env.UNSAFE_IGNORE_SSL_ERRORS &&
  process.env.UNSAFE_IGNORE_SSL_ERRORS.trim().toLowerCase() === "true"
) {
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = <any>0;
  app.on(
    "certificate-error",
    (event, webContents, url, error, certificate, callback) => {
      event.preventDefault();
      callback(true);
    }
  );
}

function randomKey(length: number): string {
  return Array.from(
    (function*() {
      for (let i = 0; i < length; i++) yield i;
    })(),
    i => "0123456789abcdef"[Math.floor(Math.random() * 16)]
  ).join("");
}
let proxyTerminateParams: any;

function createBrowserWindow(params: any): BrowserWindow {
  const partition = randomKey(10);
  const ses = session.fromPartition(partition, { cache: true });
  const window = new BrowserWindow({
    width: 800,
    height: 600,
    title: "",
    autoHideMenuBar: false
  });
  window.setMenuBarVisibility(false);
  ipcMain.on("menu", (event, template) => {
    const menu = Menu.buildFromTemplate(
      template.map(item => {
        if (!item.click) return item;
        const channel = item.click.channel;
        const message = item.click.message;
        return Object.assign(item, {
          click: () => event.sender.send(channel, message)
        });
      })
    );
    const bounds = window.getContentBounds();
    const point = screen.getCursorScreenPoint();
    menu.popup(null, { x: point.x - bounds.x, y: point.y - bounds.y });
  });

  window.on("close", e => {
    if (
      !dialog.showMessageBox(window, {
        message: "Are you sure?",
        buttons: ["Cancel", "OK"],
        cancelId: 0,
        defaultId: 1
      })
    )
      e.preventDefault();
  });

  window.loadURL(`http://localhost:9999/index.html`);
  window.webContents.on("did-finish-load", () => {
    const instanceName = randomKey(16);
    (async () => {
      try {
        await Promise.all([
          superagent
            .post(`${process.env.HOST_URL}/vo/voLaunches`)
            .set({ Authorization: `Bearer ${data.proxy.accessToken}` })
            .send({ when: new Date(), voId: data.settings.id }),
          superagent
            .post(`${process.env.HOST_URL}/vo/notifyLaunch?id=${data.launch}`)
            .set({ Authorization: `Bearer ${data.proxy.accessToken}` })
            .send({})
        ]);
        const createRes = (await superagent
          .post(`${process.env.HOST_URL}/proxy/createInstance`)
          .set({ Authorization: `Bearer ${data.proxy.accessToken}` })
          .send({
            provider: "aws",
            region: data.proxy.region,
            instanceName: `proxy-${instanceName}`
          })).body;
        const instance = (await superagent
          .post(`${process.env.HOST_URL}/proxy/getInstanceListFromProvider`)
          .set({ Authorization: `Bearer ${data.proxy.accessToken}` })
          .send({
            provider: "aws",
            region: data.proxy.region,
            filter: { name: `proxy-${instanceName}` }
          })).body[0];
        proxyTerminateParams = {
          provider: "aws",
          region: data.proxy.region,
          instanceId: instance.instanceId
        };
        ses.setProxy(
          {
            proxyRules: `http://${instance.publicIp}:8000`,
            pacScript: null,
            proxyBypassRules: null
          },
          () => {}
        );
        let connected = false;
        for (let i = 0; i < 12; i++) {
          try {
            await new Promise(resolve => setTimeout(resolve, 5000));
            await superagent
              .post(`${process.env.HOST_URL}/proxy/testProxy`)
              .set({ Authorization: `Bearer ${data.proxy.accessToken}` })
              .send({
                url: "https://www.google.com",
                proxyIp: instance.publicIp,
                proxyPort: 8000
              });
            connected = true;
            break;
          } catch (err) {}
        }
        if (!connected) {
          throw new Error("Proxy not responsive");
        }
        window.webContents.send("proxy", {
          accessToken: data.proxy.accessToken,
          partition: partition,
          vp: params,
          instance
        });
        window.setTitle(
          `${data.settings.name} - aws - ${data.proxy.region} - ${
            instance.instancenName
          }`
        );
      } catch (error) {
        dialog.showErrorBox("Error", error.message);
        app.exit();
      }
    })();
  });
  return window;
}

app.on("login", (event, wc, request, authInfo, cb) => {
  event.preventDefault();
});

app.on("window-all-closed", () => {
  if (proxyTerminateParams) {
    (async () => {
      try {
        await superagent
          .post(`${process.env.HOST_URL}/proxy/terminateInstance`)
          .set({ Authorization: `Bearer ${data.proxy.accessToken}` })
          .send(proxyTerminateParams);
      } catch (err) {
        dialog.showErrorBox(
          "Error",
          "Could not shutdown proxy machine automatically."
        );
      }
      app.quit();
    })();
  } else app.quit();
});

app.on("ready", () => {
  createBrowserWindow(data).show();
});
