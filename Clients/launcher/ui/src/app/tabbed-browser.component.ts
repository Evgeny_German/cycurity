import { WebviewTag, WebContents, webContents } from "electron";
import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  OnInit,
  OnDestroy
} from "@angular/core";
import { Observable, BehaviorSubject, Subscription } from "rxjs";
import * as normalizeUrl from 'normalize-url';
const electron = window["require"]("electron");

export interface ProxyOptions {
  accessToken: string;
  instance: any;
}

export interface Tab {
  url: Observable<{ value: string; navigated: boolean }>;
  title: Observable<string>;
  webContents: Observable<WebContents>;
  setWebContents(webContents: WebContents): void;
  close(): void;
  goToUrl(url: string, navigated: boolean): void;
}

@Component({
  selector: "tabbed-browser",
  styles: [
    `
    .nav-item {
      position: relative;
    }

    .closeTab {
      position: absolute;
      top: 9px;
      right: 10px;
      cursor:pointer;
    }

    .goButton{
      position:relative;
      top:1px;
      position:absolute;
      top:10px;
      right:10px;
      padding:0;
      height:25px;
      width:23px;
    }

    .historyButtons {
      padding:0;
      height:25px;
      width:23px;
    }

    .btn-primary.historyButtons {
      color:#fff!important;
    }

    .btn-default.historyButtons {

    }

    .hDisabled {
      opacity: 0.5;
    }

    header{
      display: table-row;
    }

    header nav{
      padding:7px 0 0 0;
    }

    header section{
      z-index:1;box-shadow:0 0 10px rgba(0,0,0,0.5);padding:5px;position:relative;border-bottom:1px solid #aaa;
    }

    header nav .tab{
      min-width:160px;
      max-width:220px;
      z-index:2;
    }

    header nav .tab *{
      text-transform:none;
    }

    .urlInput{
      width: 100%;padding-left:100px;padding-right:32px;
    }

    header nav .nav-tabs {
      padding-left:5px;
    }

    header nav li{
      cursor:pointer;
    }


    .mainContainer{
      display: table; width: 100%; height: 100%;
    }

    .mainContainer > section {
      display: table-row; height: 100%;
    }

    .mainContainer > section > div{
      display: table-cell; position: relative;
    }

    browser-tab{
      position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px;
    }


    .lds-roller {
      display: inline-block;
      position: relative;
      width: 64px;
      height: 64px;
    }
    .lds-roller div {
      animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
      transform-origin: 32px 32px;
    }
    .lds-roller div:after {
      content: " ";
      display: block;
      position: absolute;
      width: 6px;
      height: 6px;
      border-radius: 50%;
      background: #369;
      margin: -3px 0 0 -3px;
    }
    .lds-roller div:nth-child(1) {
      animation-delay: -0.036s;
    }
    .lds-roller div:nth-child(1):after {
      top: 50px;
      left: 50px;
    }
    .lds-roller div:nth-child(2) {
      animation-delay: -0.072s;
    }
    .lds-roller div:nth-child(2):after {
      top: 54px;
      left: 45px;
    }
    .lds-roller div:nth-child(3) {
      animation-delay: -0.108s;
    }
    .lds-roller div:nth-child(3):after {
      top: 57px;
      left: 39px;
    }
    .lds-roller div:nth-child(4) {
      animation-delay: -0.144s;
    }
    .lds-roller div:nth-child(4):after {
      top: 58px;
      left: 32px;
    }
    .lds-roller div:nth-child(5) {
      animation-delay: -0.18s;
    }
    .lds-roller div:nth-child(5):after {
      top: 57px;
      left: 25px;
    }
    .lds-roller div:nth-child(6) {
      animation-delay: -0.216s;
    }
    .lds-roller div:nth-child(6):after {
      top: 54px;
      left: 19px;
    }
    .lds-roller div:nth-child(7) {
      animation-delay: -0.252s;
    }
    .lds-roller div:nth-child(7):after {
      top: 50px;
      left: 14px;
    }
    .lds-roller div:nth-child(8) {
      animation-delay: -0.288s;
    }
    .lds-roller div:nth-child(8):after {
      top: 45px;
      left: 10px;
    }
    @keyframes lds-roller {
      0% {
        transform: rotate(0deg);
      }
      100% {
        transform: rotate(360deg);
      }
    }

`
  ],
  template: `

      <div class="mainContainer">
        <div *ngIf="!proxy" style="position: fixed; top: 0px; left: 0px; bottom: 0px; right: 0px; width:100%; height: 100%; background-color: rgba(255,255,255,.5); z-index: 9999999;">
          <div style="position: relative; width: 64px; height: 64px; top: 50%; left: 50%; margin-top: -32px; margin-left: -32px;">
            <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
          </div>
        </div>
        <header>
          <div *ngIf="proxy" style="font-size:11px;padding:0.5em;text-align:right;">
            <b>VP:</b> {{proxy?.vp.settings.name}} • <b>Provider:</b> AWS ({{proxy?.vp.proxy.region}}) • <b>IP:</b> {{proxy?.instance.publicIp}} ({{proxy?.instance.instancenName}})
          </div>
          <nav class="bg-dark">
            <ul class="nav nav-tabs">
              <li *ngFor="let tab of tabs" class="nav-item tab" (click)="selectedTab=tab">
                <a href="javascript:;" style="word-wrap: break-word;white-space: nowrap;overflow:hidden;" class="nav-link active" [ngClass]="{active : (tab === selectedTab) }">
                  <span style="position:relative;top:4px;word-wrap: break-word;white-space: nowrap;overflow:hidden;width:100%;margin-right:1.9em;display:inline-block;line-height:1.3em;">{{tab?.title | async}}</span>
                </a>
                <a href="javascript:;" class="closeTab" (click)="removeTab(tab)">&times;</a>
              </li>
              <li class="nav-item" (click)="addTab()">
                <span class="nav-link" data-toggle="tab">+</span>
              </li>
            </ul>
          </nav>

          <section>


            <div class="history" style="position:absolute;top:10px;left:11px;">
              <a class="btn btn-primary btn-xs historyButtons" *ngIf="canGoBack | async" (click)="goBack()">&lsaquo;</a>
              <a class="btn btn-primary hDisabled btn-xs historyButtons" *ngIf="!(canGoBack | async)">&lsaquo;</a>

              <a class="btn btn-primary btn-xs historyButtons" *ngIf="canGoForward | async" (click)="goForward()">&rsaquo;</a>
              <a class="btn btn-primary hDisabled btn-xs historyButtons" *ngIf="!(canGoForward | async)">&rsaquo;</a>

              <a class="btn btn-primary btn-xs historyButtons" *ngIf="!(isLoading | async)" (click)="reload()">↻</a>
              <a class="btn btn-primary btn-xs historyButtons" *ngIf="(isLoading | async)" (click)="cancel()">&times;</a>

            </div>

            <input #nextUrl class="form-control urlInput" type="text"
              (keyup.enter)="selectedTab?.goToUrl(nextUrl.value)" (focus)="$event.target.select()"
              ngModel="{{getUrl() | async}}"/>

            <a href="javascript:;" class="btn btn-primary btn-xs goButton"
              (click)="selectedTab?.goToUrl(nextUrl.value)">▸</a>

          </section>

        </header>

        <section>
          <div *ngFor="let tab of tabs" style="position:fixed;top:120px;right:0;bottom:0;left:0;background-color:rgba(255,255,255,1);" [style.z-index]="tab === selectedTab ? 10 : -10">
            <browser-tab [partition]="proxy?.partition" [tab]="tab"></browser-tab>
          </div>
        </section>

      </div>

    `
})
export class TabbedBrowserComponent implements OnInit, OnDestroy {
  private getUrl(): Observable<string> {
    return this.selectedTab && this.selectedTab.webContents
      ? this.selectedTab.webContents.map(c => (c ? c.getURL() : null))
      : null;
  }

  public get canGoBack(): Observable<boolean> {
    return this.selectedTab && this.selectedTab.webContents
      ? this.selectedTab.webContents.map(wc => {
          if (!wc) return false;
          return wc.canGoBack();
        })
      : Observable.of(false);
  }

  public get canGoForward(): Observable<boolean> {
    return this.selectedTab && this.selectedTab.webContents
      ? this.selectedTab.webContents.map(wc => {
          if (!wc) return false;
          return wc.canGoForward();
        })
      : Observable.of(false);
  }

  public get isLoading(): Observable<boolean> {
    return this.selectedTab && this.selectedTab.webContents
      ? this.selectedTab.webContents.map(wc => {
          if (!wc) return false;
          return wc.isLoading();
        })
      : Observable.of(false);
  }

  public goBack(): void {
    if (this.selectedTab && this.selectedTab.webContents) {
      this.selectedTab.webContents.first().subscribe(wc => {
        if (wc) wc.goBack();
      });
    }
  }

  public goForward(): void {
    if (this.selectedTab && this.selectedTab.webContents) {
      this.selectedTab.webContents.first().subscribe(wc => {
        if (wc) wc.goForward();
      });
    }
  }

  public reload(): void {
    if (this.selectedTab && this.selectedTab.webContents) {
      this.selectedTab.webContents.first().subscribe(wc => {
        if (wc) wc.reload();
      });
    }
  }

  public cancel(): void {
    if (this.selectedTab && this.selectedTab.webContents) {
      this.selectedTab.webContents.first().subscribe(wc => {
        if (wc) wc.stop();
      });
    }
  }

  @Input() public proxy: {
    accessToken: string;
    partition: string;
    instance: any;
    vp: {
      settings: {
        name: string;
        twitterHandle: string;
        twitterPassword: string;
        email: string;
        emailPassword: string;
      };
    }
  };

  private _interval: any;

  private _updaters: (() => void)[] = [];

  @ViewChild("nextUrl") public nextUrl: ElementRef;

  private _urlSubscription: Subscription;

  private _selectedTab: Tab;

  @Input()
  public get selectedTab(): Tab {
    return this._selectedTab;
  }
  public set selectedTab(value: Tab) {
    const prev = this._selectedTab;
    this._selectedTab = value;

    if (prev !== value) {
      if (prev) {
        if (this._urlSubscription) {
          this._urlSubscription.unsubscribe();
        }
      }
      if (value) {
        this._urlSubscription = value.url.subscribe(newUrl => {
          if (this.nextUrl && this.nextUrl.nativeElement) {
            this.nextUrl.nativeElement.value = newUrl.value;
          }
        });
      }
    }
    this.selectedTabChange.emit(value);
  }

  @Output() public selectedTabChange = new EventEmitter<Tab>();

  private _tabs: Tab[] = [];

  public get tabs(): Tab[] {
    return this._tabs;
  }

  private _tabsChange = new EventEmitter<Tab[]>();

  @Output()
  public get tabsChange(): EventEmitter<Tab[]> {
    return this._tabsChange;
  }
  ngOnDestroy(): void {
    clearInterval(this._interval);
  }
  ngOnInit(): void {
    const self = this;
    this._interval = setInterval(() => {
      for (let updater of self._updaters) {
        updater();
      }
    }, 500);
    electron.ipcRenderer.on("newTab", (e, url) => {
      const selectedTab = self.selectedTab;
      const newTab = self.addTab();
      newTab.goToUrl(url, false);
      self.selectedTab = selectedTab || newTab;
    });
  }

  public addTab(): Tab {
    const self = this;
    const urlSubject = new BehaviorSubject<{
      value: string;
      navigated: boolean;
    }>({ value: "about:blank", navigated: true });
    const titleSubject = new BehaviorSubject<string>("");
    const webContentsSubject = new BehaviorSubject<WebContents>(null);
    let webContents: WebContents;
    let updater: () => void;
    let lastTitle: string;
    const tab = <Tab>{
      title: titleSubject.asObservable(),
      url: urlSubject.asObservable(),
      close(): void {
        if (updater) {
          self._updaters.splice(self._updaters.indexOf(updater), 1);
        }
      },
      goToUrl(url: string, navigated: boolean = false): void {
        urlSubject.next({ value: url, navigated: navigated });
      },
      webContents: webContentsSubject.asObservable(),
      setWebContents(value: WebContents) {
        if (webContents === value) {
          return;
        }
        if (updater) {
          self._updaters.splice(self._updaters.indexOf(updater), 1);
        }
        webContents = value;
        if (self.proxy && self.proxy.accessToken) {
          webContents.session.webRequest.onBeforeSendHeaders({urls: ['*']}, (details, cb) => {
            cb({requestHeaders: Object.assign(details.requestHeaders, {'Proxy-Authorization': `Bearer ${self.proxy.accessToken}`})});
          });
        }
        webContentsSubject.next(value);
        // webContents.openDevTools();
        webContents.on("context-menu", (e, params) => {
          if (params.linkURL) {
            electron.ipcRenderer.send(
              "menu",
              [
                {
                  label: "Open link in new tab",
                  click: {
                    channel: "newTab",
                    message: params.linkURL
                  }
                }
              ]
            );
          }
        });
        webContents.on(
          "new-window",
          (e, url, name, disposition, additional) => {
            switch (disposition) {
              case "new-window":
              case "background-tab":
                {
                  const newWindowTab = self.addTab();
                  newWindowTab.goToUrl(url, false);
                  self.selectedTab = tab;
                }
                break;
              case "save-to-disk":
                break;
              default:
              case "default":
              case "other":
              case "foreground-tab":
                {
                  const newWindowTab = self.addTab();
                  newWindowTab.goToUrl(url, false);
                }
                break;
            }
          }
        );
        webContents.on("did-navigate", (e, url) => {
          if (url) {
            this.goToUrl(url, true);
          }
        });
        webContents.on("did-navigate-in-page", (e, url) => {
          if (url) {
            this.goToUrl(url, true);
          }
        });
        if (value) {
          updater = () => {
            const newTitle = value.getTitle();
            if (newTitle !== lastTitle) {
              lastTitle = newTitle;
              titleSubject.next(newTitle);
            }
          };
          self._updaters.push(updater);
        }
      }
    };
    this._tabs.push(tab);
    this._tabsChange.emit(this._tabs);
    this.selectedTab = tab;
    return tab;
  }

  public removeTab(tab: Tab): void {
    if (tab) {
      tab.close();
    }
    if (this.selectedTab === tab) {
      const otherTabs = this._tabs.filter(item => item !== tab);
      this.selectedTab = otherTabs.length
        ? otherTabs[otherTabs.length - 1]
        : null;
    }
    const index = this._tabs.findIndex(item => item === tab);
    this._tabs.splice(index, 1);
    this._tabsChange.emit(this._tabs);
  }
}
@Component({
  selector: "browser-tab",
  template: `
  <div #wvc style="height:100%;width:100%">
  </div>

    `
})
export class BrowserTabComponent implements OnDestroy {
  private _partition: string;
  @Input()
  public get partition(): string {
    return this._partition;
  }
  public set partition(value: string) {
    const prevValue = this._partition;
    this._partition = value;

    if (prevValue !== value) {
      this.createWebView();
    }
  }

  private _wvContainer: ElementRef;

  @ViewChild("wvc")
  public get wvContainer(): ElementRef {
    return this._wvContainer;
  }
  public set wvContainer(value: ElementRef) {
    const prevValue = this._wvContainer;
    this._wvContainer = value;

    if (prevValue !== value) {
      this.createWebView();
    }
  }

  private _proxy: {
    accessToken: string;
    partition: string;
    instance: any;
    vp: {
      settings: {
        twitterHandle: string;
        twitterPassword: string;
        email: string;
        emailPassword: string;
      };
    }
  };

  private _tab: Tab;

  private _webview: WebviewTag;

  private _webContents: WebContents;

  private _subscription: Subscription;

  private _tabUrl: string;

  private createWebView(): void {
    if (!this.wvContainer) return;
    if (
      this.wvContainer.nativeElement.firstChild.partition === this.partition
    ) {
      return;
    }
    const element = document.createElement("webview");
    element.src = "about:blank";
    element.partition = this.partition;
    element.allowpopups = "true";
    element.style.width = "100%";
    element.style.height = "100%";
    while (this.wvContainer.nativeElement.firstChild)
      this.wvContainer.nativeElement.removeChild(
        this.wvContainer.nativeElement.firstChild
      );
    this.wvContainer.nativeElement.appendChild(element);

    this.wv = <any>element;
  }

  @Input()
  public get tab(): Tab {
    return this._tab;
  }
  public set tab(value: Tab) {
    const prev = this._tab;
    this._tab = value;

    if (value !== prev) {
      this.createWebView();
      if (prev && this._subscription) {
        this._subscription.unsubscribe();
      }
      if (value) {
        if (this._webContents) {
          value.setWebContents(this._webContents);
        }
        this._subscription = value.url.subscribe(newUrl => {
          this._tabUrl = newUrl.value;
          if (newUrl.navigated) {
            return;
          }
          if (
            this._webContents &&
            this._webContents.getURL() !== newUrl.value
          ) {
            this._webContents.loadURL(normalizeUrl(newUrl.value));
          }
        });
      }
    }
  }

  private _wv: WebviewTag;

  get wv(): WebviewTag {
    return this._wv;
  }
  set wv(value: WebviewTag) {
    function doWhen(
      condition: () => boolean,
      action: () => void,
      interval: number = 100
    ): void {
      const intervalId = setInterval(() => {
        if (condition()) {
          clearInterval(intervalId);
          action();
        } else {
          return;
        }
      }, interval);
    }

    this._wv = value;

    if (value) {
      doWhen(
        () => !!(this._webContents = value.getWebContents()),
        () => {
          this._webview = value;
          this._webContents = value.getWebContents();
          this._webContents.on("did-fail-load", (ev, errCode, errDesc) => {
            console.error("Error loading: ", {
              code: errCode,
              description: errDesc
            });
          });
          if (this._subscription) {
            this._subscription.unsubscribe();
          }
          if (this._tab) {
            this._tab.setWebContents(this._webContents);
            this._subscription = this._tab.url.subscribe(newUrl => {
              this._tabUrl = newUrl.value;
              if (newUrl.navigated) {
                return;
              }
              if (this._webContents) {
                this._webContents.loadURL(normalizeUrl(newUrl.value));
              }
            });
          }
          this._webContents.on("will-navigate", (e, url) => {
            if (this._tabUrl === url) {
              return;
            }
            if (this._tab) {
              this._tabUrl = url;
              this._tab.goToUrl(url, true);
            }
          });
        }
      );
    }
  }

  ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }
}
