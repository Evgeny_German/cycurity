import { Component, ViewChild, ElementRef, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { TabbedBrowserComponent, Tab } from "./tabbed-browser.component";
import { Observable } from "rxjs/Observable";
import { Session } from "electron";
const electron = window["require"]("electron");

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  @ViewChild("browser") browser: TabbedBrowserComponent;

  proxy: {
    accessToken: string;
    partition: string;
    instance: any;
    vp: {
      settings: {
        twitterHandle: string;
        twitterPassword: string;
        email: string;
        emailPassword: string;
      };
    };
  };

  ngOnInit(): void {
    electron.ipcRenderer.on("proxy", (e, proxy) => {
      this.proxy = proxy;

      const tab = this.browser.addTab();
      (async () => {
        if (
          this.proxy.vp &&
          this.proxy.vp.settings &&
          this.proxy.vp.settings.email &&
          this.proxy.vp.settings.emailPassword
        ) {
          const mailProvider = this.proxy.vp.settings.email.includes("@")
            ? this.proxy.vp.settings.email.split("@")[1]
            : null;
          const sub = mailProvider.includes(".")
            ? mailProvider.split(".")[0].toLowerCase()
            : null;
          if (sub === "gmail") {
            let subscription: Subscription;
            subscription = tab.webContents.subscribe(webContents => {
              if (!webContents) {
                return;
              }
              subscription.unsubscribe();
              (async () => {
                try {
                  await new Promise((resolve, reject) => {
                    webContents.once("did-finish-load", () => resolve());
                    webContents.once(
                      "did-fail-load",
                      (event, errCode, errDesc) =>
                        reject({ code: errCode, description: errDesc })
                    );
                    webContents.loadURL(
                      `https://accounts.google.com/ServiceLogin?hl=en&passive=true`
                    );
                  });
                  // webContents.openDevTools();
                  await webContents.executeJavaScript(`(async () => {
                  (document.getElementById("identifierId")||document.getElementById("Email"))['value'] = "${
                    this.proxy.vp.settings.email
                  }";
                  (document.getElementById("identifierId")||document.getElementById("Email")).focus();
                })();`);
                  await new Promise(r => setTimeout(r, 1000));
                  webContents.sendInputEvent(<any>{
                    type: "char",
                    keyCode: "\r"
                  });
                  await webContents.executeJavaScript(`(async () => {
                  const isReady = () => {
                    const passwd = (document.querySelector("input[type=password]")||document.getElementById("Passwd"));
                    return passwd && !passwd.hasAttribute("aria-hidden") && !passwd.classList.contains("hidden");
                  };
                  await new Promise((resolve, reject) => {
                    let r;
                    r = (() => {
                      if (isReady()) {resolve();return;}
                      setTimeout(r, 100);
                    });
                    if (!isReady()) r();
                  });
                  (document.querySelector("input[type=password]")||document.getElementById("Passwd"))['value'] = "${
                    this.proxy.vp.settings.emailPassword
                  }";
                  (document.querySelector("input[type=password]")||document.getElementById("Passwd")).focus();
                })();`);
                  await new Promise(r => setTimeout(r, 1000));
                  webContents.sendInputEvent(<any>{
                    type: "char",
                    keyCode: "\r"
                  });
                } catch (error) {
                  console.error(`Error logging in to google: `, error);
                }
              })();
            });
          } else if (mailProvider) {
            tab.goToUrl(mailProvider, false);
          }
        }
      })();
      (async () => {
        if (
          this.proxy.vp &&
          this.proxy.vp.settings &&
          this.proxy.vp.settings.twitterHandle &&
          this.proxy.vp.settings.twitterPassword
        ) {
          const twitterTab = this.browser.addTab();
          let subscription: Subscription;
          subscription = twitterTab.webContents.subscribe(webContents => {
            if (!webContents) {
              return;
            }
            subscription.unsubscribe();
            (async () => {
              try {
                await new Promise((resolve, reject) => {
                  webContents.once("did-finish-load", () => resolve());
                  webContents.once("did-fail-load", (event, errCode, errDesc) =>
                    reject({ code: errCode, description: errDesc })
                  );
                  webContents.loadURL("https://twitter.com/login");
                });
                // webContents.openDevTools();
                await webContents.executeJavaScript(`new Promise((resolve, reject) => {
                  function doWhen(condition, action, interval = 100) {
                    const intervalId = setInterval(() => {
                        if (condition()) {
                            clearInterval(intervalId);
                            action();
                        } else {
                            return;
                        }
                    }, interval);
                  }
                  doWhen(() => document.querySelector(".js-username-field"), () => {
                    document.querySelector(".js-username-field")['value'] = "${
                      this.proxy.vp.settings.twitterHandle
                    }";
                    document.querySelector(".js-password-field")['value'] = "${
                      this.proxy.vp.settings.twitterPassword
                    }";
                    document.querySelector(".signin-wrapper .submit").click();
                    resolve();
                  });
                })`);
              } catch (error) {
                console.error(`Error logging in to twitter: `, error);
              }
            })();
          });
        }
      })();
    });
  }
}
