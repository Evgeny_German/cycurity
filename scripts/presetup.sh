#install node
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -;
sudo apt-get install -y nodejs;

#fix npm to run w/o sudo
#mkdir ~/.npm-global
#npm config set prefix '~/.npm-global'
#PATH=~/.npm-global/bin:$PATH
#source /etc/environment && export PATH

#install typescript
sudo npm install typescript -g;
#install pm2
sudo npm install pm2 -g