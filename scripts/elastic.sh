#http://www.itzgeek.com/how-tos/linux/ubuntu-how-tos/setup-elk-stack-ubuntu-16-04.html
#sudo journalctl --unit elasticsearch
#nano /etc/elasticsearch/jvm.options   set memory to -Xms512m
#nano /etc/elasticsearch/elasticsearch.yml
#curl -X GET http://localhost:9200
[ `whoami` = root ] || exec sudo su -c $0 root
apt-get update
#install java
apt-get install -y openjdk-8-jdk wget
#install elastic
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elk.list
apt-get update
apt-get install -y elasticsearch
#replace min max vals for jvm to 512
sed -i 's/-Xms2g/-Xms512m/g' /etc/elasticsearch/jvm.options
sed -i 's/-Xmx2g/-Xmx512m/g' /etc/elasticsearch/jvm.options
sed -i 's/^#network.host.*/network.host: 0.0.0.0/g' /etc/elasticsearch/elasticsearch.yml
#set for boot
systemctl enable elasticsearch
systemctl start elasticsearch
#test
#curl -X GET http://localhost:9200
#kibana
apt-get install -y kibana
#configure kibana
sed -i 's/^#server.host.*/server.host: "0.0.0.0"/g' /etc/kibana/kibana.yml
sed -i 's/^#elasticsearch.url.*/elasticsearch.url: "http:\/\/localhost:9200"/g' /etc/kibana/kibana.yml
#set for boot
systemctl enable kibana
systemctl start kibana

