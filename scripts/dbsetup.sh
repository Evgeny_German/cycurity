#install postgres and create cycuritydb
[ `whoami` = root ] || exec sudo su -c $0 root
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib -y
sudo -u postgres psql -U postgres -d postgres -c "alter user postgres with password 'admin';"
sudo su postgres -c "createdb cycuritydb --owner postgres"

sed -i "s/^#listen_addresses.*/listen_addresses = '*' /g" /etc/postgresql/9.5/main/postgresql.conf
#the sed '0,/this/s//to_that/' replaces only the first occurance leaving ip 6 not modified
sed -i '0,/^host.*/s//host  all  all  0.0.0.0\/0 md5/g' /etc/postgresql/9.5/main/pg_hba.conf
#restart postgre
/etc/init.d/postgresql restart
#install mysql and create cycuritydb
#sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
#sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
#sudo apt-get -y install mysql-server
#sudo mysql --user="root" --password="root" --execute="CREATE DATABASE cycuritydb CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"