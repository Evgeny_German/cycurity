#!/bin/bash
# importing lates stable NodeJS
# curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
# prerequisites 
# sudo apt-get update

# install nodeJS

# sudo apt-get install nodejs -y # python3-dev -y

# sudo npm install -g pm2
# sudo npm install -g typescript
# install all required npm packages
npm run build

pm2 start process_job.json --env staging
# config pm2 to run at startup
sudo pm2 startup
