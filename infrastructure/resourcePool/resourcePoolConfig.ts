export interface ResourcePoolConfig<T> {
    getContext?: (id: number) => any;
    getResource?: (context: any) => Promise<T>;
    stopResource?: (resource: T, context?: any) => Promise<void>;
    validateResource?: (resource: T, context?: any) => Promise<boolean>;
    getResourceTimeout?: number;
    retryGetResourceInterval?: number;
    maxLifeTimeSec?: number;
    runTaskTimeout?: number;
    minCount?: number;
    maxCount?: number;
}
