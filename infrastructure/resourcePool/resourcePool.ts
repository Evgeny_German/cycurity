import { timeout, delay, TimeoutError } from "../utils";
import { spinWait } from "../utils/spinWait";
import { ResourcePoolConfig } from "./resourcePoolConfig";
import { triggerAsyncId } from "async_hooks";

export class ResourcePool<T> {
  private resources: {
    context: any;
    resource: T;
    createdAt: Date;
    available: boolean;
  }[] = [];
  private tickets: {
    priority: number;
    queued: Date;
  }[] = [];
  private availableResources: T[] = [];
  private occupiedResources: T[] = [];
  private config: ResourcePoolConfig<T>;

  constructor(config: ResourcePoolConfig<T>) {
    this.config = Object.assign(
      <ResourcePoolConfig<T>>{
        runTaskTimeout: 300000,
        maxLifeTimeSec: 600,
        getResourceTimeout: 10000,
        retryGetResourceInterval: 10000
      },
      config
    );
    this.getResources();
  }

  async getResourceAsync(
    id: number
  ): Promise<{
    resource: T;
    context: any;
    createdAt: Date;
    available: boolean;
  }> {
    console.log(`Attempting to get resource @ ${id}.`);
    const context = this.config.getContext(id);
    try {
      let error: Error;
      const result = await timeout(
        () => this.config.getResource(context),
        this.config.getResourceTimeout,
        resource => this.config.stopResource(resource, context),
        error => this.config.stopResource(null, context)
      ).then(resource => ({
        resource: resource,
        context: context,
        createdAt: new Date(),
        available: false
      }));
      if (error) throw error;
      console.log(`Got resource @ ${id}.`);
      return result;
    } catch (error) {
      this.config.stopResource(null, context);
      console.log(`Error getting resource @ ${id}, will try again in 10s.`);
      await delay(this.config.retryGetResourceInterval);
      return await this.getResourceAsync(id);
    }
  }

  private async getResources(): Promise<void> {
    for (let i = 0; i < this.config.minCount; i++) {
      this.getResourceAsync(i).then(resource => {
        this.resources[i] = resource;
        this.resources[i].available = true;
      });
    }
  }

  private enqueue(priority: number): { priority: number; queued: Date } {
    const ticket = {
      priority: priority,
      queued: new Date()
    };
    console.log(`Enqueuing job with priority ${priority}`);
    this.tickets.push(ticket);
    return ticket;
  }

  private getHighestPriorityTicket(): { priority: number; queued: Date } {
    if (!this.tickets.length) return null;
    if (this.tickets.length === 1) return this.tickets[0];
    const highestPriority = this.tickets.sort(
      (a, b) =>
        a.priority === b.priority ? 0 : a.priority > b.priority ? -1 : 1
    )[0];
    const priorityGroup = this.tickets
      .filter(
        ticket => Math.abs(ticket.priority - highestPriority.priority) < 1.0
      )
      .sort(
        (a, b) => (a.queued === b.queued ? 0 : a.queued < b.queued ? -1 : 1)
      );
    return priorityGroup[0];
  }

  private async dequeue(
    ticket: { priority: number; queued: Date },
    interval = 100
  ): Promise<number> {
    let id: number;
    await spinWait(
      (() => {
        if (!this.resources) throw new Error();
        if (this.getHighestPriorityTicket() !== ticket) return false;
        const resourceId = this.resources.findIndex(
          resource => resource.available
        );
        if (!~resourceId) {
          return false;
        }
        this.resources[resourceId].available = false;
        id = resourceId;
        this.tickets.splice(this.tickets.indexOf(ticket), 1);
        return true;
      }).bind(this),
      interval
    );
    console.log(`Dequeued job with priority ${ticket.priority}.`);
    return id;
  }

  public async run<TResult>(
    func: (resource: T, context?: any, id?: number) => Promise<TResult>,
    priority: number,
    interval: number = 100
  ): Promise<TResult> {
    let id: number;

    const ticket = this.enqueue(priority);

    this.adjustResourceCount();

    try {
      id = await this.dequeue(ticket);
    } catch (err) {
      throw new Error("No available resources.");
    }

    let result: TResult;
    let error: Error;

    try {
      result = await timeout(
        async () =>
          await func(
            this.resources[id].resource,
            this.resources[id].context,
            id
          ),
        this.config.runTaskTimeout
      );
    } catch (err) {
      if (err instanceof TimeoutError && this.config.stopResource) {
        console.log(`Stopping resource @ ${id}.`);
        await this.config.stopResource(
          this.resources[id].resource,
          this.resources[id].context
        );
      }
      error = err;
    }
    
    (async () => {
      const thisId = id;
      this.resources[thisId].resource = await this.validateResource(thisId);
      if (await this.adjustResourceCount(thisId)) {
        this.resources[thisId].available = true;
      }
    })();
    
    if (error) throw error;
    console.log(`Finished job with resource @ ${id}.`);
    return result;
  }

  private async adjustResourceCount(id?: number): Promise<boolean> {
    const resourcesNeeded =
      this.tickets.length -
      this.resources.filter(resource => resource.available).length;
    if (this.resources.length < this.config.maxCount) {
      if (resourcesNeeded > 0) {
        const count = Math.min(
          resourcesNeeded,
          this.config.maxCount - this.resources.length
        );
        const start = this.resources.length;
        for (let i = start; i < start + count; i++) {
          this.resources[i] = {
            available: false,
            context: null,
            createdAt: new Date(),
            resource: null
          };
        }
        for (let i = start; i < start + count; i++) {
          console.log(`Adding resource @ ${i}.`);
          this.resources[i] = await this.getResourceAsync(i);
          this.resources[i].available = true;
        }
      }
      return true;
    }
    if (
      id === this.resources.length - 1 &&
      this.resources.length > this.config.minCount
    ) {
      if (resourcesNeeded < 1) {
        if (this.config.stopResource) {
          console.log(`Stopping resource @ ${id}.`);
          await this.config.stopResource(
            this.resources[id].resource,
            this.resources[id].context
          );
        }
        this.resources.splice(id, 1);
        return false;
      }
    }
    return true;
  }

  private async validateResource(id: number): Promise<T> {
    if (!this.config.validateResource) {
      if (
        this.resources[id].createdAt.valueOf() +
          this.config.maxLifeTimeSec * 1000 >=
        new Date().valueOf()
      ) {
        return this.resources[id].resource;
      }
      console.log(`Resource @ ${id} expired.`);
      return await this.restartResource(id);
    }
    let valid: boolean;
    try {
      if (
        this.resources[id].createdAt.valueOf() +
          this.config.maxLifeTimeSec * 1000 <
        new Date().valueOf()
      ) {
        throw new Error("Resource expired.");
      }
      valid = await this.config.validateResource(
        this.resources[id].resource,
        this.resources[id].context
      );
    } catch (error) {
      console.log(`Resource @ ${id} failed validation.`);
      return await this.restartResource(id);
    }
    if (!valid) {
      console.log(`Resource @ ${id} failed validation.`);
      return await this.restartResource(id);
    }
    return this.resources[id].resource;
  }

  private async restartResource(id: number): Promise<T> {
    if (this.config.stopResource) {
      this.resources[id].available = false;
      console.log(`Stopping resource @ ${id} for restart.`);
      await this.config.stopResource(
        this.resources[id].resource,
        this.resources[id].context
      );
    }
    console.log(`Starting resource @ ${id} for restart.`);
    const newResource = (this.resources[id] = await this.getResourceAsync(id));
    return newResource.resource;
  }

  public async stop(): Promise<void> {
    return this.config.stopResource
      ? <any>await Promise.all(
          this.resources.map(resource =>
            this.config.stopResource(resource.resource)
          )
        )
      : void 0;
  }
}
