import * as express from "express";
import * as http from "http";
import * as https from "https";
import * as bodyParser from "body-parser";
import * as compression from "compression";
import {ServerOptions} from "https";

export abstract class BaseExpressServer {
    protected httpServer;
    protected app: express.Express;
    protected port: number = 3000;

    constructor() {
        this.app = express();
        this.app.use(bodyParser.json({limit: "100mb"}));
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(compression());

        this.port = +(process.env["PORT"] || this.port);
        this.setLogger();
        this.setRoutes(this.app);
        this.setStaticFolders(this.app);
    }

    protected setLogger (){}

    protected abstract setRoutes(app: express.Express): void;

    protected abstract getAppName(): string;

    public startServer = (secure?: boolean, options?: ServerOptions): void => {
        this.httpServer = secure
            ? https.createServer(options, this.app)
            : http.createServer(this.app);
        this.httpServer.listen(this.port).setTimeout(600000);
        this.httpServer.on("error", this.onError);
        this.httpServer.on("listening", () => {
            console.log(`${this.getAppName()} listening on port ${this.port}`);
            console.log("you are running in " + process.env.NODE_ENV + " mode.");
        });
    };

    protected setStaticFolders(app: express.Express): void {
    }

    protected onError(err: any): void {
        switch (err.code) {
            case "EACCES":
                console.error("port requires elevated privileges");
                process.exit(1);
                break;
            case "EADDRINUSE":
                console.error("port is already in use");
                process.exit(1);
                break;
            default:
                throw err;
        }
    }

    protected setErrorHandlers(app: express.Express): void {
        app.use(
            (err: Error,
             req: express.Request,
             res: express.Response,
             next: express.NextFunction) => {
                res.status((<any>err).status || 500);
                res.send({
                    message: err.message,
                    error: err
                });
            }
        );
    }
}
