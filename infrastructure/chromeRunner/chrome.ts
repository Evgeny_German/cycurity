import { spawn, ChildProcess } from "child_process";
import * as CDP from "chrome-remote-interface";
import * as fs from "fs-extra";
import * as which from "which";
import { timeout, TimeoutError } from "../utils/timeout";
import { delay } from "../utils/delay";
import { spinWait } from "../utils/spinWait";
import { spinWaitAsync } from "../utils/index";
// import { try } from 'bluebird';

export const chromePath = (() => {
  try {
    const chrome = which.sync("chrome");
    console.log(`found "chrome" at ${chrome}`);
    return chrome;
  } catch (err) {
    try {
      const chrome = which.sync("google-chrome");
      console.log(`found "google-chrome" at ${chrome}`);
      return chrome;
    } catch (err) {
      return process.env["CHROME_PATH"];
    }
  }
})();

export function patchWindowSetup(vars: any): string {
  return `() => new Promise((resolve, reject) => {
        try {
            ${Object.keys(vars)
              .map(key => `window['${key}'] = ${JSON.stringify(vars[key])};`)
              .join("\n")}
            resolve(true);
        } catch (error) {
            resolve(false);
        }
    })`;
}

export class Chrome {
  private _loaded: boolean;
  private _navigating: string;
  private _lastResponse: any;
  private _frame: any;
  private _cdp: any;
  private _process: ChildProcess;

  public get cdp(): any {
    return this._cdp;
  }

  constructor(
    private headless: boolean = true,
    private remoteDebuggingPort: number = 9222,
    private userDataDir?: string,
    makeTempDir?: boolean,
    tempDirPrefix?: string
  ) {
    if (makeTempDir) {
      try {
        if (!fs.existsSync(tempDirPrefix || `${__dirname}/temp/`)) {
          fs.mkdirSync(tempDirPrefix || `${__dirname}/temp`);
        }
      } catch (error) {
        console.log("Error creating temp dir.");
        throw error;
      }
      this.userDataDir = fs.mkdtempSync(tempDirPrefix || `${__dirname}/temp/`);
    }
  }

  public async evaluate<T>(
    expression: () => Promise<T>,
    setup?: (() => Promise<boolean>) | string,
    executionContext?: any
  ): Promise<T> {
    let result: any;
    console.log("Evaluating expression.");
    if (setup) {
      const cont = await this._cdp.Runtime.evaluate({
        expression: `(${setup.toString()})()`,
        returnByValue: true,
        awaitPromise: true
      });
      if (!cont || !cont.result || !cont.result.value)
        throw new Error("Chrome evaluate setup was cancelled.");
    }
    result = await this._cdp.Runtime.evaluate({
      expression: `(${expression.toString()})()`,
      returnByValue: true,
      awaitPromise: true
    });
    console.log("Finished evaluating expression.");
    return <T>result;
  }

  public async navigate(
    url: string,
    waitForLoadEvent?: boolean
  ): Promise<Chrome> {
    let loadEventFired = false;
    return await new Promise<Chrome>(resolve => {
      // if (waitForLoadEvent) {
      // }
      this._navigating = url;
      this._loaded = false;
      console.log("Navigating to: ", url);
      Promise.all([this.cdp.Page.enable(), this.cdp.Network.enable()]).then(
        async () => {
          await this._cdp.Page.navigate({ url: "about:blank" });
          await spinWaitAsync(async () => {
            const history = await this._cdp.Page.getNavigationHistory();
            return history.entries[history.currentIndex].url === "about:blank";
          })
            await this._cdp.Page.navigate({ url: url })
            .then(frame => {
              this._cdp.Page.loadEventFired().then(() => {
                this._frame = frame;
                timeout(() => spinWait(() => this._loaded), 2500)
                  .then(() => {
                    this._navigating = undefined;
                    console.log("Navigated to: ", url);
                    resolve(this);
                  })
                  .catch(() => {
                    this._loaded = true;
                    this._navigating = undefined;
                    console.log("Navigated to: ", url);
                    resolve(this);
                  });
              });
            });
        }
      );
      // if (waitForLoadEvent) {
      //     await this.cdp.Page.loadEventFired();
      // }
    });
  }

  public async startCdp(
    options?: { host?: string; port?: number },
    _timeout: number = 5000
  ): Promise<Chrome> {
    let result: any;
    let isTimedOut: boolean;
    delay(_timeout).then(() => (isTimedOut = true));
    while (!result && !isTimedOut) {
      try {
        if (!this._process.pid) {
          throw new Error("Chrome process not running.");
        }
        result = await timeout(
          () =>
            new Promise((resolve, reject) => {
              try {
                CDP(options || { port: this.remoteDebuggingPort }, client => {
                  console.log(
                    "cdp connected, port: ",
                    this.remoteDebuggingPort
                  );
                  resolve(client);
                }).on("error", error => {
                  reject(error);
                });
              } catch (err) {
                reject(err);
              }
            }),
          1000
        );
      } catch (err) {
        if (err.constructor === TimeoutError) {
          console.warn("Timed out waiting for chrome to listen.");
          continue;
        }
        console.error(
          "Error while trying to connect to Chrome. will retry in 1s."
        );
        await delay(1000);
      }
    }
    if (isTimedOut) throw new TimeoutError();
    this._cdp = result;
    await this._cdp.Runtime.enable();
    this._cdp.on("Runtime.consoleAPICalled", resp => {
      console.log("Client console api: ", resp);
    });
    this.cdp.on("Network.responseReceived", resp => {
      if (
        resp.type === "Document" /* && resp.requestId === this._frame.frameId*/
      ) {
        this._lastResponse = resp;
      }
    });
    this.cdp.on("Page.loadEventFired", () => {
      if (this._navigating) {
        this.evaluate(<any>`
                    () => new Promise((resolve2, reject2) => {
                        window['pageResponse'] = ${
                          this._lastResponse
                            ? JSON.stringify(this._lastResponse.response)
                            : null
                        };
                        resolve2(window['pageResponse']);
                    })`).then(() => (this._loaded = true));
      }
    });
    this.cdp.on("error", err => console.error("CDP error: ", err));
    return this;
  }

  public async start(createUserDataDir?: boolean): Promise<Chrome> {
    if (createUserDataDir) {
      if (!fs.existsSync(this.userDataDir)) fs.mkdirSync(this.userDataDir);
    }
    try {
      this._process = await spawn(
        chromePath,
        [
          "--ignore-certificate-errors",
          "--no-sandbox",
          "--lang=en",
          this.headless ? "--headless" : null,
          this.remoteDebuggingPort
            ? `--remote-debugging-port=${this.remoteDebuggingPort}`
            : null,
          this.userDataDir ? `--user-data-dir=${this.userDataDir}` : null
        ].filter(arg => arg)
      );
      console.log(
        `Chrome started @ port ${this.remoteDebuggingPort} w/ data dir ${
          this.userDataDir
        }`
      );
    } catch (err) {
      console.log(
        `Chrome @ port ${this.remoteDebuggingPort} w/ data dir ${
          this.userDataDir
        } failed to start.`
      );
      throw err;
    }
    return this;
  }

  public async stop(deleteUserDataDir?: boolean): Promise<Chrome> {
    if (this._process) {
      this._process.kill("SIGINT");
    }
    if (deleteUserDataDir) {
      await delay(3000);
      try {
        fs.removeSync(this.userDataDir);
      } catch (error) {}
    }
    this._process = null;
    return this;
  }
}
