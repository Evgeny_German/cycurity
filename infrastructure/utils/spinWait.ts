
export function spinWait(predicate: () => boolean, interval: number = 100): Promise<any> {
    return new Promise((resolve, reject) => {
        const ivl = setInterval(() => {
            try {
                if (predicate()) {
                    clearInterval(ivl);
                } else {
                    return;
                }
            } catch (error) {
                reject(error);
                return;
            }
            resolve();
        }, interval);
    });
}

export function spinWaitAsync(predicate: () => Promise<boolean>, interval: number = 100): Promise<any> {
    return new Promise((resolve, reject) => {
        const ivl = setInterval(async () => {
            try {
                if (await predicate()) {
                    clearInterval(ivl);
                } else {
                    return;
                }
            } catch (error) {
                reject(error);
                return;
            }
            resolve();
        }, interval);
    });
}
