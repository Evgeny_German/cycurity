import {spinWait} from '.';
import Timer = NodeJS.Timer;

export abstract class BackgroundProcess {

    constructor(private readonly interval: number) {
    }

    private intervalHandle: Timer;
    private isStopped: boolean;
    private isInIteration: boolean;

    public async start(): Promise<void> {
        await this.stop();
        this.isStopped = false;
        this.intervalHandle = setInterval(() => {
            if (this.isInIteration || this.isStopped) return;
            this.isInIteration = true;
            this.work().catch(() => this.isInIteration = false).then(() => this.isInIteration = false);
        }, this.interval);
    }

    protected abstract work(): Promise<void>;

    public async stop(): Promise<void> {
        this.isStopped = true;
        if (this.intervalHandle) clearInterval(this.intervalHandle);
        if (this.isInIteration) await spinWait(() => !this.isInIteration, 10);
    }
}