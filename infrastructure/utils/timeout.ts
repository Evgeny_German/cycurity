import {spinWait} from '.';

export function timeout<T>(promise: () => Promise<T>, timeout: number, dispose?: (response: T) => void, disposeErr?: (error: Error) => void): Promise<T> {
    return new Promise((resolve, reject) => {
        let result: T;
        let error: Error;
        let finished: boolean;
        let isError: boolean;
        let isTimeout: boolean;
        setTimeout(() => {
            if (!finished) {
                isTimeout = true;
                reject(new TimeoutError());
                return;
            }
        }, timeout);
        promise().then(res => {
            if (isTimeout) {
                if (dispose) dispose(res);
                return;
            }
            resolve(res);
            result = res;
            finished = true;
        }).catch(err => {
            if (isTimeout) {
                if (disposeErr) disposeErr(err);
                return;
            }
            reject(err);
            error = err;
            isError = true;
            finished = true;
        });
    });
}

export class TimeoutError extends Error {
    constructor(message?: string) {
        super(message);
    }
}