import * as loopback from "loopback";
import * as boot from "loopback-boot";
import { autoMigrate } from "./automigrate";
import { Server } from "http";

import * as superagent from "superagent";
import * as dotEnv from "dotenv";
import * as path from "path";
import * as Url from "url";
import * as _ from "lodash";

function fixReport(app: any, report: any): Promise<void> {
  return new Promise((resolve, reject) => {
    const fixedData = report.data;
    fixedData.conversations.forEach(conv => {
      if (!conv.url) {
        return;
      }
      const parsed = parseUrl(conv.url);
      conv.url = `https://twitter.com/${parsed.userName.toLowerCase()}/status/${
        parsed.tweetId
      }`;
    });
    app.models["document"].updateAll(
      { id: report.id },
      { data: fixedData, datab: fixedData },
      (error, result) => {
        if (error) {
          reject(error);
          return;
        }
        resolve();
      }
    );
  });
}

function fetchAllReports(app: any): Promise<any[]> {
  return new Promise<any[]>((resolve, reject) => {
    app.models["document"].find({ where: { type: 2 } }, (error, result) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(result);
    });
  });
}

function fetchAllConversations(app: any): Promise<any[]> {
  return new Promise<any[]>((resolve, reject) => {
    app.models["document"].find({ where: { type: 1 } }, (error, result) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(result);
    });
  });
}

function fixConversationId(app: any, conversation: any): Promise<void> {
  return new Promise((resolve, reject) => {
    const parsed = parseUrl(conversation.documentId);
    app.models["document"].updateAll(
      { id: conversation.id },
      {
        documentId: `https://twitter.com/${parsed.userName.toLowerCase()}/status/${
          parsed.tweetId
        }`
      },
      (error, result) => {
        if (error) {
          reject(error);
          return;
        }
        resolve();
      }
    );
  });
}

async function copyConversationForReport(
  app: any,
  conversation: any,
  reportId: string,
  userId: number
): Promise<void> {
  if (!conversation.userName || !conversation.tweetId) {
    if (!conversation.url) return;
    const parsed = parseUrl(conversation.url);
    conversation.userName = parsed.userName;
    conversation.tweetId = parsed.tweetId;
  }
  let conv = await new Promise<any[]>((resolve, reject) => {
    app.models["document"].find(
      {
        where: {
          type: 1,
          userId: userId,
          documentId: `https://twitter.com/${conversation.userName.toLowerCase()}/status/${
            conversation.tweetId
          }?reportId=${reportId}`
        }
      },
      (error, result) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(result);
      }
    );
  });
  if (conv.length) {
    return;
  }
  conv = await new Promise<any[]>((resolve, reject) => {
    app.models["document"].find(
      {
        where: {
          type: 1,
          userId: userId,
          documentId: `https://twitter.com/${conversation.userName.toLowerCase()}/status/${
            conversation.tweetId
          }`
        }
      },
      (error, result) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(result);
      }
    );
  });
  if (!conv.length) {
    return;
  }
  const res = await new Promise<any>((resolve, reject) => {
    app.models["document"].create(
      {
        type: 1,
        version: 1,
        userId: userId,
        documentId: `https://twitter.com/${conversation.userName.toLowerCase()}/status/${
          conversation.tweetId
        }?reportId=${reportId}`,
        data: conv[0].data,
        datab: conv[0].data
      },
      (error, result) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(result);
      }
    );
  });
}

function deleteConversation(app: any, conversation: any): Promise<void> {
  return new Promise((resolve, reject) => {
    app.models["document"].destroyById(conversation.id, (error, result) => {
      if (error) {
        reject(error);
        return;
      }
      resolve();
    });
  });
}

function parseUrl(url: string): { userName: string; tweetId: string } {
  const u = Url.parse(url);
  return {
    userName: _.trim(u.path, "/")
      .split("/")[0]
      .toLowerCase(),
    tweetId: _.trim(u.path, "/").split("/")[2]
  };
}

async function normalizeUrls(app: any): Promise<void> {
  console.log("Starting repair reports.");
  try {
    const reports = await fetchAllReports(app);
    for (let report of reports) {
      if (
        report.data.conversations.some(c => c.reportId !== report.documentId)
      ) {
        report.data.conversations.forEach(
          c => (c.reportId = report.documentId)
        );
        await new Promise<void>((resolve, reject) => {
          app.models["document"].updateAll(
            { type: 2, userId: report.userId, documentId: report.documentId },
            { data: report.data },
            (error, result) => {
              if (error) {
                reject(error);
                return;
              }
              resolve(result);
            }
          );
        });
      }
      for (let conversation of report.data.conversations) {
        await copyConversationForReport(
          app,
          conversation,
          report.documentId,
          report.userId
        );
      }
    }
    await new Promise((resolve, reject) => {
      app.datasources.cycuritydb.connector.execute(
        `DELETE FROM document WHERE type = 1 AND NOT documentid LIKE '%reportId%';`,
        [],
        (error, result) => {
          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
  } catch (error) {
    console.error("Migration error: ", error);
    process.exit(-1);
  }
  console.log("Fixed all reports' data.");
}

export const app = loopback();

const config = require("./config.json");

let configPath = path.join(__dirname, "./config/.env");
dotEnv.config({ path: configPath });

app.start = (): Server =>
  // start the web server
  app.listen(() => {
    app["emit"]("started");
    const baseUrl = app.get("url").replace(/\/$/, "");
    console.log("Web server listening at: %s", baseUrl);
    if (app.get("loopback-component-explorer")) {
      const explorerPath = app.get("loopback-component-explorer").mountPath;
      console.log("Browse your REST API at %s%s", baseUrl, explorerPath);
    }
    (async () => {
      await autoMigrate(
        "document",
        "documentRelation",
        "globalConfig",
        "profileImage"
      );
      await normalizeUrls(app);
    })();
  });

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, err => {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) app.start();
});
