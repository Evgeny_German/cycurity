import { app } from '../server';

export function autoMigrate(...tables: string[]): Promise<void> {
  return new Promise((resolve, reject) => {
    const ds = app['datasources'].cycuritydb;
    console.log(`Starting migrate to database ${ds.connector.clientConfig.host}`)
    ds.autoupdate(tables, err => {
      if (err) {
        reject(err);
        console.error(err);
        return;
      }
      console.log(`Completed auto migrate of tables: ${tables.join(', ')}.`)
      resolve();
    });
  });
}
