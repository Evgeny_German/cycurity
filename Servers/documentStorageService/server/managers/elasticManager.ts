
import * as request from "superagent";

export class ElasticManager{

  elasticBaseRoute:string="documentStorage"
  elasticAddUpdateRoute="addOrUpdateDocument"


  saveDocument=(document:any)=>{
    const url = `${process.env.elasticServer}/${this.elasticBaseRoute}/${this.elasticAddUpdateRoute}`
    let elasticIndex = this.getElasticIndex(document)
    if(!elasticIndex){
      console.warn(`ElasticManager.saveDocument: type ${document.type} supplied in document is not supported for elastic`)
      return;
    }
    //wrap document data in new document. Extract what is needed
    const data=this.extractDataFromDocument(document);

    const sendData ={
      index:elasticIndex,
      document:data,
      id:document.documentId
    };
    return request
      .post(url)
      .send(sendData)
      .then(res=>{
        return res.body
      }).catch(err=>{
        let errRes = err;
        if(err && err.response && err.response.body){
          errRes = err.response.body;
        }
        console.error(`ElasticManager.saveDocument: calling elastic service returned an error:`,errRes);
      })
  }

  private extractDataFromDocument=(document:any):any=>{
    const data={
      userId:document.userId,
      version:document.version,
      data:document.data
    }
    return data;
  }

  private getElasticIndex=(document:any):string=>{
    switch(document.type){
      case 1:
        return "twits"
      case 3:
        return "profiles"
      default:
        return null;
    }
  }
}
