'use strict';

module.exports = function(documentRelation) {

  documentRelation.byRelationId = (ownerId: number,relationId: number,callback: (error: Error, result: any) => void): void => {
    documentRelation.find({where: {and: [{ownerId: ownerId},{relationId:relationId}]}}, callback);
  };

  documentRelation.detach = (ownerId: number, relationId: number, objectId: number, subjectId: number, callback: (error: Error, result: any) => void): void => {
    documentRelation.getApp((err, app) => {
      if (err) callback(err, null);
      app.datasources.cycuritydb.connector.execute(`DELETE FROM documentrelation WHERE ownerId = $1 AND relationId = $2 AND objectId = $3 AND subjectId = $4;`, [ownerId, relationId, objectId, subjectId] , (error2, result2) => {
        if (error2) callback(error2, null);
        callback(null, {});
      });
    });
  };

  documentRelation.remoteMethod('byRelationId', {
    accepts: [
      {arg: 'ownerId', type: 'number', required: true},
      {arg: 'relationId', type: 'number', required: true}
    ],
    returns: {arg: 'data', type: 'object'},
    http: {path: '/byRelationId', verb: 'get'}
  });

  documentRelation.remoteMethod('detach', {
    accepts: [
      {arg: 'ownerId', type: 'number', required: true, http: ctx => ctx.req.body['ownerId']},
      {arg: 'relationId', type: 'number', required: true, http: ctx => ctx.req.body['relationId']},
      {arg: 'objectId', type: 'number', required: true, http: ctx => ctx.req.body['objectId']},
      {arg: 'subjectId', type: 'number', required: true, http: ctx => ctx.req.body['subjectId']}
    ],
    returns: {arg: 'data', type: 'object'},
    http: {path: '/detach', verb: 'post'}
  });
};
