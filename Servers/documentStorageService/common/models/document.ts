import { TextEncoder, TextDecoder } from "text-encoding";
import { Application } from "loopback";
import { ElasticManager } from "../../server/managers/elasticManager";
import * as superagent from "superagent";
import * as url from "url";
import * as querystring from "querystring";

module.exports = function(Document) {
  // Document.afterRemote('**', (ctx, instance, next) => {
  //   if (ctx.result && ctx.result.data) {
  //     if (ctx.result.data.length) {
  //       ctx.result.data.forEach(document => {
  //         if (typeof(document.data) === 'string') {
  //           document.data = JSON.parse(document.data);
  //         }
  //       })
  //     } else {
  //       if (typeof(ctx.result.data.data) === 'string') {
  //         ctx.result.data.data = JSON.parse(ctx.result.data.data);
  //       }
  //     }
  //   }
  //   next();
  // });

  Document.observe("after save", (ctx, next) => {
    if (ctx.instance) {
      new ElasticManager().saveDocument(ctx.instance);
    }
    next();
  });

  Document.purge = (
    userId: number,
    ownerId: number,
    documentId: string,
    type: number,
    callback: (error: Error, result: any) => void
  ): void => {
    (async () => {
      let user: any;
      try {
        user = (await superagent
          .get(`${process.env.USER_MANAGEMENT_URL}/api/cy_users/${userId}`)
          .set({ user_id: userId })).body;
      } catch (error) {
        const newError = new Error("Unauthorized.");
        newError["status"] = 401;
        callback(newError, null);
        return;
      }
      let app: any;
      try {
        app = await new Promise((resolve, reject) => {
          Document.getApp((err, app) => {
            if (err) {
              reject(err);
              return;
            }
            resolve(app);
          });
        });
      } catch (error) {
        const newError = new Error("Unauthorized.");
        newError["status"] = 401;
        callback(newError, null);
        return;
      }
      if (
        user.adminRole !== "admin" &&
        user.adminRole !== "superadmin" &&
        !(user.meta && user.meta.modules && user.meta.modules["reports"])
      ) {
        const newError = new Error("Unauthorized.");
        newError["status"] = 401;
        callback(newError, null);
        return;
      }
      if (type === 2) {
        let instance;
        try {
          instance = await new Promise<any>((resolve, reject) => {
            Document.findOne(
              {
                where: {
                  and: [
                    { userId: ownerId },
                    { documentId: documentId },
                    { type: type }
                  ]
                }
              },
              (error, result) => {
                if (error) {
                  reject(error);
                  return;
                }
                resolve(result);
              }
            );
          });
        } catch (error) {
          const newError = new Error();
          newError["status"] = 403;
          callback(newError, null);
          return;
        }
        if (instance.data.projectId && instance.data.projectId !== "0") {
          let projects: any;
          try {
            projects = await getAllowedProjects(userId);
          } catch (error) {
            const newError = new Error();
            newError["status"] = 403;
            callback(newError, null);
            return;
          }
          if (projects.editor.some(i => i.id == instance.data.projectId)) {
            Document.destroyAll(
              {
                and: [
                  { userId: ownerId },
                  { documentId: documentId },
                  { type: type }
                ]
              },
              callback
            );
          } else {
            const newError = new Error();
            newError["status"] = 403;
            callback(newError, null);
            return;
          }
        } else {
          if (userId !== ownerId && user.adminRole !== "superadmin") {
            const newError = new Error();
            newError["status"] = 403;
            callback(newError, null);
            return;
          }
          Document.destroyAll(
            {
              and: [
                { userId: ownerId },
                { documentId: documentId },
                { type: type }
              ]
            },
            callback
          );
        }
      } else {
        Document.destroyAll(
          {
            and: [
              { userId: ownerId },
              { documentId: documentId },
              { type: type }
            ]
          },
          callback
        );
      }
    })();
  };

  // SELECT * FROM document d WHERE type = 2 AND (SELECT COUNT(*) FROM json_array_elements(d.data -> 'conversations') e WHERE e ->> 'url' ILIKE 'https://twitter.com/Frantacat/status/937822384320589824') > 0;

  Document.updateReportsPerConversation = (
    userId: number,
    ownerId: number,
    data: any,
    callback: (error: Error, result: any) => void
  ): void => {
    (async () => {
      let res: any;
      try {
        const app = await new Promise<any>((resolve, reject) => {
          Document.getApp((error, result) => {
            if (error) {
              reject(error);
              return;
            }
            resolve(result);
          });
        });
        let reports = await new Promise<any>((resolve, reject) => {
          app.datasources.cycuritydb.connector.execute(
            `SELECT * FROM document d WHERE type = $1 AND userid = $2 AND (SELECT COUNT(*) FROM json_array_elements(d.data -> 'conversations') e WHERE e ->> 'tweetId' ILIKE $3) > 0;`,
            [2, ownerId, data.tweetId],
            (error, result) => {
              if (error) {
                reject(error);
                return;
              }
              resolve(result);
            }
          );
        });
        let user: any;
        try {
          user = (await superagent
            .get(`${process.env.USER_MANAGEMENT_URL}/api/cy_users/${userId}`)
            .set({ user_id: userId })).body;
        } catch (error) {
          const newError = new Error("Unauthorized.");
          newError["status"] = 401;
          callback(newError, null);
          return;
        }
        if (
          user.adminRole !== "admin" &&
          user.adminRole !== "superadmin" &&
          !(user.meta && user.meta.modules && user.meta.modules["reports"])
        ) {
          const newError = new Error("Unauthorized.");
          newError["status"] = 401;
          callback(newError, null);
          return;
        }
        const reportsInProjects = reports.filter(
          report => report.data.projectId && report.data.projectId !== "0"
        );
        let projects: any;
        try {
          projects = await getAllowedProjects(userId);
        } catch (error) {
          const newError = new Error();
          newError["status"] = 403;
          callback(newError, null);
          return;
        }
        if (user.adminRole !== "superadmin") {
          reports = reports.filter(r => {
            return (
              !r.data.projectId ||
              r.data.projectId === "0" ||
              projects.editor.find(p => p.id === r.data.projectId)
            );
          });
        }
        await Promise.all(
          reports.map(async report => {
            report.data.conversations.forEach(conv => {
              if (data.tweetId === conv.tweetId) {
                conv.avatar = data.avatar;
                conv.fullName = data.fullName;
                conv.userName = data.userName;
                conv.tweetText = data.tweetText;
                conv.tweetId = data.tweetId;
              }
            });
            await new Promise((resolve, reject) => {
              Document.updateAll(
                { id: report.id },
                { data: report.data, datab: report.data },
                (error, result) => {
                  if (error) {
                    reject(error);
                    return;
                  }
                  resolve();
                }
              );
            });
          })
        );
        res = reports.map(r => r.data);
      } catch (error) {
        callback(error, null);
        return;
      }
      callback(null, res);
    })();
  };

  Document.getLatest = (
    userId: number,
    ownerId: number,
    documentId: string,
    type: number,
    callback: (error: Error, result: any) => void
  ): void => {
    Document.getApp((err, app) => {
      app.datasources.cycuritydb.connector.execute(
        `SELECT *, document.id as id1 FROM document
                                                      LEFT JOIN documentrelation ON
                                                        userId = ownerId AND
                                                         (document.id = objectId OR
                                                            document.id = subjectId)
                                                      WHERE
                                                        documentId = $1 AND
                                                        type = $2 AND
                                                        userId = $3 AND
                                                        version = (SELECT MAX(version) FROM document WHERE userId = $4 AND documentId = $5);`,
        [documentId, type, ownerId, ownerId, documentId],
        type === 2
          ? (error2: Error, result2: any) => {
              if (error2) {
                callback(error2, null);
                return;
              }
              if (result2.length && result2[0].data) {
                (async () => {
                  let user: any;
                  try {
                    user = (await superagent
                      .get(
                        `${
                          process.env.USER_MANAGEMENT_URL
                        }/api/cy_users/${userId}`
                      )
                      .set({ user_id: userId })).body;
                  } catch (error) {
                    const newError = new Error("Unauthorized.");
                    newError["status"] = 401;
                    callback(newError, null);
                    return;
                  }
                  if (
                    user.adminRole !== "admin" &&
                    user.adminRole !== "superadmin" &&
                    !(
                      user.meta &&
                      user.meta.modules &&
                      user.meta.modules["reports"]
                    )
                  ) {
                    const newError = new Error("Unauthorized.");
                    newError["status"] = 401;
                    callback(newError, null);
                    return;
                  }
                  if (
                    !result2[0].data.projectId ||
                    result2[0].data.projectId === "0"
                  ) {
                    callback(null, result2[0]);
                    return;
                  }
                  const reportsInProjects = result2.filter(
                    report =>
                      report.data.projectId && report.data.projectId !== "0"
                  );
                  let projects: any;
                  try {
                    projects = await getAllowedProjects(userId);
                  } catch (error) {
                    const newError = new Error();
                    newError["status"] = 403;
                    callback(newError, null);
                    return;
                  }
                  const isOwner = userId === ownerId;
                  const isAdminOrSuperadmin = user.adminRole === "superadmin" || user.adminRole === "admin";
                  const isProjectMember =
                    projects.editor.some(
                      p => p.id == result2[0].data.projectId
                    ) ||
                    projects.member.some(
                      p => p.id == result2[0].data.projectId
                    );

                  if (!isAdminOrSuperadmin && !isProjectMember) {
                    const newError = new Error();
                    newError["status"] = 403;
                    callback(newError, null);
                    return;
                  }
                  callback(null, result2[0]);
                })();
                return;
              }
              callback(null, result2[0]);
            }
          : (error2: Error, result2: any) => {
              if (error2 || !result2) {
                callback(error2, null);
                return;
              }
              try {
                if (result2 && result2.length)
                  callback(
                    null,
                    result2.reduce(
                      (sum, value) => {
                        if (value.subjectId === value.id1) {
                          if (sum.relations[value.relationId])
                            sum.relation[value.relationId].object.push(
                              value.objectId
                            );
                          else
                            sum.relations[value.relationId] = {
                              object: [value.objectId],
                              subject: []
                            };
                          return sum;
                        }
                        if (value.objectId === value.id1) {
                          if (sum.relations[value.relationId])
                            sum.relations[value.relationId].subject.push(
                              value.subjectId
                            );
                          else
                            sum.relations[value.relationId] = {
                              object: [],
                              subject: [value.subjectId]
                            };
                        }
                        return sum;
                      },
                      {
                        id: result2[0].id,
                        documentId: documentId,
                        userId: userId,
                        type: type,
                        data: result2[0].data,
                        datab: result2[0].datab,
                        version: result2[0].version,
                        relations: {}
                      }
                    )
                  );
                else callback(null, null);
              } catch (error3) {
                callback(error3, null);
              }
            }
      );
    });
  };

  Document.saveLatest = (
    userId: number,
    ownerId: number,
    documentId: string,
    type: number,
    data: any,
    callback: (error: Error, result: any) => void
  ): void => {
    (async () => {
      let user: any;
      try {
        user = (await superagent
          .get(`${process.env.USER_MANAGEMENT_URL}/api/cy_users/${userId}`)
          .set({ user_id: userId })).body;
      } catch (error) {
        const newError = new Error("Unauthorized.");
        newError["status"] = 401;
        callback(newError, null);
        return;
      }
      let app: any;
      try {
        app = await new Promise((resolve, reject) => {
          Document.getApp((err, app) => {
            if (err) {
              reject(err);
              return;
            }
            resolve(app);
          });
        });
      } catch (error) {
        const newError = new Error("Unauthorized.");
        newError["status"] = 401;
        callback(newError, null);
        return;
      }
      if (
        (type === 1 || type === 2) &&
        user.adminRole !== "superadmin" &&
        !(user.meta && user.meta.modules && user.meta.modules["reports"])
      ) {
        const newError = new Error("Unauthorized.");
        newError["status"] = 401;
        callback(newError, null);
        return;
      }
      let existing: any;
      if (type === 1) {
        const Url = url.parse(documentId);
        const reportId = querystring.parse(Url.query.toString())["reportId"];
        try {
          existing = await new Promise((resolve, reject) => {
            Document.find(
              {
                where: { userId: ownerId, documentId: reportId, type: 2 }
              },
              (error, result) => {
                if (error) {
                  reject(error);
                  return;
                }
                resolve(result);
              }
            );
          });
        } catch (error) {}
        let result: any;
        if (existing && existing.length) {
          if (
            existing[0].data.projectId &&
            existing[0].data.projectId !== "0"
          ) {
            let projects: any;
            try {
              projects = await getAllowedProjects(userId);
            } catch (error) {
              callback(error, null);
            }

            const isOwner = userId === ownerId;
            const isSuperadmin = user.adminRole === "superadmin";
            const isAdminWithReports =
              user.adminRole === "admin" &&
              user.meta &&
              user.meta.modules &&
              user.meta.modules["reports"];
            const isProjectEditor = projects.editor.some(
              p => p.id == existing[0].data.projectId
            );
            if (
              !isSuperadmin &&
              !isAdminWithReports &&
              !isProjectEditor
            ) {
              const newError = new Error("Unauthorized.");
              newError["status"] = 401;
              callback(newError, null);
              return;
            }
          } else {
            const isOwner = userId === ownerId;
            const isSuperadmin = user.adminRole === "superadmin";
            const isAdminWithReports =
              user.adminRole === "admin" &&
              user.meta &&
              user.meta.modules &&
              user.meta.modules["reports"];
            if (!isSuperadmin && !isAdminWithReports && !isOwner) {
              const newError = new Error("Unauthorized.");
              newError["status"] = 401;
              callback(newError, null);
              return;
            }
          }
        }
      } else if (type === 2) {
        try {
          existing = await new Promise((resolve, reject) => {
            Document.find(
              {
                where: { userId: ownerId, documentId: documentId, type: type }
              },
              (error, result) => {
                if (error) {
                  reject(error);
                  return;
                }
                resolve(result);
              }
            );
          });
        } catch (error) {}
        if (existing && existing.length) {
          if (
            existing[0].data.projectId &&
            existing[0].data.projectId !== "0"
          ) {
            let projects: any;
            try {
              projects = await getAllowedProjects(userId);
            } catch (error) {
              callback(error, null);
            }

            const isOwner = userId === ownerId;
            const isSuperadmin = user.adminRole === "superadmin";
            const isAdminWithReports =
              user.adminRole === "admin" &&
              user.meta &&
              user.meta.modules &&
              user.meta.modules["reports"];
            const isProjectEditor = projects.editor.some(
              p => p.id == existing[0].data.projectId
            );
            if (
              !isSuperadmin &&
              !isAdminWithReports &&
              !isProjectEditor
            ) {
              const newError = new Error("Unauthorized.");
              newError["status"] = 401;
              callback(newError, null);
              return;
            }
          } else {
            const isOwner = userId === ownerId;
            const isSuperadmin = user.adminRole === "superadmin";
            const isAdminWithReports =
              user.adminRole === "admin" &&
              user.meta &&
              user.meta.modules &&
              user.meta.modules["reports"];
            if (!isSuperadmin && !isAdminWithReports && !isOwner) {
              const newError = new Error("Unauthorized.");
              newError["status"] = 401;
              callback(newError, null);
              return;
            }
          }
        }
      }
      if (type !== 2) {
        try {
          existing = await new Promise((resolve, reject) => {
            Document.find(
              {
                where: { userId: ownerId, documentId: documentId, type: type }
              },
              (error, result) => {
                if (error) {
                  reject(error);
                  return;
                }
                resolve(result);
              }
            );
          });
        } catch (error) {}
      }
      let result: any;
      if (existing && existing.length) {
        try {
          await new Promise((resolve, reject) => {
            Document.updateAll(
              { documentId: documentId, type: type, userId: ownerId },
              {
                data: data,
                datab: data
              },
              (error, res) => {
                if (error) {
                  reject(error);
                  return;
                }
                resolve(res);
              }
            );
          });
          result = await new Promise((resolve, reject) => {
            Document.getLatest(
              userId,
              ownerId,
              documentId,
              type,
              (err, res) => {
                if (err) {
                  reject(err);
                  return;
                }
                resolve(res);
              }
            );
          });
        } catch (error) {
          callback(error, null);
          return;
        }
        callback(null, result);
        return;
      } else {
        try {
          result = await new Promise((resolve, reject) => {
            Document.create(
              {
                documentId: documentId,
                type: type,
                version: 1,
                userId: ownerId,
                data: data,
                datab: data
              },
              (error, res) => {
                if (error) {
                  reject(error);
                  return;
                }
                resolve(res);
              }
            );
          });
        } catch (error) {
          callback(error, null);
          return;
        }
        callback(null, result);
        return;
      }
    })();
  };

  async function getAllowedProjects(
    userId: number
  ): Promise<{ member: { id: number }[]; editor: { id: number }[] }> {
    return (await superagent
      .get(
        `${process.env.PROJECT_MANAGEMENT_URL}/api/projects/getSharedProjects`
      )
      .set({ user_id: userId })).body;
  }

  async function getProjectsWithUsers(
    projectIds: number[],
    userId: number
  ): Promise<any[]> {
    return (await superagent
      .get(
        `${
          process.env.PROJECT_MANAGEMENT_URL
        }/api/projects?filter=${encodeURIComponent(
          JSON.stringify({
            where: { id: { inq: projectIds } },
            include: "projectUser"
          })
        )}`
      )
      .set({ user_id: userId })).body;
  }

  Document.search = (
    userId: number,
    type: number,
    freeText: string,
    callback: (error: Error, result: any) => void
  ): void => {
    (async () => {
      let user: any;
      try {
        user = (await superagent
          .get(`${process.env.USER_MANAGEMENT_URL}/api/cy_users/${userId}`)
          .set({ user_id: userId })).body;
      } catch (error) {
        const newError = new Error("Unauthorized.");
        newError["status"] = 401;
        callback(newError, null);
        return;
      }
      let app: any;
      try {
        app = await new Promise((resolve, reject) => {
          Document.getApp((err, app) => {
            if (err) {
              reject(err);
              return;
            }
            resolve(app);
          });
        });
      } catch (error) {
        const newError = new Error("Unauthorized.");
        newError["status"] = 401;
        callback(newError, null);
        return;
      }
      let result: any;

      try {
        if (user.adminRole === "superadmin" || user.adminRole === "admin") {
          result = await new Promise((resolve, reject) => {
            app.datasources.cycuritydb.connector.execute(
              freeText
                ? `SELECT d1.* FROM document AS d1
                                                   WHERE d1.type = $1
                                                   AND version = (SELECT MAX(version) FROM document WHERE type = d1.type AND userId = d1.userId AND documentId = d1.documentId)
                                                   AND data::text LIKE $2`
                : `SELECT d1.* FROM document AS d1
                                                   WHERE d1.type = $1
                                                   AND version = (SELECT MAX(version) FROM document WHERE type = d1.type AND userId = d1.userId AND documentId = d1.documentId)`,
              freeText ? [type, `%${freeText}%`] : [type],
              (err, res) => {
                if (err) {
                  reject(err);
                  return;
                }
                resolve(
                  res.map(obj => ({
                    id: obj.id,
                    type: obj.type,
                    version: obj.version,
                    userId: obj.userid,
                    documentId: obj.documentid,
                    data: obj.data
                  }))
                );
              }
            );
          });
        } else {
          const projects = await getAllowedProjects(userId);
          result = await new Promise((resolve, reject) => {
            app.datasources.cycuritydb.connector.execute(
              freeText
                ? `SELECT * FROM document
                                        WHERE type = $1
                                          AND data::text LIKE $2
                                          AND (((datab ? 'projectId')
                                              AND (datab ? 'projectId')
                                              AND NOT (datab ->> 'projectId') IS NULL
                                              AND ((datab ->> 'projectId')::int) = ANY($3)));`
                : `SELECT * FROM document
                                        WHERE type = $1
                                          AND (
                                            (datab ? 'projectId') AND (datab ? 'projectId') AND NOT (datab ->> 'projectId') IS NULL AND ((datab ->> 'projectId')::int) = ANY($2)
                                            OR
                                            (userId = $3)
                                          );`,
              freeText
                ? [
                    type,
                    `%${freeText}%`,
                    projects.editor
                      .map(p => p.id)
                      .concat(projects.member.map(p => p.id))
                  ]
                : [
                    type,
                    projects.editor
                      .map(p => p.id)
                      .concat(projects.member.map(p => p.id)),
                    userId
                  ],
              (err, res) => {
                if (err) {
                  reject(err);
                  return;
                }
                resolve(
                  res.map(obj => ({
                    id: obj.id,
                    type: obj.type,
                    version: obj.version,
                    userId: obj.userid,
                    documentId: obj.documentid,
                    data: obj.data
                  }))
                );
              }
            );
          });
        }
      } catch (error) {
        const newError = new Error("Unauthorized.");
        newError["status"] = 401;
        callback(newError, null);
        return;
      }
      callback(null, result);
    })();
  };

  Document.remoteMethod("getLatest", {
    accepts: [
      { arg: "userId", type: "number", required: true },
      {
        arg: "ownerId",
        type: "number",
        required: true
      },
      { arg: "documentId", type: "string", required: true },
      { arg: "type", type: "number", required: true }
    ],
    returns: [{ arg: "data", type: "object" }],
    http: { path: "/getLatest", verb: "get" }
  });

  Document.remoteMethod("updateReportsPerConversation", {
    accepts: [
      {
        arg: "userId",
        type: "number",
        required: true,
        http: ctx => ctx.req.body["userId"]
      },
      {
        arg: "ownerId",
        type: "number",
        required: true,
        http: ctx => ctx.req.body["ownerId"]
      },
      {
        arg: "data",
        type: "object",
        required: true,
        http: ctx => ctx.req.body["data"]
      }
    ],
    returns: [{ root: true, type: "array" }],
    http: { path: "/updateReportsPerConversation", verb: "post" }
  });

  Document.remoteMethod("saveLatest", {
    accepts: [
      {
        arg: "userId",
        type: "number",
        required: true,
        http: ctx => ctx.req.body["userId"]
      },
      {
        arg: "ownerId",
        type: "number",
        required: true,
        http: ctx => ctx.req.body["ownerId"]
      },
      {
        arg: "documentId",
        type: "string",
        required: true,
        http: ctx => ctx.req.body["documentId"]
      },
      {
        arg: "type",
        type: "number",
        required: true,
        http: ctx => ctx.req.body["type"]
      },
      {
        arg: "data",
        type: "object",
        required: true,
        http: ctx => ctx.req.body["data"]
      }
    ],
    returns: [{ arg: "data", type: "object" }],
    http: { path: "/saveLatest", verb: "post" }
  });

  Document.remoteMethod("search", {
    accepts: [
      { arg: "userId", type: "number", required: true },
      { arg: "type", type: "number", required: true },
      { arg: "freeText", type: "string", required: false }
    ],
    returns: [{ arg: "data", type: "object" }],
    http: { path: "/search", verb: "get" }
  });

  Document.remoteMethod("purge", {
    accepts: [
      { arg: "userId", type: "number", required: true },
      {
        arg: "ownerId",
        type: "number",
        required: true
      },
      { arg: "documentId", type: "string", required: true },
      { arg: "type", type: "number", required: true }
    ],
    returns: [{ arg: "data", type: "object" }],
    http: { path: "/purge", verb: "delete" }
  });
};
