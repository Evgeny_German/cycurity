module.exports = function(ProfileImage) {
  ProfileImage.get = (
    username: string,
    callback: (error: Error, result: any) => void
  ) => {
    ProfileImage.findOne({ where: { username } }, (error, result) => {
      if (error) {
        callback(error, null);
        return;
      }
      callback(null, result ? result.url : null);
    });
  };
  ProfileImage.set = (
    data: { username: string; url: string }[],
    callback: (error: Error, result: any) => void
  ) => {
    (async () => {
      let results;
      try {
        results = await Promise.all(
          data.map(
            async image =>
              await new Promise((resolve, reject) => {
                ProfileImage.destroyAll(
                  { username: image.username },
                  (error, result) => {
                    if (error) {
                      reject(error);
                      return;
                    }
                    ProfileImage.create(
                      { username: image.username, url: image.url },
                      (error, result) => {
                        if (error) {
                          reject(error);
                          return;
                        }
                        resolve(result);
                      }
                    );
                  }
                );
              })
          )
        );
      } catch (error) {
        callback(error, null);
        return;
      }
      callback(null, results);
    })();
  };

  ProfileImage.remoteMethod("get", {
    accepts: [
      {
        arg: "username",
        type: "string",
        required: true,
        http: { source: "query" }
      }
    ],
    returns: {
      root: true,
      type: "any"
    },
    http: { verb: "get" }
  });
  ProfileImage.remoteMethod("set", {
    accepts: [
      {
        arg: "data",
        type: "array",
        required: true,
        http: { source: "body" }
      }
    ],
    returns: {
      root: true,
      type: "any"
    },
    http: { verb: "post" }
  });
};
