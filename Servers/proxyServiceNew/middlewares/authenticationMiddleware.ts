import {BearerManager} from "../bearerManager";

export class AuthenticationMiddleware {
    authenticateCall = (req, res, next) => {
        this.authenticateRequest(req.headers).then(authRes => {
            if (authRes) {
                next();
            }
            else {
                res.status(401).send("Unauthorized");
            }
        });
    }

    private authenticateRequest = (headers): Promise<boolean> => {
        return new Promise<boolean>((resolve, reject) => {
            new BearerManager().authenticateCall(headers).then(res => {
                resolve(res.isAuthorized)
            }).catch(err => {
                console.error(err);
                resolve(false);
            })
        })
    }
}