import {config} from "dotenv";
import * as path from "path"
import {displayEnv} from "dotenv-display";

let configPath = path.join(__dirname, "./config/.env")
let env = config({path: configPath});
displayEnv(env.parsed);

import {ProxyServer} from "./proxyServer";


let server = new ProxyServer();
