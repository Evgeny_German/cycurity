import * as http from "http";
import * as httpProxy from "http-proxy";
import * as express from "express";
import {BearerManager} from "./bearerManager";
import {AuthenticationMiddleware} from "./middlewares/authenticationMiddleware";
import {AuthenticationMock} from "./middlewares/authenticationMock";
import * as net from "net";

export class ProxyServer {

    proxy: httpProxy = null;
    server: http.Server = null
    private app:express.Express;
    requireAuth = true;
    private port:any=3000;
    private lastConnectTime = Date.now();

    constructor() {
        this.app = express();
        this.port = process.env.PROXY_PORT || this.port;
        this.requireAuth = this.requireAuthenticate();
        this.createProxyServer();
        this.registerToProxyEvents();
        this.setRoutes();
        this.startServer();
    }

    createProxyServer = () => {
        this.proxy = httpProxy.createProxyServer();
    }

    public startServer=()=>{
        var httpServer = http.createServer(this.app);
        httpServer.listen(this.port);
        httpServer.on("connect",this.handleHttpsRequests);
        httpServer.on('error',this.onError);
        httpServer.on('listening',this.onServerListen);
    }



    public setRoutes=()=>{
        this.app.get("/lastConnect",this.lastConnect)
        this.app.use("/",this.getAuthenticationMethod(),this.handleRequests);
    }

    private lastConnect=(req,res)=>{
        res.status(200).send({lastConnect:this.lastConnectTime})
    }

    private onServerListen=()=>{
        console.log('App listening on port ' + this.port);
        console.log("you are running in " + process.env.NODE_ENV + " mode.");
    }

    onError=(err:any)=>{
        switch (err.code) {
            case 'EACCES':
                console.error('port requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error('port is already in use');
                process.exit(1);
                break;
            default:
                throw err;
        }
    }

    registerToProxyEvents = () => {
        this.proxy.on("error", (err, req, res) => {
            res.writeHead(500, {
                "Content-Type": "text/plain"
            });
            res.end("Something went wrong. And we are reporting a custom error message.");
        });
    }

    handleRequests = (req, res) => {
        console.log(`Proxy request recieved. Url: ${req.url}`)
        this.lastConnectTime = Date.now();
        this.proxy.web(req, res, {target: req.url,changeOrigin:true});
    }

    handleHttpsRequests=(request, socket)=>{
        this.lastConnectTime = Date.now();
        let domainAndPort = request.url.split(":");
        let conn = net.connect(domainAndPort[1], domainAndPort[0], () => {
            try {
                // respond to the client that the connection was made
                socket.write("HTTP/1.1 200 OK\r\n\r\n");
                // create a tunnel between the two hosts
                socket.pipe(conn);
                conn.pipe(socket);
            } catch (err) {
                console.warn("Error in handleHttpsRequests: ",err)
            }
        });
    }

    authenticateCall = (headers): Promise<boolean> => {
        return new Promise<boolean>((resolve, reject) => {
            if (this.requireAuth) {
                new BearerManager().authenticateCall(headers).then(res => {
                    resolve(res.isAuthorized)
                }).catch(err => {
                    console.error(err);
                    resolve(false);
                })
            } else {
                resolve(true)
            }
        })
    }

    getAuthenticationMethod=()=>{
        if(this.requireAuth){
            return new AuthenticationMiddleware().authenticateCall
        }else{
            return new AuthenticationMock().authenticateCall
        }
    }

    requireAuthenticate = () => {
        if (process.env.REQUIRE_AUTHENTICATE_CALL == "1")
            return true
        else
            return false
    }
}