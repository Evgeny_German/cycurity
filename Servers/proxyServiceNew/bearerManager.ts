
import * as jwtToken from "jsonwebtoken";

export class BearerManager{

    public authenticateCall = (headers: [any]): Promise<AuthenticationResult> => {
        return new Promise<AuthenticationResult>((resolve, reject) => {
            var bearerToken;
            var bearerHeader = headers["authorization"];
            let result = new AuthenticationResult();
            if (typeof bearerHeader !== 'undefined') {
                var bearer = bearerHeader.split(" ");
                bearerToken = bearer[1];
                result.token = bearerToken;
                //get key and decode message

                this.decodeToken(bearerToken)
                    .then(decode => {
                        result.decodedToken = decode;
                        result.isAuthorized=true;
                        resolve(result)
                    })
                    .catch((err) => {
                        if (err.name == "TokenExpiredError") {
                            console.error("User is trying to access api with an expired token");
                        }
                        result.isAuthorized = false;
                        resolve(result);
                    })

            } else {
                result.isAuthorized = false;
                resolve(result);
            }
        });

    }

    private decodeToken = (bearerToken: string): Promise<any> => {
        return new Promise<{ userId: number }>((resolve, reject) => {
            jwtToken.verify(bearerToken, process.env.jwt_token_secret, (err, decoded: any) => {
                if (err) {
                    if (err.name == "TokenExpiredError") {
                        console.error("User is trying to access api with an expired token");
                    }
                    reject(err);
                }
                else {
                    resolve(decoded.data);
                }
            });
        });
    }

}

export class AuthenticationResult{
    isAuthorized:boolean;
    token:string;
    decodedToken:any;

}