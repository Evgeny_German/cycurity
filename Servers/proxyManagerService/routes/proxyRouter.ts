import {Router} from "express";
import * as express from "express";
import {loopback} from "loopback";
import {ProxyManager} from "../managers/proxyManager";
import {ProxyTester} from "../managers/proxyTester";

export class ProxyRouter {

  router: Router;

  constructor(private lb: any) {
    this.router = express.Router();
    this.createRoutes();
  }

  private createRoutes() {
    this.router.post("/setActiveImage", this.setActiveImage);
    this.router.post("/updateProviderRegions", this.updateProviderRegions);
    this.router.post("/isRegionExist", this.isRegionExist);
    this.router.get("/getSupportedRegions", this.getSupportedRegions);
    this.router.post("/getOrCreateKey", this.getOrCreateKey);
    this.router.post("/createInstance", this.createInstance);
    this.router.post("/getProxyManageList", this.getProxyManageList);
    this.router.get("/getImages", this.getImages);
    this.router.get("/getKeys", this.getKeys);
    this.router.post("/terminateInstance", this.terminateInstance);
    this.router.post("/copyImage", this.copyImage);
    this.router.post("/getImageStatus", this.getImageStatus);
    this.router.post("/getInstanceListFromProvider", this.getInstanceListFromProvider);4
    this.router.post("/testProxy",this.testProxy);
    this.router.post("/getLastConnectionTime",this.getLastConnectionTime);
  }

  setActiveImage = async (req, res, next) => {
    let imageName =req.body.imageName || null;
    let region =req.body.region || null;
    let provider =req.body.provider || null;
    if(!imageName || !region || !provider ){
      res.status(400).send("image name, region and provider are mandatory")
    }else{
      new ProxyManager(this.lb).setActiveImage(imageName,region,provider).then(settingsRes=>{
        res.status(200).send(settingsRes);
      }).catch(err=>{
        res.status(500).send(err.message)
      })
    }
  }

  updateProviderRegions=(req,res)=>{
    let provider =req.body.provider || null;
    if(!provider){
      res.status(400).send("provider is mandatory")
    }
    new ProxyManager(this.lb).setProviderRegions(provider).then(regsionsRes=>{
      res.status(200).send("Regions have been set");
    }).catch(err=>{
      res.status(500).send(err.message)
    })

  }

  isRegionExist=(req,res)=>{
    let provider =req.body.provider || null;
    let region = req.body.region || null;
    if(!provider || !region){
      res.status(400).send("provider and region are mandatory")
    }else{
      new ProxyManager(this.lb).isRegionExist(provider,region).then(result=>{
        res.status(200).send(result);
      }).catch(err=>{
        res.status(500).send(err.message)
      })
    }
  }

  getSupportedRegions=(req,res)=>{
    new ProxyManager(this.lb).getSupportedRegions().then(result=>{
      res.status(200).send(result);
    }).catch(err=>{
      res.status(500).send(err.message)
    })
  }

  getOrCreateKey=(req,res)=>{
    let keyName=req.body.keyName || null;
    let region=req.body.region || null;
    let provider=req.body.provider || null;
    if(!keyName || !region || !provider){
      res.status(400).send("keyName, provider and region are mandatory")
    }else{
      new ProxyManager(this.lb).getOrCreateKey(keyName,region, provider).then(result=>{
        res.status(200).send(result);
      }).catch(err=>{
        res.status(500).send(err.message)
      })
    }
  }

  createInstance=(req,res)=>{
    let region=req.body.region || null;
    let provider=req.body.provider || null;
    let instanceName = req.body.instanceName || process.env.DEFAULT_INSTANCE_NAME;
    let keyName=req.body.keyName || process.env.DEFAULT_PROXY_KEY_PAIR_NAME;
    let openPort = req.body.openPort || process.env.DEFAULT_PROXY_SERVICE_PORT;
    let userId = req.body.userId || null;
    if(!region || !provider || !instanceName  ){
      res.status(400).send("provider, region and userId are mandatory. instanceName, openPort and keyName are optional")
    }else{
      new ProxyManager(this.lb).createInstance(region, provider,instanceName,keyName,openPort,userId).then(result=>{
        res.status(200).send(result);
      }).catch(err=>{
        res.status(500).send(err.message)
      })
    }
  }

  getProxyManageList=(req,res)=>{
    let limit=req.body.limit || 100;
    let onlyRunning = req.body.onlyRunning || false
    new ProxyManager(this.lb).getProxyManageList(+limit,onlyRunning).then(result=>{
      res.status(200).send(result);
    }).catch(err=>{
      res.status(500).send(err.message)
    })
  }

  getImages=(req,res)=>{
    new ProxyManager(this.lb).getImages().then(result=>{
      res.status(200).send(result);
    }).catch(err=>{
      res.status(500).send(err.message)
    })
  }

  getKeys=(req,res)=>{
    new ProxyManager(this.lb).getKeys().then(result=>{
      res.status(200).send(result);
    }).catch(err=>{
      res.status(500).send(err.message)
    })
  }

  terminateInstance=(req,res)=>{
    let region=req.body.region || null;
    let provider=req.body.provider || null;
    let instanceId = req.body.instanceId || null;
    if(!instanceId || !region || !provider ){
      res.status(400).send("instanceId, region and provider are mandatory")
    }else{
      new ProxyManager(this.lb).terminateInstance(region,provider,instanceId).then(result=>{
        res.status(200).send(result);
      }).catch(err=>{
        res.status(500).send(err.message)
      })
    }
  }

  getInstanceListFromProvider=(req,res)=>{
    let region=req.body.region || null;
    let provider=req.body.provider || null;
    let filter = req.body.filter || null;
    if(!region || !provider ){
      res.status(400).send("region and provider are mandatory")
    }else{
      new ProxyManager(this.lb).getInstanceListFromProvider(region,provider,filter).then(result=>{
        res.status(200).send(result);
      }).catch(err=>{
        res.status(500).send(err.message)
      })
    }
  }

  copyImage=(req,res)=>{
    let region=req.body.region || null;
    let provider=req.body.provider || null;
    if(!region || !provider ){
      res.status(400).send("region and provider are mandatory")
    }else{
      new ProxyManager(this.lb).copyImage(region,provider).then(result=>{
        res.status(200).send(result);
      }).catch(err=>{
        res.status(500).send(err.message)
      })
    }
  }

  getImageStatus=(req,res)=>{
    let region=req.body.region || null;
    let provider=req.body.provider || null;
    let imageId = req.body.imageId || null;
    if(!region || !provider ||!imageId ){
      res.status(400).send("region , provider and imageId are mandatory")
    }else{
      new ProxyManager(this.lb).getImageStatus(region,provider,imageId).then(result=>{
        res.status(200).send(result);
      }).catch(err=>{
        res.status(500).send(err.message)
      })
    }
  }

  testProxy=(req,res)=>{
    let url  = req.body.url || null;
    let proxyIp=req.body.proxyIp || null;
    let proxyPort =req.body.proxyPort || null;
    if(!url || !proxyIp || ! proxyPort){
      res.status(400).send("url, proxyIp and proxyPort are mandatory")
    }else{
      new ProxyTester().testProxy(url,proxyIp,proxyPort).then(result=>{
        res.status(200).send(result);
      }).catch(err=>{
        res.status(500).send(err.message)
      })
    }
  }

  getLastConnectionTime=(req,res)=>{
    let region=req.body.region || null;
    let provider=req.body.provider || null;
    let instanceName = req.body.instanceName || null;
    if(!region || !provider || !instanceName ){
      res.status(400).send("region, provider and instanceName are mandatory")
    }else{
      new ProxyManager(this.lb).getLastConnectionTime(region,provider,instanceName).then(result=>{
        res.status(200).send(result);
      }).catch(err=>{
        res.status(500).send(err.message)
      })
    }
  }
}
