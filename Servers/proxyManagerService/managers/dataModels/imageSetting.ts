export type ImageSetting={
  image_name:string,
  image_id:string,
  is_active:boolean,
  create_date:Date,
  region:string,
  provider:string
}
