export type ProxyManagement = {
  id?:number
  region: string,
  provider: string,
  user_id: number,
  startup_time: Date,
  shutdown_time: Date,
  key_pair_name: string,
  state: string,
  image_id: string,
  instance_name: string,
  instance_id:string,
  proxy_port:number,
  public_ip:string,
  is_pending_image: boolean
}
