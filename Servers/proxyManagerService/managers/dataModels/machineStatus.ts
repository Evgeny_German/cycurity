export enum MachineStatus{
    running="running",
    available="available",
    pending="pending",
    terminated = "terminated"
}
