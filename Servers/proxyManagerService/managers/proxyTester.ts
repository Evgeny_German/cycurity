import * as request from "request"


export class ProxyTester{

  testProxy=(url:string,proxyIp:string,proxyPort:number):Promise<{succeeded:boolean,error?:string}>=>{
    return new Promise<{succeeded:boolean,error?:string}>((resolve,reject)=>{
      let proxy =`http://${proxyIp}:${proxyPort}`
      request({url,proxy},  (error, response, body)=> { //52.89.121.33
        if (!error && response && response.statusCode == 200) {
          return resolve({succeeded:true})
        } else {
          if(response){
            return resolve({succeeded:false,error:response.body})
          }else{
            return resolve({succeeded:false,error})
          }

        }
      })
    })
  }
}

