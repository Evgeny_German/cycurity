import {IRepository} from "./dataModels/IRepository";
import {ImageSetting} from "./dataModels/imageSetting";
import {ProxyProviderFactory} from "./proxyProviderFactory";
import {regionMaps} from "../config/regionMaps";
import {IProxyProvider} from "./proxyProviders/IProxyProvider";
import {MachineStatus} from "./dataModels/machineStatus";
import {ProxyManagement} from "./dataModels/proxyManagement";
import * as request from "superagent";

export class ProxyManager {

  constructor(private lb: IRepository) {

  }

  getProxyManageList = async (limit: number, onlyRunning: boolean, region:string=null,provider:string=null): Promise<ProxyManagement[]> => {
    try {
      let filter = <{limit:any,order:any,where?:any}>{limit, order: ["startup_time DESC"]}
      if (onlyRunning) {
        filter.where = {state: MachineStatus.running}
      }
      if(region) {
        if (!filter.where) filter.where = {}
        filter.where.region=region;
      }
      if(provider) {
        if (!filter.where) filter.where = {}
        filter.where.provider=provider;
      }
      let proxiesInstances = await this.lb.models.proxy_management.find(filter)
      if (proxiesInstances.length > 0) {
        let proxies = proxiesInstances.map(proxy => proxy.toJSON())
        return proxies;
      } else {
        return [];
      }
    } catch (err) {
      console.error("Error in getProxyManageList. ", err)
      throw(err);
    }
  }

  getImages = async () => {
    try {
      let imagesInstances = await this.lb.models.image_settings.find()
      if (imagesInstances.length > 0) {
        let images = imagesInstances.map(image => image.toJSON())
        return images;
      } else {
        return [];
      }
    } catch (err) {
      console.error("Error in getImages. ", err)
      throw(err);
    }
  }

  getKeys = async () => {
    try {
      let keyInstances = await this.lb.models.proxy_key.find()
      if (keyInstances.length > 0) {
        let keys = keyInstances.map(key => key.toJSON())
        return keys;
      } else {
        return [];
      }
    } catch (err) {
      console.error("Error in getKeys. ", err)
      throw(err);
    }
  }

  setActiveImage = async (imageName: string, region: string, provider: string): Promise<{ succeeded: boolean, error?: string }> => {
    //validate if image name already exists. If so set the is active else create new and remove the old is active
    try {
      let doesRegionExist = await this.isRegionExist(provider, region)
      if (!doesRegionExist) {
        return {
          succeeded: false,
          error: `Region ${region} does not exist in provider ${provider}. Make sure that the setProviderRegions has been set in database`
        }
      }
      let existingImageSettings = await this.lb.models.image_settings.find({where: {image_name: imageName, provider}})

      if (existingImageSettings.length > 0) {
        await this.setImagesInRegionToInactive(provider);
        await existingImageSettings[0].updateAttributes({is_active: true})
        return {succeeded: true}
      } else {
        //before adding row to db need to get the image id
        let providerApi = new ProxyProviderFactory().getProvider(provider, region)
        let existingImages = await providerApi.getImagesByName(imageName)
        if (<number>existingImages.length == 0) {
          throw new Error("Could not set image to active since image does not exist in cloud. Make sure to create and it and then try again")
        } else if (existingImages.length > 1) {
          throw new Error("Could not set image to active since cloud contains multiple images with this name. Make sure to to delete old images and try again");
        } else {
          let newImage = await this.lb.models.image_settings.create(<ImageSetting>{
            image_name: imageName,
            image_id: existingImages[0].imageId,
            is_active: true,
            create_date: new Date(),
            region,
            provider
          }).then(res => res.body);
          return {succeeded: true}
        }

      }
    } catch (err) {
      console.error("Error in setActiveImage. ", err)
      throw(err);
    }
  }

  setImagesInRegionToInactive = async (provider: string): Promise<any> => {
    try {
      let activeImage = await this.getActiveImageDbInstance(provider);
      if (activeImage) {
        await activeImage.updateAttributes({is_active: false})
        return true
      } else {
        Promise.resolve(true);
      }

    } catch (err) {
      console.error("Error in setOtherImagesToInactive. ", err)
      throw(err);
    }
  }

  //this will return an object from db before toJSON so that it can be modified and saved to db
  getActiveImageDbInstance = async (provider: string): Promise<any> => {
    let existingImageSettings = await this.lb.models.image_settings.find({where: {is_active: true, provider}})
    if (existingImageSettings.length > 0) {
      return existingImageSettings[0]
    } else {
      return null;
    }
  }

  getActiveImage = async (provider: string): Promise<ImageSetting> => {
    let imageDbInstance = await this.getActiveImageDbInstance(provider)
    if (imageDbInstance) {
      return imageDbInstance.toJSON()
    } else {
      return null;
    }

  }

  //Update the list of regions within a provider into the database. Should be done whenever a provider supports more regions
  setProviderRegions = async (provider: string): Promise<any> => {
    try {
      //delete all regions
      let regions = await this.lb.models.proxy_regions.find({where: {provider}})
      for (let region of regions) {
        await region.delete()
      }
      let providerApi = new ProxyProviderFactory().getProvider(provider, null) //get with default region
      let providerRegions = await providerApi.getRegionsList();
      for (let region of providerRegions) {
        //before creating add mapped names if they exist for the region. This is done since aws does not supply display names for regions
        let mappedRegion = regionMaps.find(x => x.provider == provider && x.regionKey == region.regionName)
        if (mappedRegion) {
          region.displayName = mappedRegion.displayName
        }
        await this.lb.models.proxy_regions.create({
          provider,
          region_key: region.regionName,
          display_name: region.displayName
        })
      }
    } catch (err) {
      console.error("setProviderRegions error. ", err)
      throw err;
    }
  }

  getSupportedRegions = async () => {
    try {
      let regions = await this.lb.models.proxy_regions.find();
      return regions.map(region => region.toJSON())
    } catch (err) {
      console.error("getSupportedRegions error. ", err)
      throw err;
    }
  }

  isRegionExist = async (provider: string, regionName: string): Promise<boolean> => {
    try {
      let regions = await this.lb.models.proxy_regions.find({where: {provider, region_key: regionName}})
      if (regions.length > 0) {
        return true;
      } else {
        return false;
      }
    } catch (err) {
      console.error("isRegionSupported error. ", err)
      throw err;
    }
  }

  getOrCreateKey = async (keyName: string, region: string, provider: string): Promise<{ keyName?: string, error?: string }> => {
    try {
      let doesRegionExist = await this.isRegionExist(provider, region)
      if (!doesRegionExist) {
        return {error: `Region ${region} does not exist in provider ${provider}. Make sure that the setProviderRegions has been set in database`}
      }
      let providerApi = new ProxyProviderFactory().getProvider(provider, region) //get with default region
      let keyResult = await providerApi.createKeyPairIfNotExist(keyName)
      //if key does not already exist - save it in db
      if (!keyResult.keyAlreadyExists) {
        await this.lb.models.proxy_key.create({key_name: keyName, pem_data: keyResult.pemData, region, provider})
      }
      return {keyName}
    } catch (err) {
      console.error("getOrCreateKey error: ", err)
      throw err;
    }

  }

  //get the list of instances on a region directly from a provider and not from local db
  getInstanceListFromProvider=async (region: string, provider: string,filter?:{name:string,status:string}):Promise<[{instancenName:string,instanceId: string,instanceType: string,status: string, imageId: string, keyName: string, publicIp: string, publicDnsName: string, launchTime: Date}]>=>{
    try{
      let providerApi = new ProxyProviderFactory().getProvider(provider, region) //get with default region
      //passing an empty name to getInstancesByName will return the entire list of instances in a region
      let name = null;
      if(filter && filter.name){
        name = filter.name;
      }
      let instances = await providerApi.getInstancesByName(name)
      if(filter && filter.status){
        instances = <any>instances.filter(instance=>instance.status==filter.status)
      }
      return instances;

    }catch (err) {
      console.error("getInstanceListFromProvider error: ", err)
      throw err;
    }
  }

  terminateInstance = async (region: string, provider: string, instanceId: string): Promise<{ succeeded: boolean }> => {
    try {
      let providerApi = new ProxyProviderFactory().getProvider(provider, region) //get with default region
      let resTerminate = await providerApi.terminateInstance(instanceId)
    }
    catch (err) {
      console.warn(`terminateInstance warning -  when trying to terminate instance ${instanceId} in region ${region} from provider ${provider}: `, err)
    }
    //even if failed to terminate - it is possible that there was a manual terminate and still need to update the row in the db if it exists
    try {
      //update status in db
      let instanceDb = await this.lb.models.proxy_management.findOne({
        where: {
          instance_id: instanceId,
          region,
          provider
        }
      })
      if (!instanceDb) {
        throw new Error(`Instance ${instanceId} does not exist in db`)
      } else {
        await instanceDb.updateAttributes({state: MachineStatus.terminated, shutdown_time: new Date()})
      }
      return {succeeded: true}
    }
    catch (err) {
      console.error("terminateInstance error: ", err)
      throw err;
    }
  }

  createInstance = async (region: string, provider: string, instanceName, keyName, openPort: number, userId: number,validateUniqueName:boolean=true): Promise<{ instanceCreated: boolean, imageCopied: boolean,publicIp?:string,instanceId?:string }> => {
    console.log(`request to create instance by userId: ${userId} in ${region} for provider: ${provider}. Instance name: ${instanceName} with keyPair : ${keyName}. Open port : ${openPort}`)
    try {
      let providerApi = new ProxyProviderFactory().getProvider(provider, region)
      //get or create key pair
      //if required - validate that instanceName is unique
      if(validateUniqueName){
        let instancesList = await providerApi.getInstancesByName(instanceName)
        let activeInstances = instancesList.filter(instance=>instance.status==MachineStatus.running || instance.status==MachineStatus.pending )
        if(activeInstances.length>0){
          let log = `There is already an Instance named '${instanceName}' that is running in region '${region}' for provider '${provider}' `;
          console.error("createInstance failed. ", log)
          throw new Error(log)
        }
      }
      let keyNameRes = await this.getOrCreateKey(keyName, region, provider);
      //get or create security group
      let secGroupRes = await this.getOrCreateSecurityGroup(providerApi, openPort)
      console.log(`creating instance ${instanceName} with security group id: ${secGroupRes.secGroupId}`)
      //get the active image for this provider
      let sourceImageDb = await this.getActiveImage(provider)
      //validate if image already exists on machine or not. If it does create instance if not need to poll for image creation
      let existingImages = await providerApi.getImagesByName(sourceImageDb.image_name)
      if (existingImages.length > 0) {
        let imageId = existingImages[0].imageId
        console.log(`creating instance ${instanceName} with exisitng image id: ${imageId}`)
        let createInstanceRes = await providerApi.createMachineInstance(instanceName, imageId, keyName, [secGroupRes.secGroupId])
        //create instance row in db
        await this.lb.models.proxy_management.create({
          instance_name: instanceName,
          startup_time: new Date(),
          is_pending_image: false,
          region,
          provider,
          user_id: userId,
          key_pair_name: keyName,
          state: MachineStatus.running,
          image_id: imageId,
          instance_id: createInstanceRes.instanceId,
          public_ip: createInstanceRes.publicIp,
          proxy_port: openPort
        })
        return {instanceCreated: true, imageCopied: false,publicIp: createInstanceRes.publicIp,instanceId: createInstanceRes.instanceId,}
      } else {
        console.log(`Copying image to region ${region} for create instance ${instanceName} request`)

        let copiedImage = await providerApi.copyImageToRegion(sourceImageDb.image_name, sourceImageDb.image_id, sourceImageDb.region)
        console.log(`Succeeded in copying image to region ${region} for create instance ${instanceName} request. Image id: ${copiedImage.imageId}. Image is now in pending state`)
        //create new row in db and most important - set the is_pending_image to true
        await this.lb.models.proxy_management.create({
          instance_name: instanceName,
          is_pending_image: true,
          region,
          provider,
          user_id: userId,
          key_pair_name: keyName,
          state: MachineStatus.pending,
          image_id: copiedImage.imageId,
          proxy_port: openPort
        })
        return {instanceCreated: false, imageCopied: true}
      }
    } catch (err) {
      console.error("createInstance error: ", err)
      throw err;
    }
  }

  createInstanceAfterImageAvailable = async (region: string, provider: string, imageId: string, instanceName, keyName, openPort: number): Promise<{ succeeded: boolean }> => {
    try {
      let providerApi = new ProxyProviderFactory().getProvider(provider, region)
      let secGroupRes = await this.getOrCreateSecurityGroup(providerApi, openPort)
      //create instance
      let imagesInstancees = await this.lb.models.proxy_management.find({
        where: {image_id: imageId, region, provider, is_pending_image: true}
      })
      //create a new instance only if pending item found in db otherwise may create endless instances and never update the database
      if (imagesInstancees.length == 1) {
        let createInstanceRes = await providerApi.createMachineInstance(instanceName, imageId, keyName, [secGroupRes.secGroupId])
        console.log(`createInstanceAfterImageAvailable: new instance ${instanceName} created `)
        //get the item from the db inorder to update it. Filter by image id provider region and pending status

        let instance = imagesInstancees[0]
        await instance.updateAttributes({
          is_pending_image: false,
          startup_time: new Date(),
          instance_id: createInstanceRes.instanceId,
          public_ip: createInstanceRes.publicIp,
          state: MachineStatus.running
        })
        return {succeeded: true}
      } else {
        throw(`createInstanceAfterImageAvailable error. Expected to find one row in proxy_management with filter: image_id:${imageId},region: ${region},provider: ${provider},state: ${MachineStatus.pending} but recieved ${imagesInstancees.length} rows`)
      }
    } catch (err) {
      console.error("createInstanceAfterImageAvailable error: ", err)
      throw err;
    }

  }

  getLastConnectionTime=async (region: string, provider: string,instanceName:string):Promise<{lastConnect:any}>=>{
    //get running instace from local db and take the public ip
    let proxyManageList = await this.getProxyManageList(10,true,region,provider)
    if(proxyManageList.length>0){
      let instance = proxyManageList.find(x=>x.instance_name.toLowerCase()==instanceName.toLowerCase())
      if(!instance){
        throw new Error(`getLastConnectionTime. Could not find an instance in region '${region} with instance name of '${instanceName}'`)
      }
      let proxyIp = instance.public_ip;
      let proxyPort = instance.proxy_port;
      let url=`http://${proxyIp}:${proxyPort}/lastConnect`
      try {
        let lastConnectResponse = await request.get(url).query({}).then(res => res.body)
        return lastConnectResponse;
      }catch (err) {
        console.error("getLastConnectionTime error: ", err)
        throw err;
      }
    }else{
      throw new Error(`According to local db there is no running proxy server in region '${region} and provider '${provider}'`)
    }

  }

  copyImage= async (region: string, provider: string):Promise<{succeeded:boolean,alreadyExists?:boolean,imageId:string}>=>{
    try {
      let providerApi = new ProxyProviderFactory().getProvider(provider, region)
      let sourceImageDb = await this.getActiveImage(provider)
      //validate if image already exists on machine or not. If it does create instance if not need to poll for image creation
      let existingImages = await providerApi.getImagesByName(sourceImageDb.image_name)
      if (existingImages.length > 0) {
        return {succeeded:true,alreadyExists:true,imageId:existingImages[0].imageId}
      }else{
        let copiedImage = await providerApi.copyImageToRegion(sourceImageDb.image_name, sourceImageDb.image_id, sourceImageDb.region)
        console.log(`copied image to region '${region}. Image id created is '${copiedImage.imageId}'`)
        return {succeeded:true,alreadyExists:false,imageId:copiedImage.imageId}
      }
    }catch (err) {
      console.error("copyImage error: ", err)
      throw err;
    }
  }

  getImageStatus=async (region: string, provider: string,imageId:string):Promise<{imageStatus:string}>=>{
    try {
      let providerApi = new ProxyProviderFactory().getProvider(provider, region)
      let image = await providerApi.getImageById(imageId)
      if (image) {
        return {imageStatus:image.status};
      }else{
        throw new Error(`Image id '${imageId} does not exist in region ${region}'`);
      }
    }catch (err) {
      console.error("getImageStatus error: ", err)
      throw err;
    }
  }

  private getOrCreateSecurityGroup = async (providerApi: IProxyProvider, openPort: number): Promise<{ secGroupId: string }> => {
    try {
      let secGroupId = null;
      let secGroupRes = await providerApi.getSecurityGroupByOpenPort(openPort)
      if (!secGroupRes.exists) {
        let permission = providerApi.getTcpPermissionForIp(openPort)
        let secGroup = await providerApi.createSecurityGroup(`proxySC_${openPort}`, [permission], "Security group for proxy service")
        secGroupId = secGroup.secGroupId;
      } else {
        secGroupId = secGroupRes.secGroupId;
      }
      return {secGroupId};
    } catch (err) {
      console.error("getOrCreateSecurityGroup error: ", err)
      throw err;
    }
  }
}
