import {IRepository} from "./dataModels/IRepository";
import {ImageSetting} from "./dataModels/imageSetting";
import {ProxyManagement} from "./dataModels/proxyManagement";
import {ProxyProviderFactory} from "./proxyProviderFactory";
import {MachineStatus} from "./dataModels/machineStatus";
import {ProxyManager} from "./proxyManager";

export class ImageToInstancePoller {
  intervalSecs: number = 15;

  constructor(private lb: IRepository) {
    this.startPolling()
  }

  startPolling=()=>{
    this.intervalSecs = +process.env.IMAGE_TO_INSTANCE_POLLER_INTERVAL_SEC || 15
    setTimeout(() => {
      this.pollImageReady()
    }, this.intervalSecs * 1000)
  }

  pollImageReady = async () => {
    try {
      //go over list of items in proxy-management table and validate if there are images pending to complete
      let pendingImages = await this.getListOfPendingImages();
      for (let pendingImage of pendingImages) {
        //for each pending validate if image is still in pending or has completed
        let providerApi = new ProxyProviderFactory().getProvider(pendingImage.provider, pendingImage.region) //get with default region
        let image = await providerApi.getImageById(pendingImage.image_id)
        if (image.status == MachineStatus.available) {
          console.log(`ImageToInstancePoller: Image ${pendingImage.image_id} status set to available. Starting create on new instance ${pendingImage.instance_name} in region ${pendingImage.region}`)
          let instanceRes = await new ProxyManager(this.lb).createInstanceAfterImageAvailable(pendingImage.region, pendingImage.provider, pendingImage.image_id,
            pendingImage.instance_name, pendingImage.key_pair_name,pendingImage.proxy_port)
        }
      }
    } catch (err) {
      console.error("pollImageReady error. ", err)
      throw err;
    }finally {
      this.startPolling()
    }
  }

  private getListOfPendingImages = async (): Promise<ProxyManagement[]> => {
    try {
      let imagesInstance = await this.lb.models.proxy_management.find({where: {is_pending_image: true}});
      if(imagesInstance.length>0){
        let result = imagesInstance.map(imgInstance => imgInstance.toJSON());
        return result;
      }else{
        return [];
      }

    } catch (err) {
      console.error("getListOfPendingImages error. ", err)
      throw err;
    }
  }
}
