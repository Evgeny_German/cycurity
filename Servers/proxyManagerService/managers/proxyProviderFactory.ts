import {AwsProxyProvider} from "./proxyProviders/aws/awsProxyProvider";
import {IProxyProvider} from "./proxyProviders/IProxyProvider";

export class ProxyProviderFactory {
    getProvider=(providerName:string,region:string):IProxyProvider=>{
        switch (providerName){
            case "aws":
                return new AwsProxyProvider(region);
            default:
                throw new Error(`ProxyProviderFactory: provider ${providerName} is not supported`);
        }
    }
}
