import {SecGroupPermission} from "./aws/awsProxyProvider";

export interface IProxyProvider {
  createKeyPairIfNotExist (keyPairName: string): Promise<{ keyAlreadyExists: boolean, pemData?: string }>;
  deleteImage  (imageId: string);
  getImageById (imageId:string): Promise<{ imageId: string, status: string }>
  getImagesByName (imageName: string): Promise<[{ imageId: string, status: string }]>;
  copyImageToRegion (imageName: string, sourceImageId: string, sourceRegion: string): Promise<{ imageId: string, alreadyExists: boolean }>
  getInstancesByName (instanceName: string):Promise<[{instancenName:string,instanceId: string,instanceType: string,status: string, imageId: string, keyName: string, publicIp: string, publicDnsName: string, launchTime: Date}]> ;
  createMachineInstance (instanceName: string, imageId: string, keyName, securityGroupIds: string[] , instanceType?:string ): Promise<{instanceId:string,publicIp:string}>;
  stopInstance  (instanceId: string): Promise<any>;
  startInstance (instanceId: string): Promise<any>;
  terminateInstance (instanceId: string): Promise<any>;
  getRegionsList ():Promise<{regionName:string,displayName}[]>;
  createSecurityGroup (secGroupName: string, permissions: SecGroupPermission[], descrition: string): Promise<{ secGroupId: string, alreadyExists: boolean }>;
  getSecurityGroupIdByName (secGroupName: string): Promise<{ secGroupId: string }>;
  getSecurityGroupByOpenPort(port:number): Promise<{ exists:boolean, secGroupId?: string }>;
  getTcpPermissionForIp (port: number, ip?: string ): SecGroupPermission ;
}
