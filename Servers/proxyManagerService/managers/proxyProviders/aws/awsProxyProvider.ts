import * as AWS from 'aws-sdk';
import {MachineStatus} from "../../dataModels/machineStatus";
import {IProxyProvider} from "../IProxyProvider";

export class AwsProxyProvider implements IProxyProvider {
  API_VERSION = "2016-11-15"

  constructor(region: string) {
    if (!region) {
      region = process.env.AWS_DEFAULT_REGION;
    }
    this.setRegion(region)
  }

  setRegion = (region: string) => {
    AWS.config.update({region});
  }

  //-----general
  getDefaultVpc = (): Promise<string> => {
    return new Promise<any>((resolve, reject) => {
      let ec2 = new AWS.EC2({apiVersion: this.API_VERSION})
      ec2.describeVpcs((err, data) => {
        if (err) {
          console.error("getVpcList Error", err);
          reject(err)
        } else {
          let vpc = data.Vpcs.find(vpc => vpc.IsDefault == true)
          if (!vpc) {
            throw new Error("getDefaultVpc error. Could not find default vpc")
          }
          resolve(vpc.VpcId)
        }
      });
    });
  };

  createKeyPairIfNotExist = async (keyPairName: string): Promise<{ keyAlreadyExists: boolean, pemData?: string }> => {
    try {
      let ec2 = new AWS.EC2({apiVersion: this.API_VERSION});
      let keyPairData = await ec2.describeKeyPairs().promise()
      let key = keyPairData.KeyPairs.find(key => key.KeyName == keyPairName);
      if (key) {
        return {keyAlreadyExists: true}
      }
      else {
        let params = {KeyName: keyPairName};
        let keyPairResult = await ec2.createKeyPair(params).promise();
        return {keyAlreadyExists: false, pemData: keyPairResult.KeyMaterial};
      }
    } catch (err) {
      console.log("createKeyPairIfNotExist.createKeyPair - Error", err);
      throw(err);
    }

  }
  //-----general end

  //------------Images
  deleteImage = (imageId: string) => {
    return new Promise<any>((resolve, reject) => {
      let ec2 = new AWS.EC2({apiVersion: this.API_VERSION});
      let params = {ImageId: imageId}
      ec2.deregisterImage(params, (err, data) => {
        if (err) {
          console.error("deleteImage Error", err);
          reject(err);
        } else {
          resolve(true);
        }
      })
    });
  }

  getImageById = async (imageId: string): Promise<{ imageId: string, status: string }> => {
    try {
      let params = {ImageIds: [imageId]};
      let ec2 = new AWS.EC2({apiVersion: this.API_VERSION})
      let data = await ec2.describeImages(params).promise()
      if (data && data.Images && data.Images.length > 0) {
        let awsimage = data.Images[0];
        let image = {
          imageId: awsimage.ImageId,
          status: awsimage.State
        }
        return image;
      } else {
        return null;
      }
    } catch (err) {
      console.error("getImageById error ", err)
      throw err;
    }
  }

  getImagesByName = (imageName: string): Promise<[{ imageId: string, status: string }]> => {
    return new Promise<any>((resolve, reject) => {
      let params = {
        Filters: [
          {Name: "name", Values: [imageName]}
        ]
      }
      new AWS.EC2({apiVersion: this.API_VERSION}).describeImages(params, (err, data) => {
        if (err) {
          console.log(err, err.stack); // an error occurred
          reject(err);
        } else {
          let images = [];
          if (data && data.Images && data.Images.length > 0) {
            for (let awsimage of data.Images) {
              let image = {
                imageId: awsimage.ImageId,
                status: awsimage.State
              }
              images.push(image)
            }

          }
          resolve(images);
        }
      })
    });
  }

  copyImageToRegion = async (imageName: string, sourceImageId: string, sourceRegion: string): Promise<{ imageId: string, alreadyExists: boolean }> => {
    try {
      //validate if image already exists
      let images = await this.getImagesByName(imageName);
      if (images && images.length == 1) { //if there is an image by that name - return the image metadata
        return ({imageId: images[0].imageId, alreadyExists: true});

      } else if (images && images.length > 1) { //if there is more than a single image - throw error
        throw new Error("There is currenlty more than a single image with this name. Delete images before creating new ones")

      } else if (images && images.length == <number>0) { //if there are no images - copy the image
        let params = {Name: imageName, SourceImageId: sourceImageId, SourceRegion: sourceRegion}
        let data = await new AWS.EC2({apiVersion: this.API_VERSION}).copyImage(params).promise();
        console.log(data);
        return ({imageId: data.ImageId, alreadyExists: false});
      }
    } catch (err) {
      console.error("copyImageToRegion error: ", err)
      throw err;
    }
  }
    //------------Images end

  //--------------Instances
  getInstancesByName = (instanceName: string):Promise<[{instancenName:string,instanceId: string,instanceType: string,status: string, imageId: string, keyName: string, publicIp: string, publicDnsName: string, launchTime: Date}]> => {
    return new Promise<any>((resolve, reject) => {
      let ec2 = new AWS.EC2({apiVersion: this.API_VERSION});
      let params = {}
      if(instanceName){
        params = {
          Filters: [{Name: "tag:Name", Values: [instanceName]}]
        }
      }
      ec2.describeInstances(params, (err, data) => {
        if (err) {
          console.error("getInstanceByName Error", err);
          reject(err)
        } else {
          let resultList = []
          if (data.Reservations.length > 0 && data.Reservations[0].Instances && data.Reservations[0].Instances.length > 0) {
            for(let reservation of data.Reservations) {
              for (let instance of reservation.Instances) {
                let name = "";
                let nameTag = instance.Tags.find(tag => tag.Key == "Name")
                if (nameTag) {
                  name = nameTag.Value
                }
                let result = {
                  instancenName: name,
                  instanceId: instance.InstanceId,
                  instanceType: instance.InstanceType,
                  status: instance.State.Name,
                  imageId: instance.ImageId,
                  keyName: instance.KeyName,
                  publicIp: instance.PublicIpAddress,
                  publicDnsName: instance.PublicDnsName,
                  launchTime: instance.LaunchTime
                }
                resultList.push(result);
              }
            }
          }
          resolve(resultList);
        }
      })
    });
  }

  createMachineInstance = async (instanceName: string, imageId: string, keyName, securityGroupIds: string[] = null, instanceType: string = "t2.micro"): Promise<{instanceId:string,publicIp:string}> => {
    try {
      let params = {ImageId: imageId, InstanceType: instanceType, KeyName: keyName, MinCount: 1, MaxCount: 1};
      //if security groups defined - add them when creating instance
      if (securityGroupIds) {
        params["SecurityGroupIds"] = securityGroupIds
      }
      let ec2 = new AWS.EC2({apiVersion: this.API_VERSION})
      let data = await ec2.runInstances(params).promise();
      //console.log(data.Instances[0].PublicIpAddress);
      if (!data.Instances || data.Instances.length == 0) {
        throw new Error("createMachineInstance. Call to runInstances has not returned a new instance");
      }
      var instance = data.Instances[0];
      console.log("Created instance id : ", instance.InstanceId);
      // Add name tag to the instance
      let tagParams = {Resources: [instance.InstanceId], Tags: [{Key: 'Name', Value: instanceName}]};
      //add tag with name
      var tagData = await ec2.createTags(tagParams).promise();

      console.log(`Machined tagged with name ${instanceName}`);
      if(!instance.PublicIpAddress){
        let pollIp = await this.pollForMachineAddress(instance.InstanceId)
        if(pollIp.publicIp){
          return {instanceId:instance.InstanceId,publicIp:pollIp.publicIp};
        }else{
          throw new Error("createMachineInstance error. The machine was created yet the public ip was not retrieved. Need to manualy terminate the machine")
        }
      }else{
        return {instanceId:instance.InstanceId,publicIp:instance.PublicIpAddress};
      }

    } catch (err) {
      console.error("createMachineInstance error: ", err)
      throw err;
    }

  }

  //po;; several times until public ip is appended to instance - this shouold take no more then several seconds
  private pollForMachineAddress=async (instanceId:string):Promise<{publicIp:string}>=>{
    let pollLimit = 15;
    let pollCount = 0
    let hasIp = false;
    let ec2 = new AWS.EC2({apiVersion: this.API_VERSION});
    let publicIp=null;
    var params = {Filters: [{Name: "instance-id",Values: [instanceId]}]};
    while(!hasIp && pollCount<=pollLimit){
      let instanceVals = await ec2.describeInstances(params).promise()
      if(instanceVals.Reservations.length>0 && instanceVals.Reservations[0].Instances.length>0){
        publicIp =instanceVals.Reservations[0].Instances[0].PublicIpAddress
        if(publicIp){
          hasIp=true;
        }
      }
      pollCount++;
    }
    return {publicIp}
  }

  stopInstance = async (instanceId: string): Promise<any> => {
    let ec2 = new AWS.EC2({apiVersion: this.API_VERSION});
    let params = {InstanceIds: [instanceId]}
    try {
      let data = await ec2.stopInstances(params).promise()
      return true;
    } catch (err) {
      console.error("Error when calling stopInstance. ", err)
      throw err;
    }
  }

  startInstance = async (instanceId: string): Promise<any> => {
    let ec2 = new AWS.EC2({apiVersion: this.API_VERSION});
    let params = {InstanceIds: [instanceId]}
    try {
      let data = await ec2.startInstances(params).promise()
      return true;
    } catch (err) {
      console.error("Error when calling startInstance. ", err)
      throw err;
    }
  }

  terminateInstance = async (instanceId: string): Promise<any> => {
    let ec2 = new AWS.EC2({apiVersion: this.API_VERSION});
    let params = {InstanceIds: [instanceId]}
    try {
      let data = await ec2.terminateInstances(params).promise()
      return true;
    } catch (err) {
      console.error("Error when calling startInstance. ", err)
      throw err;
    }
  }
  //--------------Instances End


  //----------Security groups
  //Method for creating a valid SecGroupPermission for a single port and open to all or for specific IP
  getTcpPermissionForIp = (port: number, ip: string = null): SecGroupPermission => {
    let res = {IpProtocol: "tcp", FromPort: port, ToPort: port, IpRanges: []}
    if (ip) {
      res.IpRanges.push({"CidrIp": ip})
    } else {
      res.IpRanges.push({"CidrIp": "0.0.0.0/0"}) //open for all ips
    }
    return res;
  }

  getSecurityGroupIdByName = async (secGroupName: string): Promise<{ secGroupId: string }> => {
    let params = {
      GroupNames: [secGroupName]
    };
    let ec2 = new AWS.EC2({apiVersion: this.API_VERSION})
    try {
      let data = await ec2.describeSecurityGroups(params).promise()
      if (data.SecurityGroups.length > 1) {
        throw new Error("Security group name returned more than one result")
      } else if (data.SecurityGroups.length == 1) {
        return ({secGroupId: data.SecurityGroups[0].GroupId});
      }
    } catch (err) {
      if (err.code == "InvalidGroup.NotFound") {
        return null;
      } else {
        console.log("Error in getSecurityGroupIdByName. ", err)
        throw err;
      }
    }
  };

  getSecurityGroupByOpenPort = async (port: number): Promise<{ exists: boolean, secGroupId?: string }> => {
    let params = {
      Filters: [{Name: 'ip-permission.from-port', Values: [port.toString()]}],
    };
    let ec2 = new AWS.EC2({apiVersion: this.API_VERSION})
    try {
      let data = await ec2.describeSecurityGroups(params).promise()
      if (data.SecurityGroups.length > 0) {
        //find the first one for which the port is open to all
        for (let secGroup of data.SecurityGroups) {
          let permission = secGroup.IpPermissions.find(permission => permission.FromPort == port)
          if (permission) {
            let openIpRange = permission.IpRanges.find(ipRange => ipRange.CidrIp == "0.0.0.0/0")
            if (openIpRange) {
              return {exists: true, secGroupId: secGroup.GroupId}
            }
          }
        }
        return ({exists: false});
      } else {
        return ({exists: false});
      }
    } catch (err) {
      if (err.code == "InvalidGroup.NotFound") {
        return null;
      } else {
        console.log("Error in getSecurityGroupIdByName. ", err)
        throw err;
      }
    }
  };

  createSecurityGroup = async (secGroupName: string, permissions: SecGroupPermission[], descrition: string): Promise<{ secGroupId: string, alreadyExists: boolean }> => {
    //try get security group by name
    try {
      let secGroupIdRes = await this.getSecurityGroupIdByName(secGroupName)
      if (secGroupIdRes) {
        return ({secGroupId: secGroupIdRes.secGroupId, alreadyExists: true});
      }
    } catch (err) {
      console.error("createSecurityGroup.getSecurityGroupIdByName error when trying to get security group by name", err)
      throw err
    }

    //if not exist create a new one. Start by getting the default vpc
    let vpcId = null;
    try {
      vpcId = await this.getDefaultVpc()
    } catch (err) {
      console.error("createSecurityGroup error when trying to get default vpc", err)
      throw err
    }

    //create new security group
    var paramsSecurityGroup = {Description: descrition, GroupName: secGroupName, VpcId: vpcId};
    let paramsIngress = {}
    let ec2 = new AWS.EC2({apiVersion: this.API_VERSION});
    let secGroupId = null;
    try {
      let data = await ec2.createSecurityGroup(paramsSecurityGroup).promise()
      secGroupId = data.GroupId;
      console.log(`created security group with id of ${secGroupId}. Setting permissions`);
    } catch (err) {
      console.log("createSecurityGroup Error", err);
      throw(err)
    }
    //set security groups authorizations
    try {
      paramsIngress = {
        GroupName: secGroupName,
        IpPermissions: permissions
      };
      await ec2.authorizeSecurityGroupIngress(paramsIngress).promise()
      console.log(`Set groups permissions to ${JSON.stringify(permissions)}`);
      return ({secGroupId: secGroupId, alreadyExists: false});
    } catch (err) {
      console.log("authorizeSecurityGroupIngress Error", err);
      throw(err)
    }
  };
  //----------Security groups end
  getRegionsList = async (): Promise<{ regionName: string, displayName }[]> => {
    try {
      let ec2 = new AWS.EC2({apiVersion: this.API_VERSION});
      let regions = await ec2.describeRegions({}).promise()
      let regionsMapped = regions.Regions.map(region => {
        return {regionName: region.RegionName, displayName: region.RegionName}
      })
      return regionsMapped;
    } catch (err) {
      console.log("getRegionsList Error", err);
      throw(err)
    }
  }
}

export type SecGroupPermission = {
  IpProtocol: string,
  FromPort: number,
  ToPort: number,
  IpRanges: { [key: string]: string }[]
}

