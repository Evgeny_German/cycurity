export const regionMaps=[
  {provider:"aws",regionKey:"us-east-1",displayName:"US East (N. Virginia)"},
  {provider:"aws",regionKey:"us-east-2",displayName:"US East (Ohio)"},
  {provider:"aws",regionKey:"us-west-1",displayName:"US West (N. California)"},
  {provider:"aws",regionKey:"us-west-2",displayName:"US West (Oregon)"},
  {provider:"aws",regionKey:"ca-central-1",displayName:"Canada (Central)"},
  {provider:"aws",regionKey:"eu-central-1",displayName:"EU (Frankfurt)"},
  {provider:"aws",regionKey:"eu-west-1",displayName:"EU (Ireland)"},
  {provider:"aws",regionKey:"eu-west-2",displayName:"EU (London)"},
  {provider:"aws",regionKey:"eu-west-3",displayName:"EU (Paris)"},
  {provider:"aws",regionKey:"ap-northeast-1",displayName:"Asia Pacific (Tokyo)"},
  {provider:"aws",regionKey:"ap-northeast-2",displayName:"Asia Pacific (Seoul)"},
  {provider:"aws",regionKey:"ap-northeast-3",displayName:"Asia Pacific (Osaka-Local)"},
  {provider:"aws",regionKey:"ap-southeast-1",displayName:"Asia Pacific (Singapore)"},
  {provider:"aws",regionKey:"ap-southeast-2",displayName:"Asia Pacific (Sydney)"},
  {provider:"aws",regionKey:"ap-south-1",displayName:"Asia Pacific (Mumbai)"},
  {provider:"aws",regionKey:"sa-east-1",displayName:"South America (São Paulo)"},


]
