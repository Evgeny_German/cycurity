{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "Cycurity Proxies API",
    "description": "Cycurity Proxies API definition"
  },
  "schemes": [
    "http"
  ],
  "paths": {
    "/proxy/getProxyManageList": {
      "post": {
        "tags": [
          "proxy"
        ],
        "summary": "Get the list of proxies",
        "parameters": [
          {
            "in": "body",
            "name": "limit",
            "schema": {
              "type": "object",
              "properties": {
                "limit": {
                  "type": "number",
                  "example": 10
                },
                "onlyRunning": {
                  "type": "boolean",
                  "example": false
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "list of proxies"
          }
        }
      }
    },
    "/proxy/getSupportedRegions": {
      "get": {
        "tags": [
          "proxy"
        ],
        "summary": "Get the list of regions supported in the system",
        "responses": {
          "200": {
            "description": "list of regions"
          }
        }
      }
    },
    "/proxy/getImages": {
      "get": {
        "tags": [
          "proxy"
        ],
        "summary": "Get the list of images",
        "responses": {
          "200": {
            "description": "list of images"
          }
        }
      }
    },
    "/proxy/getKeys": {
      "get": {
        "tags": [
          "proxy"
        ],
        "summary": "Get the list of keys",
        "responses": {
          "200": {
            "description": "list of keys"
          }
        }
      }
    },
    "/proxy/setActiveImage": {
      "post": {
        "tags": [
          "proxy"
        ],
        "summary": "Set an image as active by its name. This image will be used as the template from which to create proxy server. This is relevant for each provider. Make sure that the image already exists before setting as active",
        "parameters": [
          {
            "in": "body",
            "name": "data",
            "schema": {
              "type": "object",
              "properties": {
                "imageName": {
                  "type": "string"
                },
                "region": {
                  "type": "string"
                },
                "provider": {
                  "type": "string",
                  "example": "aws"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "alreadyExists?: boolean,error?:string"
          }
        }
      }
    },
    "/proxy/updateProviderRegions": {
      "post": {
        "tags": [
          "proxy"
        ],
        "summary": "Updates the local database with the list of existing regions that are defined in the provider. Once run you can view the list by calling getSupportedRegions",
        "parameters": [
          {
            "in": "body",
            "name": "data",
            "schema": {
              "type": "object",
              "properties": {
                "provider": {
                  "type": "string",
                  "example": "aws"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": ""
          }
        }
      }
    },
    "/proxy/createInstance": {
      "post": {
        "tags": [
          "proxy"
        ],
        "summary": "Creates a new instance running a proxy. If the image exist in region the creation will work sync. If required to copy the image then the creation will take several minutes. OpenPort, instanceName and keyName are optional",
        "parameters": [
          {
            "in": "body",
            "name": "data",
            "schema": {
              "type": "object",
              "properties": {
                "region": {
                  "type": "string"
                },
                "provider": {
                  "type": "string",
                  "example": "aws"
                },
                "instanceName": {
                  "type": "string"
                },
                "keyName": {
                  "type": "string"
                },
                "openPort": {
                  "type": "string",
                  "description": "If not provided will use the default value defined in server config"
                },
                "userId": {
                  "type": "number"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "instanceCreated:boolean,imageCopied:boolean"
          }
        }
      }
    },
    "/proxy/terminateInstance": {
      "post": {
        "tags": [
          "proxy"
        ],
        "summary": "Terminates an instance",
        "parameters": [
          {
            "in": "body",
            "name": "data",
            "schema": {
              "type": "object",
              "properties": {
                "region": {
                  "type": "string"
                },
                "provider": {
                  "type": "string",
                  "example": "aws"
                },
                "instanceId": {
                  "type": "string"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "succeeded:boolean"
          }
        }
      }
    },
    "/proxy/copyImage": {
      "post": {
        "tags": [
          "proxy"
        ],
        "summary": "Copies the template proxy image to a region",
        "parameters": [
          {
            "in": "body",
            "name": "data",
            "schema": {
              "type": "object",
              "properties": {
                "region": {
                  "type": "string"
                },
                "provider": {
                  "type": "string",
                  "example": "aws"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "{succeeded:boolean,alreadyExists?:boolean,imageId:string}"
          }
        }
      }
    },
    "/proxy/getImageStatus": {
      "post": {
        "tags": [
          "proxy"
        ],
        "summary": "Gets the status of an image. Possible values are pending and available",
        "parameters": [
          {
            "in": "body",
            "name": "data",
            "schema": {
              "type": "object",
              "properties": {
                "region": {
                  "type": "string"
                },
                "provider": {
                  "type": "string",
                  "example": "aws"
                },
                "imageId":{
                  "type":"string"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "{imageStatus:string}"
          }
        }
      }
    },
    "/proxy/getInstanceListFromProvider": {
      "post": {
        "tags": [
          "proxy"
        ],
        "summary": "Gets the full list of instances directly from the provider and not from local db",
        "parameters": [
          {
            "in": "body",
            "name": "data",
            "schema": {
              "type": "object",
              "properties": {
                "region": {
                  "type": "string"
                },
                "provider": {
                  "type": "string",
                  "example": "aws"
                },
                "filter": {
                  "type": "object",
                  "properties": {
                    "name": {
                      "type": "string"
                    },
                    "status": {
                      "type": "string"
                    }
                  }
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "List of instances"
          }
        }
      }
    },
    "/proxy/testProxy": {
      "post": {
        "tags": [
          "proxy"
        ],
        "summary": "Tests if a proxy server is running on specified address",
        "parameters": [
          {
            "in": "body",
            "name": "data",
            "schema": {
              "type": "object",
              "properties": {
                "url": {
                  "type": "string",
                  "example":"https://www.google.com"
                },
                "proxyIp": {
                  "type": "string"
                },
                "proxyPort": {
                  "type": "number"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Result of proxy server is running and working correct"
          }
        }
      }
    },
    "/proxy/getLastConnectionTime": {
      "post": {
        "tags": [
          "proxy"
        ],
        "summary": "Get the last time for which the proxy was used",
        "parameters": [
          {
            "in": "body",
            "name": "data",
            "schema": {
              "type": "object",
              "properties": {
                "region": {
                  "type": "string"
                },
                "provider": {
                  "type": "string",
                  "example": "aws"
                },
                "instanceName": {
                  "type": "string"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Time in epoch"
          }
        }
      }
    }

  }
}
