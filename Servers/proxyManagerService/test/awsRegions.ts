
export enum AwsRegions {
    us_east_1_n_virginia = "us-east-1",
    us_east_2_ohio = "us-east-2",
    us_west_1_n_california = "us-west-1",
    us_west_2_oregon = "us-west-2",
    eu_central_1_frankfurt = "eu-central-1"
}