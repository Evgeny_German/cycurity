import * as chai from "chai";
import * as sinon from "sinon";
import * as assert from "assert";
import * as path from "path";

var expect = chai.expect;
let should = chai.should();

import {config} from "dotenv";
import {AwsProxyProvider} from "../managers/proxyProviders/aws/awsProxyProvider";
import {AwsRegions} from "./awsRegions";


const BASE_IMAGE_ID="ami-9397af78"
const BASE_REGION=AwsRegions.eu_central_1_frankfurt;
const IMAGE_NAME="proxy image"
const KEY_PAIR_NAME = "razor_cycurity";
const PROXY_KEY_NAME="proxy_instance_key"
const SECURITY_GROUP_NAME = "proxySC"

describe("aws provider tests", () => {
    before(() => {
        let configPath = path.join(__dirname, "../config/.env")
        config({path: configPath});
    })

    it.skip("should create new ec2 instance", () => {
        let awsProvider = new AwsProxyProvider(AwsRegions.eu_central_1_frankfurt);
        return awsProvider.createMachineInstance("proxy instance",BASE_IMAGE_ID,KEY_PAIR_NAME).then(res => {
            assert(true)
        }).catch(err => {
            console.log(err);
            assert(false);
        });
    });
    it.skip("should delete image",()=>{
        let awsProvider = new AwsProxyProvider(AwsRegions.us_west_2_oregon);
        return awsProvider.deleteImage("ami-57bee82f").then(res=>{
            assert(true)
        }).catch(err=>{
            assert(false);
        })

    })
    it.skip("should copy an iam image from eu to us", () => {
        let awsProvider = new AwsProxyProvider(AwsRegions.us_west_2_oregon);
        return awsProvider.copyImageToRegion(IMAGE_NAME,BASE_IMAGE_ID,BASE_REGION).then(res => {
            assert(true)
        }).catch(err => {
            console.log(err);
            assert(false);
        });
    })
    it.skip("should get an image in region by id",()=>{
      let awsProvider = new AwsProxyProvider(AwsRegions.eu_central_1_frankfurt);
      return awsProvider.getImageById("ami-f0f9fc1b").then(res => {
        assert(true)
      }).catch(err => {
        console.log(err);
        assert(false);
      });
    })
    it.skip("should get a list of images in region by name", () => {
        let awsProvider = new AwsProxyProvider(AwsRegions.us_west_2_oregon);
        return awsProvider.getImagesByName(IMAGE_NAME).then(res => {
            assert(true)
        }).catch(err => {
            console.log(err);
            assert(false);
        });
    })
    it.skip("should return a single security group id by name",()=>{
        let awsProvider = new AwsProxyProvider(AwsRegions.eu_central_1_frankfurt);
        return  awsProvider.getSecurityGroupIdByName("scrappers").then(res=>{
            assert(true)
        }).catch(err => {
            console.log(err);
            assert(false);
        });
    })
  it.skip("should return a single security group id by open port",()=>{
    let awsProvider = new AwsProxyProvider(AwsRegions.eu_central_1_frankfurt);
    return  awsProvider.getSecurityGroupByOpenPort(3000).then(res=>{
      assert(true)
    }).catch(err => {
      console.log(err);
      assert(false);
    });
  })
    it.skip("should return default vpc",()=>{
        let awsProvider = new AwsProxyProvider(AwsRegions.eu_central_1_frankfurt);
        return  awsProvider.getDefaultVpc().then(res=>{
            assert(true)
        }).catch(err => {
            console.log(err);
            assert(false);
        });
    })
    it.skip("should create a new security group in region with limit to single port",()=>{
        let awsProvider = new AwsProxyProvider(AwsRegions.us_west_2_oregon);
        return awsProvider.createSecurityGroup(SECURITY_GROUP_NAME,[awsProvider.getTcpPermissionForIp(3000)],"proxySC").then(res=>{
            assert(true)
        }).catch(err => {
            console.log(err);
            assert(false);
        });
    })
    it.skip("should return instance properties by instance name",()=>{
        let awsProvider = new AwsProxyProvider(AwsRegions.eu_central_1_frankfurt);

        return awsProvider.getInstancesByName("proxy manage").then(res=>{
            assert(true)
        }).catch(err=>{
            assert(false);
        })

    });
    it.skip("should start an existing instance",()=>{
        let awsProvider = new AwsProxyProvider(AwsRegions.eu_central_1_frankfurt);
        return awsProvider.startInstance("i-027f976b0967d94ff").then(res=>
            assert(true)
        ).catch(err=>{
            assert(false)
        })

    })
    it.skip("should stop an existing instance",()=>{
        let awsProvider = new AwsProxyProvider(AwsRegions.eu_central_1_frankfurt);
        return awsProvider.stopInstance("i-027f976b0967d94ff").then(res=>
            assert(true)
        ).catch(err=>{
            assert(false)
        })

    })
    it.skip("should terminate an existing instance",()=>{
        let awsProvider = new AwsProxyProvider(AwsRegions.eu_central_1_frankfurt);
        return awsProvider.terminateInstance("i-027f976b0967d94ff").then(res=>
            assert(true)
        ).catch(err=>{
            assert(false)
        })

    })
    it.skip("should complete full cycle: copy image, create security group, create instance with sec group mapped",async ()=>{
        let awsProvider = new AwsProxyProvider(AwsRegions.us_west_2_oregon);
        try{

            let images = await awsProvider.getImagesByName("proxyImageNew")
            if(images.length==1){
                let secGroup = await awsProvider.createSecurityGroup("proxySC",[awsProvider.getTcpPermissionForIp(3000)],"proxy security group")
                let keyPair = await awsProvider.createKeyPairIfNotExist(PROXY_KEY_NAME)
                return awsProvider.createMachineInstance("proxy instance",images[0].imageId,PROXY_KEY_NAME,[secGroup.secGroupId]).then(res => {
                    assert(true)
                }).catch(err => {
                    console.log(err);
                    assert(false);
                });
            }else{
                assert(false,"There is a more than one machine in origin. Cannot copy")
            }

        }catch (err) {
            console.log(err)
            assert(false);
        }

    })
    it.skip("should create key pair in region if not exist",()=>{
        let awsProvider = new AwsProxyProvider(AwsRegions.us_west_2_oregon);
        return awsProvider.createKeyPairIfNotExist(PROXY_KEY_NAME).then(res => {
            assert(true)
        }).catch(err => {
            console.log(err);
            assert(false);
        });
    })

    it.skip("should create new ec2 instance on different region", async () => {
        let awsProvider = new AwsProxyProvider(AwsRegions.us_west_2_oregon);
        let groupIdRes = await awsProvider.getSecurityGroupIdByName(SECURITY_GROUP_NAME)
        return awsProvider.createMachineInstance("proxy instance","ami-80693ef8",PROXY_KEY_NAME,[groupIdRes.secGroupId]).then(res => {
            assert(true)
        }).catch(err => {
            console.log(err);
            assert(false);
        });
    });
    it.skip("should get list of regions",()=>{
      let awsProvider = new AwsProxyProvider(AwsRegions.eu_central_1_frankfurt);
      return awsProvider.getRegionsList().then((res)=>{
        assert(true)
      }).catch(err => {
        console.log(err);
        assert(false);
      });
    })


});


