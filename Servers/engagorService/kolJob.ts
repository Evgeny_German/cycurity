
export class KolJob {
    constructor(public readonly token: string,
                public readonly accountId: string,
                public readonly getSubproject: (inboxId: any) => Promise<any>,
                public readonly next: (url: string) => Promise<void>,
                public readonly inboxes: () => string[],
                public readonly nextUrl?: string) {
    }
}
