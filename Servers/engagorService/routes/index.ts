import { Router, Request, Response, NextFunction } from "express";
import * as engagor from "../engagor";
import { getToken } from "../";

class EngagorRouter {
  router: Router;

  private token: {
    access_token: string;
    expires_in: number;
    token_type: string;
    scope: string;
    refresh_token: string;
  };

  private config: {
    userName: string;
    password: string;
    clientId: string;
    clientSecret: string;
    redirectPort: string;
  };

  constructor() {
    this.config = require("../config.json");
    this.router = Router();
    this.createRoutes();
  }

  private createRoutes() {
    this.router.get("/getAuthToken", this.getAuthToken.bind(this));
    this.router.get("/getMentions", this.getMentions.bind(this));
    this.router.get("/getInboxes", this.getInboxes.bind(this));
    this.router.post("/tagMention", this.tagMention.bind(this));
  }

  private fetchAuthToken(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.token = getToken();
      resolve();
    });
  }

  private getAuthToken(req: Request, res: Response, next: NextFunction): void {
    this.fetchAuthToken().then(() => {
      res.status(200).send({
        success: true,
        model: this.token
      });
    });
  }

  private getMentions(req: Request, res: Response, next: NextFunction): void {
    const body = req.query;
    if (!this.token) {
      this.fetchAuthToken().then(code => {
        this.getMentions(req, res, next);
      });
      return;
    }
    engagor.getAccounts(this.token.access_token).then(accounts => {
      if (accounts["error"]) {
        this.handleExpiredToken(
          accounts,
          req,
          res,
          next,
          this.getMentions.bind(this)
        );
        return;
      }
      engagor
        .getMentions(
          accounts["response"].data[0].id,
          this.token.access_token,
          req.query["filters"]
        )
        .then(mentions => {
          if (mentions["error"]) {
            this.handleExpiredToken(
              accounts,
              req,
              res,
              next,
              this.getMentions.bind(this)
            );
            return;
          }
          res.status(200).send({
            success: true,
            model: mentions
          });
        });
    });
  }

  private _inboxesCache: any;
  private _inboxesCachedOn: Date;

  private async getInboxes(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    if (
      this._inboxesCache &&
      new Date().valueOf() - this._inboxesCachedOn.valueOf() < 3600000
    ) {
      res.status(200).send({ success: true, model: this._inboxesCache });
      return;
    }
    const body = req.query;
    if (!this.token) {
      this.fetchAuthToken().then(code => {
        this.getInboxes(req, res, next);
      });
      return;
    }
    let inboxes: any;
    try {
      inboxes = await engagor.getInboxes(this.token.access_token);
      this._inboxesCachedOn = new Date();
    } catch (error) {
      if (this._inboxesCache) {
        res.status(200).send({ success: true, model: this._inboxesCache });
        return;
      }
      res.status(500).send({ success: false, error: error });
      return;
    }
    res
      .status(200)
      .send({ success: true, model: (this._inboxesCache = inboxes) });
  }

  private async tagMention(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<any> {
    if (!this.token) {
      this.fetchAuthToken().then(code => {
        this.tagMention(req, res, next);
      });
      return;
    }
    let response: any;
    try {
      response = await engagor.tagMention(
        this.token.access_token,
        req.query["accountId"],
        req.query["topicId"],
        req.query["mentionId"],
        req.body
      );
    } catch (err) {
      res.status(500).send({ success: false, error: err });
      return;
    }
    res.status(200).send({ success: true, model: response });
  }

  private handleExpiredToken(
    error: any,
    req: Request,
    res: Response,
    next: NextFunction,
    callback: (req: Request, res: Response, next: NextFunction) => void
  ): boolean {
    if (error["code"] && (<string>error["code"]).includes(" ")) {
      const code = +(<string>error["code"]).split(" ")[0];
      if (code === 401 || code === 403) {
        if (this.token) {
          engagor
            .refreshEngagorAccessToken(
              this.config.clientId,
              this.config.clientSecret,
              this.token.refresh_token
            )
            .then(token => {
              this.token = <any>token;
              callback(req, res, next);
            })
            .catch(error => {
              this.fetchAuthToken().then(() => {
                callback(req, res, next);
              });
            });
        } else {
          this.fetchAuthToken().then(() => {
            callback(req, res, next);
          });
        }
        return true;
      } else {
        return false;
      }
    }
    return false;
  }
}

export const engagorRouter = new EngagorRouter().router;
