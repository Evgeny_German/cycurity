import { KolJob } from "./kolJob";
import * as engagor from "./engagor";
import {
  KolConfig,
  RangeKolConfigItem
} from "../../Common/kol/model/kol-config";
import * as URL from "url";
import { ProjectManagementFacade } from "./facades/projectManagementFacade";
import { TwitterFacade } from "./facades/twitterFacade";
import { delay } from "../../infrastructure/utils/delay";
import * as superagent from "superagent";
import { getUser } from "./index";
import { setTimeout } from "timers";

export class KolWorker {
  public async doJob(job: KolJob): Promise<void> {
    async function refresh() {
      await delay(60000);
      if (job.inboxes().length) {
        job.next(
          job.nextUrl
            ? job.nextUrl
            : `https://api.engagor.com/${
                job.accountId
              }/inbox/mentions?sort=dateadd:asc&access_token=${
                job.token
              }&topicIds=${job
                .inboxes()
                .join(",")}&limit=100&filter=${encodeURIComponent(
                `-usertag:kol-handled`
              )}`
        );
        return;
      }
      refresh();
    }

    if (!job.inboxes().length) {
      refresh();
      return;
    }
    const tFacade = new TwitterFacade();
    const facade = new ProjectManagementFacade();

    let nextCalled = false;
    let page: any;
    try {
      page = await engagor.getPage(
        job.nextUrl
          ? job.nextUrl
          : `https://api.engagor.com/${
              job.accountId
            }/inbox/mentions?sort=dateadd:asc&access_token=${
              job.token
            }&topicIds=${job
              .inboxes()
              .join(",")}&limit=100&filter=${encodeURIComponent(
              `-usertag:kol-handled`
            )}`
      );
      if (page.response.paging.next_url) {
        nextCalled = true;
        job.next(page.response.paging.next_url);
      }
      for (let i = 0; i < page.response.data.length; i++) {
        const mention = page.response.data[i];
        if (mention.source.service !== "twitter") continue;
        let parsedUrl = URL.parse(mention.source.url);
        let userName = decodeURIComponent(parsedUrl.path.split("/")[1]);
        try {
          const resp = await superagent.get(
            `https://twitter.com/${userName}/status/${
              parsedUrl.path.split("/")[3]
            }`
          );
          if (resp["redirects"] && resp["redirects"].length) {
            parsedUrl = URL.parse(resp["redirects"][0]);
          }
          userName = parsedUrl.path.split("/")[1];
        } catch (error) {}
        const subproject = await job.getSubproject(mention.topic.id);
        if (!subproject) return;
        let profile = await facade.getProfile(userName, subproject.id);
        const filterConfig = <KolConfig>subproject.kolFilterConfig;
        if (!profile) {
          const p = (await tFacade.profileAndStatisticsScrape([userName]))
            .model;
          const score = this.calculateKol(filterConfig, p[Object.keys(p)[0]]);
          profile = await facade.setProfile({
            user_name: userName,
            score: score,
            subprojectId: subproject.id
          });
          const stringifiedScore =
            profile.score <= 0
              ? 0
              : profile.score <= 0.2
                ? 1
                : profile.score <= 0.4
                  ? 2
                  : profile.score <= 0.6
                    ? 3
                    : profile.score <= 0.8 ? 4 : profile.score <= 1 ? 5 : 6;
          await engagor.tagMention(
            job.token,
            job.accountId,
            mention.topic.id,
            mention.id,
            ["kol-handled", `kol-${stringifiedScore}`]
          );
          console.log("Tagged profile: ", profile);
        }
      }
    } catch (error) {
      if (error.status == 400) {
        setTimeout(() => job.next(job.nextUrl), 3600000);
      }
    }
  }

  private calculateKol(config: KolConfig, profile: any): number {
    const following = profile.profile.friends_count;
    const followers = profile.profile.followers_count;
    const favorites = profile.profile.favourites_count;
    const followersToFollowing = following ? followers / following : 0;
    const range = KolWorker.timerange(profile.replies);
    const tweets = profile.replies.filter(reply => !reply.isReply);
    const replies = profile.replies.filter(reply => reply.isReply);
    const avgTweetsPerDay = range ? tweets.length / range : 0;
    const avgRepliesPerDay = range ? replies.length / range : 0;
    const repliesToTweets = avgTweetsPerDay
      ? avgRepliesPerDay / avgTweetsPerDay
      : 0;
    const avgRetweets = profile.replies.length
      ? profile.replies.reduce(
          (p, c) => (Number(c.retweetCount) ? p + Number(c.retweetCount) : p),
          0
        ) / profile.replies.length
      : 0;
    const avgReplies = profile.replies.length
      ? profile.replies.reduce(
          (p, c) => (Number(c.replyCount) ? p + Number(c.replyCount) : p),
          0
        ) / profile.replies.length
      : 0;
    const avgFavorites = profile.replies.length
      ? profile.replies.reduce(
          (p, c) => (
            Number(c.favoriteCount) ? p + Number(c.favoriteCount) : p, 0
          )
        ) / profile.replies.length
      : 0;
    const loc = profile.profile.extras.filter(e => e.key === "location");
    const location = loc ? loc.value : undefined;
    const language = profile.profile.lang;
    const b = profile.profile.extras.filter(e => e.key === "bio");
    const bio = b ? b.value : undefined;
    const allText = profile.replies.reduce((p, c) => `${p}${c}`, '');

    // Threshold:

    function validateRange(config: RangeKolConfigItem, value: number): boolean {
      return !(
        config &&
        config.importance &&
        ((config.from && value < config.from) ||
          (config.to && value > config.to))
      );
    }

    if (!validateRange(config.following, following)) return 0;
    if (!validateRange(config.followers, followers)) return 0;
    if (!validateRange(config.favorites, favorites)) return 0;
    if (!validateRange(config.followersToFollowingRate, followersToFollowing))
      return 0;
    if (!validateRange(config.averageTweetsPerDay, avgTweetsPerDay)) return 0;
    if (!validateRange(config.averageRepliesPerDay, avgRepliesPerDay)) return 0;
    if (!validateRange(config.repliesToTweetRate, repliesToTweets)) return 0;
    if (!validateRange(config.averageRetweets, avgRetweets)) return 0;
    if (!validateRange(config.averageReplies, avgReplies)) return 0;
    if (!validateRange(config.averageFavorites, avgFavorites)) return 0;
    if (config.location && config.location.value) {
      if (
        !config.location.value
          .map(loc => loc.toLowerCase())
          .some(l => l.includes(location.trim().toLowerCase()))
      )
        return 0;
    }
    if (config.language && config.language.value) {
      if (
        !config.language.value
          .map(loc => loc.toLowerCase())
          .some(l => l.includes(language.trim().toLowerCase()))
      )
        return 0;
    }

    // Calculation:

    return (
      ([
        { config: config.following, value: following },
        { config: config.followers, value: followers },
        {
          config: config.followersToFollowingRate,
          value: followersToFollowing
        },
        { config: config.averageTweetsPerDay, value: avgTweetsPerDay },
        { config: config.averageRepliesPerDay, value: avgRepliesPerDay },
        { config: config.repliesToTweetRate, value: repliesToTweets },
        { config: config.averageRetweets, value: avgRetweets },
        { config: config.averageReplies, value: avgReplies },
        { config: config.averageFavorites, value: avgFavorites }
      ].reduce((sum, current) => {
        if (
          !current.config ||
          !("from" in current.config) ||
          !("to" in current.config) ||
          !current.config.importance
        )
          return sum;
        const midRange = (current.config.to - current.config.from) / 2;
        const mid = midRange + current.config.from;
        const val = Math.abs(
          ((current.value >= mid ? current.config.to : current.config.from) -
            current.value) /
            midRange
        );
        return sum + current.config.importance * val;
      }, 0) +
        [
          { config: config.bioKeywords, value: bio },
          { config: config.tweetsKeywords, value: allText }
        ].reduce((sum, current) => {
          if (!current.config || !current.config.value) return sum;
          const foundWords = current.config.value
            .map(word =>
              occurrences(current.value.toLowerCase(), word.toLowerCase())
            )
            .filter(w => w > 0);
          return (
            current.config.importance *
            (foundWords.length + 1 / current.config.value.length)
          );
        }, 0)) /
      [
        config.following,
        config.followers,
        config.followersToFollowingRate,
        config.averageTweetsPerDay,
        config.repliesToTweetRate,
        config.averageRetweets,
        config.averageReplies,
        config.averageFavorites,
        config.bioKeywords,
        config.tweetsKeywords
      ]
        .filter(c => c && c.importance)
        .reduce((s, c) => s + c.importance, 0)
    );
  }

  private static timerange(replies: any[]): number {
    const tweetDates = replies.map(r => r.timestamp);
    const min = Math.min(...tweetDates);
    const max = Math.max(...tweetDates);

    return (max - min) / 1000 / 60 / 60 / 24;
  }
}

function occurrences(string, subString) {
  string += "";
  subString += "";
  if (subString.length <= 0) return string.length + 1;

  var n = 0,
    pos = 0,
    step = subString.length;

  while (true) {
    pos = string.indexOf(subString, pos);
    if (pos >= 0) {
      ++n;
      pos += step;
    } else break;
  }
  return n;
}
