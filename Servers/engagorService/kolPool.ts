import { ResourcePool } from "../../infrastructure/resourcePool/resourcePool";
import { KolWorker } from "./kolWorker";
import { KolJob } from "./kolJob";
import { delay } from "../../infrastructure/utils/delay";
import { ProjectManagementFacade } from "./facades/projectManagementFacade";

export class KolPool {

    private _inboxes: string[] = [];

    private _resourcePool: ResourcePool<KolWorker>;

    private _isRunning: boolean;

    constructor(count: any,
        public readonly token: string,
        public readonly accountId: string,
        public readonly getSubproject: (inboxId: any) => Promise<any>) {
        this._resourcePool = new ResourcePool<KolWorker>({
            getContext: id => ({ id: id }),
            getResource: async context => new KolWorker(),
            minCount: 10,
            maxCount: 10,
            maxLifeTimeSec: 9999999,
            runTaskTimeout: 9999999
        });
    }

    public async start(): Promise<void> {
        this._isRunning = true;

        const facade = new ProjectManagementFacade();
        const self = this;

        async function refresh() {
            if (!self._isRunning) return;
            let inboxes = await facade.getInboxes();
            inboxes = inboxes.filter(inbox => 'subprojectId' in inbox);
            self._inboxes = inboxes;
            await delay(60000);
            if (self._isRunning) refresh();
        }

        await refresh();

        let jobFromUrl: (url: string) => Promise<void>;
        jobFromUrl = async (url: string) => this._resourcePool.run(worker => worker.doJob(new KolJob(this.token, this.accountId, this.getSubproject, jobFromUrl, () => this._inboxes, url)), 10);
        return await this._resourcePool.run(worker => worker.doJob(new KolJob(this.token, this.accountId, this.getSubproject, jobFromUrl, () => this._inboxes)), 10);
    }

    public async stop(): Promise<void> {
        this._isRunning = false;
        return await this._resourcePool.stop();
    }

}
