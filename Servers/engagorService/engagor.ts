import * as url from 'url';
import * as https from 'https';
import * as request from 'superagent';
import {Chrome, patchWindowSetup} from '../../infrastructure/chromeRunner/chrome';
import {timeout} from "../../infrastructure/utils/timeout";
import {spinWait} from "../../infrastructure/utils/spinWait";

export const engagorScopes: () => { identify: string, accounts_read: string, accounts_write: string, socialprofiles: string, email: string } = () =>
    ({
        identify: 'identify',
        accounts_read: 'accounts_read',
        accounts_write: 'accounts_write',
        socialprofiles: 'socialprofiles',
        email: 'email'
    });

export async function getEngagorAuthCodeChrome(clientId: string, userName: string, password: string, scope: string[], redirectPort: number): Promise<string> {
    let chrome: Chrome;
    let result: string;
    return <string>await timeout(async () => {
        chrome = new Chrome(true, 9232, null, true, `${__dirname}/temp/`);
        await chrome.start(true);
        await chrome.startCdp({port: 9232});
        await chrome.cdp.Runtime.enable();
        await chrome.cdp.Network.enable();
        chrome.cdp.on('Runtime.consoleAPICalled', (resp) => {
            console.log('Client console api: ', resp);
        });
        chrome.cdp.on('Network.requestWillBeSent', (event) => {
            if (event.request.url.indexOf(`//localhost:${redirectPort}/`) >= 0) {
                chrome.stop(true);
                result = url.parse(event.request.url).query.toString().split('&').find(param => param.split('=')[0] === 'code').split('=')[1];
            }
        });
        await chrome.navigate(`https://app.engagor.com/oauth/authorize/?client_id=${clientId}&response_type=code&scope=${scope.join(' ')}`, true);
        await chrome.evaluate(() => new Promise((resolve2, reject2) => {
            setTimeout(() => {
                document.getElementById('emailInput').setAttribute('value', userName);
                document.getElementById('passwordInput').setAttribute('value', password);
                document.getElementById('loginButton').click();
                resolve2();
            }, 3000);
        }), patchWindowSetup(
            {
                userName: userName,
                password: password
            }));
        await spinWait(() => !!result);
        return result;
    }, 30000).catch(err => {
        if (chrome) {
            chrome.stop();
        }
        throw err;
    });
}

export function getEngagorAccessToken(clientId: string, clientSecret: string, code: string): Promise<{}> {
    return new Promise((resolve, reject) => {
        let data = '';
        https.get(`https://app.engagor.com/oauth/access_token/?client_id=${clientId}&client_secret=${clientSecret}&grant_type=authorization_code&code=${code}`, res => {
            res.on('error', error => reject(error));
            res.on('data', chunk => data += chunk);
            res.on('end', () => resolve(JSON.parse(data)));
        });
    });
}

export function refreshEngagorAccessToken(clientId: string, clientSecret: string, refreshToken: string): Promise<{}> {
    return new Promise((resolve, reject) => {
        let data = '';
        https.get(`https://app.engagor.com/oauth/access_token/?client_id=${clientId}&client_secret=${clientSecret}&grant_type=refresh_token&refresh_token=${refreshToken}`, res => {
            res.on('error', error => reject(error));
            res.on('data', chunk => data += chunk);
            res.on('end', () => resolve(JSON.parse(data)));
        });
    });
}

export function getPage(url: string): Promise<any> {
    return new Promise((resolve, reject) => {
        let data = '';
        https.get(url, res => {
            res.on('data', chunk => data += chunk);
            res.on('end', () => resolve(JSON.parse(data)));
        });
    });
}

export function getMentions(accountId: string, token: string, filter: string): Promise<any> {
    return new Promise((resolve, reject) => {
        let data = '';
        https.get(`https://api.engagor.com/${accountId}/inbox/mentions?access_token=${token}${filter ? `&filter=${filter}` : ''}`, res => {
            res.on('data', chunk => data += chunk);
            res.on('end', () => resolve(JSON.parse(data)));
        });
    });
}

function urlParam(param: any) {
    return encodeURIComponent(JSON.stringify(param));
}

export async function tagMention(token: string, accountId: string, topicId: string, mentionId: string, tags: string[]): Promise<any> {
    return await request.post(`https://api.engagor.com/${accountId}/inbox/mention/${topicId}/${mentionId}?access_token=${token}&updates=${urlParam({tags: tags})}&options=${urlParam({tags_edit_mode: 'add'})}`).send().then(res => res.body);
}

export async function untagMention(token: string, accountId: string, topicId: string, mentionId: string, tags: string[]): Promise<any> {
    return await request.post(`https://api.engagor.com/${accountId}/inbox/mention/${topicId}/${mentionId}?access_token=${token}&updates=${urlParam({tags: tags})}&options=${urlParam({tags_edit_mode: 'delete'})}`).send().then(res => res.body);
}

export async function getInboxes(token: string): Promise<any> {
    return await request.get(`https://api.engagor.com/me/accounts?access_token=${token}`).send().ok(res => true).then(res => res.body.response.data.map(account => account.projects.map(project => project.topics.map(topic => Object.assign(topic, {accountId: account.id}))).reduce((a, b) => [...a, ...b], [])).reduce((a, b) => [...a, ...b], []));
}

export function getAccounts(token: string): Promise<any[]> {
    return new Promise((resolve, reject) => {
        let data = '';
        https.get(`https://api.engagor.com/me/accounts?access_token=${token}`, res => {
            res.on('error', error => reject(error));
            res.on('data', chunk => data += chunk);
            res.on('end', () => resolve(JSON.parse(data)));
        });
    });
}
