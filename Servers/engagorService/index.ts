import {EngagorServer} from './server';
import * as dotEnv from 'dotenv';
import * as path from "path";
import * as engagor from './engagor';
import * as superagent from 'superagent';
import {KolPool} from "./kolPool";
import {ProjectManagementFacade} from "./facades/projectManagementFacade";

dotEnv.config({path: path.join(__dirname, './config/.env')});

const config = require('./config.json');

const server = new EngagorServer();

let token: any;

let user: any;
let cycToken: string;

export function getUser() {
    return user;
}

server.startServer();

(async () => {
    const t = (await superagent.post(`${process.env["USER_MANAGEMENT_URL"]}/api//cy_users/login`).ok(o => true).send({email:process.env["KOL_BOT_USERNAME"], password: process.env["KOL_BOT_PASSWORD"]})).body;
    user = t.token.user;
    cycToken = t.token.token;
    const facade = new ProjectManagementFacade();
    const code = await engagor.getEngagorAuthCodeChrome(config.clientId, config.userName, config.password,
        [
            engagor.engagorScopes().accounts_read,
            engagor.engagorScopes().accounts_write,
            engagor.engagorScopes().email,
            engagor.engagorScopes().identify,
            engagor.engagorScopes().socialprofiles
        ], config.redirectPort);
    token = <any>await engagor.getEngagorAccessToken(config.clientId, config.clientSecret, code);
    // const kolPool = new KolPool(10, token.access_token, '16499', async inboxId => await facade.getSubprojectByInbox(inboxId));
    // kolPool.start();
})();

export function getToken() {return token;}