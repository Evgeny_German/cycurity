import { engagorRouter } from './routes';
import * as express from 'express';
import { BaseExpressServer } from '../../infrastructure/baseExpressService/BaseExpressServer';

export class EngagorServer extends BaseExpressServer {
    protected getAppName(): string {
        return 'engagorService';
    }
    protected setRoutes(app: express.Express): void {
        app.use('/engagor', engagorRouter);
    }
}