import * as fs from "fs";
import * as path from "path";
import * as dotenv from "dotenv";
import * as superagent from "superagent";

import {
  KolConfig,
  RangeKolConfigItem
} from "../../Common/kol/model/kol-config";

dotenv.config({ path: path.join(__dirname, "./config/.env") });

const inputPath = path.join(__dirname, process.argv[process.argv.length - 1]);

console.log("input path:", inputPath);

let _state: {
  input: string;
  index: number;
  settings: KolConfig;
  userId: number;
} = JSON.parse(fs.readFileSync(inputPath).toString("utf8"));

const state = {
  get value(): {
    input: string;
    index: number;
    settings: KolConfig;
    userId: number;
  } {
    return _state;
  },
  set value(val: {
    input: string;
    index: number;
    settings: KolConfig;
    userId: number;
  }) {
    const prev = _state;
    _state = val;

    fs.writeFileSync(inputPath, Buffer.from(JSON.stringify(val), "utf8"));
  }
};

const input: { userName: string; tweetId: string }[] = require(state.value
  .input);
console.log("length: ", input.length);
const outputPath = path.join(__dirname, "./output/");

(async () => {
  if (!fs.existsSync(outputPath)) {
    fs.mkdirSync(outputPath);
  }
  while (!state.value.index || state.value.index < input.length) {
    try {
      const tweet = input[state.value.index];
      const tPath = path.join(outputPath, `${tweet.userName}.json`);
      if (!fs.existsSync(tPath)) {
        console.log(`scraping profile @`, tweet.userName);
        try {
          const profile = (await profileAndStatisticsScrape(
            state.value.userId,
            [tweet.userName]
          )).model[tweet.userName];
          const score = calculateKol(state.value.settings, profile);
          const stringifiedScore =
            score <= 0
              ? 0
              : score <= 0.2
                ? 1
                : score <= 0.4
                  ? 2
                  : score <= 0.6 ? 3 : score <= 0.8 ? 4 : score <= 1 ? 5 : 6;
          fs.writeFileSync(
            tPath,
            Buffer.from(
              JSON.stringify({ tweet, profile, score, stringifiedScore }),
              "utf8"
            )
          );
          console.log(`done profile @`, tweet.userName, ", score: ", score);
        } catch (error) {
          fs.writeFileSync(tPath, Buffer.from(JSON.stringify(error), "utf8"));
          console.error("error in profile @", tweet.userName, ": ", error);
          throw error;
        }
      }
    } catch (error) {}
    state.value = Object.assign(state.value, {
      index: (state.value.index || 0) + 1
    });
  }
  console.log("done.");
})();

function profileAndStatisticsScrape(
  userId: number,
  userNames: string[],
  concurrency?: number
): Promise<any> {
  return superagent
    .post(
      `${process.env.TWITTER_URL}/scrape/profileAndStatistics${
        concurrency ? `?concurrency=${concurrency}` : ``
      }`
    )
    .set({ user_id: userId })
    .send({ userNames: userNames })
    .then(res => res.body);
}

function calculateKol(config: KolConfig, profile: any): number {
  const following = profile.profile.friends_count;
  const followers = profile.profile.followers_count;
  const favorites = profile.profile.favourites_count;
  const followersToFollowing = following ? followers / following : 0;
  const range = timerange(profile.replies);
  const tweets = profile.replies.filter(reply => !reply.isReply);
  const replies = profile.replies.filter(reply => reply.isReply);
  const avgTweetsPerDay = range ? tweets.length / range : 0;
  const avgRepliesPerDay = range ? replies.length / range : 0;
  const repliesToTweets = avgTweetsPerDay
    ? avgRepliesPerDay / avgTweetsPerDay
    : 0;
  const avgRetweets = profile.replies.length
    ? profile.replies.reduce(
        (p, c) => (Number(c.retweetCount) ? p + Number(c.retweetCount) : p),
        0
      ) / profile.replies.length
    : 0;
  const avgReplies = profile.replies.length
    ? profile.replies.reduce(
        (p, c) => (Number(c.replyCount) ? p + Number(c.replyCount) : p),
        0
      ) / profile.replies.length
    : 0;
  const avgFavorites = profile.replies.length
    ? profile.replies.reduce(
        (p, c) => (Number(c.favoriteCount) ? p + Number(c.favoriteCount) : p, 0)
      ) / profile.replies.length
    : 0;
  const loc = profile.profile.extras.filter(e => e.key === "location");
  const location = loc ? loc.value : undefined;
  const language = profile.profile.lang;
  const b = profile.profile.extras.filter(e => e.key === "bio");
  const bio = b ? b.value : undefined;
  const allText = profile.replies.reduce((p, c) => `${p}${c}`);

  // Threshold:

  function validateRange(config: RangeKolConfigItem, value: number): boolean {
    return !(
      config &&
      config.importance &&
      ((config.from && value < config.from) || (config.to && value > config.to))
    );
  }

  if (!validateRange(config.following, following)) return 0;
  if (!validateRange(config.followers, followers)) return 0;
  if (!validateRange(config.favorites, favorites)) return 0;
  if (!validateRange(config.followersToFollowingRate, followersToFollowing))
    return 0;
  if (!validateRange(config.averageTweetsPerDay, avgTweetsPerDay)) return 0;
  if (!validateRange(config.averageRepliesPerDay, avgRepliesPerDay)) return 0;
  if (!validateRange(config.repliesToTweetRate, repliesToTweets)) return 0;
  if (!validateRange(config.averageRetweets, avgRetweets)) return 0;
  if (!validateRange(config.averageReplies, avgReplies)) return 0;
  if (!validateRange(config.averageFavorites, avgFavorites)) return 0;
  if (config.location && config.location.value) {
    if (
      !config.location.value
        .map(loc => loc.toLowerCase())
        .some(l => l.includes(location.trim().toLowerCase()))
    )
      return 0;
  }
  if (config.language && config.language.value) {
    if (
      !config.language.value
        .map(loc => loc.toLowerCase())
        .some(l => l.includes(language.trim().toLowerCase()))
    )
      return 0;
  }

  // Calculation:

  return (
    ([
      { config: config.following, value: following },
      { config: config.followers, value: followers },
      { config: config.followersToFollowingRate, value: followersToFollowing },
      { config: config.averageTweetsPerDay, value: avgTweetsPerDay },
      { config: config.averageRepliesPerDay, value: avgRepliesPerDay },
      { config: config.repliesToTweetRate, value: repliesToTweets },
      { config: config.averageRetweets, value: avgRetweets },
      { config: config.averageReplies, value: avgReplies },
      { config: config.averageFavorites, value: avgFavorites }
    ].reduce((sum, current) => {
      if (
        !current.config ||
        !("from" in current.config) ||
        !("to" in current.config) ||
        !current.config.importance
      )
        return sum;
      const midRange = (current.config.to - current.config.from) / 2;
      const mid = midRange + current.config.from;
      const val = Math.abs(
        ((current.value >= mid ? current.config.to : current.config.from) -
          current.value) /
          midRange
      );
      return sum + current.config.importance * val;
    }, 0) +
      [
        { config: config.bioKeywords, value: bio },
        { config: config.tweetsKeywords, value: allText }
      ].reduce((sum, current) => {
        if (!current.config || !current.config.value) return sum;
        const foundWords = current.config.value
          .map(word =>
            occurrences(current.value.toLowerCase(), word.toLowerCase())
          )
          .filter(w => w > 0);
        return (
          current.config.importance *
          (foundWords.length + 1 / current.config.value.length)
        );
      }, 0)) /
    [
      config.following,
      config.followers,
      config.followersToFollowingRate,
      config.averageTweetsPerDay,
      config.repliesToTweetRate,
      config.averageRetweets,
      config.averageReplies,
      config.averageFavorites,
      config.bioKeywords,
      config.tweetsKeywords
    ]
      .filter(c => c && c.importance)
      .reduce((s, c) => s + c.importance, 0)
  );
}

function timerange(replies: any[]): number {
  const tweetDates = replies.map(r => r.timestamp);
  const min = Math.min(...tweetDates);
  const max = Math.max(...tweetDates);

  return (max - min) / 1000 / 60 / 60 / 24;
}

function occurrences(string, subString) {
  string += "";
  subString += "";
  if (subString.length <= 0) return string.length + 1;

  var n = 0,
    pos = 0,
    step = subString.length;

  while (true) {
    pos = string.indexOf(subString, pos);
    if (pos >= 0) {
      ++n;
      pos += step;
    } else break;
  }
  return n;
}
