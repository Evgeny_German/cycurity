import * as request from 'superagent';
import {DocumentStorageFacade} from "./documentStorageFacade";
import {URL} from 'url';
import { getUser } from '../index';

export class TwitterFacade {

    private readonly baseUrl: string;

    constructor() {
        this.baseUrl = process.env['TWITTER_URL'];
    }

    public async profilesScrape(userNames: string[]): Promise<any> {
        const result = await request.post(`${this.baseUrl}/scrape/profiles`).set({user_id: getUser().id}).send({userNames: userNames});
        return result.body;
    }

    public profileAndStatisticsScrape(userNames: string[], concurrency?: number): any {
        return request.post(`${this.baseUrl}/scrape/profileAndStatistics${concurrency ? `?concurrency=${concurrency}` : ``}`).set({user_id: getUser().id}).send({userNames: userNames}).then(res => res.body);
    }

    public conversationByTweetScrape(tweetId: string, userName: string): any {
        return request.get(`${this.baseUrl}/scrape/conversationByTweet?tweetId=${tweetId}&userName=${userName}`).set({user_id: getUser().id}).then(res => res.body);
    }

    public async getProfile(
        userId: number,
        ownerId: number,
        userName: string
      ): Promise<any> {
        try {
          const facade = new DocumentStorageFacade();
          console.log(
            `twitterFacade: received profile request for user: ${userName}`
          );
          const latest = await facade
            .getLatest(userId, ownerId, 3, userName)
            .catch(error => {
              console.error("twitterFacade: error fetching latest.");
              throw error;
            })
            .then(result => {
              console.log("twitterFacade: got latest.");
              return result;
            });
          const oldDocument = latest && latest.data ? latest.data.data : null;
          if (oldDocument) return oldDocument;
          console.log(
            `twitterFacade: requesting profile from scraper for user: ${userName}`
          );
          const newDocument = await this.profilesScrape([userName])
            .catch(error => {
              console.error("twitterFacade: error scraping profile.");
              throw error;
            })
            .then(result => {
              console.log("twitterFacade: got profile from scraper.");
              return result["model"][userName];
            });
          await facade
            .saveLatest(userId, ownerId, 3, userName, newDocument)
            .catch(error => {
              console.error("twitterFacade: error saving latest.");
              throw error;
            })
            .then(result => {
              console.log("twitterFacade: saved latest.");
              return result;
            });
          return newDocument;
        } catch (error) {
          console.error("twitterFacade: error fetching profile.");
          throw error;
        }
      }
    }