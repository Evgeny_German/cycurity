import * as request from 'superagent';

export class DocumentStorageFacade {

    private readonly baseUrl: string;

    constructor() {
        this.baseUrl = process.env['DOCUMENT_STORAGE_URL'];
    }

    public attach(userId: number, relationId: number, objectId: number, subjectId: number): Promise<any> {
        return request.post(`${this.baseUrl}/api/documentRelations`).send({
            objectId: objectId,
            subjectId: subjectId,
            relationId: relationId,
            ownerId: userId
        }).then(res => res.body);
    }

    public detach(userId: number, relationId: number, objectId: number, subjectId: number): Promise<any> {
        return request.post(`${this.baseUrl}/api/documentRelations/detach`).send({
            objectId: objectId,
            subjectId: subjectId,
            relationId: relationId,
            ownerId: userId
        }).then(res => res.body);
    }

    public listRelations(userId: number, relationId: number): Promise<any> {
        return request.get(`${this.baseUrl}/api/documentRelations/byRelationId?userId=${userId}&relationId=${relationId}`).then(res => res.body);
    }

    public getLatest(userId: number, ownerId: number, type: number, documentId: string): Promise<any> {
        return request.get(`${this.baseUrl}/api/documents/getLatest?userId=${userId}&ownerId=${ownerId}&type=${type}&documentId=${documentId}`).then(res => res.body);
    }

    public purge(userId: number, ownerId: number, type: number, documentId: string): Promise<any> {
        return request.delete(`${this.baseUrl}/api/documents/purge?userId=${userId}&ownerId=${ownerId}&type=${type}&documentId=${documentId}`).then(res => res.body);
    }

    public saveLatest(userId: number, ownerId: number, type: number, documentId: string, data: object): Promise<any> {
        return request.post(`${this.baseUrl}/api/documents/saveLatest`).set({'Content-Type': 'application/json'}).send({
            userId: +userId,
            ownerId: +ownerId,
            type: type,
            documentId: documentId,
            data: data
        }).then(result => result.body);
    }

    public updateReportsPerConversation(userId: number, ownerId: number, data: any): Promise<any> {
        return request.post(`${this.baseUrl}/api/documents/updateReportsPerConversation`).set({'Content-Type': 'application/json'}).send({
            userId: userId,
            ownerId: ownerId,
            data: data
        }).then(result => result.body);
    }

    public search(userId: number, type: number, freeText: string): Promise<any> {
        const docStorageApi = `${this.baseUrl}/api/documents/search?userId=${userId}&type=${type}${freeText ? `&freeText=${freeText}` : ''}`
        console.log(`DocumentStorageFacade:search - search request for type :${type}, Free text: ${freeText} . Calling storage api: ${docStorageApi}`);
        return request.get(docStorageApi ).then(res => res.body);
    }
}
