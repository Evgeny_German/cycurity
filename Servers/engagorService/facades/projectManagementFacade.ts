import * as request from 'superagent';
import * as _ from 'lodash';
import { getUser } from '../index';

export class ProjectManagementFacade {

    private readonly baseUrl: string;

    constructor() {
        this.baseUrl = process.env['PROJECT_MANAGEMENT_URL'];
    }

    public async getInboxes(): Promise<any[]> {
        return (await request.get(`${this.baseUrl}/api/subprojects/engagorInboxes`).set({user_id: getUser().id})).body;
    }

    public async getSubproject(subprojectId: number): Promise<any> {
        const globalConfig = (await request.get(`${this.baseUrl}/api/globalKolConfigs/global`).set({user_id: getUser().id})).body.kol_filter_config;
        const subproject = (await request.get(`${this.baseUrl}/api/subprojects/${subprojectId}?filter=${encodeURIComponent(JSON.stringify({include: 'project'}))}`).set({user_id: getUser().id})).body;
        const projectConfig = subproject.project.kol_filter_config || {};
        const subprojectConfig = subproject.kol_filter_config || {};
        return Object.assign(subproject, {
            kolFilterConfig:
                _.merge(_.merge(globalConfig, projectConfig), subprojectConfig)
        });
    }

    public async getProfile(userName: string, subprojectId: number): Promise<any> {
        const profiles = (await request.get(`${this.baseUrl}/api/profile?filter=${encodeURIComponent(JSON.stringify({
            where: {user_name: userName, subprojectId: subprojectId},
            order: 'last_update DESC',
            limit: 1
        }))}`).set({user_id: getUser().id})).body;
        if (profiles && profiles.length) return profiles[0];
        return undefined;
    }

    public async setProfile(profile: any): Promise<any> {
        return (await request.post(`${this.baseUrl}/api/profile`).send(profile).set({user_id: getUser().id})).body;
    }

    public async getSubprojectByInbox(inboxId: number): Promise<any> {
        const globalConfig = (await request.get(`${this.baseUrl}/api/globalKolConfigs/global`).set({user_id: getUser().id})).body.kol_filter_config;
        const subprojects = (await request.get(`${this.baseUrl}/api/subprojects?filter=${encodeURIComponent(JSON.stringify({
            include: 'project',
            where: {inboxId: inboxId}
        }))}`).set({user_id: getUser().id})).body;
        const subproject = subprojects.length > 0 ? subprojects[0] : undefined;
        if (!subproject || !subproject.project) return null;
        const projectConfig = subproject.project.kol_filter_config || {};
        const subprojectConfig = subproject.kol_filter_config || {};
        return Object.assign(subproject, {
            kolFilterConfig:
                _.merge(_.merge(globalConfig, projectConfig), subprojectConfig)
        });
    }

}
