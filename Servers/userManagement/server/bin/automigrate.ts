import { app } from '../server';

export function autoMigrate(...tables: string[]): void {
  const ds = app['datasources'].cycuritydb;
  console.log(`Starting migrate to database ${ds.connector.clientConfig.host}`)
  ds.autoupdate(tables, err => {
    if (err) {
      console.log(err);
    } else {
      console.log(`Completed auto migrate of tables: ${tables.join(', ')}.`)
    }
  });
}
