import * as bcrypt from "bcrypt-nodejs";
import * as request from "superagent";
import * as path from "path";

var SALT_WORK_FACTOR = "cycurity";

module.exports = function(Cyuser) {
  Cyuser.createOptionsFromRemotingContext = function(ctx) {
    const base = this.base.createOptionsFromRemotingContext(ctx);
    const userId = ctx.req.headers["user_id"]
      ? +ctx.req.headers["user_id"]
      : null;
    return Object.assign(base, {
      userId: userId
    });
  };

  async function getUserData(userId: number, mdl?: string): Promise<any> {
    let userData: any;
    try {
      userData = await new Promise<any>((resolve, reject) => {
        Cyuser.findById(userId, (error: Error, result: any) => {
          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        });
      });
    } catch (error2) {
      const err = new Error("Unauthorized.");
      err["status"] = 401;
      throw err;
    }
    if (mdl) {
      if (
        !["admin", "superAdmin"]["includes"](userData.adminRole) &&
        (!userData.meta ||
          !userData.meta.modules ||
          !userData.meta.modules.includes(mdl))
      ) {
        const err = new Error("Unauthorized.");
        err["status"] = 401;
        throw err;
      }
    }
    return userData;
  }

  //validate unique email
  Cyuser.validatesUniquenessOf("email");

  //before saving user - encrypt passwsord
  Cyuser.observe("before save", (ctx, next) => {
    (async () => {
      if (!ctx.options.userId) {
        var err = new Error("Unauthorized.");
        err["status"] = 401;
        next(err);
        return;
      }
      let userData: any;
      try {
        userData = await getUserData(ctx.options.userId);
      } catch (error) {
        let err = new Error("Unauthorized.");
        err["status"] = 401;
        next(err);
        return;
      }
      const validatePassword = (password: string, user: any): boolean => {
        return bcrypt.compareSync(password, user.password);
      };
      if (ctx.instance) {
        if (ctx.instance.password) {
          if (ctx.isNewInstance) {
            var hash = bcrypt.hashSync(ctx.instance.password);
            ctx.instance.password = hash;
            if (
              userData.adminRole === "superadmin" ||
              (userData.meta &&
                userData.meta.modules &&
                userData.meta.modules["users"])
            ) {
              next();
              return;
            }
            let err = new Error("Unauthorized.");
            err["status"] = 401;
            next(err);
            return;
          } else {
            if (
              ctx.instance.id !== userData.id &&
              userData.adminRole !== "superadmin" &&
              !(
                userData.meta &&
                userData.meta.modules &&
                userData.meta.modules["users"]
              )
            ) {
              const err = new Error(
                "oldPassword must be provided, please use PATCH."
              );
              err["statusCode"] = 401;
              next(err);
              return;
            }
            if (!ctx.instance.oldPassword) {
              const err = new Error(
                "oldPassword must be provided, please use PATCH."
              );
              err["statusCode"] = 401;
              next(err);
              return;
            }
            Cyuser.findOne({ where: { id: ctx.instance.id } }, (err, user) => {
              if (err || !user) {
                const err = new Error("Internal server error");
                err["statusCode"] = 500;
                next(err);
                return;
              }
              if (validatePassword(ctx.instance.oldPassword, user)) {
                var hash = bcrypt.hashSync(ctx.instance.password);
                ctx.instance.password = hash;
                next();
                return;
              } else {
                const err = new Error("oldPassword is incorrect");
                err["statusCode"] = 401;
                next(err);
              }
            });
          }
        } else {
          next();
          return;
        }
      } else if (ctx.data) {
        if (
          ctx.data.id !== userData.id &&
          userData.adminRole !== "superadmin" &&
          !(
            userData.meta &&
            userData.meta.modules &&
            userData.meta.modules["users"]
          )
        ) {
          const err = new Error("Unauthorized.");
          err["statusCode"] = 401;
          next(err);
          return;
        }
        if (ctx.data.password) {
          if (!ctx.data.oldPassword) {
            const err = new Error("oldPassword must be provided");
            err["statusCode"] = 401;
            next(err);
            return;
          }
          if (validatePassword(ctx.data.oldPassword, ctx.currentInstance)) {
            var hash = bcrypt.hashSync(ctx.data.password);
            ctx.data.password = hash;
            next();
            return;
          } else {
            const err = new Error("oldPassword is incorrect");
            err["statusCode"] = 401;
            next(err);
          }
        } else {
          next();
          return;
        }
      }
    })();
  });

  //after return result - never send the password
  Cyuser.afterRemote("**", (ctx, user, next) => {
    (async () => {
      if (
        ctx.methodString === "cy_user.login" ||
        ctx.methodString === "cy_user.tokenLogin"
      ) {
        next();
        return;
      }
      if (!ctx.req.headers["user_id"]) {
        var err = new Error("Unauthorized.");
        err["status"] = 401;
        next(err);
        return;
      }
      let userData: any;
      try {
        userData = await getUserData(ctx.req.headers["user_id"]);
      } catch (error) {
        var err = new Error("Unauthorized.");
        err["status"] = 401;
        next(err);
        return;
      }
      if (ctx.result) {
        if (ctx.result.length) {
          ctx.result.forEach(user => {
            if (user.id !== userData.id && userData.adminRole !== "admin" && userData.adminRole !== "superadmin" && !(userData.meta && userData.meta.modules && userData.meta.modules["users"])) {
              user.meta = null;
            }
            user.password = null;
            if (user.oldPassword) {
              delete user.oldPassword;
            }
          });
        } else if (ctx.result.password) {
          if (ctx.result.id !== userData.id && userData.adminRole !== "admin" && userData.adminRole !== "superadmin" && !(userData.meta && userData.meta.modules && userData.meta.modules["users"])) {
            ctx.result.meta = null;
          }
          ctx.result.password = null;
          if (ctx.result.oldPassword) {
            delete ctx.result.oldPassword;
          }
        }
      }
      next();
    })();
  });

  //add login remote method
  Cyuser.login = (email: string, password: string, cb) => {
    if (!email || !password) {
      cb(null, "", "email or password are not valid");
      return;
    }
    //find user by email
    Cyuser.findOne({ where: { email: email } }, (err, user) => {
      //if user not exist return error
      if (err || !user) {
        cb(null, "", "email or password are not valid");
        return;
      } else {
        //compare password to hashed password
        var isSame = bcrypt.compareSync(password, user.password);
        if (isSame) {
          getAuthToken(user.id)
            .then(tokenRes => {
              user.password = null;
              cb(null, { token: tokenRes.token, user: user }, "");
            })
            .catch(err => {
              cb(null, "", err);
            });
        } else {
          cb(null, "", "email or password are not valid");
        }
      }
    });
  };

  Cyuser.tokenLogin = (
    token: string,
    mdl: string = null,
    callback: (error: Error, result: any) => void
  ): void => {
    (async () => {
      let user: any;
      try {
        user = (await request.get(
          `${
            process.env.authServer
          }/auth/authenticateToken?token=${encodeURIComponent(token)}`
        )).body;
      } catch (error) {
        const err = new Error("Unauthorized.");
        err["status"] = 401;
        callback(err, null);
      }
      if (!user || !user.id) {
        throw new Error("User encoded in token is invalid.");
      }
      let userData: any;
      try {
        userData = await getUserData(user.id, mdl);
      } catch (error) {
        const err = new Error("Unauthorized.");
        err["status"] = 401;
        callback(err, null);
        return;
      }
      callback(null, userData);
    })();
  };

  Cyuser.remoteMethod("tokenLogin", {
    accepts: [
      { arg: "token", type: "string" },
      { arg: "module", type: "string", required: false }
    ],
    returns: [{ root: true, type: "object" }],
    http: { verb: "GET" }
  });

  Cyuser.remoteMethod("login", {
    accepts: [
      { arg: "email", type: "string" },
      { arg: "password", type: "string" }
    ],
    returns: [
      { arg: "token", type: "string" },
      { arg: "user", type: "object" },
      { arg: "errorMessage", type: "string" }
    ]
  });
};

//call the auth service to create a token + define the experation time of the token
const getAuthToken = (userid: number): Promise<any> => {
  let url = `${process.env.authServer}/auth/createToken`;
  return request
    .post(url)
    .send({ tokenData: { id: userid }, expires: "10d" })
    .then(res => {
      return res.body;
    });
};
