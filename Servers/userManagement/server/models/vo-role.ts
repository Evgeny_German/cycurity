export default function(VoRole) {
  VoRole.createOptionsFromRemotingContext = function(ctx) {
    const base = this.base.createOptionsFromRemotingContext(ctx);
    const userId = ctx.req.headers["user_id"]
      ? +ctx.req.headers["user_id"]
      : null;
    ctx.req.userId = userId;
    return Object.assign(base, {
      userId: userId
    });
  };

  VoRole.beforeRemote("**", (ctx, role, next) => {
    const userId = ctx.req.userId;
    (async () => {
      let app: any;
      try {
        app = await new Promise<any>((resolve, reject) => {
          VoRole.getApp((error, result) => {
            if (error) {
              reject(error);
              return;
            }
            resolve(result);
          });
        });
      } catch (error) {
        const newError = new Error("Forbidden");
        newError["statusCode"] = 403;
        next(newError);
        return;
      }
      let user: any;
      try {
        user = await new Promise<any>((resolve, reject) => {
          app.models.cy_user.findById(userId, (error, result) => {
            if (error) {
              reject(error);
              return;
            }
            resolve(result);
          });
        });
      } catch (error) {
        const newError = new Error("Forbidden");
        newError["statusCode"] = 403;
        next(newError);
        return;
      }

      if (ctx.method.sharedClass.name === "voRole") {
        const write = ctx.method.accessType.toUpperCase() === "WRITE";
        if (write) {
          if (
            !(
              user.adminRole === "superadmin" ||
              (user.meta && user.meta.modules && user.meta.modules["users"])
            )
          ) {
            const newError = new Error("Forbidden");
            newError["statusCode"] = 403;
            next(newError);
            return;
          }
        }
      }
      next();
    })();
  });
}
