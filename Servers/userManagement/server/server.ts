'use strict';
import {autoMigrate} from "./bin/automigrate";

import * as dotEnv from "dotenv";
import * as path from "path"


var loopback = require('loopback');
var boot = require('loopback-boot');

//add dotenv support
let configPath = path.join(__dirname,"./config/.env")
dotEnv.config({path: configPath});

export const app = loopback();

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('User management server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
    autoMigrate('cy_user', 'voRole');
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});


