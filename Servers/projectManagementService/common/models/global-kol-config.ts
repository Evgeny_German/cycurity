'use strict';

export default function (globalKolConfig) {
  globalKolConfig.get = (callback: (error: Error, result: any) => void) => {
    globalKolConfig.findOne({order: 'id DESC'}, (error, result) => {
      if (!result) {
        callback(error, result);
        return;
      }
      callback(null, result.kol_filter_config);
    });
  };
  globalKolConfig.set = (value: any, callback: (error: Error, result: any) => void) => {
    globalKolConfig.destroyAll(undefined, (error, result) => {
      globalKolConfig.create({kol_filter_config: value}, (error2, result2) => {
        if (!result2) {callback(error2, result2);return;}
        callback(null, result2.kol_filter_config);
      });
    });
  };
  globalKolConfig.remoteMethod('get', {
    returns: [
      {arg: 'kol_filter_config', type: 'object'}
    ],
    http: {path: '/global', verb: 'get'}
  });
  globalKolConfig.remoteMethod('set', {
    accepts: [
      {arg: 'kol_filter_config', type: 'object', required: true, http: ctx => ctx.req.body['kol_filter_config']}
    ],
    returns: [
      {arg: 'kol_filter_config', type: 'object'}
    ],
    http: {path: '/global', verb: 'post'}
  });
}
