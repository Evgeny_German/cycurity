module.exports = function(Project) {
  Project.createOptionsFromRemotingContext = function(ctx) {
    const base = this.base.createOptionsFromRemotingContext(ctx);
    return Object.assign(base, {
      userId: +ctx.req.headers.user_id,
      user: ctx.req.user
    });
  };

  const writeGuard = (user, isNewInstance, data, where, next) => {
    (async () => {
      if (isNewInstance) {
        if ((await isProjectAllowed(user, null)) !== "editor") {
          const error = new Error("Unauthorized.");
          error["status"] = 401;
          next(error);
          return;
        }
      } else {
        if (data) {
          if (!("length" in data)) {
            if ((await isProjectAllowed(user, data.id)) !== "editor") {
              const error = new Error("Unauthorized.");
              error["status"] = 401;
              next(error);
              return;
            }
          } else {
            if (data.length) {
              for (let item of data) {
                if ((await isProjectAllowed(user, item.id)) !== "editor") {
                  const error = new Error("Unauthorized.");
                  error["status"] = 401;
                  next(error);
                  return;
                }
              }
            }
          }
        } else if (where) {
          const projects = await new Promise<any[]>((resolve, reject) => {
            Project.find({where}, (error, result) => {
              if (error) {
                reject(error);
                return;
              }
              resolve(result);
            });
          });
          if ((await Promise.all(projects.map(project => isProjectAllowed(user, project.id)))).some(perm => perm !== "editor")) {
            const error = new Error("Unauthorized.");
            error["status"] = 401;
            next(error);
            return;
          }
        } else {
          const error = new Error("Unauthorized.");
          error["status"] = 401;
          next(error);
          return;
        }
      }
      if (isNewInstance) {
        if (data) {
          if (!("length" in data)) {
            data.owner_id = user.id;
          } else {
            if (data.length) {
              for (let item of data.length) {
                item.owner_id = user.id;
              }
            }
          }
        }
      }
      if (data) {
        if (!("length" in data)) {
          data.last_update = new Date();
        } else {
          if (data.length) {
            for (let item of data) {
              item.last_update = new Date();
            }
          }
        }
      }
      next();
    })();
  };

  Project.observe("before save", (ctx, next) =>
    writeGuard(
      ctx.options.user,
      ctx.isNewInstance,
      ctx.instance || ctx.data,
      null,
      next
    )
  );

  Project.observe("before delete", (ctx, next) =>
    writeGuard(ctx.options.user, false, null, ctx.where, next)
  );

  const isProjectUserAllowed = async (
    user: any,
    projectuser_id: number
  ): Promise<"member" | "editor" | false> => {
    if (!projectuser_id)
      return user.adminRole === "superadmin" ||
        (user.meta && user.meta.modules && user.meta.modules["projects"])
        ? "editor"
        : user.adminRole === "admin"
          ? "member"
          : false;
    if (
      user.adminRole !== "admin" &&
      user.adminRole !== "superadmin" &&
      !(user.meta && user.meta.modules && user.meta.modules["projects"])
    ) {
      return false;
    }
    if (user.adminRole === "superadmin") {
      return "editor";
    }
    const app = await new Promise<any>((resolve, reject) => {
      Project.getApp((err, app) => {
        if (err || !app) {
          reject(err);
          return;
        }
        resolve(app);
      });
    });
    const projectUser = await new Promise<any>((resolve, reject) => {
      app.models.projectUser.findOne(
        { where: { id: projectuser_id } },
        (error, result) => {
          if (error || !result) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    const project = await new Promise<any>((resolve, reject) => {
      app.models.project.findOne(
        { where: { id: projectUser.project_id } },
        (error, result) => {
          if (!result) {
            resolve(null);
            return;
          }

          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if (!project) return false;
    if (!project.owner_id || project.owner_id === user.id) return "editor";
    const projectUsers = await new Promise<any>((resolve, reject) => {
      app.models["projectUser"].find(
        { where: { project_id: project.id, user_id: user.id } },
        (error, result) => {
          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if ((user.adminRole === "admin" && user.meta && user.meta.modules && user.meta.modules["projects"]) || projectUsers.some(projectUser => projectUser.role === "editor")) {
      return "editor";
    }
    if (
      user.adminRole === "admin" ||
      projectUsers.some(projectUser => projectUser.role === "member")
    ) {
      return "member";
    }
    return false;
  };

  const isSubprojectAllowed = async (
    user: any,
    subproject_id: number
  ): Promise<"member" | "editor" | false> => {
    if (!subproject_id)
      return user.adminRole === "superadmin" ||
        (user.meta && user.meta.modules && user.meta.modules["projects"])
        ? "editor"
        : user.adminRole === "admin"
          ? "member"
          : false;
    if (
      user.adminRole !== "admin" &&
      user.adminRole !== "superadmin" &&
      !(user.meta && user.meta.modules && user.meta.modules["projects"])
    ) {
      return false;
    }
    if (user.adminRole === "superadmin") {
      return "editor";
    }
    const app = await new Promise<any>((resolve, reject) => {
      Project.getApp((err, app) => {
        if (err || !app) {
          reject(err);
          return;
        }
        resolve(app);
      });
    });
    const subproject = await new Promise<any>((resolve, reject) => {
      app.models.subproject.findOne(
        { where: { id: subproject_id } },
        (error, result) => {
          if (error || !result) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    const project = await new Promise<any>((resolve, reject) => {
      app.models.project.findOne(
        { where: { id: subproject.projectId } },
        (error, result) => {
          if (!result) {
            resolve(null);
            return;
          }

          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if (!project) return false;
    if (!project.owner_id || project.owner_id === user.id) return "editor";
    const projectUsers = await new Promise<any>((resolve, reject) => {
      app.models.projectUser.find(
        { where: { project_id: project.id, user_id: user.id } },
        (error, result) => {
          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if ((user.adminRole === "admin" && user.meta && user.meta.modules && user.meta.modules["projects"]) || projectUsers.some(projectUser => projectUser.role === "editor")) {
      return "editor";
    }
    if (
      user.adminRole === "admin" ||
      projectUsers.some(projectUser => projectUser.role === "member")
    ) {
      return "member";
    }
    return false;
  };

  const isProjectAllowed = async (
    user: any,
    project_id: number
  ): Promise<"member" | "editor" | false> => {
    if (!project_id)
      return user.adminRole === "superadmin" ||
        (user.meta && user.meta.modules && user.meta.modules["projects"])
          ? "editor"
          : user.adminRole === "admin"
            ? "member"
            : false;

    if (user.adminRole === "superadmin") {
      return "editor";
    }
    const isOwner = await new Promise((resolve, reject) => {
      Project.findOne({ where: { id: project_id } }, (error, result) => {
        if (error || !result) {
          reject(error);
          return;
        }
        if (!result.owner_id || result.owner_id === user.id) {
          resolve(true);
          return;
        }
        resolve(false);
      });
    });
    if (isOwner) return "editor";
    const projectUsers = await new Promise<any>((resolve, reject) => {
      Project.getApp((err, app) => {
        if (err || !app) {
          reject(err);
          return;
        }
        app.models["projectUser"].find(
          { where: { project_id: project_id, user_id: user.id } },
          (error, result) => {
            if (error) {
              reject(error);
              return;
            }
            resolve(result);
          }
        );
      });
    });
    if ((user.adminRole === "admin" && user.meta && user.meta.modules && user.meta.modules["projects"]) || projectUsers.some(projectUser => projectUser.role === "editor")) {
      return "editor";
    }
    if (
      user.adminRole === "admin" ||
      projectUsers.some(projectUser => projectUser.role === "member")
    ) {
      return "member";
    }
    return false;
  };

  const isProjectShared = async (
    user: any,
    project_id: number
  ): Promise<boolean> => {
    if (user.adminRole === "admin" || user.adminRole === "superadmin") {
      return true;
    }
    const isOwner = await new Promise((resolve, reject) => {
      Project.findOne({ where: { id: project_id } }, (error, result) => {
        if (error || !result) {
          reject(error);
          return;
        }
        if (!result.owner_id || result.owner_id === user.id) {
          resolve(true);
          return;
        }
        resolve(false);
      });
    });
    if (isOwner) return true;
    const projectUsers = await new Promise<any>((resolve, reject) => {
      Project.getApp((err, app) => {
        if (err || !app) {
          reject(err);
          return;
        }
        app.models["projectUser"].find(
          { where: { project_id: project_id, user_id: user.id } },
          (error, result) => {
            if (error) {
              reject(error);
              return;
            }
            resolve(result);
          }
        );
      });
    });
    if (
      projectUsers.some(
        projectUser =>
          projectUser.role === "editor" || projectUser.role === "member"
      )
    ) {
      return true;
    }
    return false;
  };

  Project.getSharedProjects = (
    user: any,
    callback: (error, result) => void
  ): void => {
    (async () => {
      let app: any;
      try {
        app = await new Promise<any>((resolve, reject) => {
          Project.getApp((err, app) => {
            if (err || !app) {
              reject(err);
              return;
            }
            resolve(app);
          });
        });
      } catch (error) {
        callback(error, null);
        return;
      }
      let editor: any;
      try {
        editor = await new Promise((resolve, reject) => {
          app.datasources.cycuritydb.connector.execute(
            `SELECT id FROM project t WHERE $1 = ANY(SELECT owner_id FROM project p WHERE t.id = p.id) OR (SELECT MAX(id) FROM projectuser WHERE role = 'editor' AND project_id = t.id AND user_id = $2) > 0`,
            [user.id, user.id],
            (error, result) => {
              if (error) {
                reject(error);
                return;
              }
              resolve(result);
            }
          );
        });
      } catch (error) {
        callback(error, null);
        return;
      }
      let member: any;
      try {
        member = await new Promise((resolve, reject) => {
          app.datasources.cycuritydb.connector.execute(
            `SELECT id FROM project t WHERE (SELECT MAX(id) FROM projectuser WHERE role = 'member' AND project_id = t.id AND user_id = $1) > 0`,
            [user.id],
            (error, result) => {
              if (error) {
                reject(error);
                return;
              }
              resolve(result);
            }
          );
        });
      } catch (error) {
        callback(error, null);
        return;
      }
      callback(null, { member: member, editor: editor });
    })();
  };

  Project.afterRemote("**", (ctx, project, next) => {
    const user = ctx.req.user;
    (async () => {
      if (
        ctx.resultType === "project" ||
        (ctx.resultType.length && ctx.resultType[0] === "project")
      ) {
        if (ctx.result) {
          if (ctx.result.length) {
            let i = 0;
            while (i < ctx.result.length) {
              if (await isProjectAllowed(user, ctx.result[i].id)) {
                i++;
              } else {
                ctx.result.splice(i, 1);
              }
            }
          } else {
            if (!await isProjectAllowed(user, ctx.result.id)) {
              const error = new Error("Forbidden");
              error["statusCode"] = 403;
              next(error);
              return;
            }
          }
        }
      } else if (
        ctx.resultType === "subproject" ||
        (ctx.resultType.length && ctx.resultType[0] === "subproject")
      ) {
        if (ctx.result) {
          if (ctx.result.length) {
            let i = 0;
            while (i < ctx.result.length) {
              if (await isSubprojectAllowed(user, ctx.result[i].id)) {
                i++;
              } else {
                ctx.result.splice(i, 1);
              }
            }
          } else {
            if (!await isSubprojectAllowed(user, ctx.result.id)) {
              const error = new Error("Forbidden");
              error["statusCode"] = 403;
              next(error);
              return;
            }
          }
        }
      } else if (
        ctx.resultType === "projectUser" ||
        (ctx.resultType.length && ctx.resultType[0] === "projectUser")
      ) {
        if (ctx.result) {
          if (ctx.result.length) {
            let i = 0;
            while (i < ctx.result.length) {
              if (await isProjectUserAllowed(user, ctx.result[i].id)) {
                i++;
              } else {
                ctx.result.splice(i, 1);
              }
            }
          } else {
            if (!await isProjectUserAllowed(user, ctx.result.id)) {
              const error = new Error("Forbidden");
              error["statusCode"] = 403;
              next(error);
              return;
            }
          }
        }
      }
      next();
    })();
  });

  Project.remoteMethod("getSharedProjects", {
    accepts: [
      {
        arg: "user",
        type: "object",
        required: false,
        http: ctx => ctx.req.user
      }
    ],
    returns: {
      root: true,
      type: "object"
    },
    http: { path: "/getSharedProjects", verb: "get" }
  });
};
