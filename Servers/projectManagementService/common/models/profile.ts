'use strict';

module.exports = function (Profile) {
  Profile.observe('before save', (ctx, next) =>{
    if(ctx.instance){
      ctx.instance.last_update = new Date();
    }
    next()
  });
};
