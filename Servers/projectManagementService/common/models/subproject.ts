"use strict";
import { EngagorServiceFacade } from "../../server/facades/engagorServiceFacade";
import * as globalKolConfig from "./global-kol-config";

module.exports = function(Subproject) {
  Subproject.createOptionsFromRemotingContext = function(ctx) {
    const base = this.base.createOptionsFromRemotingContext(ctx);
    return Object.assign(base, {
      userId: +ctx.req.headers.user_id,
      user: ctx.req.user
    });
  };

  Subproject.engagorInboxes = (
    callback: (error: Error, result: any) => void
  ) => {
    (async () => {
      let inboxes: any[];
      try {
        inboxes = (await new EngagorServiceFacade().getInboxes()).model;
      } catch (error) {
        callback(error, null);
        return;
      }
      Subproject.find(
        { where: { inboxId: { inq: inboxes.map(inbox => inbox.id) } }, include: 'project' },
        (error, result) => {
          if (!result) {
            callback(error, result);
            return;
          }
          result = JSON.parse(JSON.stringify(result));
          result.forEach(subproject => {
            if (!subproject.project) return;
            inboxes.find(
              inbox => inbox.id === subproject.inboxId
            ).subprojectId =
              subproject.id;
          });
          callback(null, inboxes);
        }
      );
    })();
  };
  //
  // Subproject.observe('loaded', (ctx, next) => {
  //   Subproject.getApp((err, app) => {
  //     if (ctx.instance) {
  //       app.models.GlobalKolConfig.get((error, result) => {
  //         const global = result.kol_filter_config || {};
  //         Subproject.findOne({where: {id: ctx.instance.id}, include: 'project'}, (error2, result2) => {
  //           const project = Object.assign(global, result2.kol_filter_config || {});
  //           ctx.instance.kolFilterConfig = Object.assign(project, ctx.instance.kol_filter_config);
  //           next();
  //         });
  //       });
  //     }
  //     else if (ctx.data) {
  //       app.models.GlobalKolConfig.get((error, result) => {
  //         const global = result.kol_filter_config || {};
  //         Subproject.findOne({where: {id: ctx.data.id}, include: 'project'}, (error2, result2) => {
  //           const project = Object.assign(global, result2.kol_filter_config || {});
  //           ctx.data.kolFilterConfig = Object.assign(project, ctx.data.kol_filter_config);
  //           next();
  //         });
  //       });
  //     }
  //     else
  //       next();
  //   });
  //
  //
  // });

  const isSubprojectAllowed = async (
    user: any,
    subproject_id: number,
    project_id?: number
  ): Promise<"member" | "editor" | false> => {
    if (!subproject_id && !project_id) {
      return user.adminRole === "superadmin" ||
        (user.meta && user.meta.modules && user.meta.modules["projects"])
        ? "editor"
        : user.adminRole === "admin"
          ? "member"
          : false;
    }
    if (
      user.adminRole !== "admin" &&
      user.adminRole !== "superadmin" &&
      !(user.meta && user.meta.modules && user.meta.modules["projects"])
    ) {
      return false;
    }
    if (user.adminRole === "superadmin") {
      return "editor";
    }
    const app = await new Promise<any>((resolve, reject) => {
      Subproject.getApp((err, app) => {
        if (err || !app) {
          reject(err);
          return;
        }
        resolve(app);
      });
    });
    const subproject = subproject_id
      ? await new Promise<any>((resolve, reject) => {
          app.models.subproject.findOne(
            { where: { id: subproject_id } },
            (error, result) => {
              if (error || !result) {
                reject(error);
                return;
              }
              resolve(result);
            }
          );
        })
      : null;
    const project = await new Promise<any>((resolve, reject) => {
      app.models.project.findOne(
        { where: { id: subproject ? subproject.projectId : project_id } },
        (error, result) => {
          if (!result) {
            resolve(null);
            return;
          }

          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if (!project) return false;
    if (!project.owner_id || project.owner_id === user.id) return "editor";
    const projectUsers = await new Promise<any>((resolve, reject) => {
      Subproject.getApp((err, app) => {
        if (err || !app) {
          reject(err);
          return;
        }
        app.models["projectUser"].find(
          { where: { project_id: project.id, user_id: user.id } },
          (error, result) => {
            if (error) {
              reject(error);
              return;
            }
            resolve(result);
          }
        );
      });
    });
    if ((user.adminRole === "admin" && user.meta && user.meta.modules && user.meta.modules["projects"]) || projectUsers.some(projectUser => projectUser.role === "editor")) {
      return "editor";
    }
    if (
      user.adminRole === "admin" ||
      projectUsers.some(projectUser => projectUser.role === "member")
    ) {
      return "member";
    }
    return false;
  };

  const isProjectAllowed = async (
    user: any,
    project_id: number
  ): Promise<"member" | "editor" | false> => {
    if (!project_id)
      return user.adminRole === "superadmin" ||
        (user.meta && user.meta.modules && user.meta.modules["projects"])
        ? "editor"
        : user.adminRole === "admin"
          ? "member"
          : false;
    if (
      user.adminRole !== "admin" &&
      user.adminRole !== "superadmin" &&
      !(user.meta && user.meta.modules && user.meta.modules["projects"])
    ) {
      return false;
    }
    if (user.adminRole === "superadmin") {
      return "editor";
    }
    const app = await new Promise<any>((resolve, reject) => {
      Subproject.getApp((err, app) => {
        if (err || !app) {
          reject(err);
          return;
        }
        resolve(app);
      });
    });
    const isOwner = await new Promise((resolve, reject) => {
      app.models.project.findOne(
        { where: { id: project_id } },
        (error, result) => {
          if (error || !result) {
            reject(error);
            return;
          }
          if (!result.owner_id || result.owner_id === user.id) {
            resolve(true);
            return;
          }
          resolve(false);
        }
      );
    });
    if (isOwner) return "editor";
    const projectUsers = await new Promise<any>((resolve, reject) => {
      app.models.projectUser.find(
        { where: { project_id: project_id, user_id: user.id } },
        (error, result) => {
          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if ((user.adminRole === "admin" && user.meta && user.meta.modules && user.meta.modules["projects"]) || projectUsers.some(projectUser => projectUser.role === "editor")) {
      return "editor";
    }
    if (
      user.adminRole === "admin" ||
      projectUsers.some(projectUser => projectUser.role === "member")
    ) {
      return "member";
    }
    return false;
  };

  const isProjectUserAllowed = async (
    user: any,
    projectuser_id: number
  ): Promise<"member" | "editor" | false> => {
    if (!projectuser_id)
      return user.adminRole === "superadmin" ||
        (user.meta && user.meta.modules && user.meta.modules["projects"])
        ? "editor"
        : user.adminRole === "admin"
          ? "member"
          : false;
    if (user.adminRole === "superadmin") {
      return "editor";
    }
    const app = await new Promise<any>((resolve, reject) => {
      Subproject.getApp((err, app) => {
        if (err || !app) {
          reject(err);
          return;
        }
        resolve(app);
      });
    });
    const projectUser = await new Promise<any>((resolve, reject) => {
      app.models.projectUser.findOne(
        { where: { id: projectuser_id } },
        (error, result) => {
          if (error || !result) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    const project = await new Promise<any>((resolve, reject) => {
      app.models.project.findOne(
        { where: { id: projectUser.project_id } },
        (error, result) => {
          if (!result) {
            resolve(null);
            return;
          }

          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if (!project) return false;
    if (!project.owner_id || project.owner_id === user.id) return "editor";
    const projectUsers = await new Promise<any>((resolve, reject) => {
      app.models["projectUser"].find(
        { where: { project_id: project.id, user_id: user.id } },
        (error, result) => {
          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if ((user.adminRole === "admin" && user.meta && user.meta.modules && user.meta.modules["projects"]) || projectUsers.some(projectUser => projectUser.role === "editor")) {
      return "editor";
    }
    if (
      user.adminRole === "admin" ||
      projectUsers.some(projectUser => projectUser.role === "member")
    ) {
      return "member";
    }
    return false;
  };

  Subproject.afterRemote("**", (ctx, project, next) => {
    const user = ctx.req.user;
    (async () => {
      if (
        ctx.resultType === "subproject" ||
        (ctx.resultType.length && ctx.resultType[0] === "subproject")
      ) {
        if (ctx.result) {
          if (ctx.result.length) {
            let i = 0;
            while (i < ctx.result.length) {
              if (await isSubprojectAllowed(user, ctx.result[i].id)) {
                i++;
              } else {
                ctx.result.splice(i, 1);
              }
            }
          } else {
            if (!(await isSubprojectAllowed(user, ctx.result.id))) {
              const error = new Error("Forbidden");
              error["statusCode"] = 403;
              next(error);
              return;
            }
          }
        }
      } else if (
        ctx.resultType === "project" ||
        (ctx.resultType.length && ctx.resultType[0] === "project")
      ) {
        if (ctx.result) {
          if (ctx.result.length) {
            let i = 0;
            while (i < ctx.result.length) {
              if (await isProjectAllowed(user, ctx.result[i].id)) {
                i++;
              } else {
                ctx.result.splice(i, 1);
              }
            }
          } else {
            if (!(await isProjectAllowed(user, ctx.result.id))) {
              const error = new Error("Forbidden");
              error["statusCode"] = 403;
              next(error);
              return;
            }
          }
        }
      } else if (
        ctx.resultType === "projectUser" ||
        (ctx.resultType.length && ctx.resultType[0] === "projectUser")
      ) {
        if (ctx.result) {
          if (ctx.result.length) {
            let i = 0;
            while (i < ctx.result.length) {
              if (await isProjectUserAllowed(user, ctx.result[i].id)) {
                i++;
              } else {
                ctx.result.splice(i, 1);
              }
            }
          } else {
            if (!(await isProjectUserAllowed(user, ctx.result.id))) {
              const error = new Error("Forbidden");
              error["statusCode"] = 403;
              next(error);
              return;
            }
          }
        }
      }

      next();
    })();
  });

  const writeGuard = (user, isNewInstance, data, where, next) => {
    (async () => {
      if (isNewInstance) {
        if ((await isSubprojectAllowed(user, null, data.projectId)) !== "editor") {
          const error = new Error("Unauthorized.");
          error["status"] = 401;
          next(error);
          return;
        }
      } else {
        if (data) {
          if (!("length" in data)) {
            if ((await isSubprojectAllowed(user, data.id)) !== "editor") {
              const error = new Error("Unauthorized.");
              error["status"] = 401;
              next(error);
              return;
            }
          } else {
            if (data.length) {
              for (let item of data) {
                if ((await isSubprojectAllowed(user, item.id)) !== "editor") {
                  const error = new Error("Unauthorized.");
                  error["status"] = 401;
                  next(error);
                  return;
                }
              }
            }
          }
        } else if (where) {
          const subprojects = await new Promise<any[]>((resolve, reject) => {
            Subproject.find({ where }, (error, result) => {
              if (error) {
                reject(error);
                return;
              }
              resolve(result);
            });
          });
          if (
            (await Promise.all(
              subprojects.map(project => isSubprojectAllowed(user, project.id))
            )).some(perm => perm !== "editor")
          ) {
            const error = new Error("Unauthorized.");
            error["status"] = 401;
            next(error);
            return;
          }
        } else {
          const error = new Error("Unauthorized.");
          error["status"] = 401;
          next(error);
          return;
        }
      }
      if (data) {
        if (!("length" in data)) {
          data.last_update = new Date();
        } else {
          if (data.length) {
            for (let item of data) {
              item.last_update = new Date();
            }
          }
        }
      }
      next();
    })();
  };

  Subproject.observe("before save", (ctx, next) =>
    writeGuard(
      ctx.options.user,
      ctx.isNewInstance,
      ctx.instance || ctx.data,
      null,
      next
    )
  );

  Subproject.observe("before delete", (ctx, next) =>
    writeGuard(ctx.options.user, false, null, ctx.where, next)
  );

  Subproject.remoteMethod("engagorInboxes", {
    returns: {
      root: true,
      type: "array"
    },
    http: { path: "/engagorInboxes", verb: "get" }
  });
};
