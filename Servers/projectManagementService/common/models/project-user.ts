"use strict";

module.exports = function(ProjectUser) {

  ProjectUser.createOptionsFromRemotingContext = function(ctx) {
    const base = this.base.createOptionsFromRemotingContext(ctx);
    return Object.assign(base, {
      userId: +ctx.req.headers.user_id,
      user: ctx.req.user
    });
  };

  ProjectUser.projects = (
    user_id: number,
    callback: (error: Error, result: any) => void
  ) => {
    ProjectUser.getApp((err, app) => {
      ProjectUser.find({ where: { user_id: user_id } }, (error, result) => {
        if (error || !result) {
          callback(error, null);
          return;
        }
        if (!result.length) {
          app.models["project"].find(
            {
              where: {
                owner_id: { inq: [user_id, 0] }
              },
              include: 'projectUser'
            },
            callback
          );
          return;
        }
        app.models["project"].find(
          {
            where: {
              or: [
                { owner_id: { inq: [user_id, 0] } },
                { id: { inq: result.map(r => r.project_id) } }
              ]
            },
            include: 'projectUser'
          },
          callback
        );
      });
    });
  };

  const isProjectUserAllowed = async (
    user: any,
    projectuser_id: number,
    project_id: number
  ): Promise<"member" | "editor" | false> => {
    if (!projectuser_id && !project_id) {
      return user.adminRole === "superadmin" ||
        (user.meta && user.meta.modules && user.meta.modules["projects"])
        ? "editor"
        : user.adminRole === "admin"
          ? "member"
          : false;
    }
    if (
      user.adminRole !== "admin" &&
      user.adminRole !== "superadmin" &&
      !(user.meta && user.meta.modules && user.meta.modules["projects"])
    ) {
      return false;
    }
    if (user.adminRole === "superadmin") {
      return "editor";
    }
    const app = await new Promise<any>((resolve, reject) => {
      ProjectUser.getApp((err, app) => {
        if (err || !app) {
          reject(err);
          return;
        }
        resolve(app);
      });
    });
    // const projectUser = await new Promise<any>((resolve, reject) => {
    //   ProjectUser.findOne(
    //     { where: { id: projectuser_id } },
    //     (error, result) => {
    //       if (error || !result) {
    //         reject(error);
    //         return;
    //       }
    //       resolve(result);
    //     }
    //   );
    // });
    const project = await new Promise<any>((resolve, reject) => {
      app.models.project.findOne(
        { where: { id: project_id } },
        (error, result) => {
          if (!result) {
            resolve(null);
            return;
          }

          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if (!project) return false;
    if (!project.owner_id || project.owner_id === user.id) return "editor";
    const projectUsers = await new Promise<any>((resolve, reject) => {
      app.models["projectUser"].find(
        { where: { project_id: project.id, user_id: user.id } },
        (error, result) => {
          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if ((user.adminRole === "admin" && user.meta && user.meta.modules && user.meta.modules["projects"]) || projectUsers.some(projectUser => projectUser.role === "editor")) {
      return "editor";
    }
    if (
      user.adminRole === "admin" ||
      projectUsers.some(projectUser => projectUser.role === "member")
    ) {
      return "member";
    }
    return false;
  };
  const isProjectAllowed = async (
    user: any,
    project_id: number
  ): Promise<"member" | "editor" | false> => {
    if (!project_id)
      return user.adminRole === "superadmin" ||
        (user.meta && user.meta.modules && user.meta.modules["projects"])
        ? "editor"
        : user.adminRole === "admin"
          ? "member"
          : false;
    if (user.adminRole === "superadmin") {
      return "editor";
    }
    const app = await new Promise<any>((resolve, reject) => {
      ProjectUser.getApp((err, app) => {
        if (err || !app) {
          reject(err);
          return;
        }
        resolve(app);
      });
    });
    const isOwner = await new Promise((resolve, reject) => {
      app.models.project.findOne(
        { where: { id: project_id } },
        (error, result) => {
          if (error || !result) {
            reject(error);
            return;
          }
          if (!result.owner_id || result.owner_id === user.id) {
            resolve(true);
            return;
          }
          resolve(false);
        }
      );
    });
    if (isOwner) return "editor";
    const projectUsers = await new Promise<any>((resolve, reject) => {
      app.models.projectUser.find(
        { where: { project_id: project_id, user_id: user.id } },
        (error, result) => {
          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if ((user.adminRole === "admin" && user.meta && user.meta.modules && user.meta.modules["projects"]) || projectUsers.some(projectUser => projectUser.role === "editor")) {
      return "editor";
    }
    if (
      user.adminRole === "admin" ||
      projectUsers.some(projectUser => projectUser.role === "member")
    ) {
      return "member";
    }
    return false;
  };
  const isSubprojectAllowed = async (
    user: any,
    subproject_id: number
  ): Promise<"member" | "editor" | false> => {
    if (!subproject_id)
      return user.adminRole === "superadmin" ||
        (user.meta && user.meta.modules && user.meta.modules["projects"])
        ? "editor"
        : user.adminRole === "admin"
          ? "member"
          : false;
    if (
      user.adminRole !== "admin" &&
      user.adminRole !== "superadmin" &&
      !(user.meta && user.meta.modules && user.meta.modules["projects"])
    ) {
      return false;
    }
    if (user.adminRole === "superadmin") {
      return "editor";
    }
    const app = await new Promise<any>((resolve, reject) => {
      subproject.getApp((err, app) => {
        if (err || !app) {
          reject(err);
          return;
        }
        resolve(app);
      });
    });
    const subproject = await new Promise<any>((resolve, reject) => {
      app.models.subproject.findOne(
        { where: { id: subproject_id } },
        (error, result) => {
          if (error || !result) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    const project = await new Promise<any>((resolve, reject) => {
      app.models.project.findOne(
        { where: { id: subproject.projectId } },
        (error, result) => {
          if (!result) {
            resolve(null);
            return;
          }

          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if (!project) return false;
    if (!project.owner_id || project.owner_id === user.id) return "editor";
    const projectUsers = await new Promise<any>((resolve, reject) => {
      app.models.projectUser.find(
        { where: { project_id: project.id, user_id: user.id } },
        (error, result) => {
          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });
    if ((user.adminRole === "admin" && user.meta && user.meta.modules && user.meta.modules["projects"]) || projectUsers.some(projectUser => projectUser.role === "editor")) {
      return "editor";
    }
    if (
      user.adminRole === "admin" ||
      projectUsers.some(projectUser => projectUser.role === "member")
    ) {
      return "member";
    }
    return false;
  };

  ProjectUser.afterRemote("**", (ctx, project, next) => {
    const user = ctx.req.user;
    (async () => {
      if (ctx.methodString === "projectUser.projects") {
        if (
          user.adminRole !== "admin" &&
          user.adminRole !== "superadmin" &&
          (!user.meta || !user.meta.modules || !user.meta.modules.users)
        ) {
          const error = new Error("Forbidden");
          error["statusCode"] = 403;
          next(error);
          return;
        }
      } else if (
        ctx.resultType === "projectUser" ||
        (ctx.resultType.length && ctx.resultType[0] === "projectUser")
      ) {
        if (ctx.result) {
          if (ctx.result.length) {
            let i = 0;
            while (i < ctx.result.length) {
              if (await isProjectUserAllowed(user, ctx.result[i].id, ctx.result[i].project_id)) {
                i++;
              } else {
                ctx.result.splice(i, 1);
              }
            }
          } else {
            if (!await isProjectUserAllowed(user, ctx.result.id, ctx.result.project_id)) {
              const error = new Error("Forbidden");
              error["statusCode"] = 403;
              next(error);
              return;
            }
          }
        }
      } else if (
        ctx.resultType === "project" ||
        (ctx.resultType.length && ctx.resultType[0] === "project")
      ) {
        if (ctx.result) {
          if (ctx.result.length) {
            let i = 0;
            while (i < ctx.result.length) {
              if (await isProjectAllowed(user, ctx.result[i].id)) {
                i++;
              } else {
                ctx.result.splice(i, 1);
              }
            }
          } else {
            if (!await isProjectAllowed(user, ctx.result.id)) {
              const error = new Error("Forbidden");
              error["statusCode"] = 403;
              next(error);
              return;
            }
          }
        }
      } else if (
        ctx.resultType === "subproject" ||
        (ctx.resultType.length && ctx.resultType[0] === "subproject")
      ) {
        if (ctx.result) {
          if (ctx.result.length) {
            let i = 0;
            while (i < ctx.result.length) {
              if (await isSubprojectAllowed(user, ctx.result[i].id)) {
                i++;
              } else {
                ctx.result.splice(i, 1);
              }
            }
          } else {
            if (!await isSubprojectAllowed(user, ctx.result.id)) {
              const error = new Error("Forbidden");
              error["statusCode"] = 403;
              next(error);
              return;
            }
          }
        }
      }

      next();
    })();
  });

  const writeGuard = (user, isNewInstance, data, where, next) => {
    (async () => {
      if (isNewInstance) {
        if ((await isProjectUserAllowed(user, null, data.project_id)) !== "editor") {
          const error = new Error("Unauthorized.");
          error["status"] = 401;
          next(error);
          return;
        }
      } else {
        if (data) {
          if (!("length" in data)) {
            if ((await isProjectUserAllowed(user, data.id, data.project_id)) !== "editor") {
              const error = new Error("Unauthorized.");
              error["status"] = 401;
              next(error);
              return;
            }
          } else {
            if (data.length) {
              for (let item of data) {
                if ((await isProjectUserAllowed(user, item.id, item.project_id)) !== "editor") {
                  const error = new Error("Unauthorized.");
                  error["status"] = 401;
                  next(error);
                  return;
                }
              }
            }
          }
        } else if (where) {
          const projectUsers = await new Promise<any[]>((resolve, reject) => {
            ProjectUser.find({ where }, (error, result) => {
              if (error) {
                reject(error);
                return;
              }
              resolve(result);
            });
          });
          if (
            (await Promise.all(
              projectUsers.map(project => isProjectUserAllowed(user, project.id, project.project_id))
            )).some(perm => perm !== "editor")
          ) {
            const error = new Error("Unauthorized.");
            error["status"] = 401;
            next(error);
            return;
          }
        } else {
          const error = new Error("Unauthorized.");
          error["status"] = 401;
          next(error);
          return;
        }
      }
      next();
    })();
  };

  ProjectUser.observe("before save", (ctx, next) =>
    writeGuard(
      ctx.options.user,
      ctx.isNewInstance,
      ctx.instance || ctx.data,
      null,
      next
    )
  );

  ProjectUser.observe("before delete", (ctx, next) =>
    writeGuard(
      ctx.options.user,
      false,
      null,
      ctx.where,
      next
    )
  );

  ProjectUser.remoteMethod("projects", {
    accepts: [
      {
        arg: "user_id",
        type: "number",
        required: true
      }
    ],
    returns: {
      root: true,
      type: "array"
    },
    http: { path: "/projects", verb: "get" }
  });
};
