import { app } from '../server';

export function autoMigrate(callback: (error: Error, result: any) => void, ...tables: string[]): void {
  const ds = app['datasources'].cycuritydb;
  ds.autoupdate(tables, err => {
    callback(err, null);
  });
}
