import * as loopback from "loopback";
import * as boot from "loopback-boot";
import * as dotEnv from "dotenv";
import { autoMigrate } from "./automigrate";
import { Server } from "http";
import * as path from "path";
import { Request, Response, NextFunction } from "express";
import * as superagent from "superagent";
const config = require("./config.json");

let configPath = path.join(__dirname, "./config/.env");
dotEnv.config({ path: configPath });

function authMiddleware(req: Request, res: Response, next: NextFunction): void {
  (async () => {
    const userId = req.headers["user_id"] ? +req.headers["user_id"] : null;
    if (!userId) {
      res.status(401).send("Unauthorized.");
      return;
    }
    let user: any;
    try {
      user = (await superagent
        .get(`${process.env.USER_MANAGEMENT_URL}/api/cy_users/${userId}`)
        .set({ user_id: userId })).body;
    } catch (error) {
      res.status(401).send("Unauthorized.");
      return;
    }
    req["user"] = user;
    next();
  })();
}

export const app = loopback();

app.start = (): Server =>
  // start the web server
  app.listen(() => {
    app["emit"]("started");
    const baseUrl = app.get("url").replace(/\/$/, "");
    console.log("Web server listening at: %s", baseUrl);
    if (app.get("loopback-component-explorer")) {
      const explorerPath = app.get("loopback-component-explorer").mountPath;
      console.log("Browse your REST API at %s%s", baseUrl, explorerPath);
    }

    autoMigrate(
      (error, result) => {
        if (error) {
          app["datasources"].cycuritydb.connector.execute(
            `ALTER TABLE project ADD owner_id integer;`,
            (error2, result2) => {
              if (error2) {
                console.error(error2);
                return;
              }
              app["datasources"].cycuritydb.connector.execute(
                `UPDATE project SET owner_id = 0 WHERE owner_id IS NULL;`,
                (error3, result3) => {
                  if (error3) {
                    console.error(error3);
                    return;
                  }
                  autoMigrate((error4, result4) => {
                    if (error4) {
                      console.error(error4);
                      return;
                    }
                    console.log("automigrated successfully.");
                  });
                }
              );
            }
          );
          return;
        }
        console.log("automigrated successfully.");
      },
      "project",
      "projectUser",
      "subproject",
      "globalKolConfig",
      "profile"
    );
  });
app.use(authMiddleware);
// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, err => {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) app.start();
});
