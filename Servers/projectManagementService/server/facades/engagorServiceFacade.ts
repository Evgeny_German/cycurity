import * as request from "superagent"


export class EngagorServiceFacade {

    baseUrl = `${process.env.ENGAGOR_SERVICE_URL}`;

    getMentions = () => {
        return request.get(this.baseUrl + '/engagor/getMentions')
            .send()
            .then(res => {
                return res.body
            });
    };

    getInboxes = () => {
        return request.get(this.baseUrl + '/engagor/getInboxes')
            .send()
            .then(res => {
                return res.body
            });
    };

    public search(freeText: string): Promise<any> {
        return request.get(`${this.baseUrl}/engagor/search?freeText=${freeText}`)
            .send()
            .then(res => {
                return res.body;
            })
    }
}
