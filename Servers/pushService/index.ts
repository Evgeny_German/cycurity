import * as ws from "ws";
import * as dotenv from "dotenv";
import * as path from "path";
import { IncomingMessage } from "http";
import * as express from "express";
import * as superagent from "superagent";
import * as url from "url";
import * as https from "https";
import * as http from "http";
import * as fs from "fs";

interface Registration {
  connectionId: number;
  lastSeen: Date;
  user: any;
  socket: any;
  channels: string[];
  messagesNotDelivered: { channel: string; data: any; id: number }[];
  ackWaiters: { [id: number]: () => void };
  getMessageId(): number;
}

dotenv.config({ path: path.join(__dirname, "./config/.env") });

async function authenticate(req: IncomingMessage): Promise<any> {
  const reqUrl = url.parse(req.url, true);
  if (!reqUrl.query["access_token"]) return null;
  return (await superagent
    .post(`${process.env.AUTH_SERVER}/auth/getUserDetailsFromToken`)
    .send({ authorization: `Bearer ${reqUrl.query["access_token"]}` })).body
    .user;
}

const server =
  (process.env.ADMIN_HOST_SECURE || "").trim().toLowerCase() === "true"
    ? https.createServer({
        key: fs.readFileSync(
          path.join(__dirname, process.env["ADMIN_HOST_KEY"])
        ),
        cert: fs.readFileSync(
          path.join(__dirname, process.env["ADMIN_HOST_CERT"])
        ),
        ca: JSON.parse(process.env["ADMIN_HOST_CA"]).map(c =>
          fs.readFileSync(path.join(__dirname, c))
        )
      })
    : http.createServer();

const wsServer = new ws.Server({
  server
});

const port = +process.env.PORT || 3002;
const publishPort = +process.env.PUBLISH_PORT || 3003;

server.once("listening", () =>
  console.log("push service listening for subscribers on port ", port)
);

server.listen(port);

const keepAlive = +process.env.KEEPALIVE || 5000;
const timeout = +process.env.TIMEOUT || 10000;

// const messages: {
//   [channelId: number]: { received: Date; data: any }[];
// } = {};

let connectionCounter = 0;

function createConnection(): number {
  return (connectionCounter = (connectionCounter + 1) % 100000000);
}

const registrations: Registration[] = [];

setInterval(() => {
  for (let registration of registrations) {
    if (new Date().valueOf() - registration.lastSeen.valueOf() > 900000) {
      registrations.splice(registrations.indexOf(registration), 1);
    }
  }
}, 900000);

const app = express();

app.use(express.json({ limit: "100mb" }));

app.post("/publish", (req, res, next) => {
  res.status(200).send();
  publishMessage(req.query["channel"], req.body);
});

app.listen(publishPort, () =>
  console.log("push service listening for publishers on port ", publishPort)
);

function terminate(socket: any): void {
  try {
    socket.close();
  } catch (e) {
    try {
      socket.terminate();
    } catch (e) {}
  }
}

async function deliverMessage(
  message: { channel: string; data: any; id: number },
  registration: Registration
): Promise<void> {
  const socket = registration.socket;
  await new Promise((resolve, reject) => {
    let finished = false;
    registration.ackWaiters[message.id] = () => {
      if (finished) return;
      finished = true;
      console.log(
        `message delivered to registration ${registration.connectionId}.`
      );
      resolve();
    };
    try {
      socket.send(JSON.stringify(message), err => {
        if (err) {
          if (finished) return;
          finished = true;
          console.error(
            `error delivering message to registration ${
              registration.connectionId
            }.`
          );
          registration.messagesNotDelivered.push(message);
          terminate(socket);
          reject();
        } else {
          setTimeout(() => {
            if (finished) return;
            finished = true;
            console.error(
              `error delivering message to registration ${
                registration.connectionId
              }.`
            );
            registration.messagesNotDelivered.push(message);
            terminate(socket);
            reject();
          }, 5000);
        }
      });
    } catch (err) {
      finished = true;
      console.error(
        `error delivering message to registration ${registration.connectionId}.`
      );
      registration.messagesNotDelivered.push(message);
      terminate(socket);
      reject();
    }
  });
}

function publishMessage(channel: string, message: any) {
  const targets = registrations.filter(r => r.channels.some(c => c == channel));
  for (let target of targets) {
    deliverMessage(
      { channel, data: message, id: target.getMessageId() },
      target
    ).catch(() => {});
  }
}

wsServer.on("connection", (socket, req) => {
  (async () => {
    const user = await authenticate(req);

    if (!user) {
      console.error("user authentication failed.");
      terminate(socket);
      return;
    }

    const reqUrl = url.parse(req.url, true);

    let isNew = false;

    let registration: Registration;

    if (reqUrl.query["reconnect"]) {
      registration = registrations.find(
        r => r.connectionId == Number(reqUrl.query["reconnect"])
      );
      if (registration) {
        console.log(`registration ${registration.connectionId} reconnected.`);
        registration.socket = socket;
        registration.lastSeen = new Date();
        isNew = false;
      } else {
        console.log(`registration ${reqUrl.query["reconnect"]} not found.`);
        isNew = true;
      }
    } else {
      isNew = true;
    }

    if (isNew) {
      let messageId = 0;
      registration = <Registration>{
        connectionId: createConnection(),
        lastSeen: new Date(),
        user,
        socket,
        channels: [],
        messagesNotDelivered: [],
        ackWaiters: {},
        getMessageId(): number {
          return ++messageId;
        }
      };
      console.log(`new registration ${registration.connectionId}.`);
    }

    try {
      socket.send(`id-${registration.connectionId}`, err => {
        if (err) {
          terminate(socket);
        }
      });
    } catch (err) {
      terminate(socket);
    }

    if (!isNew && registration.messagesNotDelivered.length) {
      const copy = Array.from(registration.messagesNotDelivered);
      for (let mes of copy) {
        registration.messagesNotDelivered.splice(
          registration.messagesNotDelivered.indexOf(mes),
          1
        );
        deliverMessage(mes, registration).catch(() => {});
      }
    }

    function seen() {
      registration.lastSeen = new Date();
      setTimeout(() => {
        if (new Date().valueOf() - registration.lastSeen.valueOf() > keepAlive)
          try {
            socket.send("ping", err => {
              if (err) {
                terminate(socket);
              }
            });
          } catch (err) {
            terminate(socket);
          }
      }, keepAlive);
      setTimeout(() => {
        if (new Date().valueOf() - registration.lastSeen.valueOf() > timeout) {
          terminate(socket);
        }
      }, timeout);
    }

    function pingHandler(data) {
      seen();
      try {
        socket.pong(data, false, err => {
          if (err) {
            terminate(socket);
          }
        });
      } catch (err) {
        terminate(socket);
      }
    }

    function pongHandler(data) {
      seen();
    }

    function messageHandler(data) {
      seen();
      if (data == "pong") {
        return;
      }
      if (data === "ping") {
        data = "pong";
        try {
          socket.send(data, err => {
            if (err) {
              terminate(socket);
            }
          });
        } catch (err) {
          terminate(socket);
        }
        return;
      }
      if (data.startsWith && data.startsWith("ack-")) {
        const id = data.split("-")[1];
        if (registration.ackWaiters[id]) {
          registration.ackWaiters[id]();
          delete registration.ackWaiters[id];
        }
        return;
      }
      const request = <
        { type: "subscribe" | "unsubscribe"; channels: string[] }
      >JSON.parse(data);
      if (request.type === "subscribe") {
        console.log(
          `registration ${
            registration.connectionId
          } subscribed to channels: ${request.channels.join(", ")}.`
        );
        for (let channel of request.channels) {
          const index = registration.channels.indexOf(channel);
          if (index < 0) {
            registration.channels.push(channel);
          }
        }
      } else if (request.type === "unsubscribe") {
        console.log(
          `registration ${
            registration.connectionId
          } unsubscribed to channels: ${request.channels.join(", ")}.`
        );
        for (let channel of request.channels) {
          const index = registration.channels.indexOf(channel);
          if (index >= 0) {
            registration.channels.splice(index, 1);
          }
        }
      }
    }

    socket.once("close", () => {
      console.log(`registration ${registration.connectionId} disconnected.`);
      try {
        socket.terminate();
      } catch (e) {}

      // registrations.splice(registrations.indexOf(registration), 1);
    });

    socket.on("ping", pingHandler);
    socket.on("pong", pongHandler);
    socket.on("message", messageHandler);
    if (isNew) registrations.push(registration);
  })();
});
