import * as elasticsearch from "elasticsearch";

export class DocumentElasticManager {

    public isElasticConnected = (): Promise<boolean> => {
        let eClient = this.getElasticClient();
        return eClient.ping({requestTimeout: 30000});
    }

    //Add or update a document in elastic. If id set then if exist in db the document will be updated
    //Type is optional
    public addOrUpdateDocument = (index: string,  document: any, id?: string) => {
        let eClient = this.getElasticClient();

        let content = {
            index, type:index,body: document
        }

        if (id) {
            content = Object.assign({}, content, {id})
        }

        return eClient.index(content).then(res=>{
            console.log(`DocumentElasticManager: add or update completed succesfully. Id =${res._id}`)
            return res;
        }).catch(err=>{
            console.error(`DocumentElasticManager: Error during add or update.`,err)
            throw err;
        })
    }

    public getById = (index: string, id: string, bodyOnly: boolean = true) => {
        let eClient = this.getElasticClient();
        return eClient.get({
            index, type:index, id
        }).then(res => {
            if (!bodyOnly) {
                return res;
            } else {
                return res._source;
            }
        }, err => {
            throw err;
        });
    }

    public deleteDocument=(index: string, id: string)=>{
        let eClient = this.getElasticClient();
        return eClient.delete({index,type:index,id});
    }

    private getElasticClient = (): elasticsearch.Client => {
        let elasticAddress = process.env.ELASTIC_ADDRESS;
        return new elasticsearch.Client({
            host: elasticAddress,
            log: process.env.ELASTIC_LOG_LEVEL || "warning"
        })
    }
}