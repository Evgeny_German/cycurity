
import * as path from "path";
import * as dotEnv from "dotenv";
import {DocumentStorageElastic} from "./server";

let configPath = path.join(__dirname, "./config/.env")
dotEnv.config({path: configPath});

const server = new DocumentStorageElastic();

server.startServer();
