import {BaseExpressServer} from "../../infrastructure/baseExpressService/BaseExpressServer";
import {Express, Router} from "express";
import * as express from "express";
import * as path from "path";
import {documentStorageRouter} from "./routes/documentStorageRouter";
import {DocumentElasticManager} from "./managers/documentElasticManager";


export class DocumentStorageElastic extends BaseExpressServer {

    constructor() {
        super();
        this.validateElasticConnected();

    }

    private validateElasticConnected=()=>{
        new DocumentElasticManager().isElasticConnected().then(isConnected=>{
            if(isConnected){
                console.log(`DocumentElasticManager connected to ${process.env.ELASTIC_ADDRESS}`)
            }else{
                console.error(`DocumentElasticManager could not connect to ${process.env.ELASTIC_ADDRESS}`)
            }
        }).catch(err=>{
            console.error(`DocumentElasticManager error when connecting to  ${process.env.ELASTIC_ADDRESS}`,err)
        })
    }

    protected setRoutes(app: Express): void {
        app.use("/documentStorage", documentStorageRouter);
    }

    protected getAppName(): string {
        return 'DocumentStorageElastic';
    }



}
