import {Request, Response, NextFunction, Router} from "express";
import {RequestValidation} from "express-validator";
import * as ExpressValidator from "express-validator";
import {check, validationResult} from "express-validator/check";
import {DocumentElasticManager} from "../managers/documentElasticManager";

export class DocumentStorageRouter {


    router: Router;

    constructor() {
        this.router = Router();
        this.router.use(ExpressValidator());
        this.createRoutes();
    }

    private createRoutes() {
        this.router.post("/addOrUpdateDocument", [check("index").exists(), check("document").exists()], this.addOrUpdateDocument);
        this.router.post("/getDocumentById", [check("index").exists(),  check("id").exists()], this.getDocumentById);
        this.router.post("/deleteDocument", [check("index").exists(), check("id").exists()], this.deleteDocument);
    }

    private addOrUpdateDocument = (req: Request, res: Response, next: NextFunction) => {
        try {
            //checks that mandatory params exist
            validationResult(<any>req).throw();
        } catch (err) {
            res.status(422).json({errors: err.mapped()});
        }
        new DocumentElasticManager()
            .addOrUpdateDocument(req.body.index, req.body.document, req.body.id)
            .then((docRes:any) => {
                res.status(200).send({result: docRes.result, id: docRes._id,version:docRes._version});
            }, err => {
                res.status(500).send({err:err.message});
            })

    }

    private getDocumentById = (req: Request, res: Response, next: NextFunction): void => {
        try {
            validationResult(<any>req).throw();
        } catch (err) {
            res.status(422).json({errors: err.mapped()});
        }
        new DocumentElasticManager()
            .getById(req.body.index, req.body.id, true)
            .then(docRes => {
                res.status(200).send(docRes)
            }, err => {
                res.status(500).send(err)
            })
    }

    private deleteDocument = (req: Request, res: Response, next: NextFunction): void => {
        try {
            validationResult(<any>req).throw();
        } catch (err) {
            res.status(422).json({errors: err.mapped()});
        }
        new DocumentElasticManager()
            .deleteDocument(req.body.index, req.body.id)
            .then(docRes => {
                res.status(200).send({result: docRes.result, id: docRes._id,version:docRes._version});
            }, err => {
                res.status(500).send(err)
            })
    }

}

export const documentStorageRouter = new DocumentStorageRouter().router;
