import * as chai  from "chai";
import * as sinon from "sinon";
import {DocumentElasticManager} from "../managers/documentElasticManager";
import * as Assert from "assert";

let expect = chai.expect;
let should = chai.should();

let testIndex = "test_index";
let testType = "test_index";
let updatedContent = "updated content";

describe("Tests for elastic manager",()=>{
    let manager:DocumentElasticManager;
    beforeEach(()=>{
        manager = new DocumentElasticManager();
    })
    it("should connect to elastic when elastic is running",()=>{
        return manager.isElasticConnected().then((res)=>{
            //if we reached her then the test passed
            Assert.ok(true,"Connected to elastic");
        },err=>{
            //if reached here then we failed
            Assert.fail("Not connected","Connected","Could not connect to elastic");
        })
    });

    it("should create new document with new id",()=>{
        return manager.addOrUpdateDocument(testIndex,{content:"test"}).then((res:any)=>{
            Assert.ok(`document created. id assigned: ${res._id} `);
        },err=>{
            Assert.fail("Not created","created","Could not create a document");
        })
    });

    it("should create a new document with id supplied",()=>{
        return manager.addOrUpdateDocument(testIndex,{content:"test"},"1").then((res:any)=>{
            Assert.ok(`document created. id assigned: ${res._id} `);
        },err=>{
            Assert.fail("Not created","created","Could not create a document");
        })
    });
    it("should update content of an existing document",()=>{
        return manager.addOrUpdateDocument(testIndex,{content:updatedContent},"1").then((res:any)=>{
            expect(res.created).to.eq(false);
        },err=>{
            Assert.fail("Not created","created","Could not create a document");
        })
    });

    it("should get by id when id exists",()=>{
        return manager.getById(testIndex,"1",false).then((res:any)=>{
            expect(res.found).eq(true)
        },err=>{
            throw(err);
        })
    });
    it("should not return get by id when using wrong type",()=>{
        return manager.getById(testIndex,"1",false).then((res:any)=>{
            expect(res.found).eq(true)
        },err=>{
            throw(err);
        })
    });

    it("should get by id only body when id exists",()=>{
        return manager.getById(testIndex,"1").then((res:any)=>{
            expect(res.content).eq(updatedContent);
        },err=>{
            throw(err);
        })
    });

    it("should error when id not exist",()=>{
        return manager.getById(testIndex,"2",false).then((res:any)=>{
            Assert.fail("Error","Not error","Expected to error")
        },err=>{
            Assert.ok("Id not found")
        })
    });

    it("should delete an existing document",()=>{
        return manager.deleteDocument(testIndex,"1").then((res:any)=>{
            expect(res.found).eq(true);
            expect(res.result).eq("deleted");
        },err=>{
            throw(err);
        })
    })

})