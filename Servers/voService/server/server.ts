import * as loopback from "loopback";
import * as boot from "loopback-boot";
import * as dotEnv from "dotenv";
import { autoMigrate } from "./automigrate";
import { Server } from "http";
import * as path from "path";

const config = require("./config.json");

let configPath = path.join(__dirname, "./config/.env");
dotEnv.config({ path: configPath });

export const app = loopback();

app.start = (): Server =>
  // start the web server
  app.listen(() => {
    app["emit"]("started");
    const baseUrl = app.get("url").replace(/\/$/, "");
    console.log("Web server listening at: %s", baseUrl);
    if (app.get("loopback-component-explorer")) {
      const explorerPath = app.get("loopback-component-explorer").mountPath;
      console.log("Browse your REST API at %s%s", baseUrl, explorerPath);
    }
    autoMigrate((err, result) => {

    }, 'voAction', 'voStatus', 'voActionStatus', 'vo', 'voActionLog', 'voAudit', 'voLaunch');
  });

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, err => {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) app.start();
});


