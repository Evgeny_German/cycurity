'use strict';
import * as dotEnv from "dotenv";
import * as path from "path"

module.exports = function(server) {
  //add dotenv support
  let configPath = path.join(__dirname,"../config/.env")
  dotEnv.config({path: configPath});

  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  router.get('/', server.loopback.status());
  server.use(router);
};
