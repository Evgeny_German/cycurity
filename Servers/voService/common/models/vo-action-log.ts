"use strict";

module.exports = function(VoActionLog) {
  VoActionLog.createOptionsFromRemotingContext = function(ctx) {
    const base = this.base.createOptionsFromRemotingContext(ctx);
    return Object.assign(base, {
      userId: +ctx.req.headers.user_id
    });
  };
  VoActionLog.observe("after save", async (ctx, next) => {
    const userId = ctx.options.userId;
    let app: any;
    try {
      app = await new Promise((resolve, reject) => {
        VoActionLog.getApp((error, result) => {
          if (error || !result) {
            reject(error);
            return;
          }
          resolve(result);
        });
      });
    } catch (error) {
      next(error);
      return;
    }
    try {
      await new Promise((resolve, reject) => {
        app.models.vo.upsert(
          { id: ctx.instance.voId, lastActionDate: new Date(), lastUserActing: userId },
          { userId },
          (error, result) => {
            if (error) {
              reject(error);
              return;
            }
            resolve(result);
          }
        );
      });
    } catch (error) {
      next(error);
      return;
    }
  });
};
