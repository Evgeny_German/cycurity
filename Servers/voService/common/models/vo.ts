import * as superagent from "superagent";

function difference(object: any, base: any) {
  if (object === base) return null;
  if (object && !base) return object;
  if (base && !object) return object;
  if (!object && !base) return null;
  if (typeof object === "object") {
    const diff = {};
    for (let prop in object) {
      const propDiff = difference(object[prop], base[prop]);
      if (propDiff != null && propDiff != undefined) diff[prop] = propDiff;
    }
    for (let prop in base) {
      if (!(prop in object)) diff[prop] = undefined;
    }
    if (Object.keys(diff).length) {
      return diff;
    }
    return null;
  }
  return object;
}

async function Audit(
  app: any,
  voId: number,
  prevSettings: any,
  newSettings: any,
  userId: number
): Promise<any> {
  return new Promise((resolve, reject) => {
    app.models["voAudit"].create(
      {
        voId: voId,
        when: new Date(),
        userId: userId,
        newValue: difference(newSettings, prevSettings)
      },
      (error, result) => {
        if (error || !result) {
          reject(error);
          return;
        }
        resolve(result);
      }
    );
  });
}

export default function(Vo) {
  Vo.createOptionsFromRemotingContext = function(ctx) {
    const base = this.base.createOptionsFromRemotingContext(ctx);
    return Object.assign(base, {
      userId: +ctx.req.headers.user_id
    });
  };

  Vo.updateVos = (
    vos: number[],
    value: {
      projectId?: number;
      topicId?: number;
      maintenanceProfileId?: number;
      regionId?: number;
      languageId?: number;
    },
    userId: number,
    callback: any
  ): void => {
    (async () => {
      try {
        const voModels = await new Promise<any>((resolve, reject) => {
          Vo.find({ where: { id: { inq: vos } } }, (error, result) => {
            if (error) {
              reject(error);
              return;
            }
            resolve(result);
          });
        });
        for (let vo of voModels) {
          if (!vo.settings) vo.settings = {};
          if ("projectId" in value) {
            if (!vo.settings.projectTopicCombo)
              vo.settings.projectTopicCombo = {};
            vo.settings.projectTopicCombo.project = value.projectId;
          }
          if ("topicId" in value) {
            if (!vo.settings.projectTopicCombo)
              vo.settings.projectTopicCombo = {};
            vo.settings.projectTopicCombo.topic = value.topicId;
          }
          if ("maintenanceProfileId" in value) {
            vo.statusId = value.maintenanceProfileId;
          }
          if ("regionId" in value) {
            vo.settings.currentLocation = value.regionId;
          }
          if ("languageId" in value) {
            vo.settings.language = value.languageId;
          }
          await new Promise<any>((resolve, reject) => {
            Vo.upsert(
              vo,
              { userId },
              (error, result) => {
                if (error) {
                  reject(error);
                  return;
                }
                resolve(result);
              },
              { userId }
            );
          });
        }
      } catch (error) {
        callback(error, null);
        return;
      }
      callback(null, null);
    })();
  };

  Vo.checkout = (voId: number, userId: number, callback) => {
    (async () => {
      let user;
      let vo;
      try {
        user = (await superagent
          .get(`${process.env.USER_MANAGEMENT_URL}/api/cy_users/${userId}`)
          .set({ user_id: userId })).body;

        vo = await new Promise<any>((resolve, reject) => {
          Vo.findById(voId, (err, res) => {
            if (err) {
              reject(err);
              return;
            }
            resolve(res);
          });
        });
      } catch (error) {
        callback(error, null);
        return;
      }
      if (vo.checkedOutTo || user.adminRole !== "superadmin") {
        callback(new Error("Unauthorized."), null);
        return;
      }
      try {
        await new Promise<any>((resolve, reject) => {
          Vo.upsert({ id: voId, checkedOutTo: userId }, (err, res) => {
            if (err) {
              reject(err);
              return;
            }
            resolve(res);
          });
        });
      } catch (error) {
        callback(error, null);
        return;
      }
      callback(null, {});
    })();
  };

  Vo.release = (voId: number, userId: number, callback) => {
    (async () => {
      let user;
      let vo;
      try {
        user = (await superagent
          .get(`${process.env.USER_MANAGEMENT_URL}/api/cy_users/${userId}`)
          .set({ user_id: userId })).body;

        vo = await new Promise<any>((resolve, reject) => {
          Vo.findById(voId, (err, res) => {
            if (err) {
              reject(err);
              return;
            }
            resolve(res);
          });
        });
      } catch (error) {
        callback(error, null);
        return;
      }
      if (vo.checkedOutTo != userId && user.adminRole !== "superadmin") {
        callback(new Error("Unauthorized."), null);
        return;
      }
      try {
        await new Promise<any>((resolve, reject) => {
          Vo.upsert({ id: voId, checkedOutTo: null }, (err, res) => {
            if (err) {
              reject(err);
              return;
            }
            resolve(res);
          });
        });
      } catch (error) {
        callback(error, null);
        return;
      }
      callback(null, {});
    })();
  };

  Vo.observe("after save", async (ctx, next) => {
    const userId = ctx.options.userId;
    if (ctx.isNewInstance && ctx.instance) {
      try {
        const app = await new Promise((resolve, reject) => {
          Vo.getApp((error, result) => {
            if (error || !result) {
              reject(error);
              return;
            }
            resolve(result);
          });
        });
        if (ctx.instance.settings) {
          ctx.instance.settings.name = ctx.instance.name;
        }
        await Audit(app, ctx.instance.id, null, ctx.instance.settings, userId);
      } catch (error) {
        next(error);
        return;
      }
    }
  });

  Vo.observe("before save", async (ctx, next) => {
    const userId = ctx.options.userId;
    if (!ctx.isNewInstance) {
      const instance = ctx.instance || ctx.data;
      try {
        const prev = await new Promise<any>((resolve, reject) => {
          Vo.findById(instance.id, (err, result) => {
            if (err) {
              reject(err);
              return;
            }
            resolve(result);
          });
        });
        const newData = instance;
        const app = await new Promise<any>((resolve, reject) => {
          Vo.getApp((error, result) => {
            if (error || !result) {
              reject(error);
              return;
            }
            resolve(result);
          });
        });
        if (newData.settings) {
          newData.settings.name = newData.name;
          if (newData.statusId != prev.statusId) {
            if (newData.statusId) {
              const status = await new Promise<any>((resolve, reject) => {
                app.models["voStatus"].findById(
                  newData.statusId,
                  (error, result) => {
                    if (error) {
                      reject(error);
                      return;
                    }
                    resolve(result);
                  }
                );
              });
              if (status) {
                newData.settings.maintenanceProfile = status.caption;
              }
            } else {
              delete newData.settings["maintenanceProfile"];
            }
          }
        }
        await Audit(app, prev.id, prev.settings, newData.settings, userId);
      } catch (error) {
        next(error);
        return;
      }
    } else if (ctx.isNewInstance) {
      if (ctx.instance.settings) {
        ctx.instance.settings.name = ctx.instance.name;
      } else {
        ctx.instance.settings = { name: ctx.instance.name };
      }
    }
  });

  Vo.afterRemote("**", async (ctx, vo, next) => {
    async function complementVo(vo: any, app: any): Promise<any> {
      try {
        if (!vo.statusId) {
          return;
        }
        const status = JSON.parse(
          JSON.stringify(
            await new Promise<any>((resolve, reject) => {
              app.models["voStatus"].findById(
                vo.statusId,
                {
                  include: {
                    actionStatus: {
                      action: {
                        relation: "actionLog",
                        scope: {
                          where: { voId: vo.id }
                        }
                      }
                    }
                  }
                },
                (error, result) => {
                  if (error) {
                    reject(error);
                    return;
                  }
                  resolve(result);
                }
              );
            })
          )
        );
        if (!status) {
          return;
        }
        const missingActions = status.actionStatus.filter(acSt => {
          if (!acSt || !acSt.action) return false;

          if (!acSt.actionInterval) {
            return false;
          }

          const sortedLog = (<any[]>acSt.action.actionLog)
            .map(al => new Date(al.when).valueOf())
            .sort((x, y) => (x === y ? 0 : x < y ? 1 : -1));
          const last = sortedLog.length ? sortedLog[0] : 0;
          const targetDate = last + acSt.actionInterval * 1000;

          return (
            acSt.action.actionLog.some(
              i => new Date().valueOf() > targetDate
            ) || !acSt.action.actionLog.length
          );
        });
        const daysDelayed = status.actionStatus
          .filter(acSt => acSt.actionInterval)
          .map(acSt => {
            if (!acSt || !acSt.action) return 0;
            const sortedLog = (<any[]>acSt.action.actionLog)
              .map(al => new Date(al.when).valueOf())
              .sort((x, y) => (x === y ? 0 : x < y ? 1 : -1));
            const last = sortedLog.length ? sortedLog[0] : 0;
            const targetDate = last.valueOf() + acSt.actionInterval * 1000;

            return (new Date().valueOf() - targetDate) / (1000 * 60 * 60 * 24);
          });
        const lastAction = await new Promise<any>((resolve, reject) => {
          app.models["voActionLog"].findOne(
            { where: { voId: vo.id }, order: "when DESC", limit: 1 },
            (error, result) => {
              if (error) {
                reject(error);
                return;
              }
              resolve(result);
            }
          );
        });
        const maxDaysDelayed = Math.max(...daysDelayed);
        vo.daysDelayed = maxDaysDelayed;
        vo.missingActions = missingActions.map(ac => {
          const act = JSON.parse(JSON.stringify(ac.action));
          if (act["actionLog"]) {
            delete act["actionLog"];
          }
          return act;
        });
        vo.lastActionUserId = lastAction ? lastAction.userId : null;
        vo.lastActionDate = lastAction ? lastAction.when : null;
      } catch (error) {
        console.error("Complement VO error: ", error);
      }
    }
    if (ctx.result) {
      const app = await new Promise<any>((resolve, reject) => {
        Vo.getApp((error, result) => {
          if (error || !result) {
            reject(error);
            return;
          }
          resolve(result);
        });
      });
      if (ctx.result.length) {
        await Promise.all(ctx.result.map(vo => complementVo(vo, app)));
      } else {
        await complementVo(ctx.result, app);
      }
    }
  });

  Vo.remoteMethod("checkout", {
    accepts: [
      { arg: "voId", type: "number", http: ctx => ctx.req.query.voId },
      { arg: "userId", type: "number", http: ctx => +ctx.req.headers.user_id }
    ],
    http: { verb: "post" }
  });
  Vo.remoteMethod("release", {
    accepts: [
      { arg: "voId", type: "number", http: ctx => ctx.req.query.voId },
      { arg: "userId", type: "number", http: ctx => +ctx.req.headers.user_id }
    ],
    http: { verb: "post" }
  });
  Vo.remoteMethod("updateVos", {
    accepts: [
      { arg: "vos", type: "array", http: ctx => ctx.req.body.vos },
      { arg: "value", type: "object", http: ctx => ctx.req.body.value },
      { arg: "userId", type: "number", http: ctx => +ctx.req.headers.user_id }
    ],
    http: { verb: "post" }
  });
}
