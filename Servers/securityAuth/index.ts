/**
 * Created by ben.m on 02/01/17.
 */
import * as dotEnv from "dotenv";
import {Server} from "./server";
import * as path from "path";

let configPath = path.join(__dirname,"./config/.env")
let config = dotEnv.config({path: configPath});
const server = new Server();

server.setRoutes();
server.setStaticFolders();
server.setErrorHandlers();
server.startServer();