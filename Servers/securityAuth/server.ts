/**
 * Created by ben.m on 29/12/16.
 */



import * as express from "express";
import * as http from "http";
import * as bodyParser from "body-parser"
import * as compression from "compression"
import {authRoute} from "./routing/authApi";

export class Server{
    private app:express.Express;
    private port="3000";

    constructor(){
        this.app = express();
        this.app.use(compression());
        this.app.use(bodyParser.json()); // support json encoded bodies
        this.app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

        this.port = process.env.PORT || this.port;
        console.log(`ctor port set to : ${this.port}`)
    }

    public setRoutes=()=>{
        this.app.use("/auth" , authRoute);
    }

    public startServer=()=>{
        var httpServer = http.createServer(this.app);
        httpServer.listen(this.port);
        httpServer.on('error',this.onError);
        httpServer.on('listening',()=>{
            console.log('SecurityAuth app listening on port ' + this.port);
            console.log("you are running in " + process.env.NODE_ENV + " mode.");
        });
    }


    public setStaticFolders=()=>{
        var path = require('path');
        let clientPath = path.join(__dirname, '../<client folder>/dist');
        this.app.use(express.static(clientPath));
    }


    onError=(err:any)=>{
        switch (err.code) {
            case 'EACCES':
                console.error('port requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error('port is already in use');
                process.exit(1);
                break;
            default:
                throw err;
        }
    }

    public setErrorHandlers=()=>{
        this.app.use((err:Error, req:express.Request, res:express.Response, next:express.NextFunction)=>{
            res.status((<any>err).status || 500);
            res.send({
                message: err.message,
                error: err
            });
        });
    }
}
