/**
 * Created by ben.m on 29/12/16.
 */
import { BearerAuthManager } from "../authentication/bearerAuthManager";
import { Router } from "express-serve-static-core";
import * as express from "express";
import { AuthenticationResult } from "../dataModels/authenticationResult";
import { Request, Response, NextFunction } from "express";

class AuthApi {
  private _checkModulePermissions: string = "/checkModulePermissions";

  protected _router: Router;

  public getRouter() {
    return this._router;
  }
  private bearerAuthManager: BearerAuthManager;
  constructor() {
    this._router = express.Router();
    this.configRoutes();
    this.bearerAuthManager = new BearerAuthManager();
  }

  protected configRoutes(): void {
    /**
     * @swagger
     *   api/authenticate:
     *       post:
     *          description: This method should checks if the authorization request header is valid or not
     *       produces:
     *          - application/json
     *       parameters:
     *          - name: authorization
     *            description: user decoded token value
     *            in:  formData
     *            required: true
     *            type: string
     *       responses:
     *          200:
     *          description: login
     */
    this._router.post("/authenticate", this.authenticateCall);
    /**
     * @swagger
     * api/createToken:
     *   post:
     *     description: the method creates new authorization token with the relevant tokenData and expires time
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: tokenData
     *         description: tokenData to encode with the token.
     *         in: formData
     *         required: true
     *         type: string
     *       - name: expires
     *         description: expiretion duration Eg 60,2 days,10h,7d.
     *         in: formData
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: token json
     */
    this._router.post("/createToken", this.createTokenCall);
    /**
     * @swagger
     * api/checkModulePermissions:
     *   post:
     *     description: this method checks the permission of a module Enum by the user token
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: token
     *         description: user encoded token value
     *         in: formData
     *         required: true
     *         type: string
     *       - name: module
     *         description: the module for check
     *         in: formData
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: token json
     */
    //this._router.post(this._checkModulePermissions,this.checkModulePermissions);
    /**
     * @swagger
     * api/getUserDetailsFromToken:
     *   post:
     *     description: this method
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: authorization
     *         description: the target token to decrypt.
     *         in: formData
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: token json
     */
    this._router.post("/getUserDetailsFromToken", this.getUserDetailsFromToken);
    this._router.get("/authenticateToken", this.authenticateToken);
  }

  private authenticateCall = (req, res, next) => {
    this.bearerAuthManager
      .authenticateCall(req.body)
      .then((result: AuthenticationResult) => {
        res.json(result);
      })
      .catch(err => {
        next(err);
      });
  };

  private authenticateToken = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    let user: any;
    try {
      user = await this.bearerAuthManager.decodeToken(req.query["token"]);
    } catch (error) {
        res.status(401).send({message: 'Unauthorized'});
        return;
    }
    res.status(200).send(user);
  };

  private createTokenCall = (req, res, next) => {
    try {
      //BodyValidator.mandatory(req.body, ['tokenData','expires'], next);
      res.json({
        token: this.bearerAuthManager.createToken(
          req.body.tokenData,
          req.body.expires
        )
      });
    } catch (err) {
      next(err);
    }
  };

  private getUserDetailsFromToken = (req, res, next) => {
    this.bearerAuthManager
      .getUserDetailsFromToken(req.body.authorization)
      .then(results => {
        res.json({
          user: results
        });
      })
      .catch(err => {
        next(err);
      });
  };

  // private checkModulePermissions = (req,res,next) => {
  //     this.bearerAuthManager.checkModulePermissions(req.body.token, req.body.module)
  //         .then(isPremited => {
  //             res.json(isPremited);
  //         })
  //         .catch(err => {
  //             next(err);
  //         });
  // }
}

export const authRoute = new AuthApi().getRouter();
