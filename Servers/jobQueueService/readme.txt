Flow related to working with jobs:
1. Create a job using POST /jobs (no data is required for input). The output will include a job id
2. Create a task and map to job using regular POST /tasks (only data required is job_id). Result will include a  task id
3. Once task is completed use api - POST /tasks/{id}/setCompleted (no data is required)
4. To view job status use api- GET /jobs/{id}/getJobsStatus
