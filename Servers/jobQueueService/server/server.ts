import * as loopback from "loopback";
import * as boot from "loopback-boot";
import * as dotEnv from "dotenv";
import * as path from "path";
import * as Url from "url";
import { startMonitor } from "./health-monitor";

let configPath = path.join(__dirname, "./config/.env");
dotEnv.config({ path: configPath });

var app = (module.exports = loopback());

app.use((req, res, next) => {
  const url = Url.parse(req.url);
  const segments = url.path.split("/");
  const lastSegment = segments[segments.length - 1];
  if (
    req.method.toLowerCase() === "get" &&
    url.path.startsWith("/api/jobs") &&
    Number.isNaN(Number.parseInt(lastSegment))
  ) {
    res
      .status(500)
      .send("This endpoint was shutdown, Job screen is down temporarily.");
    return;
  }
  next();
});

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit("started");
    var baseUrl = app.get("url").replace(/\/$/, "");
    console.log("Web server listening at: %s", baseUrl);
    if (app.get("loopback-component-explorer")) {
      var explorerPath = app.get("loopback-component-explorer").mountPath;
      console.log("Browse your REST API at %s%s", baseUrl, explorerPath);
    }

    autoUpdate("task", "job", "mailTask", "mailJob");
    (async () => {
      await startMonitor(app);
    })();
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) app.start();
});

var autoUpdate = (...tables: string[]): void => {
  const ds: any = app["datasources"][Object.keys(app["datasources"])[0]];
  console.log(`Starting migrate to database ${ds.connector.settings.database}`);
  ds.autoupdate(tables, err => {
    if (err) {
      console.log(err);
    } else {
      console.log(`Completed auto migrate of tables: ${tables.join(", ")}.`);
    }
  });
};
