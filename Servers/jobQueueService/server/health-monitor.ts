export interface Runnable {
    stop(): void;
}

export async function startMonitor(app: any): Promise<Runnable> {
    let isStopped: boolean;
    let handle: any;

    async function tick(): Promise<void> {
        if (isStopped) {
            clearInterval(handle);
            return;
        }
        const hourAgo = new Date();
        hourAgo.setHours(hourAgo.getHours() - 1);

        await new Promise((resolve, reject) => {
            app.models.job.updateAll({ and: [{ start_date: { lt: hourAgo } }, { status: { inq: ['started', 'queued'] } }] }, { status: 'error', output: { success: false, output: null, error: new Error('Timeout') } }, (err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(result);
            })
        });
    }

    handle = setInterval(tick, 300000);
    tick();
    return {
        stop(): void {
            isStopped = true;
        }
    };
}