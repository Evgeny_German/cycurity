import * as moment from "moment";

export class JobsManager {
  //goes over jobs tasks and retur the task status
  getJobsStatus = (jobModel: any, jobId: number): Promise<any> => {
    return new Promise<any>((resolve, reject) => {
      jobModel.findById(jobId, { include: "tasks" }, (err, job) => {
        if (err) {
          reject(err);
          return;
        } else {
          try {
            let jobInstance = job.toJSON();
            if (jobInstance.tasks.length) {
              let totalHours = this.getTotalHours(jobInstance);
              let totalCompleted = this.getTotalCompleted(jobInstance);
              let totalPending = jobInstance.tasks.length - totalCompleted;
              let isCompleted =
                jobInstance.tasks.length == totalCompleted ? true : false;
              resolve({
                isCompleted,
                totalHours,
                numTasks: jobInstance.tasks.length,
                totalCompleted,
                totalPending
              });
            } else {
              resolve({
                message: "job does not contain tasks",
                isCompleted: false
              });
            }
          } catch (ex) {
            reject(ex);
          }
        }
      });
    });
  };

  //find all tasks that have there status set to 1
  private getTotalCompleted = (job): number => {
    let totalCompleted = job.tasks.reduce((a, b) => {
      if (b.status === 'finished' || b.status === 'error') {
        return a + 1;
      } else {
        return a;
      }
    }, 0);
    return totalCompleted;
  };

  //finds min and max tasks and computes the delta
  private getTotalHours = (job): string => {
    let minTask = this.getMinTask(job.tasks, "start_date");
    let maxTask = this.getMaxTask(job.tasks, "end_date");

    let totalHours = "";
    if (minTask && maxTask) {
      let minDate = moment(minTask.start_date);
      let maxDate = moment(maxTask.end_date);
      let duration = moment.duration(maxDate.diff(minDate));
      totalHours = `${duration.hours()}:${duration.minutes()}:${duration.seconds()}`;
    }
    return totalHours;
  };

  private getMinTask = (taskArray: any[], key: string): any => {
    return taskArray.reduce((a, b) => (a[key] < b[key] ? a : b));
  };

  private getMaxTask = (taskArray: any[], key: string): any => {
    return taskArray.reduce((a, b) => {
      if (a[key] && !b[key]) {
        return a;
      } else if (!a[key] && b[key]) {
        return b;
      } else if (!a[key] && !b[key]) {
        return null;
      } else {
        return a[key] > b[key] ? a : b;
      }
    });
  };
}
