'use strict';

module.exports = function(Task) {
  Task.observe('before save', (ctx, next) =>{
    //only when adding
    if(ctx && ctx.instance && !ctx.instance.id) {
      if (!ctx.instance.status) {
        ctx.instance.status = 0;
      }
      ctx.instance.start_date = new Date()
    }
    next()
  });

  Task.setCompleted=(id:number,cb)=>{
    let data = {id,end_date:new Date(),status:1};
    Task.upsert(data,(err,res)=>{
      if(err){
        cb(err,false)
      }else{
        cb(null,true)
      }
    })
  }

  Task.remoteMethod("setCompleted", {
    accepts: {arg: 'id', type: 'number', required: true},
    http: {path: '/:id/setCompleted', verb: 'post'},
    returns: {arg: 'completed', type: 'boolean'}
  });
};
