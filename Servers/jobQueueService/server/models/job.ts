"use strict";
import {JobsManager} from "../managers/jobsManager";

module.exports = function (Job) {
  //before save set the start date
  Job.observe("before save", (ctx, next) => {
    if (ctx.instance) {
      ctx.instance.start_date = new Date();
    }
    next();
  });

  Job.enqueue = (user_id: number,
                 type: number,
                 status: string,
                 payload: any,
                 tasks: any[],
                 callback: (error: Error, res: any) => void): void => {
    Job.getApp((error, app) => {
      if (error) {
        callback(error, null);
        return;
      }
      Job.findOne(
        {where: {type: type}, order: "secondary_id DESC", limit: 1},
        (error1, res) => {
          if (error1) {
            callback(error1, null);
            return;
          }
          const newId = res ? res.secondary_id + 1 : 1;
          Job.create(
            {
              user_id: user_id,
              type: type,
              payload: payload,
              secondary_id: newId,
              status: status
            },
            (error2, res1) => {
              if (error2 || !res1) {
                callback(error2, null);
                return;
              }
              if (tasks && tasks.length) {
                app.models.task.create(
                  tasks.map(t => {
                    t.job_id = res1.id;
                    return t;
                  }),
                  (error3, res2) => {
                    if (error3) {
                      callback(error3, null);
                      return;
                    }
                    Job.findById(res1.id, {include: "tasks"}, callback);
                  }
                );
              } else {
                if (error2) {
                  callback(error2, null);
                  return;
                }
                Job.findById(res1.id, {include: "tasks"}, callback);
              }
            }
          );
        }
      );
    });
  };

  Job.getJobsStatus = (id: number, cb) => {
    new JobsManager().getJobsStatus(Job, id).then(
      res => {
        cb(null, res);
      },
      err => {
        cb(err);
      }
    );
  };

  Job.remoteMethod("enqueue", {
    accepts: [
      {arg: "user_id", type: "number", required: true},
      {arg: "type", type: "number", required: true},
      {arg: "status", type: "string", required: true},
      {arg: "payload", type: "object", required: false},
      {arg: "tasks", type: "array", required: false}
    ],
    http: {path: "/enqueue", verb: "post"},
    returns: {root: true, type: "object"}
  });

  Job.remoteMethod("getJobsStatus", {
    accepts: {arg: "id", type: "number", required: true},
    http: {path: "/:id/getJobsStatus", verb: "get"},
    returns: {arg: "status", type: "Object"}
  });
};
