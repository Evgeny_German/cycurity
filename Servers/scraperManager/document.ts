import * as superagent from "superagent";
import { dochost } from "./config";

export async function getLatest(userId, ownerId, type, documentId) {
  return (await superagent.get(
    `${dochost}/api/documents/getLatest?userId=${userId}&ownerId=${ownerId}&type=${type}&documentId=${documentId}`
  )).body;
}

export async function saveLatest(userId, ownerId, type, documentId, data) {
  return (await superagent
    .post(`${dochost}/api/documents/saveLatest`)
    .set({ "Content-Type": "application/json" })
    .send({
      userId: +userId,
      ownerId: +ownerId,
      type: type,
      documentId: documentId,
      data: data
    })).body;
}

