import * as superagent from "superagent";
// import { DOMParser } from "xmldom";
import * as $ from "cheerio";

// const parser = new DOMParser();

export async function diffBot(link) {
  return (await superagent.get(
    `https://api.diffbot.com/v3/analyze?token=767f2164830059ec6488dcf5ffe129b0&url=${encodeURIComponent(
      link
    )}`
  )).body;
}

export function findChildNode(type, node) {
  for (const n of node) {
    if (n.tagName) {
      if (n.tagName == type) {
        return n;
      } else {
        return findChildNode(type, n.childNodes);
      }
    }
  }
}

export function findChildNodeByClass(cls, node) {
  for (const n of node) {
    if (n.attribs) {
      for (const attr in n.attribs) {
        let classes = n.attribs[attr];
        // for (const c in classes) {
        if (classes.includes(cls)) {
          return n;
        } else {
          return findChildNodeByClass(cls, n.childNodes);
        }
        // }
      }
    }
  }
}

// Parse media object HTML
export function parseMediaHTML(dataHTML) {
  let html = $(dataHTML)[0];
  let children = html.childNodes || [];
  let dataArray = [];
  // console.log(children)
  for (let i = 0; i < children.length; i++) {
    let nodeClass, nodeHref, nodeAlt;
    if (children[i].type === "tag") {
      for (let attr in children[i].attribs) {
        if (attr) {
          switch (attr) {
            case "class":
              nodeClass = children[i].attribs[attr];
              break;
            case "href":
              nodeHref = children[i].attribs[attr];
              break;
            case "alt":
              nodeAlt = children[i].attribs[attr];
              break;

            default:
              break;
          }
        }
      }
      if (children[i].tagName == "div") {
        switch (nodeClass) {
          case "AdaptiveMedia-singlePhoto":
            const imageAttr = findChildNode("img", children[i].childNodes);
            let image = "";
            if (imageAttr) {
              for (const s in imageAttr.attribs) {
                if (s == "src") {
                  image = imageAttr.attribs[s];
                }
              }
              dataArray.push({ type: "image", src: image });
            }
            break;
          case "AdaptiveMedia-quadPhoto":
            const imagesAttr = findChildNode("img", children[i].childNodes);
            let images = "";
            if (imagesAttr) {
              for (const s in imagesAttr.attribs) {
                if (s == "src") {
                  images = imagesAttr.attribs[s];
                }
              }
              dataArray.push({ type: "image", src: images });
            }

            break;
          case "AdaptiveMedia-video":
            const videoAttr = findChildNodeByClass(
              "PlayableMedia-player",
              children[i].childNodes
            );
            let video = "";
            if (videoAttr) {
              for (const s in videoAttr.attribs) {
                if (s == "style") {
                  video = videoAttr.attribs[s].split("('")[1].split("')")[0];
                }
              }
              dataArray.push({ type: "video", src: video });
            }

            break;
          default:
            break;
        }
      }
    }
  }
  return dataArray;
}

function getText(element) {
  return element.data
    ? element.data +
        (element.children && element.children.length
          ? element.children.map(child => getText(child)).join("")
          : "")
    : element.children && element.children.length
      ? element.children.map(child => getText(child)).join("")
      : "";
}

// Parse tweet object HTML
export async function parseTweetHTML(dataHTML) {
  let html = $(dataHTML)[0];
  let children = html.childNodes || [];
  let dataArray = [];
  let linkPreviews = [];
  for (let i = 0; i < children.length; i++) {
    let nodeClass, nodeHref, nodeAlt;
    if (children[i].type === "text") {
      dataArray.push({
        type: "text",
        text: getText(children[i])
      });
    }
    if (children[i].type === "tag") {
      for (let attr in children[i].attribs) {
        if (attr) {
          switch (attr) {
            case "class":
              nodeClass = children[i].attribs[attr];
              break;
            case "href":
              nodeHref = children[i].attribs[attr];
              break;
            case "alt":
              nodeAlt = children[i].attribs[attr];
              break;

            default:
              break;
          }
        }
      }
      switch (children[i].tagName) {
        case "a":
          switch (nodeClass.split(" ")[0]) {
            case "twitter-hashtag":
              dataArray.push({
                type: "hashtag",
                text: getText(children[i]),
                href: nodeHref
              });
              break;
            case "twitter-timeline-link":
              // if (nodeClass.includes("u-hidden")) break;
              let dBot = await diffBot(nodeHref);
              linkPreviews.push(
                children[i].attribs && children[i].attribs["data-expanded-url"]
                  ? Object.assign(dBot, {
                      expandedUrl: children[i].attribs["data-expanded-url"]
                    })
                  : dBot
              );
              dataArray.push({
                type: "link",
                text: getText(children[i]),
                href: nodeHref
              });
              break;
            case "twitter-atreply":
              dataArray.push({
                type: "mention",
                text: getText(children[i]),
                href: nodeHref
              });
              break;
            default:
              break;
          }
          break;
        case "img":
          if (nodeClass == "Emoji Emoji--forText") {
            dataArray.push({ type: "text", text: nodeAlt });
          }
          break;
      }
    }
  }
  return [dataArray, linkPreviews];
}

// Parse Quote object HTML
export function parseQuoteHTML(dataHTML) {
  let html = $(dataHTML)[0];
  let dataObj = {};

  function parseNode(node) {
    if (node.type === "tag" && node.childNodes.length) {
      const clss = node.attribs["class"];
      if (clss) {
        if (clss.includes("QuoteTweet-link js-nav")) {
          const href = node.attribs["href"].split("/");
          dataObj["userName"] = href[1];
          dataObj["tweetId"] = href[3];
          dataObj["url"] = `https://twitter.com/${href[1]}/status/${href[3]}`;
          return;
        } else if (clss.includes("QuoteTweet-fullname u-linkComplex-target")) {
          dataObj["fullName"] = getText(node);
          return;
        } else if (clss.includes("username u-dir u-textTruncate")) {
          dataObj["userName"] = getText(node.lastChild);
          return;
        } else if (clss.includes("QuoteMedia-videoPreview")) {
          dataObj["videoPreview"] = node.childNodes[1].attribs["src"];
          return;
        } else if (clss.includes("QuoteMedia-photoContainer")) {
          dataObj["imagePreview"] = node.childNodes[1].attribs["src"];
          return;
        } else if (clss.includes("tweet-text")) {
          dataObj["tweetText"] = getText(node);
          return;
        }
      }
      for (let child of node.childNodes) {
        parseNode(child);
      }
    }
  }

  parseNode(html);

  return dataObj;
}

// Parse Extras object HTML
export function parseExtrasHTML(dataHTML) {
  let html = $(dataHTML)[0];
  let children = html.childNodes || [];
  let dataObj = [];
  // console.log(html.childElementCount)
  for (let i = 0; i < children.length; i++) {
    if (children[i].type === "tag" && children[i].attribs) {
      for (const cls in children[i].attribs) {
        let name = children[i].attribs[cls];
        let nodeClass = cls;
        if (nodeClass == "class" && name.startsWith("ProfileHeaderCard-")) {
          let key = name.split(" ")[0].split("-")[1];
          let value = getText(children[i]).trim();
          dataObj.push({ key, value });
        }
      }
    }
  }
  return dataObj;
}
// indexind the tweet
export async function indexing(obj) {
  if ("replies" in obj && !("ancestors" in obj) && !("descendants" in obj))
    return obj;

  let counter = 0;
  // setting new replies attribute to the object
  obj["replies"] = [];

  // loop over ancestors
  if (obj["ancestors"] && obj["ancestors"].length > 0) {
    for (const r in obj["ancestors"]) {
      obj["ancestors"][r]["index"] = counter;
      obj["replies"].push(obj["ancestors"][r]);
      counter++;
    }
  }
  // indexing twitter position
  if (obj["tweet"]) {
    obj["tweet"]["index"] = counter;
    counter++;
  }

  // loop over descendants
  if (obj["descendants"] && obj["descendants"].length > 0) {
    for (const r in obj["descendants"]) {
      obj["descendants"][r]["index"] = counter;
      obj["replies"].push(obj["descendants"][r]);
      counter++;
    }
  }
  delete obj["ancestors"];
  delete obj["descendants"];

  return obj;
}

function findDup(arr: any[]) {
  return arr.reduce(
    (sum, current) =>
      sum.find(element => element.timestamp === current.timestamp)
        ? sum
        : [...sum, current],
    []
  );
}

// modify the tweet object
export async function modifyObj(returnData) {
  let obj = {};
  for (let key in returnData) {
    if (returnData.hasOwnProperty(key)) {
      let element = returnData[key];
      // loop inside each object to extract the inner objects
      for (let key in element) {
        if (element.hasOwnProperty(key)) {
          let e = element[key];

          if (key == "ancestors" || key == "descendants") {
            e = findDup([].concat(...e));
          }
          // merge to one general conversation object
          for (let val in e) {
            if (val) {
              for (const prop in e[val]) {
                if (e[val][prop] != null) {
                  switch (prop) {
                    case "tweetData":
                      let tpData = await parseTweetHTML(e[val][prop]);
                      e[val][prop] = tpData[0];
                      if (tpData[1].length > 0) {
                        e[val]["linkPreviews"] = tpData[1];
                      }
                      break;
                    case "tweetMedia":
                      e[val][prop] = parseMediaHTML(e[val][prop]);
                      break;
                    case "quote":
                      e[val][prop] = parseQuoteHTML(e[val][prop]);
                      break;
                    default:
                      break;
                  }
                }
              }
            }
            switch (e[val]) {
              case "false":
                e[val] = false;
                break;
              case "true":
                e[val] = true;
                break;
              case "null":
                e[val] = null;
                break;
            }
            if (e[val] != null) {
              switch (val) {
                case "tweetData":
                  let tData = await parseTweetHTML(e[val]);
                  e[val] = tData[0];
                  if (tData[1].length > 0) {
                    e["linkPreviews"] = tData[1];
                  }
                  break;
                case "tweetMedia":
                  e[val] = parseMediaHTML(e[val]);
                  break;
                case "quote":
                  e[val] = parseQuoteHTML(e[val]);
                  break;
                case "profile_user":
                  let pro_data = JSON.parse(e[val]);
                  if (obj.hasOwnProperty("profile")) {
                    let extras = obj["profile"]["extras"];
                    let image = obj["profile"]["profile_image_url_https"];
                    obj["profile"] = pro_data.profile_user;
                    obj["profile"]["extras"] = extras;
                    obj["profile"]["profile_image_url_https"] = image;
                  } else {
                    obj["profile"] = pro_data.profile_user;
                  }
                  break;
                case "profile_image":
                  if (obj.hasOwnProperty("profile")) {
                    obj["profile"]["profile_image_url_https"] = e[val];
                  } else {
                    obj["profile"] = {};
                    obj["profile"]["profile_image_url_https"] = e[val];
                  }
                  break;
                case "extras":
                  if (obj.hasOwnProperty("profile")) {
                    obj["profile"]["extras"] = parseExtrasHTML(e[val]);
                  } else {
                    obj["profile"] = {};
                    obj["profile"]["extras"] = parseExtrasHTML(e[val]);
                  }
                  break;

                default:
                  break;
              }
            }
          }
          if (!obj.hasOwnProperty(key)) {
            obj[key] = e;
          }
        }
      }
    }
  }

  const result = await indexing(obj);

  [result.tweet, ...result.replies].forEach(tweet => {
    if (tweet && tweet.quote && tweet.quote.url && tweet.linkPreviews) {
      const index = tweet.linkPreviews.findIndex(
        lp => lp.expandedUrl && lp.expandedUrl == tweet.quote.url
      );
      if (index > -1) {
        tweet.linkPreviews.splice(index, 1);
      }
    }
  });

  return result;
}
