import * as superagent from "superagent";

const {
  localhost,
  dochost,
  jobhost,
  scraperjobhost,
  port
} = require("./config");

export function getSearchScraperInput(job, jobId, handle) {
  return {
    name: "",
    url: `https://twitter.com/search?l=&q=from:${encodeURIComponent(
      job.userName
    )}&src=typd`,
    externalId: jobId,
    elements: [
      {
        name: "replies",
        selector: ".tweet",
        selectorType: "css",
        resolve: "",
        repeat: 1,
        limit: 0,
        limitType: "epoch",
        limitValue: "",
        properties: [
          {
            name: "isReply",
            selector: "::attr(data-is-reply-to)",
            resolve: "boolean",
            selectorType: "css",
            repeat: 0,
            limit: 0
          },
          {
            name: "timestamp",
            selector: ".content ._timestamp::attr(data-time-ms)",
            selectorType: "css",
            resolve: "",
            repeat: 0,
            limit: 1,
            limitType: "epoch",
            limitValue: String(getEpoch())
          },
          {
            name: "tweetId",
            selector: "::attr(data-item-id)",
            resolve: "",
            selectorType: "css",
            repeat: 0,
            limit: 0
          },
          {
            name: "tweetMedia",
            selector: ".content .AdaptiveMedia-container",
            resolve: "",
            selectorType: "css",
            repeat: 0,
            limit: 0
          },
          {
            name: "tweetData",
            selector: ".content .tweet-text",
            resolve: "",
            selectorType: "css",
            repeat: 0,
            limit: 0
          },
          {
            name: "tweetText",
            selector: ".content .tweet-text::text",
            selectorType: "css",
            resolve: "",
            repeat: 0,
            limit: 0
          },
          {
            name: "userName",
            selector: "::attr(data-screen-name)",
            selectorType: "css",
            resolve: "",
            repeat: 0,
            limit: 0
          },
          {
            name: "fullName",
            selector: "::attr(data-name)",
            selectorType: "css",
            resolve: "",
            repeat: 0,
            limit: 0
          },
          {
            name: "avatar",
            selector: ".content .avatar::attr(src)",
            selectorType: "css",
            resolve: "",
            repeat: 0,
            limit: 0
          },
          {
            name: "retweetCount",
            selector:
              ".content .ProfileTweet-action--retweet .ProfileTweet-actionCountForPresentation::text",
            selectorType: "css",
            resolve: "",
            repeat: 0,
            limit: 0
          },
          {
            name: "replyCount",
            selector:
              ".content .ProfileTweet-action--reply .ProfileTweet-actionCountForPresentation::text",
            selectorType: "css",
            resolve: "",
            repeat: 0,
            limit: 0
          },
          {
            name: "favoriteCount",
            selector:
              ".content .ProfileTweet-action--favorite .ProfileTweet-actionCountForPresentation::text",
            selectorType: "css",
            resolve: "",
            repeat: 0,
            limit: 0
          },
          {
            name: "quote",
            selector: ".content .QuoteTweet-innerContainer .tweet-content",
            selectorType: "css",
            resolve: "",
            repeat: 0,
            limit: 0
          }
        ]
      }
    ],
    scroll: {
      active: "true",
      method: "scroll",
      "method-scroll-indicator": "has_more_items",
      "method-scroll-indicator-value": "True",
      "method-scroll-value-selector":
        "#timeline > .stream-container::attr(data-min-position)",
      "method-scroll-more-values-selectors": [],
      "method-scroll-value": "min_position",
      "method-scroll-data-type": "html",
      "method-scroll-data": "items_html",
      "method-scroll-url": `https://twitter.com/i/search/timeline?vertical=default&q=from:${encodeURIComponent(
        job.userName
      )}&src=typd&include_available_features=1&include_entities=1&max_position={}&reset_error_state=false`,
      "method-link-selector": ".next a::attr(href)",
      "method-link-iterator-url": "https://test-site.com/page-{}/",
      "method-link-iterator-start": "1",
      limit: "epoch", // | html | text",
      "limit-method": "gt", //eq | ne | gt | ge | lt | le",
      "limit-type": "css", // xpath
      "limit-value": "1522541674",
      "limit-selector": ".tweet.permalink-tweet ._timestamp::attr(data-time-ms)"
    }
  };
}

export function getTweetAndRepliesScraperInput(job, jobId, handle) {
  return [
    {
      name: "tweet",
      url: `https://twitter.com/${encodeURIComponent(job.userName)}/status/${
        job.tweetId
      }`,
      externalId: jobId,
      elements: [
        {
          name: "timestamp",
          selector: ".tweet.permalink-tweet ._timestamp::attr(data-time-ms)",
          selectorType: "css",
          resolve: "",
          repeat: 0,
          limit: 0,
          properties: []
        },
        {
          name: "tweetId",
          selector: ".tweet.permalink-tweet::attr(data-tweet-id)",
          selectorType: "css",
          resolve: "",
          repeat: 0,
          limit: 0,
          properties: []
        },
        {
          name: "tweetMedia",
          selector: ".tweet.permalink-tweet .AdaptiveMedia-container",
          selectorType: "css",
          resolve: "",
          repeat: 0,
          limit: 0,
          properties: []
        },
        {
          name: "tweetData",
          selector: ".tweet.permalink-tweet .tweet-text",
          selectorType: "css",
          resolve: "",
          repeat: 0,
          limit: 0,
          properties: []
        },
        {
          name: "tweetText",
          selector: ".tweet.permalink-tweet .tweet-text::text",
          selectorType: "css",
          resolve: "",
          repeat: 0,
          limit: 0,
          properties: []
        },
        {
          name: "avatar",
          selector:
            ".tweet.permalink-tweet .avatar.js-action-profile-avatar::attr(src)",
          selectorType: "css",
          resolve: "",
          repeat: 0,
          limit: 0,
          properties: []
        },
        {
          name: "userName",
          selector: ".tweet.permalink-tweet .username > b::text",
          selectorType: "css",
          resolve: "",
          repeat: 0,
          limit: 0,
          properties: []
        },
        {
          name: "fullName",
          selector: ".tweet.permalink-tweet .FullNameGroup > .fullname::text",
          selectorType: "css",
          resolve: "",
          repeat: 0,
          limit: 0,
          properties: []
        },
        {
          name: "retweetCount",
          selector:
            ".tweet.permalink-tweet  .ProfileTweet-action--retweet .ProfileTweet-actionCount::attr(data-tweet-stat-count)",
          selectorType: "css",
          resolve: "",
          repeat: 0,
          limit: 0,
          properties: []
        },
        {
          name: "replyCount",
          selector:
            ".tweet.permalink-tweet .ProfileTweet-action--reply .ProfileTweet-actionCount::attr(data-tweet-stat-count)",
          selectorType: "css",
          resolve: "",
          repeat: 0,
          limit: 0,
          properties: []
        },
        {
          name: "favoriteCount",
          selector:
            ".tweet.permalink-tweet .ProfileTweet-action--favorite .ProfileTweet-actionCount::attr(data-tweet-stat-count)",
          selectorType: "css",
          resolve: "",
          repeat: 0,
          limit: 0,
          properties: []
        },
        {
          name: "quote",
          selector:".tweet.permalink-tweet .QuoteTweet-container",
          selectorType: "css",
          resolve: "",
          repeat: 0,
          limit: 0,
          properties: []
        }
      ],
      scroll: {
        active: "false",
        method: "link | request | scroll",
        "method-selector": ".next a::attr(href)",
        limit: "epoc",
        "limit-selector": ""
      }
    },
    {
      name: "",
      url: `https://twitter.com/${encodeURIComponent(job.userName)}/status/${
        job.tweetId
      }`,
      externalId: jobId,
      elements: [
        {
          name: "ancestors",
          selector: ".ancestor",
          selectorType: "css",
          resolve: "",
          repeat: 1,
          limit: 0,
          properties: [
            {
              name: "timestamp",
              selector: ".content ._timestamp::attr(data-time-ms)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "tweetId",
              selector: "::attr(data-item-id)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "tweetMedia",
              selector: ".content .AdaptiveMedia-container",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "tweetData",
              selector: ".content .tweet-text",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "tweetText",
              selector: ".content .tweet-text::text",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "userName",
              selector: "::attr(data-screen-name)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "fullName",
              selector: "::attr(data-name)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "avatar",
              selector: ".content .avatar::attr(src)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "retweetCount",
              selector:
                ".content .ProfileTweet-action--retweet .ProfileTweet-actionCount::attr(data-tweet-stat-count)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "replyCount",
              selector:
                ".content .ProfileTweet-action--reply .ProfileTweet-actionCount::attr(data-tweet-stat-count)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "favoriteCount",
              selector:
                ".content .ProfileTweet-action--favorite .ProfileTweet-actionCount::attr(data-tweet-stat-count)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "quote",
              selector: ".QuoteTweet-container",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            }
          ]
        }
      ],
      scroll: {
        active: "true",
        method: "scroll",
        "method-scroll-indicator": "has_more_items",
        "method-scroll-indicator-value": "True",
        "method-scroll-value-selector":
          "#descendants > .stream-container::attr(data-min-position)",
        "method-scroll-more-values-selectors": [
          ".tweet.permalink-tweet .username > b::text"
        ],
        "method-scroll-value": "min_position",
        "method-scroll-data-type": "html",
        "method-scroll-data": "items_html",
        "method-scroll-url": `https://twitter.com/i/{1}/conversation/${
          job.tweetId
        }?include_available_features=1&include_entities=1&max_position={0}&reset_error_state=false`,
        "method-link-selector": ".next a::attr(href)",
        "method-link-iterator-url": "https://test-site.com/page-{}/",
        "method-link-iterator-start": "1",
        limit: "epoch| html | text",
        "limit-method": "gt eq | ne | gt | ge | lt | le",
        "limit-value": "1522541674",
        "limit-selector":
          ".tweet.permalink-tweet ._timestamp::attr(data-time-ms)"
      }
    },
    {
      name: "",
      url: `https://twitter.com/${encodeURIComponent(job.userName)}/status/${
        job.tweetId
      }`,
      externalId: jobId,
      elements: [
        {
          name: "descendants",
          selector: ".descendant",
          selectorType: "css",
          resolve: "",
          repeat: 1,
          limit: 0,
          properties: [
            {
              name: "timestamp",
              selector: ".content ._timestamp::attr(data-time-ms)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "tweetId",
              selector: "::attr(data-item-id)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "tweetMedia",
              selector: ".content .AdaptiveMedia-container",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "tweetData",
              selector: ".content .tweet-text",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "tweetText",
              selector: ".content .tweet-text::text",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "userName",
              selector: "::attr(data-screen-name)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "fullName",
              selector: "::attr(data-name)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "avatar",
              selector: ".content .avatar::attr(src)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "retweetCount",
              selector:
                ".content .ProfileTweet-action--retweet .ProfileTweet-actionCount::attr(data-tweet-stat-count)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "replyCount",
              selector:
                ".content .ProfileTweet-action--reply .ProfileTweet-actionCount::attr(data-tweet-stat-count)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "favoriteCount",
              selector:
                ".content .ProfileTweet-action--favorite .ProfileTweet-actionCount::attr(data-tweet-stat-count)",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            },
            {
              name: "quote",
              selector: ".QuoteTweet-container",
              selectorType: "css",
              resolve: "",
              repeat: 0,
              limit: 0
            }
          ]
        }
      ],
      scroll: {
        active: "true",
        method: "scroll",
        "method-scroll-indicator": "has_more_items",
        "method-scroll-indicator-value": "True",
        "method-scroll-value-selector":
          "#descendants > .stream-container::attr(data-min-position)",
        "method-scroll-more-values-selectors": [
          ".tweet.permalink-tweet .username > b::text"
        ],
        "method-scroll-value": "min_position",
        "method-scroll-data-type": "html",
        "method-scroll-data": "items_html",
        "method-scroll-url": `https://twitter.com/i/{1}/conversation/${
          job.tweetId
        }?include_available_features=1&include_entities=1&max_position={0}&reset_error_state=false`,
        "method-link-selector": ".next a::attr(href)",
        "method-link-iterator-url": "https://test-site.com/page-{}/",
        "method-link-iterator-start": "1",
        limit: "epoch| html | text",
        "limit-method": "gt eq | ne | gt | ge | lt | le",
        "limit-value": "1522541674",
        "limit-selector":
          ".tweet.permalink-tweet ._timestamp::attr(data-time-ms)"
      }
    }
  ];
}

export function getProfileScraperInput(job, jobId, handle) {
  return {
    name: "profile",
    url: `https://twitter.com/${encodeURIComponent(job.userName)}`,
    externalId: jobId,
    elements: [
      {
        name: "profile_user",
        selector: ".json-data::attr(value)",
        selectorType: "css",
        resolve: "",
        repeat: 0,
        limit: 0,
        properties: []
      },
      {
        name: "extras",
        selector: ".ProfileHeaderCard",
        selectorType: "css",
        resolve: "",
        repeat: 0,
        limit: 0,
        properties: []
      },
      {
        name: "profile_image",
        selector: ".ProfileAvatar-image::attr(src)",
        selectorType: "css",
        resolve: "",
        repeat: 0,
        limit: 0,
        properties: []
      }
    ],
    scroll: {
      active: "false",
      method: "link | request | scroll",
      "method-selector": ".next a::attr(href)",
      limit: "epoc",
      "limit-selector": ""
    }
  };
}

export async function scrape(input, type, handle, priority, extraParams?: string) {
  console.log(`Sending job to scraper with handle '${handle}', type: ${type}.`)
  return (await superagent
    .post(
      `${scraperjobhost}/api/scrapeJobs/enqueueJob?callbackUrl=${encodeURIComponent(
        `http://${localhost}:${port}/scrape/callback?type=${encodeURIComponent(
          type
        )}&handle=${encodeURIComponent(handle)}${extraParams?extraParams:''}`
      )}&priority=${encodeURIComponent(priority)}`
    )
    .send({
      scrape: input,
      callback: {
        type: "hook",
        url: `${scraperjobhost}/api/scrapeJobs/finishJob?id={}`,
        headers: {},
        payload: {}
      }
    })).body;
}

function getEpoch(days: number = 90): number {
  let d = new Date();
  d.setDate(d.getDate() - days);
  return Math.round(d.getTime() / 1000) * 1000;
}
