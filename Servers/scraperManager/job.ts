import * as superagent from "superagent";
import * as _ from "lodash";
import * as Url from "url";
import { getLatest, saveLatest } from "./document";
import { jobhost, pushServiceHost } from "./config";

export async function createMailJob(
  mail: string,
  tasks: any[]
): Promise<number> {
  const jobId = (await superagent.post(`${jobhost}/api/mailJobs`).send({
    mail,
    started: new Date(),
    done: false
  })).body.id;
  await superagent
    .post(`${jobhost}/api/mailTasks`)
    .send(tasks.map(task => Object.assign(task, { mailJob_id: jobId })));
  return jobId;
}

export async function getMailJob(id: number): Promise<any> {
  return (await superagent.get(
    `${jobhost}/api/mailJobs/${id}?filter=${encodeURIComponent(
      JSON.stringify({ include: "mailTasks" })
    )}`
  )).body;
}

export async function saveMailJob(mailJob: any): Promise<void> {
  return (await superagent.patch(`${jobhost}/api/mailJobs`).send(mailJob)).body;
}

export async function saveMailTask(mailTask: any): Promise<void> {
  return (await superagent.patch(`${jobhost}/api/mailTasks`).send(mailTask))
    .body;
}

export async function deleteMailJob(id: number): Promise<void> {
  await superagent.delete(`${jobhost}/api/mailJobs/${id}/mailTasks`).send();
  await superagent.delete(`${jobhost}/api/mailJobs/${id}`).send();
}

// get job id from job manager
export async function createJob(url, userId, ownerId, reportId) {
  // get the user and conversation from URL
  let parsedUrl = url.split("/");
  // extracting username and tweet id from URL
  let userName = parsedUrl[3],
    tweetId = parsedUrl[5];
  console.log(reportId);
  // request to job service to create a job object
  return (await superagent.post(`${jobhost}/api/jobs/enqueue`).send({
    user_id: userId,
    payload: { userName, reportId, tweetId, ownerId },
    type: 2,
    status: "queued",
    tasks: undefined
  })).body;
}

// // update job manager with the data
export async function finishJob(id, status, output) {
  // console.log(`update status: ${status}`)
  // generate patch request with the returned data
  const job = (await superagent.get(`${jobhost}/api/jobs/${id}`)).body;

  let latest;
  latest = await getLatest(
    job.user_id,
    job.payload.ownerId,
    2,
    job.payload.reportId
  );

  const conv = latest.data.data.conversations.find(conv => {
    if (!conv.url) return false;
    const url = parseUrl(conv.url);
    return (
      url.tweetId === job.payload.tweetId &&
      url.userName === job.payload.userName
    );
  });
  if (!conv) return;
  if (status !== "scraped") {
    conv.status = status;
    await saveLatest(
      job.user_id,
      job.payload.ownerId,
      2,
      job.payload.reportId,
      latest.data.data
    );
    await superagent
      .post(`${pushServiceHost}/publish?channel=${job.id}`)
      .send({ done: true });
    return;
  }
  const parsed = parseUrl(conv.url);
  conv.avatar = output.output.tweet.avatar;
  conv.fullName = output.output.tweet.fullName;
  conv.userName = output.output.tweet.userName;
  conv.tweetText = output.output.tweet.tweetText;
  conv.tweetId = output.output.tweet.tweetId;
  conv.reportId = job.payload.reportId;
  await Promise.all([
    (async () =>
      await saveLatest(
        job.user_id,
        job.payload.ownerId,
        1,
        `https://twitter.com/${parsed.userName}/status/${
          parsed.tweetId
        }?reportId=${job.payload.reportId}`,
        {
          conversation: output.output,
          lastScraped: new Date()
        }
      ))(),
    (async () =>
      await superagent
        .patch(`${jobhost}/api/jobs`)
        .send({ id: job.id, status: "finished", output: output }))()
  ]);
  latest.data.data.conversations = _.uniqBy(
    latest.data.data.conversations,
    conv =>
      conv["url"]
        ? conv["url"].split("/")[conv["url"].split("/").length - 1]
        : null
  );
  await saveLatest(
    job.user_id,
    job.payload.ownerId,
    2,
    job.payload.reportId,
    latest.data.data
  );
  await superagent
    .post(`${pushServiceHost}/publish?channel=${job.id}`)
    .send({ done: true });
}

function parseUrl(url) {
  const u = Url.parse(_.trim(url));
  return {
    userName: _.trim(u.path, "/")
      .split("/")[0]
      .toLowerCase(),
    tweetId: _.trim(u.path, "/").split("/")[2]
  };
}
