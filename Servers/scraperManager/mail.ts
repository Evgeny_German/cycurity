import * as fs from "fs";
import * as nodemailer from "nodemailer";
import * as superagent from "superagent";
import * as path from "path";
import {
  getProfileScraperInput,
  getSearchScraperInput,
  getTweetAndRepliesScraperInput,
  scrape
} from "./scraper";
import { modifyObj } from "./parser";
import { localhost, dochost, jobhost, scraperjobhost, port } from "./config";
import { createMailJob, getMailJob, saveMailTask, deleteMailJob } from "./job";

const csvStringify = require("csv-stringify");
const mail = nodemailer.createTransport(<any>{
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT,
  secure: false,
  requireTLS: true,
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASS
  }
});

async function scrapeUserNames(usernames, mail) {
  const jobId = await createMailJob(
    mail,
    usernames.map(username => ({ username }))
  );

  const job = await getMailJob(jobId);

  console.log(`Starting mail job id: ${jobId}, ${usernames.length} profiles.`);
  for (let username of usernames) {
    try {
      await scrape(
        [
          getProfileScraperInput(
            {
              userName: username
            },
            0,
            jobId
          ),
          getSearchScraperInput({ userName: username }, 0, jobId)
        ],
        "mail",
        jobId,
        10,
        `&username=${encodeURIComponent(username)}`
      );
    } catch (error) {
      const task = job.mailTasks.find(t => t.username === username);
      task.profile = {error};
      task.done = true;
      task.errored = true;
      task.error = error;
      await saveMailTask(task);
    }
  }
}

function getFileName() {
  const chars =
    "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  const getChars = function*(length) {
    for (let i = 0; i < length; i++)
      yield chars[Math.floor(Math.random() * chars.length)];
  };
  return Array.from(getChars(10)).join("");
}

const fields = [
  "id",
  "id_str",
  "name",
  "screen_name",
  "location",
  "url",
  "description",
  "protected",
  "followers_count",
  "friends_count",
  "listed_count",
  "created_at",
  "favourites_count",
  "utc_offset",
  "time_zone",
  "geo_enabled",
  "verified",
  "statuses_count",
  "lang",
  "contributors_enabled",
  "is_translator",
  "is_translation_enabled",
  "profile_background_color",
  "profile_background_image_url",
  "profile_background_image_url_https",
  "profile_background_tile",
  "profile_image_url",
  "profile_image_url_https",
  "profile_banner_url",
  "profile_link_color",
  "profile_sidebar_border_color",
  "profile_sidebar_fill_color",
  "profile_text_color",
  "profile_use_background_image",
  "has_extended_profile",
  "default_profile",
  "default_profile_image",
  "following",
  "follow_request_sent",
  "notifications",
  "business_profile_state",
  "translator_type",
  "extras",
  "averageTweetsPerDay",
  "averageRepliesPerDay",
  "averageRetweetsReceived",
  "averageRepliesReceived",
  "averageFavoritesReceived",
  "retweetsToTweets",
  "repliesToTweets"
];

const json2csv = data => {
  return [["username", ...fields]].concat(
    data.map(task => {
      return task.errored || "error" in task.profile
        ? [task.username, "Error:", task.profile.error]
        : [task.username, ...fields.map(key => task.profile.output[key])];
    })
  );
};

const timerange = replies => {
  const tweetDates = replies.map(r => r.timestamp);
  const min = Math.min(...tweetDates);
  const max = Math.max(...tweetDates);

  return (max - min) / 1000 / 60 / 60 / 24;
};

export function scrapeMailCallback(req, res, next) {
  res.status(200).send({});
  (async () => {
    let result;
    let sup;
    let isError;

    if (req.body.error) {
      isError = true;
      sup = req.body.error;
    } else {
      try {
        result = await modifyObj(req.body.output);

        sup = (() => {
          const following = result.profile.friends_count;
          const followers = result.profile.followers_count;
          const favorites = result.profile.favourites_count;
          const followersToFollowing = following ? followers / following : 0;
          const range = timerange(result.replies);
          const tweets = result.replies.filter(reply => !reply.isReply);
          const replies = result.replies.filter(reply => reply.isReply);
          const retweets = result.replies.filter(reply => !!reply.quote);
          const avgTweetsPerDay = range ? tweets.length / range : 0;
          const avgRepliesPerDay = range ? replies.length / range : 0;
          const repliesToTweets = avgTweetsPerDay
            ? avgRepliesPerDay / avgTweetsPerDay
            : 0;
          const retweetsToTweets = retweets.length / tweets.length;
          const avgRetweets = result.replies.length
            ? result.replies.reduce(
                (p, c) =>
                  Number(c.retweetCount) ? p + Number(c.retweetCount) : p,
                0
              ) / result.replies.length
            : 0;
          const avgReplies = result.replies.length
            ? result.replies.reduce(
                (p, c) => (Number(c.replyCount) ? p + Number(c.replyCount) : p),
                0
              ) / result.replies.length
            : 0;
          const avgFavorites = result.replies.length
            ? result.replies.reduce(
                (p, c) => (
                  Number(c.favoriteCount) ? p + Number(c.favoriteCount) : p, 0
                )
              ) / result.replies.length
            : 0;
          return Object.assign(result.profile, {
            averageTweetsPerDay: avgTweetsPerDay,
            averageRepliesPerDay: avgRepliesPerDay,
            averageRetweetsReceived: avgRetweets,
            averageRepliesReceived: avgReplies,
            averageFavoritesReceived: avgFavorites,
            retweetsToTweets,
            repliesToTweets
          });
        })();
      } catch (error) {
        isError = true;
        sup = error;
      }
    }

    let job: any;
    try {
      job = await getMailJob(req.query.handle);
      console.warn(`Received callback for finished job: ${req.query.handle}`);
    } catch {}

    if (!job) {
      return;
    }

    const task = job.mailTasks.find(t => t.username === req.query.username);

    if (isError) {
      let retries = task.retries || 0;
      console.error(
        `Received error for profile ${req.query.username}, job id: ${
          req.query.handle
        }, ${retries} retries.`
      );
      if (retries < 3) {
        task.retries++;
        await saveMailTask(task);
        try {
          await scrape(
            [
              getProfileScraperInput(
                {
                  userName: req.query.username
                },
                0,
                req.query.handle
              ),
              getSearchScraperInput(
                { userName: req.query.username },
                0,
                req.query.handle
              )
            ],
            "mail",
            req.query.handle,
            10,
            `&username=${encodeURIComponent(req.query.username)}`
          );
        } catch (error) {
          task.profile = {error};
          task.done = true;
          task.errored = true;
          task.error = error;
          await saveMailTask(task);
        }
      } else {
        task.profile = { error: {} };
        task.done = true;
        task.errored = true;
        await saveMailTask(task);
      }
    } else {
      task.profile = { output: sup };
      task.done = true;
      task.errored = false;
      console.log(
        `Received result for profile ${req.query.username}, job id: ${
          req.query.handle
        }.`
      );
      await saveMailTask(task);
    }

    if (!job.mailTasks.some(t => !t.done)) {
      console.log(`Finished mail job, id: ${req.query.handle}.`);
      await deleteMailJob(job.id);
      let buffer = new Buffer(0);
      await new Promise((resolve, reject) => {
        const stringifier = csvStringify();
        let row;
        stringifier.on("readable", () => {
          while ((row = stringifier.read())) {
            buffer = Buffer.concat([buffer, row]);
          }
        });
        json2csv(job.mailTasks).forEach(item => stringifier.write(item));
        stringifier.end(() => {
          resolve();
        });
      });
      const sendResult = await mail.sendMail(<any>{
        from: process.env.SMTP_USER,
        to: job.mail,
        subject: "Scrape profiles results (" + new Date() + ")",
        text: `Profiles:${"\r\n"}${job.mailTasks
          .map(t => t.username)
          .join(", ")}`,
        textEncoding: "utf8",
        attachments: [{ content: buffer, filename: "results.csv" }]
      });
      console.log("sent mail to ", job.mail);
    }
  })();
}

export function scrapeMail(req, res, next) {
  scrapeUserNames(req.body, req.query["mail"]);
  res.status(200).send({});
}
