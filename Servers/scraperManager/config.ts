import * as dotenv from "dotenv";
import * as path from "path";

const conf = dotenv.config({ path: path.join(__dirname, "./config/.env") });

export const localhost = process.env.LOCAL_HOST;
export const dochost = process.env.DOCUMENT_STORAGE_SERVICE_URL;
export const jobhost = process.env.JOB_SERVICE_URL;
export const scraperjobhost = process.env.SCRAPER_JOB_SERVICE_URL;
export const pushServiceHost = process.env.PUSH_SERVICE_URL;
export const port = +process.env.SERVER_PORT || 3001;
