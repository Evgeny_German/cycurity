import {
  getProfileScraperInput,
  getSearchScraperInput,
  getTweetAndRepliesScraperInput,
  scrape
} from "./scraper";
import { modifyObj } from "./parser";
import { scrapeMailCallback, scrapeMail } from "./mail";
import { localhost, dochost, jobhost, scraperjobhost, port } from "./config";
import { createJob, finishJob } from "./job";

import * as express from "express";
import * as path from "path";
import * as Url from "url";
import * as bodyParser from "body-parser";
import * as http from "http";
import * as superagent from "superagent";
import * as cors from "cors";
import * as fs from "fs";
import * as _ from "lodash";

const app = express();

app.use(cors());

app.use(bodyParser.json({ limit: "100mb" }));
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  res.send("ScrapIT App");
});

async function getNewProfileImageUrls(images) {
  return await superagent.post(`${dochost}/api/profileImages/set`).send(images);
}

async function handleConversationProfileImages(conversation) {
  const images = [
    {
      username: conversation.tweet.userName.toLowerCase() + " avatar",
      image: conversation.profile.profile_image_url_https
    },
    {
      username: conversation.tweet.userName.toLowerCase() + " background",
      image: conversation.profile.profile_background_image_url
    },
    {
      username: conversation.tweet.userName.toLowerCase() + " background_https",
      image: conversation.profile.profile_background_image_url_https
    },
    {
      username: conversation.tweet.userName.toLowerCase() + " profile",
      image: conversation.profile.profile_image_url
    },
    {
      username: conversation.tweet.userName.toLowerCase() + " profile_https",
      image: conversation.profile.profile_image_url_https
    },
    {
      username: conversation.tweet.userName.toLowerCase() + " profile_banner",
      image: conversation.profile.profile_banner_url
    },
    ...conversation.replies.map(reply => ({
      username: reply.userName.toLowerCase() + " avatar",
      image: reply.avatar
    }))
  ].reduce((sum, current) => {
    if (!current.image) return sum;
    let existing;
    if ((existing = sum.find(i => i.username === current.username))) {
      if (existing.image == current.image) {
        return sum;
      }
    }
    return [...sum, current];
  }, []);
  await getNewProfileImageUrls(
    images.map(image => ({ url: image.image, username: image.username }))
  );
}

async function handleProfileImages(profile) {
  const images = [
    {
      username: profile.screen_name.toLowerCase() + " avatar",
      image: profile.profile_image_url_https
    },
    {
      username: profile.screen_name.toLowerCase() + " background",
      image: profile.profile_background_image_url
    },
    {
      username: profile.screen_name.toLowerCase() + " background_https",
      image: profile.profile_background_image_url_https
    },
    {
      username: profile.screen_name.toLowerCase() + " profile",
      image: profile.profile_image_url
    },
    {
      username: profile.screen_name.toLowerCase() + " profile_https",
      image: profile.profile_image_url_https
    },
    {
      username: profile.screen_name.toLowerCase() + " profile_banner",
      image: profile.profile_banner_url
    }
  ].reduce((sum, current) => {
    if (!current.image) return sum;
    let existing;
    if ((existing = sum.find(i => i.username === current.username))) {
      if (existing.image == current.image) {
        return sum;
      }
    }
    return [...sum, current];
  }, []);
  await getNewProfileImageUrls(
    images.map(image => ({ url: image.image, username: image.username }))
  );
}

let syncRequestCounter = 0;

const syncRequests = {};

function getSyncRequestId() {
  return (syncRequestCounter = (syncRequestCounter + 1) % 1000000000);
}

async function handleConversationCallback(handle, result) {
  if (result.error) {
    await finishJob(handle, "error", { success: false, error: result.error });
    return;
  }
  let out;
  try {
    out = await modifyObj(result.output);
  } catch (error) {
    await finishJob(handle, "error", { success: false, error: error });
    return;
  }
  await finishJob(handle, "scraped", { success: true, output: out });
  try {
    await handleConversationProfileImages(out);
  } catch (error) {}
}

async function handleSyncCallback(handle, result) {
  const handler = syncRequests[handle];
  if (handler) {
    delete syncRequests[handle];
    if (result.error) {
      handler.reject(result.error);
      return;
    }
    handler.resolve(result.output);
  }
}

async function waitSyncCallback(handle, timeout = 60000000) {
  return new Promise((resolve, reject) => {
    syncRequests[handle] = { resolve, reject };
    setTimeout(() => {
      const handler = syncRequests[handle];
      if (handler) {
        delete syncRequests[handle];
        handler.reject(new Error("Timeout"));
        return;
      }
    }, timeout);
  });
}

app.post("/mail", scrapeMail);

app.post("/scrape/callback", (req, res, next) => {
  console.log(`Received job callback with handle '${req.query.handle}', type: ${req.query.type}.`);
  (async () => {
    switch (req.query.type) {
      case "sync":
        handleSyncCallback(req.query.handle, req.body);
        break;
      case "conversation":
        handleConversationCallback(req.query.handle, req.body);
        break;
      case "mail":
        scrapeMailCallback(req, res, next);
        return;
      default:
        res.status(400).send();
        return;
    }
    res.status(200).send();
  })();
});

app.post("/scrape/profileAndStatistics", (req, res, next) => {
  (async () => {
    let model;
    try {
      model = await scrapeProfileAndStatistics(req.body.userNames);
    } catch (error) {
      res.status(500).send({
        success: false,
        error: {
          messsage: "Internal server error",
          origianlError: error
        }
      });
      return;
    }
    res.status(200).send({ model, success: true });
    try {
      for (let key in model) {
        await handleProfileImages(model[key].profile);
      }
    } catch (error) {
      console.error("Error: ", error);
    }
  })();
});

app.post("/scrape/profiles", (req, res, next) => {
  (async () => {
    let model;
    try {
      model = await scrapeProfiles(req.body.userNames);
    } catch (error) {
      res.status(500).send({
        success: false,
        error: {
          messsage: "Internal server error",
          origianlError: error
        }
      });
      return;
    }

    res.status(200).send({
      model,
      success: true
    });
    try {
      for (let key in model) {
        await handleProfileImages(model[key].profile);
      }
    } catch (error) {
      console.error("Error: ", error);
    }
  })();
});

app.get("/scrape/conversationByTweet", (req, res, next) => {
  (async () => {
    let userName, tweetId;
    if (!(userName = req.query.userName)) {
      res.status(400).send("userName is missing.");
      return;
    }
    if (!(tweetId = req.query.tweetId)) {
      res.status(400).send("tweetId is missing.");
      return;
    }

    const requestId = getSyncRequestId();
    let model;
    try {
      model = await scrapeConversationSync(userName, tweetId);
    } catch (error) {
      res.status(500).send({ success: false, error });
      return;
    }
    res.status(200).send({ model, success: true });
    try {
      await handleConversationProfileImages(model);
    } catch (error) {
      console.error("Error: ", error);
    }
  })();
});

app.post("/scrape/scrapeReport", (req, res) => {
  (async () => {
    let userId, ownerId, reportId, urls;
    if (!(userId = req.body.userId)) {
      res.status(400).send("userId is missing.");
      return;
    }

    if (!(ownerId = req.body.ownerId)) {
      res.status(400).send("userId is missing.");
      return;
    }

    if (!(reportId = req.body.reportId)) {
      res.status(400).send("reportId is missing.");
      return;
    }

    if (!(urls = req.body.urls)) {
      res.status(400).send("urls is missing.");
      return;
    }

    let jobs;

    try {
      jobs = await scrapeReport(userId, ownerId, reportId, urls);
    } catch (error) {
      res.status(500).send(error);
      return;
    }

    res.status(200).send(jobs);
  })();
});

async function scrapeProfiles(userNames: string[]): Promise<any> {
  return (await Promise.all(
    userNames.map(async userName => {
      const requestId = getSyncRequestId();
      await scrape(
        [getProfileScraperInput({ userName }, 0, requestId)],
        "sync",
        requestId,
        10
      );

      return {
        userName,
        profile: await modifyObj(
          [].concat(...(<any[]>await waitSyncCallback(requestId)))
        )
      };
    })
  )).reduce(
    (sum, item) => Object.assign(sum, { [item.userName]: item.profile }),
    {}
  );
}

async function scrapeProfileAndStatistics(userNames: string[]): Promise<any> {
  return (await Promise.all(
    userNames.map(async userName => {
      const requestId = getSyncRequestId();
      await scrape(
        [
          getProfileScraperInput({ userName }, 0, requestId),
          getSearchScraperInput({ userName }, 0, requestId)
        ],
        "sync",
        requestId,
        10
      );
      return {
        userName,
        profile: await modifyObj(
          [].concat(...(<any[]>await waitSyncCallback(requestId)))
        )
      };
    })
  )).reduce(
    (sum, item) => Object.assign(sum, { [item.userName]: item.profile }),
    {}
  );
}

async function scrapeReport(userId, ownerId, reportId, urls): Promise<any[]> {
  let jobs;

  jobs = await Promise.all(
    urls.map(ul => createJob(ul, userId, ownerId, reportId))
  );

  await Promise.all(
    jobs.map(
      async job =>
        await scrape(
          [
            getProfileScraperInput(job.payload, job.id, job.id),
            ...getTweetAndRepliesScraperInput(job.payload, job.id, job.id)
          ],
          "conversation",
          job.id,
          20
        )
    )
  );

  return jobs;
}

async function scrapeConversationSync(
  userName: string,
  tweetId: string
): Promise<any> {
  let result: any;
  const requestId = getSyncRequestId();
  await scrape(
    [
      getProfileScraperInput({ userName, tweetId }, 0, requestId),
      ...getTweetAndRepliesScraperInput({ userName, tweetId }, 0, requestId)
    ],
    "sync",
    requestId,
    20
  );

  return await modifyObj(
    [].concat(...(<any[]>await waitSyncCallback(requestId)))
  );
}

// log to server.log file
app.use((req, res, next) => {
  var date = new Date().toString();
  var log = `${date}: ${req.ip} ${req.method} ${req.url} \n`;
  fs.appendFile("log/server.log", log, e => {
    if (e) console.log(e);
  });
  next();
});

// listen on twitter scraper port 3001
const server = app.listen(port, () => {
  console.log(`ScrapIT service is running at port ${port}`);
});

server.setTimeout(600000);
