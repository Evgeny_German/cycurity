import * as request from "superagent";

export async function createJob(
  user_id: number,
  type: number,
  payload: any,
  status: string,
  tasks: any[]
): Promise<any> {
  return (await request
    .post(`${process.env.JOB_SERVICE_URL}/api/jobs/enqueue`)
    .send({ user_id: user_id, type: type, status: status, payload: payload, tasks: tasks })).body;
}

export async function updateTask(id: number, type: number, status: string): Promise<any> {
  return (await request
    .patch(`${process.env.JOB_SERVICE_URL}/api/tasks`)
    .send({ id: id, status: status, type: type })).body;
}

export async function updateJob(
  id: number,
  status: string,
  output: any
): Promise<any> {
  return (await request
    .patch(`${process.env.JOB_SERVICE_URL}/api/jobs`)
    .send({ id: id, status: status, output: output })).body;
}
