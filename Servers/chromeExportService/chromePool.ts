
import {Chrome} from '../../infrastructure/chromeRunner';
import {ResourcePool} from '../../infrastructure/resourcePool/resourcePool';
import * as fs from 'fs-extra';

export async function startPool(): Promise<ResourcePool<Chrome>> {
    if (fs.existsSync(`${__dirname}/temp`)) {
        fs.removeSync(`${__dirname}/temp/`);
    }
    return new ResourcePool<Chrome>({
        getContext: id => {
            return {
                port: 9266 + id
            };
        },
        minCount: 1,
        maxCount: 3,
        getResource: async context => {
            const chrome = new Chrome(true, context.port, null, true, `${__dirname}/temp/`);
            await chrome.start(true);
            await chrome.startCdp();
            return chrome;
        },
        stopResource: async resource => {
            if (resource) {
                await resource.stop(true);
            }
        },
        validateResource: async resource => {
            return await resource.evaluate<boolean>(() => new Promise(r => r(true)));
        }
    });
}
