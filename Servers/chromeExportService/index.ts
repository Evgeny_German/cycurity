import { HtmlExportServer } from './server';
import * as path from 'path';
import * as dotEnv from 'dotenv';
import * as fs from "fs-extra";

const configPath = path.join(__dirname, './config/.env');
dotEnv.config({path: configPath});

const tempUrl = path.join(__dirname, '/impl/tempUrl');

if (fs.existsSync(tempUrl)) {
    fs.removeSync(tempUrl);
}

const server = new HtmlExportServer();

server.startServer();
