import * as Url from "url";
import * as fs from "fs-extra";
import * as path from "path";
import * as officegen from "officegen";
import { spinWait } from "../../../infrastructure/utils/spinWait";
import { updateTask } from "../jobManager";
import * as superagent from "superagent";
import * as uuid from "uuid";
import * as http from "http";
import * as request from "request";
import { Page, Browser } from "puppeteer";
import * as puppeteer from "puppeteer";

export type size = {
  x: number;
  y: number;
  width: number;
  height: number;
};

export type chromeSize = {
  width: number;
  height: number;
  deviceScaleFactor: number;
  mobile: boolean;
};

export type exportOptions = {
  windowSize: chromeSize;
  clip: size;
};

export async function exportImage(
  chrome: Page,
  options: exportOptions
): Promise<any> {
  await chrome.setViewport({
    width: options.windowSize.width,
    height: options.windowSize.height,
    deviceScaleFactor: options.windowSize.deviceScaleFactor,
    isMobile: options.windowSize.mobile
  });
  const data = await chrome.screenshot({
    type: "png",
    clip: options.clip
  });
  return Buffer.from(<any>data, "base64");
}

export async function exportUrl(
  chrome: Page,
  url: string,
  outDir: string
): Promise<{ fileNames: string[]; meta: any }> {
  console.log(`Exporting ${url}`);

  const parsed = Url.parse(url);
  await chrome.goto(
    `${parsed.protocol}//${parsed.host}/wrapper.html${parsed.path}${
      parsed.hash
    }`,
    { waitUntil: "load" }
  );

  const rr = await chrome.evaluate(<any>`new Promise(resolve => {
        function doWhen(condition, action, interval = 100) {
            const intervalId = setInterval(() => {
                if (condition()) {
                    clearInterval(intervalId);
                    action();
                } else {
                    return;
                }
            }, interval);
        }
        doWhen(() => window.wrapper && window.wrapper.contentWindow && 'wrapperLoaded' in window.wrapper.contentWindow, () => setTimeout(resolve, 5000), 1000);
    })`);

  const res = <any>await chrome.evaluate(`new Promise(resolve => {
        let global = window["wrapper"].contentWindow;
        resolve({
            clipRects: global["printHelper"].ripBySelector("#conversationContainer", ".wBox", 640),
            meta: global["printHelper"].meta
        });
    })`);

  const clipRects: {
    clipRects: { width: number; height: number }[];
    meta: any;
  } = res;

  let exported = [];
  let index = 0;

  for (let clipRect of clipRects.clipRects) {
    await chrome.evaluate(<any>`new Promise((resolve, reject) => {
            window.wrapper.contentWindow.ripElement(${index});
            resolve();
        })`);
    console.log(`Exporting png @ ${index}`);
    exported.push(
      await exportImage(chrome, {
        windowSize: {
          width: 1920,
          height: 1080,
          deviceScaleFactor: 1.0,
          mobile: false
        },
        clip: {
          width: clipRect.width,
          height: clipRect.height,
          x: 0,
          y: 0
        }
      })
    );
    index++;
  }

  return {
    fileNames: await Promise.all(
      exported.map(
        (png, index) =>
          new Promise<string>(resolve => {
            const fileName = path.join(outDir, `/${index}.png`);
            const stream = fs.createWriteStream(fileName);
            stream.end(png, () => {
              console.log(`finished ${index + 1}/${exported.length}.`);
              resolve(fileName);
            });
          })
      )
    ),
    meta: clipRects.meta
  };
}

export async function exportToWord(
  urls: string[],
  beforeEach: (url: string, index: number) => Promise<void>,
  afterEach: (url: string, index: number) => Promise<void>
): Promise<string> {
  console.log("starting export to word..");
  const chromePool = await puppeteer.launch({ ignoreHTTPSErrors: true });
  let tempUrl = path.join(__dirname, "/tempUrl");
  if (!fs.existsSync(tempUrl)) {
    fs.mkdirSync(tempUrl);
  }
  tempUrl = fs.mkdtempSync(tempUrl + "/");
  const docx = officegen({
    type: "docx"
  });
  const exported = [];

  let index = 0;
  for (let url of urls) {
    const page = await chromePool.newPage();
    const pth = path.join(tempUrl, `${index}`);
    fs.mkdirSync(pth);
    await beforeEach(url, index);
    const exp = await exportUrl(page, url, pth);
    await afterEach(url, index);
    exported[index] = {
      render(docx: any): void {
        const p1 = docx.createP();
        for (let file of exp.fileNames) {
          p1.addImage(file);
          //p1.addLineBreak();
        }
        docx.putPageBreak();
      },
      title: exp.meta.title
    };
    await page.close();
    index++;
  }

  await chromePool.close();
  const p2 = docx.createP();
  for (let exp of exported) {
    p2.addText(exp.title, { font_face: "Arial" });
    p2.addLineBreak();
  }
  docx.putPageBreak();
  for (let exp of exported) {
    exp.render(docx);
  }
  const name = uuid();
  const out = path.join(__dirname, `./${name}`);
  const output = fs.createWriteStream(out);
  await new Promise(resolve => {
    output.on("close", () => {
      resolve();
    });
    docx.generate(output);
  });
  console.log(`finished creating word document. Saved to ${out}`);
  return name;
}
