import * as AWS from "aws-sdk";

export async function uploadBuffer(key: string, data: AWS.S3.Body, config: {region: string, bucket: string, accessKeyId: string, secretAccessKey: string}): Promise<AWS.S3.ManagedUpload.SendData> {
    const s3 = new AWS.S3({
        apiVersion: "2006-03-1",
        region: config.region,
        credentials: {
            accessKeyId: config.accessKeyId,
            secretAccessKey: config.secretAccessKey
        }
    });
    const res = await s3.upload({Key: key, Bucket: config.bucket, Body: data}, {}).promise();
    return res;
}