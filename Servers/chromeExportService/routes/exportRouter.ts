import { Request, Response, NextFunction, Router } from "express";
import * as path from "path";
import * as fs from "fs";
import * as uuid from "uuid";
import * as exporter from "../impl/exporter";
import { ResourcePool } from "../../../infrastructure/resourcePool/resourcePool";
import { startPool } from "../chromePool";
import { createJob, updateJob, updateTask } from "../jobManager";
import { uploadBuffer } from "../s3";
import * as superagent from "superagent";

class ExportRouter {
  public router: Router;

  constructor() {
    this.router = Router();
    this.createRoutes();
  }

  private createRoutes(): void {
    this.router.post("/word", this.exportWord.bind(this));
  }

  private async exportWord(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const userId = +req.body.user_id;
    const urls: string[] = req.body.urls;
    const selector: string = req.body.selector;
    const name = req.body.name;
    const job = await createJob(
      userId,
      1,
      {
        name: name
      },
      "started",
      urls.map(url => ({
        type: 1,
        status: "queued"
      }))
    );

    console.log(job);

    res.status(200).send(job);

    if (!fs.existsSync(path.join(__dirname, `../out`))) {
      fs.mkdirSync(path.join(__dirname, `../out`));
    }

    let fileName: string;
    try {
      fileName = await exporter.exportToWord(
        urls,
        async (url, index) =>
          await updateTask(job.tasks[index].id, 1, "started"),
        async (url, index) =>
          await updateTask(job.tasks[index].id, 1, "finished")
      );
    } catch (err) {
      await updateJob(job.id, "error", {
        error: err,
        output: null,
        success: false
      });
      await superagent
        .post(
          `${process.env.PUSH_SERVICE_URL}/publish?channel=${
            job.id
          }`
        )
        .send({ done: true });
      return;
    }
    const output = path.join(__dirname, `../impl/${fileName}`);
    const result = await uploadBuffer(fileName, fs.readFileSync(output), {
      bucket: process.env.S3_BUCKET,
      region: process.env.S3_REGION,
      accessKeyId: process.env.S3_KEY_ID,
      secretAccessKey: process.env.S3_SECRET
    });
    await updateJob(job.id, "finished", {
      error: null,
      output: fileName,
      success: true
    });
    await superagent
      .post(
        `${process.env.PUSH_SERVICE_URL}/publish?channel=${
          job.id
        }`
      )
      .send({ done: true });
    try {
      fs.unlinkSync(output);
    } catch (error) {}
  }
}

export const exportRouter = new ExportRouter().router;
