import { BaseExpressServer } from '../../infrastructure/baseExpressService/BaseExpressServer';
import * as express from 'express';
import { exportRouter } from './routes/exportRouter';
import * as path from "path";

export class HtmlExportServer extends BaseExpressServer {

    protected setRoutes(app: express.Express): void {
        app.use('/', (req, res, next) => {
            if (req.path !== '/') {
                next();
                return;
            }
            res.status(200).send({});
        });
        let exportresultspath = path.join(__dirname, '../chromeExportService/out')
        this.app.use('/exportResults/', express.static(exportresultspath));
        app.use('/export', exportRouter);

    }

    protected getAppName(): string {
        return 'htmlExportServer';
    }

}

