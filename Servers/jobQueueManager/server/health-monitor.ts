import * as superagent from "superagent";
import { setTimeout } from "timers";

export async function monitorHealth(
  jobModel: any,
  intervalMs: number,
  timeoutMs: number
): Promise<void> {
  const scheduled = async () => {
    await timeoutJobs(jobModel, timeoutMs);
    setTimeout(scheduled, intervalMs);
  };
  setTimeout(scheduled, intervalMs);
}

export async function timeoutJobs(
  jobModel: any,
  timeoutMs: number
): Promise<any> {
  const jobs = await new Promise<any>((resolve, reject) => {
    jobModel.find(
      {
        where: {
          and: [
            { status: "started" },
            {
              updated: {
                lte: new Date(new Date().valueOf() - timeoutMs).valueOf()
              }
            }
          ]
        }
      },
      (error, result) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(result);
      }
    );
  });
  if (jobs && jobs.length) {
    const noRetries = jobs.filter(job => !job.output);
    const firstRetry = jobs.filter(
      job => job.output && !("retries" in job.output)
    );
    const multiRetries = jobs.filter(job => job.output && job.output.retries);
    const fail = jobs.filter(
      job => job.output && "retries" in job.output && !job.output.retries
    );

    await new Promise((resolve, reject) => {
      jobModel.updateAll(
        { id: { inq: noRetries.map(job => job.id) } },
        {
          status: "queued",
          output: { message: "timeout" },
          updated: new Date()
        },
        (error, result) => {
          if (error) {
            reject(error);
            return;
          }
          resolve(result);
        }
      );
    });

    await Promise.all(
      firstRetry.map(retryJob => {
        return new Promise((resolve, reject) => {
          jobModel.updateAll(
            { id: retryJob.id },
            {
              status: "queued",
              output: { message: "timeout", retries: 5 },
              updated: new Date(),
              priority: retryJob.priority - 1
            },
            (error, result) => {
              if (error) {
                reject(error);
                return;
              }
              resolve(result);
            }
          );
        });
      })
    );

    await Promise.all(
      multiRetries.map(retryJob => {
        return new Promise((resolve, reject) => {
          jobModel.updateAll(
            { id: retryJob.id },
            {
              status: "queued",
              output: {
                message: "timeout",
                retries: retryJob.output.retries - 1
              },
              updated: new Date()
            },
            (error, result) => {
              if (error) {
                reject(error);
                return;
              }
              resolve(result);
            }
          );
        });
      })
    );

    await Promise.all(
      fail.map(retryJob => {
        return new Promise((resolve, reject) => {
          jobModel.updateAll(
            { id: retryJob.id },
            {
              status: "error",
              output: { message: "timeout", retries: 0 },
              updated: new Date()
            },
            (error, result) => {
              if (error) {
                reject(error);
                return;
              }
              resolve(result);
            }
          );
        });
      })
    );

    await Promise.all(
      jobs.filter(job => job.callbackUrl).map(job =>
        superagent
          .post(job.callbackUrl)
          .send({ error: { message: "timeout" } })
          .catch(err => {})
      )
    ).catch(err => {});
  }
}
