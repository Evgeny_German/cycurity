import * as loopback from "loopback";
import * as boot from "loopback-boot";
import * as dotenv from "dotenv";
import * as path from "path";
import { monitorHealth } from "./health-monitor";

dotenv.config({ path: path.join(__dirname, "./config/.env") });

function autoMigrate(
  datasource: string,
  callback: (error: Error, result: any) => void,
  ...tables: string[]
): void {
  const ds = app["datasources"][datasource];
  ds.autoupdate(tables, err => {
    callback(err, null);
  });
}

const app = loopback();

export default app;

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit("started");
    var baseUrl = app.get("url").replace(/\/$/, "");
    console.log("Web server listening at: %s", baseUrl);
    if (app.get("loopback-component-explorer")) {
      var explorerPath = app.get("loopback-component-explorer").mountPath;
      console.log("Browse your REST API at %s%s", baseUrl, explorerPath);
    }
    autoMigrate(
      "cycuritydb",
      error => {
        if (error) {
          console.error("automigrate failed.");
          process.exit(-1);
        }
        console.log("finished automigrate.");
      },
      "scrapeJob"
    );
    let prop = (<any>app.models).scrapeJob;
    monitorHealth(prop, 300000, 900000);
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) app.start();
});
