import * as superagent from "superagent";

module.exports = function(Job) {
  Job.enqueueJob = function(
    input: any,
    priority: number,
    callbackUrl: string,
    callback: ((error: Error, result: any) => void)
  ): void {
    Job.create(
      { input: input, status: "queued", priority: priority, callbackUrl: callbackUrl, updated: new Date() },
      callback
    );
  };

  Job.dequeueJob = function(
    callback: (error: Error, result: any) => void
  ): void {
    Job.findOne(
      { where: { status: "queued" }, order: ["priority DESC", "id ASC"], limit: 1 },
      (error, result) => {
        if (error) {
          callback(error, null);
          return;
        }
        if (result) {
          Job.updateAll(
            { id: result.id },
            { status: "started", updated: new Date() },
            (error2, result2) => {
              if (error2) {
                callback(error2, null);
                return;
              }
              callback(null, result);
            }
          );
          return;
        }
        callback(null, null);
      }
    );
  };

  Job.finishJob = function(
    id: number,
    output: any,
    error: any,
    callback: (error: Error, result: any) => void
  ) {
    Job.upsert(
      {
        id: id,
        output: output || error,
        status: output ? "finished" : "error",
        updated: new Date()
      },
      (error2, result) => {
        if (error2 || !result) {
          callback(error2, null);
          return;
        }
        if (result.callbackUrl) {
          (async () => {
            try {
              await superagent
                .post(result.callbackUrl)
                .send(error ? { error: error } : { output: output });
            } catch (error) {}
          })();
        }
        callback(null, result);
      }
    );
  };

  Job.remoteMethod("dequeueJob", {
    returns: {
      root: true,
      type: "object"
    },
    http: { verb: "POST" }
  });

  Job.remoteMethod("enqueueJob", {
    accepts: [
      {
        arg: "input",
        type: "object",
        required: true,
        http: { source: "body" }
      },
      {
        arg: "priority",
        type: "number",
        required: true,
        http: { source: "query" }
      },
      {
        arg: "callbackUrl",
        type: "string",
        required: false,
        http: { source: "query" }
      }
    ],
    returns: {
      root: true,
      type: "object"
    },
    http: { verb: "POST" }
  });

  Job.remoteMethod("finishJob", {
    accepts: [
      { arg: "id", type: "number", required: true, http: { source: "query" } },
      {
        arg: "output",
        type: "any",
        required: false,
        http: ctx => ctx.req.body.output
      },
      {
        arg: "error",
        type: "any",
        required: false,
        http: ctx => ctx.req.body.error
      }
    ],
    returns: {
      root: true,
      type: "object"
    },
    http: { verb: "POST" }
  });
};
