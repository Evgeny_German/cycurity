import { BaseExpressServer } from "../../infrastructure/baseExpressService/BaseExpressServer";
import { Express, Router } from "express";
import * as express from "express";
import * as path from "path";
import { documentStorageRouter } from "./routes/documentStorageRouter";
import { userManageRouter } from "./routes/userManageRouter";
import {
  authMiddleware,
  authenticateModule
} from "./middlewares/authMiddleware";
import { engagorServiceRouter } from "./routes/engagorServiceRouter";
import { htmlExportRouter } from "./routes/htmlExportRouter";
import { twitterRouter } from "./routes/twitterRouter";
import { utilsRouter } from "./routes/utilsRouter";
import { projectManagementRouter } from "./routes/projectManagementRouter";
import * as url from "url";
import { jobRouter } from "./routes/jobRouter";
import * as request from "superagent";
import { voRouter } from "./routes/voRouter";
import { ILogOptions, LoggerImpl } from "./logger/loggerImpl";
import { DocumentStorageFacade } from "./facades/documentStorageFacade";
import { Request, Response } from "express";

import { S3 } from "aws-sdk";
import { proxyRouter } from "./routes/proxyRouter";

export class AdminHost extends BaseExpressServer {
  constructor() {
    super();
  }

  protected setLogger() {
    if (process.env.SUPPORT_S3_LOGS == "true") {
      let loggerConfig: ILogOptions = {
        console: false,
        s3: {
          bucket: process.env.S3_BUCKET_NAME,
          access_key_id: process.env.S3_ACCESS_KEY,
          secret_access_key: process.env.S3_SECRET_ACCESS_KEY,
          folder: "routes"
        }
      };
      this.app.use(new LoggerImpl().getExpressLogger(loggerConfig));
    }
  }

  public getProfileImage(req: Request, res: Response): void {
    (async () => {
      await new DocumentStorageFacade().getProfileImage(
        req.query["username"],
        res
      );
    })();
  }

  protected setRoutes(app: Express): void {
    app.get("/getProfileImage", this.getProfileImage);
    app.use("/projectManagement", authMiddleware, projectManagementRouter);
    app.use("/proxy", authMiddleware, proxyRouter);
    app.use(
      "/vo",
      authenticateModule("vp", req => {
        const parsed = url.parse(req.originalUrl);
        const normalized = parsed.pathname.trim().toLowerCase();
        return normalized === "/vo/voroles" || normalized === "/vo/volaunches";
      }),
      voRouter
    );
    app.use("/documentStorage", authMiddleware, documentStorageRouter);
    app.use("/userManage", userManageRouter);
    app.use("/job", authMiddleware, jobRouter);
    app.use("/engagor", authMiddleware, engagorServiceRouter);
    app.use("/scrape", authenticateModule("reports"), twitterRouter);
    app.use(
      "/export",
      authenticateModule("reports", req => {
        const parsed = url.parse(req.originalUrl);
        return parsed.pathname.trim().toLowerCase() === "/export/word";
      }),
      htmlExportRouter
    );
    app.use("/utils", utilsRouter);
  }

  protected getAppName(): string {
    return "AdminHost";
  }

  setStaticFolders(app: Express) {
    let distpath = path.join(__dirname, "../../Clients/adminClient/dist");
    this.app.use(express.static(distpath));
    let modulespath = path.join(
      __dirname,
      "../../Clients/adminClient/node_modules"
    );
    this.app.use(express.static(modulespath));
    let staticpath = path.join(__dirname, "./staticStorage");
    this.app.use("/static/", express.static(staticpath));
    this.app.use("/s3/", (req, res, next) => {
      (async () => {
        const s3 = new S3({
          apiVersion: "2006-03-1",
          region: process.env.STATIC_S3_REGION,
          credentials: {
            accessKeyId: process.env.STATIC_S3_KEY_ID,
            secretAccessKey: process.env.STATIC_S3_SECRET
          }
        });
        let stream;
        try {
          const u = url.parse(req.url).pathname.split("/");
          const key = u[u.length - 1];
          stream = s3
            .getObject({ Bucket: process.env.STATIC_S3_BUCKET, Key: key })
            .createReadStream();
        } catch (error) {
          res.status(400).send({});
          return;
        }
        stream.pipe(
          res.status(200),
          { end: true }
        );
      })();
    });
    this.app.use("/exportResults/", (req, res, next) => {
      request(
        req.method,
        `${process.env.EXPORT_SERVICE}${url.parse(req.originalUrl).path}`
      )
        .send(req.body)
        .pipe(res);
    });
    app.use("/", (req, res, next) => {
      if (req.path !== "/") {
        next();
        return;
      }
      res
        .status(302)
        .set({ location: "/index.html" })
        .send();
    });
    app.use((req, res, next) => {
      if (!url.parse(req.url).path.startsWith("/wrapper.html")) {
        next();
        return;
      }

      res.status(200).sendFile("wrapper.html", {
        root: path.resolve(__dirname, "../../Clients/adminClient/dist")
      });
    });
    this.app.use("/apiref", express.static(path.join(__dirname, "./swagger")));
  }
}
