import * as request from "superagent";
import { DocumentStorageFacade } from "./documentStorageFacade";
import { twitterRouter } from "../routes/twitterRouter";
import { URL } from "url";
import { error } from "util";
import { json } from "body-parser";
import { queryJobs, updateJob } from "../jobManager";
import * as Url from "url";
import * as _ from "lodash";

export class TwitterFacade {
  private readonly baseUrl: string;

  constructor() {
    this.baseUrl = process.env["TWITTER_URL"];
  }

  public async mail(mail: string, usernames: string[]): Promise<void> {
    await request
      .post(`${this.baseUrl}/mail?mail=${encodeURIComponent(mail)}`)
      .send(usernames);
  }

  public async profilesScrape(userNames: string[]): Promise<any> {
    const result = await request
      .post(`${this.baseUrl}/scrape/profiles`)
      .send({ userNames: userNames });
    return result.body;
  }

  public profileAndStatisticsScrape(
    userNames: string[],
    concurrency?: number
  ): any {
    return request
      .post(
        `${this.baseUrl}/scrape/profileAndStatistics${
          concurrency ? `?concurrency=${concurrency}` : ``
        }`
      )
      .send({ userNames: userNames })
      .then(res => res.body);
  }

  public conversationByTweetScrape(tweetId: string, userName: string): any {
    return request
      .get(
        `${
          this.baseUrl
        }/scrape/conversationByTweet?tweetId=${tweetId}&userName=${userName}`
      )
      .then(res => res.body);
  }

  public async getProfile(
    userId: number,
    ownerId: number,
    userName: string
  ): Promise<any> {
    try {
      const facade = new DocumentStorageFacade();
      console.log(
        `twitterFacade: received profile request for user: ${userName}`
      );
      const latest = await facade
        .getLatest(userId, ownerId, 3, userName)
        .catch(error => {
          console.error("twitterFacade: error fetching latest.");
          throw error;
        })
        .then(result => {
          console.log("twitterFacade: got latest.");
          return result;
        });
      const oldDocument = latest && latest.data ? latest.data.data : null;
      if (oldDocument) return oldDocument;
      console.log(
        `twitterFacade: requesting profile from scraper for user: ${userName}`
      );
      const newDocument = await this.profilesScrape([userName])
        .catch(error => {
          console.error("twitterFacade: error scraping profile.");
          throw error;
        })
        .then(result => {
          console.log("twitterFacade: got profile from scraper.");
          return result["model"][userName];
        });
      await facade
        .saveLatest(userId, ownerId, 3, userName, newDocument)
        .catch(error => {
          console.error("twitterFacade: error saving latest.");
          throw error;
        })
        .then(result => {
          console.log("twitterFacade: saved latest.");
          return result;
        });
      return newDocument;
    } catch (error) {
      console.error("twitterFacade: error fetching profile.");
      throw error;
    }
  }

  public async scrapeReport(
    userId: number,
    ownerId: number,
    reportId: string,
    urls: string[]
  ) {
    function parseUrl(url: string): { userName: string; tweetId: string } {
      const u = Url.parse(_.trim(url));
      return {
        userName: _.trim(u.path, "/")
          .split("/")[0]
          .toLowerCase(),
        tweetId: _.trim(u.path, "/").split("/")[2]
      };
    }
    const newUrls = await Promise.all(
      urls.map(async u => {
        let parsed = parseUrl(u);
        let resp;
        try {
          resp = await request.get(
            `https://twitter.com/${parsed.userName}/status/${parsed.tweetId}`
          );
        } catch (error) {}
        if (resp && resp["redirects"] && resp["redirects"].length) {
          let p = parseUrl(resp["redirects"][resp["redirects"].length - 1]);
          if (p.tweetId) {
            parsed = p;
          }
          return `https://twitter.com/${parsed.userName}/status/${
            parsed.tweetId
          }`;
        }
        return null;
      })
    );
    if (newUrls.some(u => !!u)) {
      const facade = new DocumentStorageFacade();
      const report = await facade.getLatest(userId, ownerId, 2, reportId);
      report.data.data.conversations.forEach(con => {
        const p1 = parseUrl(con.url);
        const u = newUrls.findIndex(r => {
          const p2 = parseUrl(r);
          return p1.tweetId === p2.tweetId;
        });
        if (u >= 0) {
          con.url = newUrls[u];
          const p3 = parseUrl(newUrls[u]);
          con.userName = p3.userName;
          con.tweetId = p3.tweetId;
        }
      });
      await facade.saveLatest(userId, ownerId, 2, reportId, report.data.data);
    }
    newUrls.forEach((u, i) => {
      if (u) urls[i] = u;
    });
    return (await request.post(`${this.baseUrl}/scrape/scrapeReport`).send({
      userId: userId,
      ownerId: ownerId,
      reportId: reportId,
      urls: urls
    })).body;
  }

  public async fetchReport(
    userId: number,
    ownerId: number,
    reportId: string
  ): Promise<any> {
    function parseUrl(url: string): { userName: string; tweetId: string } {
      const u = Url.parse(_.trim(url));
      return {
        userName: _.trim(u.path, "/")
          .split("/")[0]
          .toLowerCase(),
        tweetId: _.trim(u.path, "/").split("/")[2]
      };
    }

    const facade = new DocumentStorageFacade();
    let latest: any;
    latest = await facade.getLatest(userId, ownerId, 2, reportId);
    /*const jobs = (await queryJobs({
      where: {
        and: [
          { user_id: userId },
          { type: 2 },
          { status: { inq: ["queued", "started", "scraped", "error"] } }
        ]
      }
    })).filter(job => job.payload && job.payload.reportId === reportId);
    const scraped = jobs.filter(job => job.status === "scraped");
    if (scraped && scraped.length) {
      await Promise.all(
        scraped.map(async job => {
          const conv = latest.data.data.conversations.find(conv => {
            if (!conv.url) return false;
            const url = parseUrl(conv.url);
            return (
              url.tweetId === job.payload.tweetId &&
              url.userName === job.payload.userName
            );
          });
          if (!conv) return;
          const parsed = parseUrl(conv.url);
          conv.avatar = job.output.output.tweet.avatar;
          conv.fullName = job.output.output.tweet.fullName;
          conv.userName = job.output.output.tweet.userName;
          conv.tweetText = job.output.output.tweet.tweetText;
          conv.tweetId = job.output.output.tweet.tweetId;
          conv.reportId = reportId;
          const o = {
            success: job.output.success,
            error: undefined,
            output: undefined
          };
          await Promise.all([
            (async () =>
              await facade.saveLatest(
                userId,
                ownerId,
                1,
                `https://twitter.com/${parsed.userName}/status/${
                  parsed.tweetId
                }?reportId=${reportId}`,
                {
                  conversation: job.output.output,
                  lastScraped: new Date()
                }
              ))(),
            (async () => await updateJob(job.id, "finished", o))()
          ]);
        })
      );
      await facade.saveLatest(userId, ownerId, 2, reportId, latest.data.data);
    }
    jobs.filter(job => job.status !== "scraped").forEach(job => {
      const conv = latest.data.data.conversations.find(conv => {
        const parsed = parseUrl(conv.url);
        return (
          parsed.tweetId === job.payload.tweetId &&
          parsed.userName === job.payload.userName
        );
      });
      if (!conv) return;
      conv.status = job.status;
    });*/
    return latest;
  }

  public async conversationByTweet(
    userId: number,
    ownerId: number,
    userName: string,
    tweetId: string,
    reportId: string,
    forceRescrape: boolean
  ): Promise<any> {
    try {
      function parseUrl(url: string): { userName: string; tweetId: string } {
        const u = Url.parse(_.trim(url));
        return {
          userName: _.trim(u.path, "/")
            .split("/")[0]
            .toLowerCase(),
          tweetId: _.trim(u.path, "/").split("/")[2]
        };
      }
      let url = `https://twitter.com/${userName}/status/${tweetId}`;
      let parsed = { userName: userName, tweetId: tweetId };
      let deleted = false;
      let nameChanged = false;
      let resp;
      try {
        resp = await request.get(
          `https://twitter.com/${userName}/status/${parsed.tweetId}`
        );
      } catch (error) {
        deleted = error.status === 404;
      }
      if (resp && resp["redirects"] && resp["redirects"].length) {
        let p = parseUrl(resp["redirects"][resp["redirects"].length - 1]);
        if (p.tweetId) {
          parsed = p;
          nameChanged = true;
        }
      }
      // userName = parsed.userName;
      // tweetId = parsed.tweetId;
      const facade = new DocumentStorageFacade();
      console.log(
        `twitterFacade: received conversation request for user: ${
          parsed.userName
        }, tweet: ${url}`
      );
      url = `https://twitter.com/${parsed.userName}/status/${
        parsed.tweetId
      }?reportId=${reportId}`;
      const latest = await facade
        .getLatest(userId, ownerId, 1, url)
        .catch(error => {
          console.error("twitterFacade: error fetching latest.");
          throw error;
        })
        .then(result => {
          console.log("twitterFacade: got latest.");
          return result;
        });
      const oldDocument = latest && latest.data ? latest.data.data : null;
      let thresholdDate: Date;
      if (oldDocument) {
        thresholdDate = new Date(oldDocument.lastScraped);
        thresholdDate.setMinutes(
          thresholdDate.getMinutes() + +process.env.scraperCacheMinutes
        );
      }
      if (deleted && forceRescrape) {
        throw new Error("Tweet deleted.");
      }
      if (
        !deleted &&
        (forceRescrape ||
          nameChanged ||
          !thresholdDate ||
          Date.now() > thresholdDate.valueOf())
      ) {
        console.log(
          `twitterFacade: requesting tweet from scraper for url: ${url}`
        );
        try {
          const res = await request
            .get(
              `${this.baseUrl}/scrape/conversationByTweet?tweetId=${
                parsed.tweetId
              }&userName=${parsed.userName}`
            )
            .then(result => {
              console.log("twitterFacade: got conversation from scarper.");
              return result;
            });
          const newDocument = res["body"]["model"];
          if (!newDocument.profile) {
            throw new Error("No profile");
          }
          if (!oldDocument) {
            const res2 = await facade
              .saveLatest(userId, ownerId, 1, url, {
                lastScraped: new Date(),
                conversation: newDocument
              })
              .catch(error => {
                console.error("twitterFacade: error saving latest.");
                throw error;
              })
              .then(result => {
                console.log("twitterFacade: saved latest.");
                return result;
              });
            await this.updateReportsPerConversation(userId, ownerId, {
              userName: parsed.userName,
              tweetId: parsed.tweetId,
              avatar: res2.data.data.conversation.tweet.avatar,
              fullName: res2.data.data.conversation.tweet.fullName,
              tweetText: res2.data.data.conversation.tweet.tweetText,
              reportId: reportId
            });
            return res2.data.data.conversation;
          } else {
            const diff = diffTweets(
              oldDocument.conversation.replies,
              newDocument.replies
            );
            oldDocument.conversation.tweet = Object.assign(newDocument.tweet, {
              meta: oldDocument.conversation.tweet.meta
            });
            oldDocument.conversation.profile = Object.assign(
              newDocument.profile,
              {
                meta: oldDocument.conversation.profile.meta
              }
            );
            oldDocument.conversation.replies = diff;
            const res2 = await facade
              .saveLatest(userId, ownerId, 1, url, {
                lastScraped: new Date(),
                conversation: oldDocument.conversation
              })
              .catch(error => {
                console.error("twitterFacade: error saving latest.");
                throw error;
              })
              .then(result => {
                console.log("twitterFacade: saved latest.");
                return result;
              });
            await this.updateReportsPerConversation(userId, ownerId, {
              userName: parsed.userName,
              tweetId: parsed.tweetId,
              avatar: res2.data.data.conversation.tweet.avatar,
              fullName: res2.data.data.conversation.tweet.fullName,
              tweetText: res2.data.data.conversation.tweet.tweetText,
              reportId: reportId
            });
            return res2.data.data.conversation;
          }
        } catch (err) {
          return await facade
            .getLatest(
              userId,
              ownerId,
              1,
              `https://twitter.com/${userName}/status/${tweetId}?reportId=${reportId}`
            )
            .catch(error => {
              console.error("twitterFacade: error fetching latest.");
              throw error;
            })
            .then(result => {
              console.log("twitterFacade: got latest.");
              return result.data.data.conversation;
            });
        }
      } else {
        console.log(
          `twitterFacade: returning conversation from document storage for tweet: ${url}`
        );
        return oldDocument.conversation;
      }
    } catch (error) {
      console.error("error getting conversation by tweet: ", error);
      throw error;
    }
  }

  public async updateReportsPerConversation(
    userId: number,
    ownerId: number,
    data: any
  ): Promise<void> {
    const facade = new DocumentStorageFacade();
    return await facade.updateReportsPerConversation(userId, ownerId, data);
  }
}

function diffTweets(
  oldTweets: {
    tweetId: string;
    tweetText: string;
    fullName: string;
    userName: string;
    deleted: boolean;
  }[],
  newTweets: {
    tweetId: string;
    tweetText: string;
    fullName: string;
    userName: string;
    deleted: boolean;
  }[]
): {
  tweetId: string;
  tweetText: string;
  fullName: string;
  userName: string;
  deleted: boolean;
}[] {
  oldTweets = oldTweets.reverse();
  newTweets = newTweets.reverse();
  return newTweets
    .map(tweet => {
      const oldTweet = oldTweets.find(
        tweet1 => tweet.tweetId === tweet1.tweetId
      );
      if (oldTweet) oldTweets = oldTweets.filter(tweet1 => tweet1 !== oldTweet);
      return Object.assign(tweet, {
        meta: oldTweet ? oldTweet["meta"] : undefined
      });
    })
    .reverse();
}
