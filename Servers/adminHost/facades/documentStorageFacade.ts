import * as request from "superagent";
import { TwitterFacade } from "./twitterFacade";
import { Response } from "express";

export class DocumentStorageFacade {
  private readonly baseUrl: string;

  constructor() {
    this.baseUrl = process.env["DOCUMENT_STORAGE_URL"];
  }

  private imagePromises = {};

  public async getProfileImage(userName: string, res: Response) {
    let record;
    try {
      record = (await request.get(
        `${this.baseUrl}/api/profileImages/get?username=${encodeURIComponent(
          userName
        )}`
      )).body;
    } catch (error) {}

    let resp: request.Response;
    if (record) {
      try {
        resp = await request.get(record);
      } catch (error) {
        console.error(error);
      }
    }

    if (resp) {
      res
        .set({
          "Content-Type": resp.get("Content-Type"),
          "Content-Length": resp.get("Content-Length")
        })
        .status(200)
        .send(resp.body);
      return;
    }

    const spaces = Array.from(userName).filter(c => c === " ").length;
    if (spaces != 1) {
      res.status(400).send();
      return;
    }

    const split = userName.split(" ");
    if (
      split[1] !== "avatar" &&
      split[1] !== "profile_image" &&
      split[1] !== "profile_banner" &&
      split[1] !== "profile_image_https" &&
      split[1] !== "profile_background_image" &&
      split[1] !== "profile_background_image_https"
    ) {
      res.status(400).send();
      return;
    }

    let profiles;

    if (this.imagePromises[split[0]]) {
      try {
        profiles = await this.imagePromises[split[0]];
      } catch (error) {
        res.status(500).send();
        return;
      }
    } else {
      this.imagePromises[split[0]] = new TwitterFacade().profilesScrape([
        split[0]
      ]);
      try {
        profiles = await this.imagePromises[split[0]];
      } catch (error) {
        delete this.imagePromises[split[0]];
        res.status(500).send();
        return;
      }
      delete this.imagePromises[split[0]];
    }

    const profile = profiles.model[Object.keys(profiles.model)[0]].profile;

    if (!profile) {
      res.status(500).send();
      return;
    }

    let url;

    if (split[1] === "avatar") {
      url = profile.profile_image_url_https;
    }
    if (split[1] === "profile_image") {
      url = profile.profile_image_url;
    }
    if (split[1] === "profile_image_https") {
      url = profile.profile_image_url_https;
    }
    if (split[1] === "profile_background_image") {
      url = profile.profile_background_image_url;
    }
    if (split[1] === "profile_background_image_https") {
      url = profile.profile_background_image_url_https;
    }
    if (split[1] === "profile_banner") {
      if (!profile.profile_banner_url) {
        res.status(404).send();
        return;
      }
      url = profile.profile_banner_url;
    }
    if (!url) {
      res.status(500).send();
      return;
    }
    let result;
    try {
      result = (await request.get(url)).body;
    } catch (error) {
      try {
        url = `${url}/1500x500`;
        result = (await request.get(url)).body;
      } catch (error2) {
        res.status(error.status || 500).send(error);
        return;
      }
    }
    await request.post(`${this.baseUrl}/api/profileImages/set`).send([
      {
        username: userName,
        url
      }
    ]);
    res.status(200).send(result);
  }

  public setProfileImage(username: string, url: string): Promise<any> {
    return request
      .post(`${this.baseUrl}/api/profileImages/set`)
      .send([{ username, url }])
      .then(res => res.body);
  }

  public attach(
    userId: number,
    relationId: number,
    objectId: number,
    subjectId: number
  ): Promise<any> {
    return request
      .post(`${this.baseUrl}/api/documentRelations`)
      .send({
        objectId: objectId,
        subjectId: subjectId,
        relationId: relationId,
        ownerId: userId
      })
      .then(res => res.body);
  }

  public detach(
    userId: number,
    relationId: number,
    objectId: number,
    subjectId: number
  ): Promise<any> {
    return request
      .post(`${this.baseUrl}/api/documentRelations/detach`)
      .send({
        objectId: objectId,
        subjectId: subjectId,
        relationId: relationId,
        ownerId: userId
      })
      .then(res => res.body);
  }

  public listRelations(userId: number, relationId: number): Promise<any> {
    return request
      .get(
        `${
          this.baseUrl
        }/api/documentRelations/byRelationId?userId=${userId}&relationId=${relationId}`
      )
      .then(res => res.body);
  }

  public getLatest(
    userId: number,
    ownerId: number,
    type: number,
    documentId: string
  ): Promise<any> {
    return request
      .get(
        `${
          this.baseUrl
        }/api/documents/getLatest?userId=${userId}&ownerId=${ownerId}&type=${type}&documentId=${documentId}`
      )
      .then(res => res.body);
  }

  public purge(
    userId: number,
    ownerId: number,
    type: number,
    documentId: string
  ): Promise<any> {
    return request
      .delete(
        `${
          this.baseUrl
        }/api/documents/purge?userId=${userId}&ownerId=${ownerId}&type=${type}&documentId=${documentId}`
      )
      .then(res => res.body);
  }

  public saveLatest(
    userId: number,
    ownerId: number,
    type: number,
    documentId: string,
    data: object
  ): Promise<any> {
    return request
      .post(`${this.baseUrl}/api/documents/saveLatest`)
      .set({ "Content-Type": "application/json" })
      .send({
        userId: +userId,
        ownerId: +ownerId,
        type: type,
        documentId: documentId,
        data: data
      })
      .then(result => result.body);
  }

  public updateReportsPerConversation(
    userId: number,
    ownerId: number,
    data: any
  ): Promise<any> {
    return request
      .post(`${this.baseUrl}/api/documents/updateReportsPerConversation`)
      .set({ "Content-Type": "application/json" })
      .send({
        userId: userId,
        ownerId: ownerId,
        data: data
      })
      .then(result => result.body);
  }

  public search(userId: number, type: number, freeText: string): Promise<any> {
    const docStorageApi = `${
      this.baseUrl
    }/api/documents/search?userId=${userId}&type=${type}${
      freeText ? `&freeText=${freeText}` : ""
    }`;
    console.log(
      `DocumentStorageFacade:search - search request for type :${type}, Free text: ${freeText} . Calling storage api: ${docStorageApi}`
    );
    return request.get(docStorageApi).then(res => res.body);
  }
}
