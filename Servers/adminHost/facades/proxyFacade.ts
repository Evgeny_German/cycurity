import * as superagent from "superagent";

export class ProxyFacade {
  constructor(private readonly baseUrl: string) {}

  public async getProxyManageList(
    userId: number,
    limit: number,
    onlyRunning: boolean
  ): Promise<any> {
    return (await superagent
      .post(`${this.baseUrl}/proxy/getProxyManageList`)
      .set({ user_id: userId })
      .send({ limit, onlyRunning })).body;
  }
  public async getSupportedRegions(userId: number): Promise<any> {
    return (await superagent
      .get(`${this.baseUrl}/proxy/getSupportedRegions`)
      .set({ user_id: userId })).body;
  }
  public async getImages(userId: number): Promise<any> {
    return (await superagent
      .get(`${this.baseUrl}/proxy/getImages`)
      .set({ user_id: userId })).body;
  }
  public async setActiveImage(
    userId: number,
    imageName: string,
    region: string,
    provider: string
  ): Promise<any> {
    return (await superagent
      .post(`${this.baseUrl}/proxy/setActiveImage`)
      .set({ user_id: userId })
      .send({ imageName, region, provider })).body;
  }
  public async updateProviderRegions(
    userId: number,
    provider: string
  ): Promise<any> {
    return (await superagent
      .post(`${this.baseUrl}/proxy/updateProviderRegions`)
      .set({ user_id: userId })
      .send({ provider })).body;
  }
  public async createInstance(
    userId: number,
    region: string,
    provider: string,
    instanceName: string
  ): Promise<any> {
    return (await superagent
      .post(`${this.baseUrl}/proxy/createInstance`)
      .set({ user_id: userId })
      .send({ userId, region, provider, instanceName })).body;
  }
  public async terminateInstance(
    userId: number,
    region: string,
    provider: string,
    instanceId: string
  ): Promise<any> {
    return (await superagent
      .post(`${this.baseUrl}/proxy/terminateInstance`)
      .set({ user_id: userId })
      .send({ region, provider, instanceId })).body;
  }
  public async copyImage(
    userId: number,
    region: string,
    provider: string
  ): Promise<any> {
    return (await superagent
      .post(`${this.baseUrl}/proxy/copyImage`)
      .set({ user_id: userId })
      .send({ region, provider })).body;
  }
  public async getImageStatus(
    userId: number,
    region: string,
    provider: string,
    imageId: string
  ): Promise<any> {
    return (await superagent
      .post(`${this.baseUrl}/proxy/getImageStatus`)
      .set({ user_id: userId })
      .send({ region, provider, imageId })).body;
  }
  public async getInstanceListFromProvider(
    userId: number,
    region: string,
    provider: string,
    filter: any
  ): Promise<any> {
    return (await superagent
      .post(`${this.baseUrl}/proxy/getInstanceListFromProvider`)
      .set({ user_id: userId })
      .send({ region, provider, filter })).body;
  }
  public async testProxy(
    userId: number,
    url: string,
    proxyIp: string,
    proxyPort: any
  ): Promise<any> {
    return (await superagent
      .post(`${this.baseUrl}/proxy/testProxy`)
      .set({ user_id: userId })
      .send({ url, proxyIp, proxyPort })).body;
  }
  public async getLastConnectionTime(
    userId: number,
    region: string,
    provider: string,
    instanceName: string
  ): Promise<any> {
    return (await superagent
      .post(`${this.baseUrl}/proxy/getLastConnectionTime`)
      .set({ user_id: userId })
      .send({ userId, region, provider, instanceName })).body;
  }

}
