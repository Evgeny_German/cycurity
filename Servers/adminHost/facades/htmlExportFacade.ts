//import request = require("superagent");
import * as http from "http";
import * as request from "request"


export class HtmlExportFacade {

    //Not in use since need to route the result back
    public exportPdf(url: string, token: string, response: http.ServerResponse) {
        let htmlRendererUrl = process.env.EXPORT_SERVICE
        let fullUrl = `${url};access_token=${token}`

        return request.post({
            url: htmlRendererUrl, form: {
                fileName: "report.pdf",
                url: fullUrl
            }
        }).pipe(response);

    }
}
