import * as request from "superagent";

export class UserManageFacade {
  baseUrl = `${process.env.userManagementAdd}/api`;

  login = (email: string, password: string) => {
    let url = `${this.baseUrl}/cy_users/login`;
    return request
      .post(url)
      .send({ email, password })
      .then(res => {
        return res.body;
      });
  };

  loginToken = (token: string) => {
    let url = `${this.baseUrl}/cy_users/tokenLogin?token=${encodeURIComponent(
      token
    )}`;
    return request.get(url).then(res => {
      return res.body;
    });
  };

  public async getUsers(filter: any): Promise<any> {
    return await request.get(
      `${this.baseUrl}/cy_users${
        filter ? `?filter=${encodeURIComponent(JSON.stringify(filter))}` : ""
      }`
    );
  }
}
