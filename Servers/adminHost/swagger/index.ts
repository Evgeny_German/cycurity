import { SwaggerUIBundle, SwaggerUIStandalonePreset } from "swagger-ui-dist";

window.onload = function() {
  var swaggerOptions = {
    url: "./swagger.yaml",
    dom_id: "body",
    deepLinking: true,
    presets: [SwaggerUIBundle.presets.apis, SwaggerUIStandalonePreset],
    plugins: [SwaggerUIBundle.plugins.DownloadUrl],
    layout: "StandaloneLayout"
  };

  var ui = SwaggerUIBundle(swaggerOptions);

  window["ui"] = ui;
};
