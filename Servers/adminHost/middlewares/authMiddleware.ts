import * as request from "superagent";
import { Request, Response, NextFunction } from "express";

class AuthenticationMiddleware {
  authenticateCall = (req, res, next) => {
    let url = process.env.securityApiBase + "auth/authenticate";
    request
      .post(url)
      .send(req.headers)
      .end((err, authRes) => {
        if (err) {
          res.sendStatus(401, err);
        } else {
          if (authRes && authRes.body && authRes.body.isAuthorized) {
            req.token = authRes.body.token;
            req.decodedToken = authRes.body.decodedToken;
            next();
          } else {
            res.sendStatus(401, err);
          }
        }
      });
  };
}

export const authMiddleware = new AuthenticationMiddleware().authenticateCall;
export function authenticateModule(
  module: string,
  except: (req: Request) => boolean = null
): (req: Request, res: Response, next: NextFunction) => void {
  return (req, res, next) => {
    (async () => {
      const token =
        req.headers["authorization"] &&
        (<string>req.headers["authorization"]).startsWith("Bearer ")
          ? (<string>req.headers["authorization"]).split(" ")[1]
          : null;
      if (!token) {
        res.status(401).send("Unauthorized");
        return;
      }
      req["token"] = token;
      let user: any;
      try {
        user = (await request.get(
          `${
            process.env.USER_MANAGEMENT_URL
          }/api/cy_users/tokenLogin?token=${encodeURIComponent(token)}`
        )).body;
      } catch (error) {
        res.status(401).send("Unauthorized");
        return;
      }
      req["decodedToken"] = user;
      if (except && except(req)){
        next();
        return;
      }
      if (
        user.adminRole === "superadmin" ||
        (user.meta && user.meta.modules && user.meta.modules[module])
      ) {
        next();
        return;
      } else if (user.adminRole === "admin") {
        if (req.method.toLowerCase() === "get" || req.method.toLowerCase() === "head") {
          next();
          return;
        }
      }
      res.status(401).send("Unauthorized");
    })();
  };
}
