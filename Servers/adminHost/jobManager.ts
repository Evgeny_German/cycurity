import * as request from "superagent";

export async function queryJobs(filter?: any): Promise<any> {
  return (await request.get(
    `${process.env.JOB_SERVICE_URL}/api/jobs${
      filter ? `?filter=${encodeURIComponent(JSON.stringify(filter))}` : ""
    }`
  )).body;

}

export async function updateJob(
    id: number,
    status: string,
    output: any
  ): Promise<any> {
    return (await request
      .patch(`${process.env.JOB_SERVICE_URL}/api/jobs`)
      .send({ id: id, status: status, output: output })).body;
  }
  