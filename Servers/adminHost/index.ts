import * as fs from 'fs';
import * as path from "path";
import * as dotEnv from "dotenv";
import {AdminHost} from "./server";
import {LoggerImpl} from "./logger/loggerImpl";

let configPath = path.join(__dirname, "./config/.env")
dotEnv.config({path: configPath});

if(process.env.SUPPORT_S3_LOGS=="true") {
    new LoggerImpl().getConsoleLogger({console: true,
        s3: {bucket: process.env.S3_BUCKET_NAME,access_key_id: process.env.S3_ACCESS_KEY,secret_access_key: process.env.S3_SECRET_ACCESS_KEY,folder: "console"}});
}

const server = new AdminHost();

server.startServer(( process.env["ADMIN_HOST_SECURE"] || '').toLowerCase() == "true", {
    key: fs.readFileSync(path.join(__dirname, process.env["ADMIN_HOST_KEY"])),
    cert: fs.readFileSync(path.join(__dirname, process.env["ADMIN_HOST_CERT"])),
    ca: JSON.parse(process.env["ADMIN_HOST_CA"]).map(c => fs.readFileSync(path.join(__dirname, c)))
});

