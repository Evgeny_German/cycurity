import * as winston from "winston";
import * as expressWinston from "express-winston";
import * as os from "os";
import * as path from "path";

export var consoleLogger = null;

export type fileOption = { fileName: string, datePattern?: string, prepend?: boolean, maxDays?: number }
export type s3Option = { bucket: string, folder?: string, access_key_id: string, secret_access_key: string, nameFormat?: string }

export type ILogOptions = {
    console: boolean,
    file?: fileOption,
    s3?: s3Option
}
const defaultConsoleLogOptions: ILogOptions = {console: true,}
const defaultReqLogOptions: ILogOptions = {console: false, file: {fileName: "requests.log"}}

/*
In indexs.js add first thing:
new LoggerImpl().getConsoleLogger({console:true,file:{include:true,fileName:"consolelogs.log"}})

If logging requests as well inside server.js add before creating a router:
this.app.use(new LoggerImpl().getExpressLogger({console:true,file:{include:true,fileName:"consolelogs.log"}));
*/
export class LoggerImpl {

    getConsoleLogger = (options: ILogOptions = defaultConsoleLogOptions) => {
        let logTransports = this.getTransportsList(options);
        consoleLogger = new winston.Logger({
            transports: logTransports
        });
        require("./consoleOverrides")
    }


    getExpressLogger = (options: ILogOptions = defaultReqLogOptions) => {
        //Remove remark to add request body
        expressWinston.requestWhitelist.push('body')
        //Remove remark to add response body
        expressWinston.responseWhitelist.push('body')

        let expressLogger = expressWinston.logger({
            transports: this.getTransportsList(options),
            meta: true, // optional: control whether you want to log the meta data about the request (default to true)
            msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
            expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
            colorize: true, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
            ignoreRoute: function (req, res) {
                return false;
            } // optional: allows to skip some log messages based on request and/or response
        })
        return expressLogger;
    }

    private getTransportsList = (options: ILogOptions): any[] => {
        let logTransports = []
        if (options.console) {
            logTransports.push(new (winston.transports.Console)())
        }
        if (options && options.file) {
            logTransports.push(this.getRotateFileTransport(options.file))
        }
        if (options && options.s3) {
            logTransports.push(this.getS3Transport(options.s3))
        }
        return logTransports
    }

    private getRotateFileTransport = (fileOption: fileOption) => {
        //need to be called before using winston.transports.DailyRotateFile
        require('winston-daily-rotate-file');
        return new winston.transports.DailyRotateFile(
            {
                filename: fileOption.fileName,
                datePattern: fileOption.datePattern || 'yyyy-MM-dd.',
                prepend: fileOption.prepend || true,
                maxDays: fileOption.maxDays || 10,
            });
    }

    private getS3Transport = (s3Option: s3Option) => {
        var S3StreamLogger = require('s3-streamlogger').S3StreamLogger;
        var s3stream = new S3StreamLogger({
            bucket: s3Option.bucket,
            access_key_id: s3Option.access_key_id,
            secret_access_key: s3Option.secret_access_key,
            folder: s3Option.folder,
            name_format: s3Option.nameFormat || `%Y-%m-%d-%H-%M-%S-%L-${path.basename(process.cwd())}.log`
        });
        s3stream.on('error', function (err) {
            // there was an error!
            console.log('error', 'logging transport error', err)
        });
        var transport = new (winston.transports.File)({
            stream: s3stream
        });
        return transport;
    }
}





