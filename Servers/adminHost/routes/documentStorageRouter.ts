import { Request, Response, NextFunction, Router } from "express";
import * as express from "express";
import { DocumentStorageFacade } from "../facades/documentStorageFacade";

class DocumentStorageRouter {
  router: Router;

  constructor() {
    this.router = express.Router();
    this.createRoutes();
  }

  private createRoutes() {
    this.router.post("/setProfileImage", this.setProfileImage);
    this.router.get("/search", this.search);
    this.router.get("/listRelations", this.listRelations);
    this.router.get("/getLatest", this.getLatest);
    this.router.post("/saveLatest", this.saveLatest);
    this.router.delete("/purge", this.purge);
    this.router.post("/attach", this.attach);
    this.router.post("/detach", this.detach);
  }

  
  public setProfileImage(req: Request, res: Response): void {
    (async () => {
      let result;
      try {
        result = await new DocumentStorageFacade().setProfileImage(
          req.query["username"],
          req.query["url"]
        );
      } catch (error) {
        res.status(500).send(error);
        return;
      }
      res.status(200).send(result);
    })();
  }

  public attach(req: Request, res: Response, next: NextFunction): void {
    const userId = +req["decodedToken"].id;
    const relationId = +req.query["relationId"];
    const objectId = +req.query["objectId"];
    const subjectId = +req.query["subjectId"];
    new DocumentStorageFacade()
      .attach(userId, relationId, objectId, subjectId)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(500).send(err);
      });
  }

  public detach(req: Request, res: Response, next: NextFunction): void {
    const userId = +req["decodedToken"].id;
    const relationId = +req.query["relationId"];
    const objectId = +req.query["objectId"];
    const subjectId = +req.query["subjectId"];
    new DocumentStorageFacade()
      .detach(userId, relationId, objectId, subjectId)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(500).send(err);
      });
  }

  public listRelations(req: Request, res: Response, next: NextFunction): void {
    const userId = +req["decodedToken"].id;
    const relationId = +req.query["relationId"];
    new DocumentStorageFacade()
      .listRelations(userId, relationId)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(500).send(err);
      });
  }

  public getLatest(req: Request, res: Response, next: NextFunction): void {
    const userId = +req["decodedToken"].id;
    const ownerId = +req.query["ownerId"];
    const type = +req.query["type"];
    const documentId = req.query["documentId"];
    new DocumentStorageFacade()
      .getLatest(userId, ownerId, type, documentId)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(500).send(err);
      });
  }

  public purge(req: Request, res: Response, next: NextFunction): void {
    const userId = +req["decodedToken"].id;
    const ownerId = +req.query["ownerId"];
    const type = +req.query["type"];
    const documentId = req.query["documentId"];
    new DocumentStorageFacade()
      .purge(userId, ownerId, type, documentId)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(500).send(err);
      });
  }

  public saveLatest(req: Request, res: Response, next: NextFunction): void {
    const userId = +req["decodedToken"].id;
    const ownerId = +req.query["ownerId"];
    const type = +req.query["type"];
    const data = req.body;
    const documentId = req.query["documentId"];
    new DocumentStorageFacade()
      .saveLatest(userId, ownerId, type, documentId, data)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(500).send(err);
      });
  }

  public search(req: Request, res: Response, next: NextFunction): void {
    const userId = +req["decodedToken"].id;
    const type = +req.query["type"];
    const freeText = req.query["freeText"];
    new DocumentStorageFacade()
      .search(userId, type, freeText)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(500).send(err);
      });
  }
}

export const documentStorageRouter = new DocumentStorageRouter().router;
