import { Router, Request, Response } from "express";
import * as express from "express";
import { TwitterFacade } from "../facades/twitterFacade";
import { DocumentStorageFacade } from "../facades/documentStorageFacade";
import * as http from "http";
import * as url from "url";
import * as request from "superagent";

class VoRouter {
  router: Router;

  private get voUrl(): string {
    return process.env["VO_URL"];
  }

  constructor() {
    this.router = express.Router();
    this.createRoutes();
  }

  private createRoutes() {
    this.router.post('/notifyLaunch', VoRouter.notifyLaunch);
    this.router.use("/", this.routeRequest.bind(this));
  }

  public static notifyLaunch(req, res, next): void {
    (async () => {
      res.status(200).send({});
      try {
        await request.post(`${process.env.PUSH_SERVICE_URL}/publish?channel=launch-${req.query['id']}`).send(req.body);
      } catch(error) {
      }
    })();
  }

  public async routeRequest(req: Request, res: Response): Promise<void> {
    const _url = url.parse(req.url);
    const newReq = request(
      req.method,
      `${this.voUrl}/api${_url.path}`
    );
    newReq.set({ user_id: req["decodedToken"].id });
    let result: any;
    try {
      result = (await newReq.send(
        req.headers["content-length"] ? req.body : undefined
      )).body;
    } catch (err) {
      res.status(500).send(err);
      return;
    }
    res.status(200).send(result);
  }
}

export const voRouter = new VoRouter().router;
