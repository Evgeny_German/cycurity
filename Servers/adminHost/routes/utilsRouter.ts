import { Router, Request, Response, NextFunction } from "express";
import * as express from "express";
import * as request from "superagent";
import * as uuid from "uuid";
import * as fs from "fs";
import * as path from "path";

class UtilsRouter {
  router: Router;

  constructor() {
    this.router = express.Router();
    this.createRoutes();
  }

  private createRoutes() {
    this.router.get("/preview", this.preview);
    this.router.post("/upload", UtilsRouter.upload);
  }

  preview = (req, res, next) => {
    const token = req.query["token"];
    const url = req.query["url"];
    request
      .get(
        `https://api.diffbot.com/v3/analyze?token=${
          token
        }&url=${encodeURIComponent(url)}`
      )
      .then(result => {
        res.status(200).send(result.body);
      });
  };

  public static async upload(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    if (!fs.existsSync(path.join(__dirname, `../staticStorage`))) {
      fs.mkdirSync(path.join(__dirname, `../staticStorage`));
    }
    const fileName = path.join(__dirname, `../staticStorage/${uuid()}`);
    await new Promise((resolve, reject) => {
      const stream = fs.createWriteStream(fileName);
      stream.on("open", () => {
        req.pipe(stream);
      });
      stream.on("finish", () => {
        resolve();
      });
      stream.on("error", () => {
        reject();
      });
    });
    res.status(200).send(path.parse(fileName).name);
  }
}

export const utilsRouter = new UtilsRouter().router;
