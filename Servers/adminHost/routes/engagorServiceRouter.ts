import { Router, Request, Response, NextFunction } from "express";
import { EngagorServiceFacade } from "../facades/engagorServiceFacade";

class EngagorMangeRouter {

  router: Router;


  constructor() {
    this.router = Router();
    this.createRoutes();
  }

  private createRoutes() {
    this.router.get("/getMentions", this.getMentions);
    this.router.get("/search", this.search);
  }

  getMentions = (req, res, next) => {
    new EngagorServiceFacade().getMentions().then(facadeRes => {
      res.status(200).send(facadeRes)
    }).catch(err => {
      res.status(500).send(err)
    })

  }

  public search(req: Request, res: Response, next: NextFunction): void {
    const request = req.query;
    new EngagorServiceFacade().search(request['freeText']).then(facadeRes => {
      res.status(200).send(facadeRes)
    }).catch(err => {
      res.status(500).send(err)
    })

  }
}

export const engagorServiceRouter = new EngagorMangeRouter().router;
