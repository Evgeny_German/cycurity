import { Router } from "express";
import * as express from "express";
import { TwitterFacade } from "../facades/twitterFacade";
import { DocumentStorageFacade } from "../facades/documentStorageFacade";

class TwitterRouter {
  router: Router;

  constructor() {
    this.router = express.Router();
    this.createRoutes();
  }

  private createRoutes() {
    this.router.get(
      "/conversationByTweetScrape",
      this.conversationByTweetScrape
    );
    this.router.get("/profile", this.profile);
    this.router.post(
      "/profileAndStatisticsScrape",
      this.profileAndStatisticsScrape
    );
    this.router.post("/mail", (req,res,next) => {
      if (req["decodedToken"].adminRole === "superadmin" || req["decodedToken"].adminRole === "admin" || req["decodedToken"].meta.modules.mass) {
        next();
        return;
      }
      res.status(403).send({});
    }, this.mail);
    this.router.get("/conversationByTweet", this.conversationByTweet);
    this.router.get("/fetchReport", this.fetchReport.bind(this));
    this.router.post("/scrapeReport", this.scrapeReport.bind(this));
    this.router.get("/searchConversation", this.searchConversation);
    this.router.post("/saveConversation", this.saveConversation);
  }

  mail = (req, res, next) => {
    (async () => {
      try {
        await new TwitterFacade().mail(req.query.mail, req.body);
      } catch (error) {
        res.status(500).send(error);
        return;
      }
      res.status(200).send({});
    })();
  };

  profile = async (req, res, next) => {
    const userName = req.query["userName"];
    const ownerId = +req.query["ownerId"];
    try {
      const profile = await new TwitterFacade().getProfile(
        req.decodedToken.id,
        ownerId,
        userName
      );
      res.status(200).send(profile);
    } catch (error) {
      res.status(500).send(error);
    }
  };

  conversationByTweet = (req, res, next) => {
    const ownerId = +req.query["ownerId"];
    const userName = req.query["userName"];
    const tweetId = req.query["tweetId"];
    const reportId = req.query["reportId"];
    const forceRescrape = req.query["forceRescrape"] || false;
    new TwitterFacade()
      .conversationByTweet(
        req.decodedToken.id,
        ownerId,
        userName,
        tweetId,
        reportId,
        forceRescrape
      )
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        if (err.code === "ERR_INVALID_URL") {
          res.status(422).send(err);
          return;
        }
        res.status(err.status || 500).send({status: err.status || 500, message: err.message || "Internal Server Error"});
      });
  };

  async fetchReport(req, res, next) {
    const reportId = req.query["reportId"];
    const ownerId = +req.query["ownerId"];
    new TwitterFacade()
      .fetchReport(req.decodedToken.id, ownerId, reportId)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(500).send(err);
      });
  }

  async scrapeReport(req, res, next) {
    const urls = req.body["urls"];
    const reportId = req.body["reportId"];
    const ownerId = req.body["ownerId"];
    new TwitterFacade()
      .scrapeReport(req.decodedToken.id, ownerId, reportId, urls)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(500).send(err);
      });
  }

  saveConversation = (req, res, next) => {
    const userId = +req["decodedToken"].id;
    const ownerId = +req.query["ownerId"];
    const type = +req.query["type"];
    const data = req.body;
    const documentId = req.query["documentId"];
    const facade = new DocumentStorageFacade();
    facade
      .getLatest(userId, ownerId, type, documentId)
      .then(data2 => {
        facade
          .saveLatest(userId, ownerId, type, documentId, {
            lastScraped: data2.data.lastScraped,
            conversation: data
          })
          .then(facadeRes => {
            res.status(200).send(facadeRes);
          })
          .catch(err => {
            res.status(500).send(err);
          });
      })
      .catch(err => {
        res.status(500).send(err);
      });
  };

  conversationByTweetScrape = (req, res, next) => {
    let tweetId = req.query["tweetId"];
    let userName = req.query["userName"];
    new TwitterFacade()
      .conversationByTweetScrape(tweetId, userName)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(500).send(err);
      });
  };

  profileAndStatisticsScrape = (req, res, next) => {
    const userNames: string[] = req.body["userNames"];
    const concurrency = req.query["concurrency"];
    new TwitterFacade()
      .profileAndStatisticsScrape(userNames, concurrency)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(500).send(err);
      });
  };

  searchConversation = (req, res, next) => {
    const userId = +req["decodedToken"].id;
    let freeText = req.query["freeText"];
    new DocumentStorageFacade()
      .search(userId, 1, freeText)
      .then(facadeRes => {
        res.status(200).send(facadeRes.data.map(doc => doc.data.conversation));
      })
      .catch(err => {
        res.status(500).send(err);
      });
  };
}

export const twitterRouter = new TwitterRouter().router;
