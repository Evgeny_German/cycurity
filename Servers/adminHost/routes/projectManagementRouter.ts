import { Router, Request, Response } from "express";
import * as express from "express";
import { TwitterFacade } from "../facades/twitterFacade";
import { DocumentStorageFacade } from "../facades/documentStorageFacade";
import * as http from "http";
import * as url from "url";
import * as request from "superagent";

class ProjectManagementRouter {
  router: Router;

  private get projectManagementUrl(): string {
    return process.env["PROJECT_MANAGEMENT_URL"];
  }

  constructor() {
    this.router = express.Router();
    this.createRoutes();
  }

  private createRoutes() {
    this.router.use("/", this.routeRequest.bind(this));
  }

  public async routeRequest(req: Request, res: Response): Promise<void> {
    const _url = url.parse(req.url);
    const newReq = request(
      req.method,
      `${this.projectManagementUrl}/api${_url.path}`
    );
    newReq.set({ user_id: req["decodedToken"].id });
    let result: any;
    try {
      result = (await newReq.send(
        req.headers["content-length"] ? req.body : undefined
      )).body;
    } catch (err) {
      res.status(500).send(err);
      return;
    }
    res.status(200).send(result);
  }
}

export const projectManagementRouter = new ProjectManagementRouter().router;
