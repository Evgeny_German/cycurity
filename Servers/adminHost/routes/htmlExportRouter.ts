import {Router, Request, Response, NextFunction} from "express";
import {HtmlExportFacade} from "../facades/htmlExportFacade";
import * as request from 'superagent';

class HtmlExportRouter {

    router: Router;


    constructor() {
        this.router = Router();
        this.createRoutes();
    }

    private createRoutes() {
        this.router.post("/word", this.exportWord);
    }

    exportWord = (req, res, next) => {
        //new HtmlExportFacade().exportPdf(req.body.url,req.token,res)
        let htmlRendererUrl = process.env.EXPORT_SERVICE + '/export/word'
        console.log(`sending request to export word. url: ${htmlRendererUrl}`)
        let fullUrls = req.body.urls.map(url => `${url};access_token=${req.token};printMode=true`);

        request.post(htmlRendererUrl).send({
            user_id: +req.decodedToken.id,
            name: req.body.name,
            fileName: "report.docx",
            urls: fullUrls,
            selector: '.wBox'
        }).then(response=>{
            res.status(200).send({response:response.body})
        }).catch(error=>{
            console.error('twitterFacade: error saving latest.');
            res.status(500).send({error:error.message})
        });
    }

}

export const htmlExportRouter = new HtmlExportRouter().router;
