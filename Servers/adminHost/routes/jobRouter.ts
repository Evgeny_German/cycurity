import { Router, Request, Response } from "express";
import * as express from "express";
import * as http from "http";
import * as url from "url";
import * as request from "superagent";

class JobRouter {
  router: Router;

  private get jobServiceUrl(): string {
    return process.env["JOB_SERVICE_URL"];
  }

  constructor() {
    this.router = express.Router();
    this.createRoutes();
  }

  private createRoutes() {
    this.router.use("/", this.routeRequest.bind(this));
  }

  public async routeRequest(req: Request, res: Response): Promise<void> {
    const _url = url.parse(req.url);
    const newReq = request(
      req.method,
      `${this.jobServiceUrl}/api${_url.path}`
    );
    let result: any;
    try {
      result = (await newReq.send(
        req.headers["content-length"] ? req.body : undefined
      )).body;
    } catch (err) {
      res.status(500).send(err);
      return;
    }
    res.status(200).send(result);
  }
}

export const jobRouter = new JobRouter().router;
