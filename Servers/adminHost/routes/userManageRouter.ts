import { Router, Request, Response, NextFunction } from "express";
import * as express from "express";
import { UserManageFacade } from "../facades/userManageFacade";
import * as url from "url";
import * as request from "superagent";
import {
  authMiddleware,
  authenticateModule
} from "../middlewares/authMiddleware";

class UserMangeRouter {
  router: Router;

  private get userManagementUrl(): string {
    return process.env["USER_MANAGEMENT_URL"];
  }

  constructor() {
    this.router = express.Router();
    this.createRoutes();
  }

  private createRoutes() {
    this.router.post("/login", this.login);
    this.router.get("/tokenLogin", this.tokenLogin);
    this.router.use("/", this.routeRequest.bind(this));
  }

  login = (req, res, next) => {
    let email = req.body.email;
    let password = req.body.password;
    new UserManageFacade()
      .login(email, password)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(500).send(err);
      });
  };

  tokenLogin = (req, res, next) => {
    let token = req.query["token"];
    new UserManageFacade()
      .loginToken(token)
      .then(facadeRes => {
        res.status(200).send(facadeRes);
      })
      .catch(err => {
        res.status(401).send(new Error("Unauthorized."));
      });
  };

  public async routeRequest(req: Request, res: Response): Promise<void> {
    const _url = url.parse(req.url);
    const newReq = request(
      req.method,
      `${this.userManagementUrl}/api${_url.path}`
    );
    const token =
      req.headers["authorization"] &&
      (<string>req.headers["authorization"]).startsWith("Bearer ")
        ? (<string>req.headers["authorization"]).split(" ")[1]
        : null;
    const user = await new UserManageFacade().loginToken(token);
    let result: any;
    try {
      result = (await newReq
        .set({ user_id: user.id })
        .send(req.headers["content-length"] ? req.body : undefined)).body;
    } catch (err) {
      res.status(500).send(err);
      return;
    }
    res.status(200).send(result);
  }
}

export const userManageRouter = new UserMangeRouter().router;
