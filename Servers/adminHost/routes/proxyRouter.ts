import { Router, Request, Response, NextFunction } from "express";
import * as url from "url";
import * as express from "express";
import * as request from "superagent";
import { ProxyFacade } from "../facades/proxyFacade";

class ProxyRouter {

  router: Router;

  private get proxyManagementUrl(): string {
    return process.env["PROXY_MANAGEMENT_URL"];
  }

  constructor() {
    this.router = express.Router();
    this.createRoutes();
  }

  private createRoutes() {
    this.router.post("/getProxyManageList", this.getProxyManageList.bind(this));
    this.router.get("/getSupportedRegions", this.getSupportedRegions.bind(this));
    this.router.get("/getImages", this.getImages.bind(this));
    this.router.post("/setActiveImage", this.setActiveImage.bind(this));
    this.router.post("/updateProviderRegions", this.updateProviderRegions.bind(this));
    this.router.post("/createInstance", this.createInstance.bind(this));
    this.router.post("/terminateInstance", this.terminateInstance.bind(this));
    this.router.post("/copyImage", this.copyImage.bind(this));
    this.router.post("/getImageStatus", this.getImageStatus.bind(this));
    this.router.post("/getInstanceListFromProvider", this.getInstanceListFromProvider.bind(this));
    this.router.post("/testProxy", this.testProxy.bind(this));
    this.router.post("/getLastConnectionTime", this.getLastConnectionTime.bind(this));
  }

  public getProxyManageList(req: Request, res: Response, next: NextFunction) {
    (async () => {
      let result: any;
      try {
        result = await new ProxyFacade(this.proxyManagementUrl).getProxyManageList(
          req["decodedToken"].id,
          req.body["limit"],
          req.body["onlyRunning"]
        );
      } catch (error) {
        res.status(error.status || 500).send(error);
        return;
      }
      res.send(result);
    })();
  }

  public getSupportedRegions(req: Request, res: Response, next: NextFunction) {
    (async () => {
      let result: any;
      try {
        result = await new ProxyFacade(this.proxyManagementUrl).getSupportedRegions(
          req["decodedToken"].id
        );
      } catch (error) {
        res.status(error.status || 500).send(error);
        return;
      }
      res.send(result);
    })();
  }

  public getImages(req: Request, res: Response, next: NextFunction) {
    (async () => {
      let result: any;
      try {
        result = await new ProxyFacade(this.proxyManagementUrl).getImages(
          req["decodedToken"].id
        );
      } catch (error) {
        res.status(error.status || 500).send(error);
        return;
      }
      res.send(result);
    })();
  }

  public setActiveImage(req: Request, res: Response, next: NextFunction) {
    (async () => {
      let result: any;
      try {
        result = await new ProxyFacade(this.proxyManagementUrl).setActiveImage(
          req["decodedToken"].id,
          req.body["imageName"],
          req.body["region"],
          req.body["provider"]
        );
      } catch (error) {
        res.status(error.status || 500).send(error);
        return;
      }
      res.send(result);
    })();
  }

  public updateProviderRegions(req: Request, res: Response, next: NextFunction) {
    (async () => {
      let result: any;
      try {
        result = await new ProxyFacade(this.proxyManagementUrl).updateProviderRegions(
          req["decodedToken"].id,
          req.body["provider"]
        );
      } catch (error) {
        res.status(error.status || 500).send(error);
        return;
      }
      res.send(result);
    })();
  }

  public createInstance(req: Request, res: Response, next: NextFunction) {
    (async () => {
      let result: any;
      try {
        result = await new ProxyFacade(this.proxyManagementUrl).createInstance(
          req["decodedToken"].id,
          req.body["region"],
          req.body["provider"],
          req.body["instanceName"]
        );
      } catch (error) {
        res.status(error.status || 500).send(error);
        return;
      }
      res.send(result);
    })();
  }
  
  public terminateInstance(req: Request, res: Response, next: NextFunction) {
    (async () => {
      let result: any;
      try {
        result = await new ProxyFacade(this.proxyManagementUrl).terminateInstance(
          req["decodedToken"].id,
          req.body["region"],
          req.body["provider"],
          req.body["instanceId"]
        );
      } catch (error) {
        res.status(error.status || 500).send(error);
        return;
      }
      res.send(result);
    })();
  }

  public copyImage(req: Request, res: Response, next: NextFunction) {
    (async () => {
      let result: any;
      try {
        result = await new ProxyFacade(this.proxyManagementUrl).copyImage(
          req["decodedToken"].id,
          req.body["region"],
          req.body["provider"]
        );
      } catch (error) {
        res.status(error.status || 500).send(error);
        return;
      }
      res.send(result);
    })();
  }

  public getImageStatus(req: Request, res: Response, next: NextFunction) {
    (async () => {
      let result: any;
      try {
        result = await new ProxyFacade(this.proxyManagementUrl).getImageStatus(
          req["decodedToken"].id,
          req.body["region"],
          req.body["provider"],
          req.body["imageId"]
        );
      } catch (error) {
        res.status(error.status || 500).send(error);
        return;
      }
      res.send(result);
    })();
  }

  public getInstanceListFromProvider(req: Request, res: Response, next: NextFunction) {
    (async () => {
      let result: any;
      try {
        result = await new ProxyFacade(this.proxyManagementUrl).getInstanceListFromProvider(
          req["decodedToken"].id,
          req.body["region"],
          req.body["provider"],
          req.body["filter"]
        );
      } catch (error) {
        res.status(error.status || 500).send(error);
        return;
      }
      res.send(result);
    })();
  }

  public testProxy(req: Request, res: Response, next: NextFunction) {
    (async () => {
      let result: any;
      try {
        result = await new ProxyFacade(this.proxyManagementUrl).testProxy(
          req["decodedToken"].id,
          req.body["url"],
          req.body["proxyIp"],
          req.body["proxyPort"]
        );
      } catch (error) {
        res.status(error.status || 500).send(error);
        return;
      }
      res.send(result);
    })();
  }

  public getLastConnectionTime(req: Request, res: Response, next: NextFunction) {
    (async () => {
      let result: any;
      try {
        result = await new ProxyFacade(this.proxyManagementUrl).getLastConnectionTime(
          req["decodedToken"].id,
          req.body["region"],
          req.body["provider"],
          req.body["instanceName"]
        );
      } catch (error) {
        res.status(error.status || 500).send(error);
        return;
      }
      res.send(result);
    })();
  }

}

export const proxyRouter = new ProxyRouter().router;
