asyncio==3.4.3
cryptography==2.1.4
Flask==0.12.2
psutil==5.4.5
requests==2.18.4
Scrapy==1.5.0
scrapy-crawlera==1.3.0
scrapy-splash==0.7.2