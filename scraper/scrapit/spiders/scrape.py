# -*- coding: utf-8 -*-
import scrapy
import json
import requests
from scrapy_splash import SplashRequest
# ============================================================================================================
# Generic scrape via scheme
# ARGS:
# scrapy crawl scrape -a job={} -a flow="" -o jobID.json
# ============================================================================================================


class ScrapeSpider(scrapy.Spider):
    name = 'scrape'
    job = {}
    # empty object to contain the elements
    obj = {}
    formatVars = []
    completed = False
    linkIterationNumber = 0;

    def __init__(self, job=None, flow=None, *args, **kwargs):
        self.job = json.loads(job.replace("\"", "\\\"").replace("\'", "\""))

    def start_requests(self):
        self.log('---> start scraping process')
        yield scrapy.Request(url=self.job["url"], callback=self.parse, errback=self.err, dont_filter=True)

    def is_json(self, myjson):
        try:
            json_object = json.loads(myjson.decode('utf8'))
        except ValueError:
            return False
        return True                

    def parse(self, response):
        json_response = None
        jsonCheck = self.is_json(response.body)
        html_body = response

        if jsonCheck:
            json_response = json.loads(response.body.decode('utf8'))
            if self.job["scroll"]["active"] == 'true' and self.job["scroll"]["method"] == 'scroll':
                if (str(json_response[self.job["scroll"]["method-scroll-indicator"]]) == self.job["scroll"]["method-scroll-indicator-value"]):
                    # Parse Tweet and Reply info from GET request
                    html_body = scrapy.Selector(text=json_response[self.job["scroll"]["method-scroll-data"]], type="html")
                else:
                    html_body = scrapy.Selector(text=json_response[self.job["scroll"]["method-scroll-data"]], type="html")
                    self.completed = True

        # set all requested element
        elements = self.job["elements"]
        # loop throught elements
        for e in elements:
            if e['name'] == "descendants" or  e['name'] == "ancestors":
                xmore = html_body.css('.ThreadedConversation-moreReplies::attr(data-expansion-url)').extract()
                for x in xmore:
                    requests.get('https://twitter.com{}'.format(x))
            # store limit value
            limit = int(e["limit"])
            if e["selectorType"] == 'xpath':
                mainObject = html_body.xpath(e["selector"])
            else:
                mainObject = html_body.css(e["selector"])
            
            # check if element exist in the response data or not
            if mainObject:
                # check for properties existence
                if e["properties"]:
                    propArray = []
                    for prop in mainObject:
                        innerObj = {}
                        for el in e["properties"]:
                            # check if element is repetative
                            if el["repeat"] and int(el["repeat"]) > 0:
                                if el["selectorType"] == 'xpath':
                                    item = prop.xpath(el["selector"]).extract()
                                    if el["resolve"] == "boolean" and item:
                                        item = True
                                    if el["resolve"] == "boolean" and not item:
                                        item = False
                                else:
                                    item = prop.css(el["selector"]).extract()
                                    if el["resolve"] == "boolean" and item:
                                        item = True
                                    if el["resolve"] == "boolean" and not item:
                                        item = False
                            else:
                                if el["selectorType"] == 'xpath':
                                    item = prop.xpath(el["selector"]).extract_first()
                                    if el["resolve"] == "boolean" and item:
                                        item = True
                                    if el["resolve"] == "boolean" and not item:
                                        item = False
                                else:
                                    item = prop.css(el["selector"]).extract_first()
                                    if el["resolve"] == "boolean" and item:
                                        item = True
                                    if el["resolve"] == "boolean" and not item:
                                        item = False
                            # self.log(el["name"])
                            if el["limit"] and int(el["limit"]) > 0:
                                if el["limitType"] == 'epoch':
                                    if int(item) < int(el["limitValue"]):
                                        self.completed = True
                                else:
                                    item = item[0:el["limit"]]
                            # save to item property
                            innerObj[el["name"]] = item
                        # push property to element main array
                        propArray.append(innerObj)
                    allElemens = propArray
                    # print(allElemens)
                else:
                    # check if element is repetative
                    if e["repeat"] and int(e["repeat"]) > 0:
                        allElemens = mainObject.extract()
                    else:
                        allElemens = mainObject.extract_first()

                # limit number of elements 
                if limit > 0:
                    if e["limitType"] == 'epoch':
                        self.completed = True
                    else:
                        allElemens = allElemens[0:limit]

                
                if e["resolve"] == "boolean":
                    self.obj[e["name"]] = True
                else:
                    # check if key already exists in main object and is of type list
                    if e["name"] in self.obj and isinstance(self.obj[e["name"]], list) and e["repeat"] > 0:
                        self.obj[e["name"]].append(allElemens)

                    # check if key already exists in main object and is of type str
                    if e["name"] in self.obj and isinstance(self.obj[e["name"]], str):
                        # skipping the data re-inserting
                        pass
                    if e["name"] not in self.obj:
                        self.obj[e["name"]] = allElemens
            else:
                if not self.completed:
                    self.completed = True
                    if e["resolve"] == "boolean":
                        self.obj[e["name"]] = False
                    else:
                        self.obj[e["name"]] = 'null'
                        if e["repeat"] > 0:
                            self.obj[e["name"]] = []
                    
        # limit all data by
        # if self.job["scroll"]["limit-selector"] != '':
        #     if self.job["scroll"]["limit"] == 'epoch':
        #         if self.job["scroll"]["limit-type"] == 'xpath':
        #             test_limit = html_body.xpath(e["selector"])
        #         else:
        #             test_limit = html_body.css(self.job["scroll"]["limit-selector"])
        #         print('==============================>>>>>>>>>>>>>>>>>>>>>> '+str(test_limit))
        #         if test_limit and self.methodTest(self.job["scroll"]["limit-method"],self.job["scroll"]["limit-value"],test_limit):
        #             print('==============================>>>>>>>>>>>>>>>>>>>>>> True')
        
        # self.formatVars = []
        # check for scrolling/ pagination method 
        if self.job["scroll"]["active"] == "true" and not self.completed: # and mainObject:
            if self.job["scroll"]["method-scroll-more-values-selectors"] != [] and self.formatVars == []:
                for value in self.job["scroll"]["method-scroll-more-values-selectors"]:
                    self.formatVars.append(html_body.css(value).extract_first())
            # check type of scroll/ pagination
            if self.job["scroll"]["method"] == 'link':
                link = html_body.css(self.job["scroll"]["method-link-selector"]).extract_first()
                # check for link existence
                if link:
                    # join full URL with the link
                    nextUrl = html_body.urljoin(link)
                    # parse the next page
                    yield scrapy.Request(url=nextUrl, callback=self.parse)
                else:
                    self.completed = True
            if self.job["scroll"]["method"] == 'link-iterator':
                if self.job["scroll"]["method-link-iterator-url"] != "" and self.job["scroll"]["method-link-iterator-start"] != "":
                    if self.linkIterationNumber > int(self.job["scroll"]["method-link-iterator-start"]):
                       self.linkIterationNumber = self.linkIterationNumber  + 1
                    else:
                        self.linkIterationNumber = int(self.job["scroll"]["method-link-iterator-start"]) + 1
                    nextUrl = str(self.job["scroll"]["method-link-iterator-url"]).format(str(self.linkIterationNumber))
                    check= requests.get(nextUrl)
                    # print(check.status_code)
                    if check.status_code >= 200 and check.status_code < 300:
                        yield scrapy.Request(url=nextUrl, callback=self.parse)
                    else:
                        self.completed = True
                    # print(link)
            if self.job["scroll"]["method"] == 'scroll':
                formatVls = (', '.join(str(x) for x in self.formatVars));
                if json_response:
                    nextUrl = str(self.job["scroll"]["method-scroll-url"]).format(json_response[self.job["scroll"]["method-scroll-value"]],formatVls)
                    check= requests.get(nextUrl)
                    if check.status_code >= 200 and check.status_code < 300:
                        yield scrapy.Request(url=nextUrl, callback=self.parse)
                else:
                    if self.job["scroll"]["method-scroll-value-selector"] != '':
                        key = html_body.css(self.job["scroll"]["method-scroll-value-selector"]).extract_first()
                        if key:
                            nextUrl = str(self.job["scroll"]["method-scroll-url"]).format(key,formatVls)
                            # print('----> URL from key: '+nextUrl)
                            check= requests.get(nextUrl)
                            if check.status_code >= 200 and check.status_code < 300:
                                yield scrapy.Request(url=nextUrl, callback=self.parse)
                            else:
                                self.completed = True
                        else:
                            self.completed = True                        
        else:
            self.completed = True

        if self.completed:
            if self.job["name"] == "":
                yield self.obj
            else:
                yield {self.job["name"]:self.obj} 

    def err(self,failure):
        yield { 'error': 404 }