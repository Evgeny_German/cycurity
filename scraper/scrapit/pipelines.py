# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals
from scrapy.conf import settings
from scrapy.exceptions import DropItem

class ScrapitPipeline(object):
    def __init__(self, crawler, spiderParams):
        self.crawler = crawler
        crawler.signals.connect(self.handle_spider_closed, signals.spider_closed)

    def process_item(self, item, spider):
        return item

    @classmethod
    def from_crawler(cls, crawler):
        spiderParams = crawler.spider
        return cls(crawler, spiderParams)

    def handle_spider_closed(self, spider, reason):
        # Save scraped data when spider closed
        if hasattr(spider, 'save_to_file'):
            spider.save_to_file()