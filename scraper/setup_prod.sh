#!/bin/bash
# importing lates stable NodeJS
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
# prerequisites 
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt-get update
# remove python 2 to avoid errors
# sudo apt-get purge python3* -y
# sudo apt autoremove -y
# install nodeJS python3 and pip

sudo apt-get install nodejs python3 python3-pip -y # python3-dev -y
sudo rm /usr/bin/python
sudo ln -s /usr/bin/python3 /usr/bin/python

# upgrade pip to the latest version
sudo pip3 install --upgrade pip
# install pm2 globally
sudo npm install -g pm2
cd scraper/
# install all required pip packages
sudo pip3 install -r requirements.txt
# run pm2 from json
pm2 start process_scrapers.json --env production
# config pm2 to run at startup
sudo pm2 startup