from flask import Flask, request, jsonify, abort, Response
import os,sys,psutil
from datetime import datetime
import requests
import time
import json

poll = True

# polling timer vars
sleep_intreval = 1.01
time_counter = 0
mem_threshold = 95
proc_threshold = 90
if os.getenv("POLLING_STATE"):
    poll = bool(os.getenv("POLLING_STATE"))

# environment vars
env_scraper_url = 'http://localhost:5000'
if os.getenv("SCRAPER_URL"):
    env_scraper_host = os.getenv("SCRAPER_URL")
env_job_url = 'http://localhost:3016'
if os.getenv("JOB_SERVICE_URL"):
    env_job_url = os.getenv("JOB_SERVICE_URL")
if os.getenv("MEMORY_THRESHOLD"):
    mem_threshold = int(os.getenv("MEMORY_THRESHOLD"))

# test if memory is over the threshold and there are 
def test_mem():
    global mem_threshold,PROCNAME,proc_threshold
    counter = 0
    for proc in psutil.process_iter():
        try:
            if "python" in proc.name():
                for child in proc.children(recursive=True):
                    try:
                        if "scrapy" in child.name():
                            counter = counter+1
                    except Exception as e:
                        print("-----> Error: {}".format(e))
                        pass
        except Exception as e:
            print("-----> Error: {}".format(e))
            pass
    percent = int(psutil.virtual_memory().percent)
    return percent < mem_threshold and counter<proc_threshold

def time_keeper(test):
    global time_counter
    global sleep_intreval
    # test if recived any response
    if test or sleep_intreval > 15:
        time_counter = 0
        sleep_intreval = 1.01
    time_counter = time_counter + 1
    if time_counter%1000 == 0:
        time_counter = 0
        sleep_intreval = sleep_intreval + 1
        print(sleep_intreval)

def polling():
    if poll:
        print("###########################\n- Polling Service started -\n###########################\n")
    while poll:
        if test_mem():
            try:
                res = requests.post('{}/api/scrapeJobs/dequeueJob'.format(env_job_url), data={})
                # check if data is received from Job Service 
                test = (res.status_code == 200 and res.json() and res.json()['input'] != {})
                time_keeper(test)
                if test:
                    print("---> Scraping job {} started".format(res.json()['id']))
                    requests.post('{}/scrape/{}'.format(env_scraper_url,res.json()['id']), json=res.json()['input'])
                # sleep until next poll
                time.sleep(sleep_intreval)
            except Exception as e:
                # raise e
                print("-----------\nError: \n{}\n-----------\n\nRestarting Polling service...".format(e))
                print('')
                time.sleep(1)
                polling()
        else:
            print("-----------\nOut Of Memory! waiting...")
            time.sleep(5)

        

if __name__ == '__main__':
    # print("-------- Polling Service --------")
    polling()