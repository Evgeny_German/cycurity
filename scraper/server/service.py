from flask import Flask, request, jsonify, abort, Response
import os
import sys
import asyncio
from datetime import datetime
import urllib.parse
import subprocess
import threading
import requests
import time
import json
import hashlib

# declaring the app as flask application
app = Flask(__name__)

env_port = 5000
env_poller_port = 5001
if os.getenv("SCRAPER_PORT"):
    env_port = os.getenv("SCRAPER_PORT")
if os.getenv("SCRAPER_PORT"):
    env_poller_port = os.getenv("SCRAPER_PORT")

timeout = 10

@app.route("/", methods=['GET'])
def index():
    return "<strong>Scrapy python service API</strong>"

@app.route("/get/<string:flow>/<string:jobID>", methods=['GET'])
def getJobByID(flow, jobID):
    resp = ('', 404)
    file = str('output/'+flow+'/'+jobID+".json")
    if os.path.exists(file) and os.stat(file).st_size > 0:
        try:
            job = open(file, "r", encoding="utf8")
            # print(job.read())
        except:
            raise
        resp = Response(json.dumps(job.read()), status=200,headers={'Content-Type':'application/json'})
        # resp = job.read()
    # print(resp)
    return resp


def getJob(flow, jobID):
    resp = ''
    file = str('output/'+flow+'/'+jobID+".json")
    if os.path.exists(file) and os.stat(file).st_size > 0:
        try:
            job = open(file, "r", encoding="utf8")
        except:
            raise
        resp = job.read()
    return resp

# hook test url


@app.route("/hook/", methods=['POST'])
def hook(jsonData=None):
    if request.method == 'POST':
        return json.dumps(request.get_json())
    return jsonData

# scrape by json/file method


@app.route("/scrape/<string:extJobId>", methods=['POST'])
def scrape(extJobId):
    # craete flow ID
    flow = hashlib.md5(str(time.time()).encode('utf-8')).hexdigest()
    # get the json data
    data = request.get_json()
    # generate flow folder if not exists
    folder = "output/"+flow
    if not os.path.exists(folder):
        os.makedirs(folder)
    try:
        extJobId = int(extJobId)
    except Exception as e:
        print(e)
    # loop and run the jobs
    jobs = run(data, flow, data["callback"], extJobId)
    return json.dumps({'name': flow, flow: jobs})


def onExit(callback, flow, extJobID, jobList):
    if callback["type"] == 'hook':
        data = []
        # extract job JSON data
        try:
            for j in jobList:
                loader = getJob(flow, j)
                # print('-------> '+str(loader))
                data.append(json.loads(loader))
            if data:
                success = 'true'
        except:
            success = 'false'
        # validate hook URL existence
        if callback["url"]:
            # gen POST request to given URL with JSON data and optional headers
            if type(extJobID) is int and extJobID > 0:
                callback_url = str(callback["url"]).format(extJobID)
                tmpData = []
                err = False
                for job in data:
                    if 'error' in job[0]:
                        print('error')
                        err = True
                        continue
                    tmpData.append(job[0])
                if err:
                    data = {"error": {"message": "Not Found", "status": 404}}
                else:
                    # data = json.dumps({"output":tmpData})
                    data = {"output": tmpData}
                # requests.post(callback_url,json=json.dumps({"output":data}),headers=callback["headers"])
            else:
                callback_url = str(callback["url"]).format(flow, success)
            try:
                requests.post(callback_url, json=data,
                              headers=callback["headers"])
            except Exception as e:
                raise e


def runInThread(onExit, popenArgs, callback, flow, extJobID, processes, jobList):
    proc = subprocess.Popen(popenArgs)
    processes.append(proc)
    proc.wait()
    processes.pop()
    if len(processes) == 0:
        onExit(callback, flow, extJobID, jobList)

# run loop over json object


def run(data, flow, callback, extJobID):
    start = datetime.now()
    jobList = []
    processes = []
    # loop over the given jobs in the main "scrape" object
    for scraper in data["scrape"]:
        # create job ID
        jobID = hashlib.sha1(str(time.time()).encode('utf-8')).hexdigest()
        jobList.append(jobID)
        # transform job object
        job = 'job={}'.format(str(scraper))
        file = 'output/'+flow+'/'+jobID + '.json'
        flowID = 'flow={}'.format(str(flow))
        action = ['scrapy', 'crawl', 'scrape',
                  '-a', job, '-a', flowID, '-o', file]
        # create subprocess for scraper
        # subprocess.Popen(action)
        thread = threading.Thread(target=runInThread, args=(
            onExit, action, callback, flow, extJobID, processes, jobList))
        thread.start()
        # check for hook existence
        end = datetime.now()
        print(' ---> job is done! took {}/s'.format(str(end-start)))
    return jobList


if __name__ == '__main__':
    # running at port localhost:5000 by default
    app.run(debug=False, host="0.0.0.0", port=env_port)
